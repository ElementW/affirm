#!/bin/sh
if [ -z "$ROOTDIR" ]; then
  echo "Error: no ROOTDIR specified"
  exit 1
fi
OUTDIR="$ROOTDIR/output"
TMPFILE=$(mktemp)
TMPFILE2=$(mktemp)
nm -S --size-sort -t d "$OUTDIR/afFIRM.arm9.elf" >"$TMPFILE"
sed -ie 's/^/9 /' "$TMPFILE"
nm -S --size-sort -t d "$OUTDIR/afFIRM.arm11.elf" >"$TMPFILE2"
sed -ie 's/^/11 /' "$TMPFILE2"
cat "$TMPFILE" "$TMPFILE2" | cut -d ' ' -f 1,3- | tr ' ' '|' | \
    awk -F '|' '{OFS="|";gsub(/^0+/,"",$2);print}' | sort -t '|' -k2,2rn -k4,4 -k1,1rn | \
    c++filt | column -t -s '|' -N K,SIZE,TYPE,NAME -R K,SIZE
rm "$TMPFILE" "$TMPFILE2"
