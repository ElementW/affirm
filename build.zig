// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const builtin = @import("builtin");
const Build = std.Build;
const Module = Build.Module;
const Target = std.Target;
const Step = Build.Step;

const afbuild = @import("afbuild.zig");
const CommonModules = afbuild.CommonModules;
const Machine = afbuild.Machine;
const CompileOptions = afbuild.CompileOptions;

pub fn build(b: *Build) void {
    const optimize_opt = b.standardOptimizeOption(.{});
    const machine_opt = b.option(Machine.Enum, "machine", "Machine to compile Affinis for") orelse {
        std.log.err("Unspecified machine. Use -Dmachine=<target>", .{});
        std.log.info("Available machines:", .{});
        for (std.meta.fieldNames(Machine.Enum)) |mach| {
            std.log.info("- {s}", .{mach});
        }
        return;
    };
    const machine = Machine.byEnum(machine_opt);
    std.debug.print("Building for {s}\n", .{machine.name});

    const afu = afbuild.moduleAfu(b);
    const common_modules: CommonModules = .{
        .afu = afu,
        .afsys = afbuild.moduleAfsys(b, afu),
    };

    const options: CompileOptions = .{
        .optimize = optimize_opt,
    };
    const kernel_step = b.step("kernel", if (machine.parts.len == 1) "Build kernel" else "Build all kernels");
    const kernel_install_step = b.step("kernel-install", if (machine.parts.len == 1) "Install kernel" else "Install all kernels");
    b.getInstallStep().dependOn(kernel_install_step);
    for (machine.parts) |*part| {
        const part_kernel_compile = part.compile_kernel(b, &common_modules, machine, part, &options);
        if (machine.parts.len == 1) {
            kernel_step.dependOn(&part_kernel_compile.step);
            kernel_install_step.dependOn(&b.addInstallArtifact(part_kernel_compile, .{}).step);
            if (part.target.os.tag == .linux) {
                const kernel_run_step = b.step("kernel-run", "Run linux-usermode kernel");
                kernel_run_step.dependOn(&b.addRunArtifact(part_kernel_compile).step);
            }
        } else {
            const part_kernel_step = b.step(b.fmt("kernel-{s}", .{part.id}), b.fmt("Build {s} kernel", .{part.name}));
            part_kernel_step.dependOn(&part_kernel_compile.step);
            kernel_step.dependOn(part_kernel_step);
            const part_kernel_install_step = b.step(b.fmt("kernel-{s}-install", .{part.id}), b.fmt("Install {s} kernel", .{part.name}));
            part_kernel_install_step.dependOn(&b.addInstallArtifact(part_kernel_compile, .{}).step);
            kernel_install_step.dependOn(part_kernel_install_step);
        }
        // b.getInstallStep().dependOn(&b.addInstallArtifact(part_kernel_compile, .{
        //     .dest_sub_path = "bin-out-test/kernel.elf",
        // }).step);
    }

    const kernel_test_build = b.addTest(.{
        .root_source_file = b.path("src/afk/afk.zig"),
        .test_runner = .{ .path = b.path("afbuild/test_runner.zig"), .mode = .simple },
    });
    common_modules.addImports(kernel_test_build.root_module);
    const kernel_test_build_step = b.step("kernel-test-build", "Build kernel unit tests");
    kernel_test_build_step.dependOn(&kernel_test_build.step);
    const kernel_test_run_step = b.step("kernel-test", "Run kernel unit tests");
    kernel_test_run_step.dependOn(&b.addRunArtifact(kernel_test_build).step);

    const part_compile_params: afbuild.modules.PartCompileParams = .{
        .machine = machine,
        .part = &machine.parts[0],
        .common_modules = &common_modules,
        .compile_options = options,
    };
    afbuild.modules.addModules(b, &part_compile_params);
}
