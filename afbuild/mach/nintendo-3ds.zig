// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const Build = std.Build;
const Step = Build.Step;
const Target = std.Target;

const afbuild = @import("../../afbuild.zig");

pub const mach: afbuild.Machine = .{
    .id = "nintendo-3ds",
    .name = "Nintendo (Old/New) 3DS",
    .parts = &.{
        .{
            .id = "arm9",
            .name = "ARM9",
            .target = .{
                .cpu = std.Target.arm.cpu.arm946e_s.toCpu(.arm),
                .os = afbuild.kernel_os,
                .abi = .eabi,
                .ofmt = .elf,
            },
            .linkscript_params = .{
                .output_format = "elf32-littlearm",
                .output_arch = "arm",
                .base_addr = 0x08000000 + 0x40,
            },
            .compile_kernel = compileKernel9,
        },
        .{
            .id = "arm11",
            .name = "ARM11",
            .target = .{
                .cpu = std.Target.arm.cpu.mpcore.toCpu(.arm),
                .os = afbuild.kernel_os,
                .abi = .eabi,
                .ofmt = .elf,
            },
            .linkscript_params = .{
                .output_format = "elf32-littlearm",
                .output_arch = "arm",
                .base_addr = 0x1FF80000,
            },
            .compile_kernel = compileKernel11,
        },
    },
    .assemble_kernel = stepFirm,
};

fn compileKernel9(
    b: *Build,
    common_modules: *const afbuild.CommonModules,
    machine: *const afbuild.Machine,
    part: *const afbuild.Machine.Part,
    options: *const afbuild.CompileOptions,
) *Step.Compile {
    _ = machine;
    const cross_target: Build.ResolvedTarget = b.resolveTargetQuery(Target.Query.fromTarget(part.target));
    const kernel9 = b.addExecutable(Build.ExecutableOptions{
        .name = "kernel9.elf",
        .root_source_file = b.path("src/kernel9/main.zig"),
        .target = cross_target,
        .optimize = options.optimize,
        .linkage = .static,
    });
    kernel9.defineCMacro("AF_ARM9", null);
    kernel9.addAssemblyFile(b.path("src/kernel9/start.S"));
    kernel9.addAssemblyFile(b.path("src/afk/arch/arm/irq/irq.S"));
    const afk9 = b.createModule(Build.Module.CreateOptions{
        .root_source_file = b.path("src/afk/afk.zig"),
    });
    common_modules.addImports(afk9);
    common_modules.addImports(&kernel9.root_module);
    kernel9.root_module.addImport("afk", afk9);
    kernel9.setLinkerScript(afbuild.linkscript.create(b, .{
        .output_format = "elf32-littlearm",
        .output_arch = "arm",
        .start_addr = 0x08000000 + 0x40,
    }));
    return kernel9;
}

fn compileKernel11(
    b: *Build,
    common_modules: *const afbuild.CommonModules,
    machine: *const afbuild.Machine,
    part: *const afbuild.Machine.Part,
    options: *const afbuild.CompileOptions,
) *Step.Compile {
    _ = machine;
    const cross_target: Build.ResolvedTarget = b.resolveTargetQuery(Target.Query.fromTarget(part.target));
    const kernel11 = b.addExecutable(Build.ExecutableOptions{
        .name = "kernel11.elf",
        .root_source_file = b.path("src/kernel11/main.zig"),
        .target = cross_target,
        .optimize = options.optimize,
        .linkage = .static,
    });
    kernel11.defineCMacro("AF_ARM11", null);
    kernel11.addAssemblyFile(b.path("src/kernel11/start.S"));
    kernel11.addAssemblyFile(b.path("src/afk/arch/arm/irq/irq.S"));
    const afk11 = b.createModule(Build.Module.CreateOptions{
        .root_source_file = b.path("src/afk/afk.zig"),
    });
    common_modules.addImports(afk11);
    common_modules.addImports(&kernel11.root_module);
    kernel11.root_module.addImport("afk", afk11);
    kernel11.setLinkerScript(afbuild.linkscript.create(b, .{
        .output_format = "elf32-littlearm",
        .output_arch = "arm",
        .start_addr = 0x1FF80000,
    }));
    return kernel11;
}

const use_zig_firmtool = false;
fn buildFirm(b: *Build, kernel9_compile: *Step.Compile, kernel11_compile: *Step.Compile) *Step.Run {
    const firmtool: ?*Build.Step.Compile = if (use_zig_firmtool) b.addExecutable(Build.ExecutableOptions{
        .name = "firmtool",
        .root_source_file = b.path("tools/firmtool.zig"),
        .target = b.host,
    }) else null;

    const firm_dest = b.getInstallPath(.bin, "affinis.firm");
    const firm_build = if (firmtool) |f| b.addRunArtifact(f) else b.addSystemCommand(&.{"firmtool"});
    firm_build.addArgs(&.{
        "build",
        firm_dest,
        "-D",
    });
    firm_build.addArtifactArg(kernel9_compile);
    firm_build.addArtifactArg(kernel11_compile);
    firm_build.addArgs(&.{
        "-C",
        "NDMA",
        "XDMA",
        "-b",
    });
    // firmtool is missing. Install using `python3 -m pip install git+https://github.com/TuxSH/firmtool.git`
    return firm_build;
}

fn stepFirm(b: *Build, outputs: []const afbuild.Machine.PartOutputs) *Step {
    return &buildFirm(b, outputs[0].kernel, outputs[1].kernel).step;
}
