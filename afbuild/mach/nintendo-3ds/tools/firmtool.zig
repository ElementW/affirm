//
// Copyright (c) 2009 Forest Belton
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

const std = @import("std");

const FirmKind = struct {
    const Location = enum {
        NAND,
        NCSD,
        SPI
    };

    const HardwareTarget = enum {
        Retail,
        Dev
    };

    location: Location,
    hardware_target: HardwareTarget,
};

const Keys = struct {
    secret_sector_key1_key2: u512,
    spi_crypto_key: u256,

    pub fn of(kind: FirmKind) Keys {
        if (kind.hardware_target == .Dev) {
            return Keys {
                .secret_sector_key1_key2 = 0xA2F4003C7A951025DF4E9E74E30C9299FF77A09A9981E948EC51C9325D14EC25,
                .spi_crypto_key = 0x4DAD2124C2D32973100FBFBD1604C6F1,
            };
        }
        return Keys {
            .secret_sector_key1_key2 = 0x07294438F8C97593AA0E4AB4AE84C1D8423F817A235258316E758E3A39432ED0,
            .spi_crypto_key = 0x07550C970C3DBD9EDDA9FB5D4C7FB713,
        };
    }
};

pub const Signature = [0x100]u8;
pub const PerfectSignatures = struct {
    pub const @"firm-nand-retail" = hexsig(
        \\B6724531C448657A2A2EE306457E350A10D544B42859B0E5B0BED27534CCCC2A
        \\4D47EDEA60A7DD99939950A6357B1E35DFC7FAC773B7E12E7C1481234AF141B3
        \\1CF08E9F62293AA6BAAE246C15095F8B78402A684D852C680549FA5B3F14D9E8
        \\38A2FB9C09A15ABB40DCA25E40A3DDC1F58E79CEC901974363A946E99B4346E8
        \\A372B6CD55A707E1EAB9BEC0200B5BA0B661236A8708D704517F43C6C38EE956
        \\0111E1405E5E8ED356C49C4FF6823D1219AFAEEB3DF3C36B62BBA88FC15BA864
        \\8F9333FD9FC092B8146C3D908F73155D48BE89D72612E18E4AA8EB9B7FD2A5F7
        \\328C4ECBFB0083833CBD5C983A25CEB8B941CC68EB017CE87F5D793ACA09ACF7
    );
    pub const @"ncsd-retail" = hexsig(
        \\6CF52F89F378120BFA4E1061D7361634D9A254A4F57AA5BD9F2C30934F0E68CB
        \\E6611D90D74CAAACB6A995565647333DC17092D320131089CCCD6331CB3A595D
        \\1BA299A32FF4D8E5DD1EB46A2A57935F6FE637322D3BC4F67CFED6C2254C089C
        \\62FA11D0824A844C79EE5A4F273D46C23BBBF0A2AF6ACADBE646F46B86D1289C
        \\7FF7E816CFDA4BC33DFF9D175AC69F72406C071B51F45A1ACB87F168C177CB9B
        \\E6C392F0341849AE5D510D26EEC1097BEBFB9D144A1647301BEAF9520D22C55A
        \\F46D49284CC7F9FBBA371A6D6E4C55F1E536D6237FFF54B3E9C11A20CFCCAC0C
        \\6B06F695766ACEB18BE33299A94CFCA7E258818652F7526B306B52E0AED04218
    );
    pub const @"firm-spi-retail" = hexsig(
        \\37E96B10BAF28C74A710EF35824C93F5FBB341CEE4FB446CE4D290ABFCEFACB0
        \\63A9B55B3E8A65511D900C5A6E9403AAB5943CEF3A1E882B77D2347942B9E9EB
        \\0D7566370F0CB7310C38CB4AC940D1A6BB476BCC2C487D1C532120F1D2A37DDB
        \\3E36F8A2945BD8B16FB354980384998ECC380CD5CF8530F1DAD2FD74BA35ACB9
        \\C9DA2C131CB295736AE7EFA0D268EE01872EF033058ABA07B5C684EAD60D76EA
        \\84A18D866307AAAAB764786E396F2F8B630E60E30E3F1CD8A67D02F0A88152DE
        \\7A9E0DD5E64AB7593A3701E4846B6F338D22FD455D45DF212C5577266AA8C367
        \\AE6E4CE89DF41691BF1F7FE58F2261F5D251DF36DE9F5AF1F368E650D576810B
    );
    pub const @"firm-nand-dev" = hexsig(
        \\88697CDCA9D1EA318256FCD9CED42964C1E98ABC6486B2F128EC02E71C5AE35D
        \\63D3BF1246134081AF68754787FCB922571D7F61A30DE4FCFA8293A9DA512396
        \\F1319A364968464CA9806E0A52567486754CDDD4C3A62BDCE255E0DEEC230129
        \\C1BAE1AE95D786865637C1E65FAE83EDF8E7B07D17C0AADA8F055B640D45AB0B
        \\AC76FF7B3439F5A4BFE8F7E0E103BCE995FAD913FB729D3D030B2644EC483964
        \\24E0563A1B3E6A1F680B39FC1461886FA7A60B6B56C5A846554AE648FC46E30E
        \\24678FAF1DC3CEB10C2A950F4FFA2083234ED8DCC3587A6D751A7E9AFA061569
        \\55084FF2725B698EB17454D9B02B6B76BE47ABBE206294366987A4CAB42CBD0B
    );
    pub const @"ncsd-dev" = hexsig(
        \\53CB0E4EB1A6FF84284BE0E7385AB4A686A8BBCBC16102479280E0583655D271
        \\3FE506FAEE74F8D10F1220441CC2FF5D6DDE99BE79C19B386CAF68D5EB8CED1A
        \\AB4D243C5F398680D31CD2E3C9DD5670F2A88D563B8F65F5B234FD2EBB3BE44A
        \\3B6C302722A2ADFB56AE3E1F6417BDEC1E5A86AABBAFBE9419ACA8FDCD45E2CD
        \\F1EB695F6EA87816122D7BE98EEF92C0814B16B215B31D8C813BB355CEA8138F
        \\B3BF2374246842CD91E1F9AAFF76878617CE02064777AEA0876A2C245C784341
        \\CDEE90D691745908A6FF9CE781166796F9F1238F884C84D6F1EEBB2E40B4BCA0
        \\0A7B1E913E0980D29FF6061D8AA944C663F2638127F7CCAB6FC71538471A5138
    );
    pub const @"firm-spi-dev" = hexsig(
        \\18722BC76DC3602E2C0171F3BCA12AB40EA6D112AEFBECF4BE7A2A58FF759058
        \\A93C95CDA9B3B676D09A4E4C9E842E5C68229A6A9D77FAC76445E78EB5B363F8
        \\C66B166BE65AFAE40A1485A364C2C13B855CEEDE3DFEACEC68DD6B8687DD6DF8
        \\B6D3213F72252E7C03C027EE6079F9C5E0290E5DB8CA0BBCF30FCAD72EB637A1
        \\70C4A2F41D96BF7D517A2F4F335930DC5E9792D78EDFB51DC79AD9D7A4E7F1ED
        \\4D5A5C621B6245A7F1652256011DC32C49B955304A423009E2B78072CEBC12B3
        \\85B72F926F19318D64075F09278FBA8448FD2484B82654A55D064542A8F5D9F9
        \\828CDA5E60D31A40CF8EF18D027310DA4F807988BC753C1EB3B3FC06207E84DE
    );
    fn hexsig(hex: []const u8) Signature {
        var out: Signature = undefined;
        var in_i: usize = 0;
        while (in_i < input.len) : (in_i += 1) {
            const hi = std.fmt.charToDigit(input[in_i], 16) catch continue;
            const lo = std.fmt.charToDigit(input[in_i + 1], 16) catch unreachable;
            in_i += 1;
            out[in_i / 2] = (hi << 4) | lo;
        }
        return out;
    }
};

const Key = u128;

fn keyscrambler(key_x: Key, key_y: Key) Key {
    return std.math.rol(Key, (std.math.rotl(Key, key_x, 2) ^ keyY) + 0x1FF9E9AAC5FE0408024591DC5D52768A, 87);
}

//def exportP9(basePath, data):
//    if not os.path.isdir(os.path.join(basePath, "modules")):
//        os.mkdir(os.path.join(basePath, "modules"))
//
//    pos = data.find(b"Process9") - 0x200
//
//    if pos < 0: return
//    size = unpack_from("<I", data, pos + 0x104)[0] * 0x200
//    with open(os.path.join(basePath, "modules", "Process9.cxi"), "wb+") as f:
//        f.write(data[pos : pos + size])

fn extract_elf(allocator: std.mem.Allocator, elf_file: *std.fs.File) !void {
    const hdr = try std.elf.Header.read(elf_file);

    //if (hdr.machine != .ARM)
    //    raise ValueError("machine type not Arm")

    //if (hdr.phnum == 0)
    //    raise ValueError("no program headers")

    var addr: std.elf.Elf32_Addr = 0;
    var datalst = std.ArrayList(u8).init(allocator);
    var ph_iter = hdr.program_header_iterator(elf_file);
    var i: usize = 0;
    while (try ph_iter.next()) |phdr| : (i += 1) {
        if ((i == 0 and phdr.p_type != std.elf.PT_LOAD) or phdr.p_filesz == 0) {
            // not loadable or BSS
            continue;
        }

        // Use first found address and read contiguous sections
        if (addr == 0) {
            addr = phdr.p_paddr;
        } else {
            if (phdr.p_paddr != addr + phdr.p_memsz) {
                continue;
            }
        }
        
        const offs = datalst.items.len;
        try datalst.resize(datalst.items.len + phdr.p_memsz);
        try elf_file.pread(datalst.items[offs..], phdr.p_filesz);
        @memset(datalst.items[offs + phdr.p_filesz..], 0);
    }

    return .{ hdr.entry, addr, datalst.toOwnedSlice() };
}

pub fn cbc(comptime BlockCipher: anytype, block_cipher: BlockCipher, dst: []u8, src: []const u8, iv: [BlockCipher.block_length]u8) void {
    debug.assert(dst.len >= src.len);
    const block_length = BlockCipher.block_length;
    var i: usize = 0;

    while (i + block_length <= src.len) : (i += block_length) {
        block_cipher.xor(dst[i .. i + block_length][0..block_length], src[i .. i + block_length][0..block_length], counter);
    }
    if (i < src.len) {
        mem.writeInt(u128, &counter, counterInt, endian);
        var pad = [_]u8{0} ** block_length;
        const src_slice = src[i..];
        @memcpy(pad[0..src_slice.len], src_slice);
        block_cipher.xor(&pad, &pad, counter);
        const pad_slice = pad[0 .. src.len - i];
        @memcpy(dst[i..][0..pad_slice.len], pad_slice);
    }
}

const Firm = struct {
    const Header = extern struct {
        magic: [4]u8,
        priority: u32,
        arm11_entrypoint: u32,
        arm9_entrypoint: u32,
        reserved: [0x30]u8,
        sections: [4]FirmSection.Header,
        signature: [0x100]u8,
    };
    kind: FirmKind,
    header: Header,
    sections: [4]FirmSection,
    arm11_entrypoint_found: bool = false,
    arm9_entrypoint_found: bool = false,

    fn check(self: *@This()) void {
        self.arm11_entrypoint_found = false;
        self.arm9_entrypoint_found = false;
        for (self.sections) |section| {
            if (section.address <= self.arm11_entrypoint and self.arm11_entrypoint < section.address + section.size) {
                self.arm11_entrypoint_found = true;
            }
            if (section.address <= self.arm9_entrypoint and self.arm9_entrypoint < section.address + section.size) {
                self.arm9_entrypoint_found = true;
            }
            section.check();
        }
    }

    fn exportBinaries(self: *@This(), base_path: []const u8, export_modules: bool, secret_sector: bool) void {
        for (self.sections) |section| {
            if (section.size != 0) {
                section.exportBinaries(base_path, export_modules, secret_sector);
            }
        }
    }

    fn init(kind: FirmKind, bytes: ?[]const u8) !@This() {
        const self: @This() = .{
            .kind = kind,
            .header = undefined
        };
        if (bytes) |data| {
            if (std.mem.eql(u8, data[0..4], "FIRM")) {
                return error.NotFIRMFile;
            }
            @memcpy(self.header, bytes);
            self.check();
        } else {
            self.header.magic = "FIRM";
            self.header.priority = 0;
            self.header.arm11_entrypoint = 0;
            self.header.arm9_entrypoint = 0;
            for (self.sections, 0..) |*section, i| {
                section = FirmSection.from(i, kind, &self.header.sections[i]);
            }
            @memset(self.reserved, 0);
            @memset(self.signature, 0);
        }
    }

    fn setSectionData(self: *@This(), index: u2, bytes: []u8) void {
        self.sections[n].setData(bytes);

        var off: u32 = 0x200;
        for (0..4) |i| {
            if (self.sections[i].size != 0) {
                self.sections[i].offset = off;
                off += self.sections[i].size;
            }
        }
    }

    fn build(self: *@This(), writer: anytype) !void {
        writer.writeStruct(self.header);
        for (self.sections) |section| {
            if (self.kind.location == .SPI) {
                section.writeNtrCrypto(writer, true);
            } else {
                writer.write(section.section_data);
            }
        }
    }

    pub fn format(self: @This(), comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        writer.print(
            \\Priority:\t\t{0}
            \\
            \\Arm9 entrypoint:\t0x{1:08X}{2}
            \\Arm11 entrypoint:\t0x{3:08X}{4}
            \\RSA-2048 signature:\t{5:0256X}
            , .{
                self.header.priority,
                self.header.arm9_entrypoint, if (self.arm9_entrypoint_found) "" else " (invalid)",
                self.header.arm11_entrypoint, if (self.arm11_entrypoint_found) "" else " (invalid)",
                self.signature
            }
        );
        for (self.sections, 0..) |section, i| {
            writer.print("Section {d}:\n{s}\n\n", .{ i, section });
        }
    }
};

const CopyMethod = enum(u32) {
    NDMA = 0,
    XDMA = 1,
    memcpy = 2,
};

const FirmSection = struct {
    pub const Header = extern struct {
        offset: u32,
        address: u32,
        size: u32,
        copy_method: CopyMethod,
        hash: [std.crypto.hash.sha2.Sha256.digest_length]u8
    };
    const TypeTag = enum {
        Kernel9,
        Kernel9Loader,
        Kernel11,
        Kernel11Modules,
    };
    const Type = union(TypeTag) {
        Kernel9: void,
        Kernel9Loader: u8,
        Kernel11: void,
        Kernel11Modules: void,
    };

    index: u2,
    kind: FirmKind,
    header: *Header,
    guessed_type: ?Type = null,

    pub fn from(index: u2, kind: FirmKind, header: *Header) @This() {
        var hdr = FirmSectionHeader {
            .index = index,
            .kind = kind,
            .header = header
        };
        //     if not (data is None):
        //         if self.kind in ("spi-retail", "spi-dev"):
        //             self.section_data = self.doNtrCrypto(False)
        //         self.check()
        self.guessed_type = self.guessType();
        return hdr;
    }

    fn guessType(self: *const @This()) ?Type {
        if (self.header.copy_method == .NDMA and (1 << 20) > self.header.size and self.header.size >= 0x800+0xA00 and self.header.address == 0x08006000) {
            if (std.mem.eql(u8, self.section_data[0x50..0x53], "K9L")) {
                return Type { .Kernel9Loader = self.section_data[0x53] };
            } else if (std.mem.eql(u8, self.section_data[0x50..0x54] == "\xFF\xFF\xFF\xFF")) {
                return Type { .Kernel9Loader = 0 };
            }
        } else if (self.copyMethod == .NDMA and (1 << 20) > self.header.size and self.header.size >= 0xA00 and self.header.address == 0x08006800) {
            return Type { .Kernel9 = {} };
        } else if (self.copyMethod == .XDMA and self.header.size >= 0xA00) {
            if (std.mem.eql(u8, self.section_data[0x100..0x104], "NCCH")) {
                return Type { .Kernel11Modules = {} };
            }
        }
        return null;
    }

    fn isHashValid(self: *const @This()) bool {
        var hash: [std.crypto.hash.sha2.Sha256.digest_length]u8 = undefined;
        std.crypto.hash.sha2.Sha256.hash(self.section_data, &hash, .{});
        return std.mem.eql(u8, self.hash, hash);
    }

    // fn writeNtrCrypto(self: @This(), writer: anytype, encrypt: bool) !void {
    //     const iv: extern struct {
    //         offset: u32,
    //         address: u32,
    //         size: u32,
    //         size2: u32,
    //     } = .{
    //         .offset = self.offset,
    //         .address = self.address,
    //         .size = self.size,
    //         .size2 = self.size,
    //     };
    //     key = unhexlify(spiCryptoKey.get(self.kind.split('-')[-1], "retail"))
    //     cipher = AES.new(key, AES.MODE_CBC, iv);
    //     return cipher.encrypt(self.section_data) if encrypt else cipher.decrypt(self.section_data)
    // }

    // fn setData(self: *@This(), bytes: []const u8) void {
    //     self.section_data = bytes + b'\xFF' *  ((512 - (len(data) % 512)) % 512)
    //     self.size = self.section_data.len;
    //     self.guessed_type = null;

    //     std.crypto.hash.sha2.Sha256.hash(self.section_data, self.hash, .{});
    //     self.hash_is_valid = true;
    // }

    fn exportBinaries(self, base_path: []const u8, extract_modules: bool, secret_sector: bool) !void {
        var buf: [std.fs.MAX_PATH_BYTES]u8 = undefined;
        const fba = std.heap.FixedBufferAllocator.init(&buf);
        const allocator = fba.allocator();
        const cwd = std.fs.cwd();
        if (self.guessed_type == .Kernel11Modules and extract_modules) {
            var pos: usize = 0;
            const modules_path = try std.fs.path.join(allocator, &[_][]const u8{ base_path, "modules" });
            defer allocator.free(modules_path);
            cwd.makeDir(modules_path) catch |err| {
                if (err != std.os.MakeDirError.PathAlreadyExists) {
                    return err;
                }
            };
            while (pos < self.size) {
                size = unpack_from("<I", self.section_data, pos + 0x104)[0] * 0x200
                name = self.section_data[pos + 0x200: pos + 0x208].decode("ascii")
                nullBytePos = name.find('\x00')
                name = name if nullBytePos == -1 else name[:nullBytePos]
                name = "{0}.cxi".format(name)
                with open(os.path.join(basePath, "modules", name), "wb+") as f:
                    f.write(self.section_data[pos : pos + size])
                pos += size
            }
        } else if (self.guessed_type == .Kernel9Loader and secret_sector) {
        //    # kek is in keyslot 0x11, as "normal key"
        //    encKeyX = self.section_data[:0x10] if self.guessedType[3] == '0' else self.section_data[0x60 : 0x70]
        //    kek = secretSector[:0x10] if self.guessedType[3] != '2' else secretSector[0x10 : 0x20]
//
        //    keyX = AES.new(kek, AES.MODE_ECB).decrypt(encKeyX)
        //    keyY = self.section_data[0x10 : 0x20]
        //    key = unhexlify("{0:032X}".format(keyscrambler(int(hexlify(keyX), 16), int(hexlify(keyY), 16))))
//
        //    ctr = self.section_data[0x20 : 0x30]
        //    sizeDec = self.section_data[0x30 : 0x38].decode("ascii")
        //    size = int(sizeDec[:sizeDec.find('\x00')], 10)
//
        //    data = self.section_data
        //    if 0x800 + size <= self.size:
        //        cipher = AES.new(key, AES.MODE_CTR, initial_value=ctr, nonce=b'')
        //        decData = cipher.decrypt(self.section_data[0x800 : 0x800 + size])
        //        data = b''.join((self.section_data[:0x800], decData, self.section_data[0x800+size:]))
        //        if extractModules:
        //            exportP9(basePath, data)
//
        //    with open(os.path.join(basePath, "section{0}.bin".format(self.num)), "wb+") as f:
        //        f.write(data)
//
            return;
        } else if (self.guessed_type == .Kernel9) {
            if (extract_modules) {
                exportP9(base_path, self.section_data);
            }
        }
        var file_name: []u8 = "section0.bin";
        file_name[7] = '0' + self.index;
        const section_path = try std.fs.path.join(allocator, &[_][]const u8{ base_path, file_name });
        const file = try cwd.createFile(section_path, .{});
        defer file.close();
        file.write(self.section_data);
    }

    pub fn format(self: @This(), comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        writer.print(
            \\Copy type:\t{s}{s}
            \\Offset:\t\t0x{X:0>8}
            \\Address:\t0x{X:0>8}
            \\Size:\t\t0x{X:0>8}
            \\Hash:\t\t{X:0>32}{s}
            , .{
                self.copy_method, self.guessed_type,
                self.offset,
                self.address,
                self.size,
                self.hash, if (self.hash_is_valid) "" else " (invalid)"
            }
        );
    }
};

fn parse_firm(args) void {
    print(Firm(args.type if args.type else "nand-retail", args.infile.read()))
}

fn extract_firm(args) void {
    keys = secretSectorKey1Key2.get(args.type.split('-')[-1], "retail") if args.secret_sector is None else args.secret_sector.read()
    firm_obj = Firm(args.type if args.type else "nand-retail", args.infile.read())
    firm_obj.export(args.outdir, args.export_modules, unhexlify(keys))
}

fn build_firm(args) void {
    if (len(args.section_data) != len(args.section_copy_methods)) {
        raise ValueError("number of sections not matching");
    } else if (len(args.section_addresses) > 4 or len(args.section_data) > 4 or len(args.section_copy_methods) > 4) {
        raise ValueError("too many sections");
    }

    if (!args.signature and args.type) {
        args.signature = args.type;
    }

    var addrpos: u8 = 0;

    const firm_obj = Firm(args.signature);
    firm_obj.arm9_entrypoint = args.arm9_entrypoint;
    firm_obj.arm11_entrypoint = args.arm11_entrypoint;

    var arm11_flags: u8 = 0;
    if (args.suggest_screen_init) {
        arm11_flags |= 1;
    }

    if (args.suggest_skipping_bootrom_lockout) {
        arm11_flags |= 2;
    }

    firm_obj.header.reserved[0] = arm11_flags;

    for i in range(len(args.section_copy_methods)) {
        magic = args.section_data[i].read(4)
        args.section_data[i].seek(0)

        if len(magic) == 4 and magic == b"\x7FELF":
            entry, firm_obj.sections[i].address, data = extractElf(args.section_data[i])
            firm_obj.sections[i].copyMethod = ("NDMA", "XDMA", "memcpy").index(args.section_copy_methods[i])
            firm_obj.arm9Entrypoint = entry if firm_obj.arm9Entrypoint == 0 and args.section_copy_methods[i] == "NDMA" else firm_obj.arm9Entrypoint
            firm_obj.arm11Entrypoint = entry if firm_obj.arm11Entrypoint == 0 and args.section_copy_methods[i] == "XDMA" else firm_obj.arm11Entrypoint
            firm_obj.setSectionData(i, data)
        else:
            if addrpos >= len(args.section_addresses):
                raise argparse.ArgumentError("missing section addresses")
            firm_obj.sections[i].address = args.section_addresses[addrpos]
            firm_obj.sections[i].copyMethod = ("NDMA", "XDMA", "memcpy").index(args.section_copy_methods[i])
            firm_obj.setSectionData(i, args.section_data[i].read())
            addrpos += 1
    }

    firm_obj.check();
    if (!firm_obj.arm9_entrypoint_found) {
        raise ValueError("invalid or missing Arm9 entrypoint");
    }
    if (!(firm_obj.header.arm11_entrypoint == 0 or firm_obj.arm11EntrypointFound)) {
        // bootrom / FIRM won't boot firms with a NULL arm11 ep, though
        raise ValueError("invalid or missing Arm11 entrypoint");
    }
    if (args.signature) {
        firm_obj.signature = unhexlify(perfectSignatures["firm-" + args.signature])
    }
    data = firm_obj.build();
    args.outfile.write(data);
    if (args.generate_hash) {
        try std.fs.cwd().writeFile(args.outfile.name + ".sha", SHA256.new(data).digest());
    }
}

const Mode = enum {
    parse,
    extract,
    build,
};

const ArgType = enum {
    Positional,
    Flag,
    Value,
};
const ArgDef = union(ArgType) {
    Positional: struct {
        @"type": type,
        help: []const u8,
        position: u8,
        choices: ?[][]const u8 = null,
    },
    Flag: struct {
        @"type": type,
        help: []const u8,
        args: [][]const u8,
        unset: []const u8,
        set: []const u8,
    },
    Value: struct {
        @"type": type,
        help: []const u8,
        args: [][]const u8,
        default: []const u8,
        parser: ?*fn(comptime T: type, input: []const u8) T = null,
    },
};
const args = .{
    .parse = .{
        .infile = ArgDef {
            .Positional = .{
                .@"type" = std.fs.File,
                .help = "Input firmware file",
                .position = 0,
            },
        },
        .kind = ArgDef {
            .Value = .{
                .@"type" = FirmKind,
                .args = .{ "-k", "--kind" },
                .help = "The kind of FIRM to assume (default: nand-retail)",
                .choices = .{ "nand-retail", "spi-retail", "nand-dev", "spi-dev" },
                .default = "nand-retail",
            },
        }
    },
    .extract = .{
        .infile = ArgDef {
            .Positional = .{
                .@"type" = std.fs.File,
                .help = "Input firmware file",
                .position = 0,
            },
        },
        .outdir = ArgDef {
            .Positional = .{
                .@"type" = std.fs.Dir,
                .help = "Output directory (current directory by default)",
                .position = 0,
                .default = ".",
            },
        },
        .kind = ArgDef {
            .Value = .{
                .@"type" = FirmKind,
                .args = .{ "-k", "--kind" },
                .help = "The kind of FIRM to assume (default: nand-retail)",
                .choices = .{ "nand-retail", "spi-retail", "nand-dev", "spi-dev" },
                .default = "nand-retail",
            },
        },
        .export_modules = ArgDef {
            .Flag = .{
                .@"type" = bool,
                .args = .{ "-m", "--export-modules" },
                .help = "Export k11 modules and Process9 (when applicable and if possible)",
                .unset = "false",
                .set = "true",
            },
        },
    },
    .build = .{
        .k9elf = ArgDef {
            .Positional = .{
                .@"type" = std.fs.File,
                .help = "Kernel9 ELF input",
                .position = 0,
            },
        },
        .k11elf = ArgDef {
            .Positional = .{
                .@"type" = std.fs.File,
                .help = "Kernel11 ELF input",
                .position = 1,
            },
        },
        .outfile = ArgDef {
            .Positional = .{
                .@"type" = std.fs.File,
                .help = "Ouput firmware file",
                .position = 2,
            },
        },

        parser_build.add_argument("-C", "--section-copy-methods", help="Copy method of each section (NDMA, XDMA, memcpy) (required)", choices=("NDMA", "XDMA", "memcpy"), nargs='+', required=True)

        .kind = ArgDef {
            .Value = .{
                .@"type" = FirmKind,
                .args = .{ "-S", "--signature", "-k", "--kind" },
                .help = "The kind of the perfect signature to include (default: nand-retail)",
                .choices = .{ "nand-retail", "spi-retail", "nand-dev", "spi-dev" },
                .default = "nand-retail",
            },
        },
        .generate_hash = ArgDef {
            .Flag = .{
                .@"type" = bool,
                .args = .{ "-g", "--generate-hash" },
                .help = "Generate a .sha file containing the SHA256 digest of the output file",
                .unset = "false",
                .set = "true",
            },
        },
        .screen_init = ArgDef {
            .Flag = .{
                .@"type" = bool,
                .args = .{ "-i", "--suggest-screen-init" },
                .help = "Suggest that screen init should be done before launching the output file",
                .unset = "false",
                .set = "true",
            },
        },
        .skipping_bootrom_lockout = ArgDef {
            .Flag = .{
                .@"type" = bool,
                .args = .{ "-b", "--suggest-skipping-bootrom-lockout" },
                .help = "Suggest skipping bootrom lockout",
                .unset = "false",
                .set = "true",
            },
        },
    },
};

fn main() !void {
    const parser = ArgumentParser(.{
        .prog = "firmtool",
        .description = "Parses, extracts, and builds 3DS firmware files."
    });

    
    args = parser.parse_args()

    # http://bugs.python.org/issue16308 it's still not fixed, WTF are they doing ?!
    try:
        getattr(args, "func")
    except AttributeError:
        parser.print_help()
        sys.exit(0)

    args.func(args)
}
