#!/usr/bin/env python3
import sys
import struct
import argparse
from io import StringIO
from typing import Iterable, Optional


class AutoRepr:
    def __repr__(self):
        items = ("%s = %r" % (k, v) for k, v in self.__dict__.items())
        return "<%s: {%s}>" % (self.__class__.__name__, ', '.join(items))


def _bitprop(b: int) -> property:
    def getter(self):
        return ((self.flags >> b) & 1) == 1
    def setter(self, value: bool):
        if value:
            self.flags |= 1 << b
        else:
            self.flags &= ~(1 << b)
    return property(getter, setter)


class ARM11KernelCapability:
    class Entry:
        pass

    class InterruptInfo(Entry):
        def __init__(self, interrupt_mask: int):
            self.interrupt_mask = interrupt_mask

        def __repr__(self):
            return "InterruptInfo(0b{:028b})".format(self.interrupt_mask)

    class SystemCallMask(Entry):
        def __init__(self, table_index: int, mask: int):
            self.table_index = table_index
            self.mask = mask

        def __repr__(self):
            return "SystemCallMask({}, 0b{:024b})".format(
                self.table_index, self.mask)

    class KernelVersion(Entry):
        def __init__(self, major: int, minor: int):
            self.major = major
            self.minor = minor

        def __repr__(self):
            return "KernelVersion({}, {})".format(self.major, self.minor)

    class HandleTableSize(Entry):
        def __init__(self, size: int):
            self.size = size

        def __repr__(self):
            return "HandleTableSize({})".format(self.size)

    class KernelFlags(Entry):
        def __init__(self, flags: Optional[int] = None, **kwargs):
            if flags is None:
                for k, v in kwargs.items():
                    setattr(self, k, v)
            else:
                self.flags = flags

        def __repr__(self):
            return "KernelFlags(" + ", ".join(
                ("{}={}".format(n, getattr(self, n))
                 for n in self.FIELD_NAMES)) + ")"

        allow_debug = _bitprop(0)
        force_debug = _bitprop(1)
        allow_non_alphanum = _bitprop(2)
        shared_page_writing = _bitprop(3)
        privilege_priority = _bitprop(4)
        allow_main_args = _bitprop(5)
        shared_device_memory = _bitprop(6)
        runnable_on_sleep = _bitprop(7)
        @property
        def memory_type(self):
            return (self.flags >> 11) & 0b1111
        @memory_type.setter
        def memory_type(self, value: int):
            self.flags = (self.flags & ~(0b1111 << 11)) | ((value & 0b1111) << 11)
        special_memory = _bitprop(12)
        core2_access = _bitprop(13)

        FIELD_NAMES = [
            'allow_debug',
            'force_debug',
            'allow_non_alphanum',
            'shared_page_writing',
            'privilege_priority',
            'allow_main_args',
            'shared_device_memory',
            'runnable_on_sleep',
            'memory_type',
            'special_memory',
            'core2_access'
        ]

    class MapMemoryRange(Entry):
        def __init__(self, start: int, end: int, ro: bool):
            self.start = start
            self.end = end
            self.ro = ro

        def __repr__(self):
            return "MapMemoryRange(0x{:08X}, 0x{:08X}, ro={})".format(
                self.start, self.end, self.ro)

    class MapMemoryPage(Entry):
        def __init__(self, address: int, ro: bool):
            self.address = address
            self.ro = ro

        def __repr__(self):
            return "MapMemoryPage(0x{:08X}, ro={})".format(
                self.address, self.ro)

    @classmethod
    def _from_bits_single(cls, bits: int):
        if bits >> 28 == 0b1110:
            return cls.InterruptInfo(bits & 0xFFFFFFF)
        if bits >> 27 == 0b11110:
            return cls.SystemCallMask((bits >> 24) & 0b111, bits & 0xFFFFFF)
        if bits >> 25 == 0b1111110:
            return cls.KernelVersion((bits >> 8) & 0xFF, bits & 0xFF)
        if bits >> 24 == 0b11111110:
            return cls.HandleTableSize(bits & 0x7FFFF)
        if bits >> 23 == 0b111111110:
            return cls.KernelFlags(bits & 0x7FFFFF)
        if bits >> 21 == 0b11111111100:
            return cls.MapMemoryRange((bits & 0xFFFFF) << 12, 0, ((bits >> 20) & 1) == 1)
        if bits == 0xFFFFFFFF:
            return None
        if bits >> 21 == 0b11111111111:
            return cls.MapMemoryPage((bits & 0xFFFFF) << 12, ((bits >> 20) & 1) == 1)
        raise ValueError(bin(bits))

    @classmethod
    def from_bits(cls, descriptors: Iterable[int]):
        raw_desc = [cls._from_bits_single(x) for x in descriptors]
        result = []
        prev_range = None
        for desc in raw_desc:
            if isinstance(desc, cls.MapMemoryRange):
                if prev_range is None:
                    prev_range = desc
                else:
                    prev_range.end = desc.start
                    result.append(prev_range)
                    prev_range = None
            elif desc is not None:
                result.append(desc)
        if prev_range is not None:
            raise ValueError("Unterminated memory range mapping")
        return result

class ExHeader:
    def __init__(self):
        pass

    def load(self, data):
        main_struct = struct.Struct('<512s512s256s256s512s')
        sci, aci, sig, pubkey, aci2 = main_struct.unpack(data.read(main_struct.size))
        aci_struct = struct.Struct('<368s128s16s')
        arm11syscaps, arm11kcaps, arm9acl = aci_struct.unpack(aci)
        arm11kcaps_struct = struct.Struct('<28I16x')
        arm11kcaps_descriptors = arm11kcaps_struct.unpack(arm11kcaps)
        print('\n'.join(map(repr, ARM11KernelCapability.from_bits(arm11kcaps_descriptors))))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('exhfile')
    args = parser.parse_args()

    exh = ExHeader()
    with open(args.exhfile, 'rb') as exhfile:
      exh.load(exhfile)

 
