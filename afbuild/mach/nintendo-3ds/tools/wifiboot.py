#!/usr/bin/env python3
# kate: space-indent on; indent-width 4;
import argparse
import firmtool.__main__ as ft
import socket
import struct
import datetime

def bcd(i):
    return ((i // 10) << 4) | (i % 10)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('address')
    parser.add_argument('file', type=argparse.FileType('rb'))
    args = parser.parse_args()

    firm = ft.Firm('nand-retail', args.file.read())
    args.file.seek(0)

    disc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    disc.bind(('<broadcast>', 17491))

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as query:
        query.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        query.sendto(b'3dsfirmboot', ('<broadcast>', 17491))

    buf = None
    while buf != b'bootfirm3ds':
        buf, addr = disc.recvfrom(256)
        print(buf, addr)
    __import__('os').exit()

    print("Connecting to {}".format(args.address))
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
    sock.connect((args.address, 17491))
    print("Connected")
    sock.send(args.file.seek(0x200))
    now = datetime.datetime.now()
    sock.send(struct.pack(
        '<8s24s8B3I92x',
        b'BootINFO', b'ElementW wifiboot\0\0\0\0\0\0\0',
        bcd(now.second), bcd(now.minute), bcd(now.hour),
        bcd(now.weekday()), bcd(now.day), bcd(now.month),
        bcd(now.year % 100), bcd(now.year // 100),
        0, 0, 0))
    print(sock.recv(4))
    sock.close()
