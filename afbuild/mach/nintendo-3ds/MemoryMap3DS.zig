// SPDX-License-Identifier: EUPL-1.2
const MemoryArea = @import("../afk.zig").mm.MemoryArea;

const ARM9 = struct {
    const ITCM = MemoryArea.init("ITCM", 0x07FF8000, 0x00008000);
    const ARM9Mem = MemoryArea.init("ARM9Mem", 0x08000000, 0x00100000);
    const ARM9MemNew = MemoryArea.init("ARM9MemNew", 0x08000000, 0x00180000);
    const IOMem = MemoryArea.init("IO", 0x10000000, 0x00500000);
    const VRAM = MemoryArea.init("VRAM", 0x18000000, 0x00600000);
    const DSP = MemoryArea.init("DSP", 0x1FF00000, 0x00080000);
    const AXIWRAM = MemoryArea.init("AXIWRAM", 0x1FF80000, 0x00080000);
    const FCRAM = MemoryArea.init("FCRAM", 0x20000000, 0x08000000);
    const FCRAMNew = MemoryArea.init("FCRAMNew", 0x20000000, 0x10000000);
    const DTCM = MemoryArea.init("DTCM", 0xFFF00000, 0x00004000);
    const Bootrom = MemoryArea.init("Bootrom", 0xFFFF0000, 0x00010000);
};

const ARM11 = struct {
    const Bootrom = MemoryArea.init(0x00000000, 0x00010000);
    const IOMem = MemoryArea.init(0x10000000, 0x00500000);
    const MPCorePrivate = MemoryArea.init(0x17E00000, 0x00002000);
    const L2CacheController = MemoryArea.init(0x17E10000, 0x00001000);
    const VRAM = MemoryArea.init(0x18000000, 0x00600000);
    const New3DSAdditional = MemoryArea.init(0x1F000000, 0x00400000);
    const DSP = MemoryArea.init(0x1FF00000, 0x00080000);
    const AXIWRAM = MemoryArea.init(0x1FF80000, 0x00080000);
    const FCRAM = MemoryArea.init(0x20000000, 0x08000000);
    const FCRAMNew = MemoryArea.init(0x20000000, 0x10000000);
    const BootromMirror = MemoryArea.init(0xFFFF0000, 0x00010000);
};
