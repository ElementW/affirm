// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const Target = std.Target;

const afbuild = @import("../../afbuild.zig");

pub const mach: afbuild.Machine =
    .{
    .id = "rvvm",
    .name = "RVVM RISC-V64 emulator",
    .parts = &.{
        .{
            .id = "main",
            .name = "main",
            .target = .{
                .cpu = (Target.Cpu.Model{
                    .name = "baseline_rv64",
                    .llvm_name = null,
                    .features = Target.riscv.featureSet(&.{
                        .@"64bit",
                        .m,
                        .a,
                        .f,
                        .d,
                        .c,
                        .zkr,
                        .zicbom,
                        .zicboz,
                    }),
                }).toCpu(.riscv64),
                .os = afbuild.kernel_os,
                .abi = .eabi,
                .ofmt = .elf,
            },
            .code_model = .default,
            .linkscript_params = .{
                .output_format = "elf64-littleriscv",
                .output_arch = "riscv",
                .base_addr = 0x80200000,
            },
        },
    },
};
