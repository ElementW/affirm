// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const Target = std.Target;

const afbuild = @import("../../afbuild.zig");

pub const machs = [_]afbuild.Machine{.{
    .id = "linux-usermode-x86_64",
    .name = "Linux Usermode (x86_64)",
    .parts = &.{.{
        .id = "main",
        .name = "main",
        .target = .{
            .cpu = std.Target.x86.cpu.x86_64.toCpu(.x86_64),
            .os = .{
                .tag = .linux,
                .version_range = .{
                    .linux = .{
                        .range = .{
                            .min = .{
                                .major = 6,
                                .minor = 0,
                                .patch = 0,
                            },
                            .max = .{
                                .major = 0,
                                .minor = 0,
                                .patch = 0,
                            },
                        },
                        .glibc = .{
                            .major = 0,
                            .minor = 0,
                            .patch = 0,
                        },
                        .android = 0,
                    },
                },
            },
            .abi = .gnu,
            .ofmt = .elf,
            .dynamic_linker = .init("/lib64/ld-linux-x86-64.so.2"),
        },
        .code_model = .default,
        .linkscript_params = null,
        // .{
        //     .output_format = switch (builtin.target.cpu.arch) {
        //         .x86_64 => "elf64-x86-64",
        //         .riscv32 => "elf32-littleriscv",
        //         .riscv64 => "elf64-littleriscv",
        //         else => |arch| @panic("Unsupported host arch \"" ++ @tagName(arch) ++ "\" for linkscript output_format"),
        //     },
        //     .output_arch = switch (builtin.target.cpu.arch) {
        //         .x86_64 => "i386:x86-64",
        //         .riscv32, .riscv64 => "riscv",
        //         else => |arch| @panic("Unsupported host arch \"" ++ @tagName(arch) ++ "\" for linkscript output_arch"),
        //     },
        //     .start_addr = 0x10000000,
        //     .has_tls = true,
        //     .has_dynamic = true,
        // },
    }},
}};
