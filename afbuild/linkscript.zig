// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const Build = std.Build;
const Step = Build.Step;

pub const LinkscriptParams = struct {
    output_format: []const u8,
    output_arch: []const u8,
    base_addr: usize,
    entry_addr: ?usize = null,
    stack_size: usize = 4096 * 16,
    stack_align: usize = 4096,
    has_tls: bool = false,
    has_dynamic: bool = false,
    separate_text_data: bool = false,
    load_elf_program_headers: bool = false,
    include_debug_info: bool = false,
};

pub const LinkscriptGenStep = struct {
    step: Step,
    params: LinkscriptParams,
    generated_file: std.Build.GeneratedFile,

    pub fn getOutput(self: *LinkscriptGenStep) Build.LazyPath {
        return .{ .generated = .{ .file = &self.generated_file } };
    }
};

fn make(step: *Step, options: Build.Step.MakeOptions) !void {
    _ = options;
    const b = step.owner;
    const arena = b.allocator;
    const gen: *LinkscriptGenStep = @fieldParentPtr("step", step);
    var man = b.graph.cache.obtain();
    defer man.deinit();

    man.hash.addBytes(gen.params.output_format);
    man.hash.addBytes(gen.params.output_arch);
    man.hash.add(gen.params.base_addr);
    man.hash.add(gen.params.entry_addr != null);
    man.hash.add(gen.params.entry_addr orelse 0);
    man.hash.add(gen.params.stack_size);
    man.hash.add(gen.params.stack_align);
    man.hash.add(gen.params.has_tls);
    man.hash.add(gen.params.has_dynamic);
    man.hash.add(gen.params.separate_text_data);
    man.hash.add(gen.params.load_elf_program_headers);
    man.hash.add(gen.params.include_debug_info);

    const hit = try step.cacheHit(&man);
    gen.generated_file.path = try b.cache_root.join(arena, &.{
        "o", &man.final(), "linkscript.ld",
    });
    if (hit) {
        // cache hit, skip generating
        step.result_cached = true;
        return;
    }

    try b.cache_root.handle.makePath(std.fs.path.dirname(gen.generated_file.path.?).?);

    const file = try std.fs.createFileAbsolute(gen.generated_file.path.?, .{});
    defer file.close();
    try std.fmt.format(file.writer(),
        \\/* SPDX-License-Identifier: EUPL-1.2 */
        \\OUTPUT_FORMAT("{[output_format]s}")
        \\OUTPUT_ARCH("{[output_arch]s}")
        \\ENTRY(_start)
        \\
        \\PHDRS
        \\{{
        \\    headers PT_PHDR PHDRS ;
        \\    text PT_LOAD ;
    , .{
        .output_format = gen.params.output_format,
        .output_arch = gen.params.output_arch,
    });
    if (gen.params.separate_text_data) {
        try file.writer().writeAll(
            \\    data PT_LOAD ;
        );
    }
    if (gen.params.has_tls) {
        try file.writer().writeAll(
            \\    tls PT_TLS ;
        );
    }
    if (gen.params.has_dynamic) {
        try file.writer().writeAll(
            \\    dynamic PT_DYNAMIC ;
        );
    }
    if (gen.params.load_elf_program_headers) {
        try file.writer().writeAll(
            \\    load_headers PT_LOAD PHDRS FILEHDR ;
        );
    }
    const code_destination = "text";
    const ro_data_destination = if (gen.params.separate_text_data) "data" else "text";
    const rw_data_destination = if (gen.params.separate_text_data) "data" else "text";
    try std.fmt.format(file.writer(),
        \\
        \\}}
        \\
        \\MEMORY {{
        \\    load (rwx) : ORIGIN = 0x{[base_addr]X}, LENGTH = 256M
        \\}}
        \\
        \\SECTIONS
        \\{{
        \\    . = 0x{[base_addr]X};
        \\    __start__ = ABSOLUTE(.);
        \\
        \\    /* Executable code */
        \\    .text.header : ALIGN(4) {{
        \\        __text_header_start = .;
        \\        *(.text.header);
        \\        __text_header_end = .;
        \\    }} >load :{[code_destination]s}
        \\    .text.start : ALIGN(4) {{
    , .{
        .base_addr = gen.params.base_addr,
        .code_destination = code_destination,
    });
    if (gen.params.entry_addr) |entry_addr| {
        try std.fmt.format(file.writer(),
            \\        . = 0x{[entry_addr]X};
            \\
        , .{ .entry_addr = entry_addr });
    }
    try std.fmt.format(file.writer(),
        \\
        \\        __text_start_start = .;
        \\        *(.text.start);
        \\        __text_start_end = .;
        \\    }} >load :{[code_destination]s}
        \\    .text : ALIGN(16) {{
        \\        __text_start = .;
        \\        *(.text*);
        \\        . = ALIGN(16);
        \\        __text_end = .;
        \\    }} >load :{[code_destination]s}
        \\
        \\    /* Read-only data */
        \\    .rodata : ALIGN(16) {{
        \\        __rodata_start = .;
        \\        *(.rodata*);
        \\        . = ALIGN(16);
        \\        __rodata_end = .;
        \\    }} >load :{[ro_data_destination]s}
        \\
    , .{
        .code_destination = code_destination,
        .ro_data_destination = ro_data_destination,
    });

    try file.writer().writeAll("\n    /* Debug-as-data sections */\n");
    const debug_sections = [_][]const u8{
        "debug_info",
        "debug_abbrev",
        "debug_str",
        "debug_line",
        "debug_ranges",
    };
    if (gen.params.include_debug_info) {
        if (true) {
            @panic(
                \\NYI: including debug info is broken right now because LLD
                \\processes the .debug_* sections and clears the SHF_ALLOC
                \\flag of the output section, with no option to set it back.
                \\TODO: implement own compact form of panic-print info
            );
        }
        for (debug_sections) |section| {
            try std.fmt.format(file.writer(),
                \\    .rodata.{[section]s} : {{
                \\        PROVIDE(__{[section]s}_start = ABSOLUTE(.));
                \\        KEEP(*(.{[section]s}));
                \\        PROVIDE(__{[section]s}_end = ABSOLUTE(.));
                \\    }} >load :{[ro_data_destination]s}
                \\
            , .{
                .section = section,
                .ro_data_destination = ro_data_destination,
            });
        }
    } else {
        for (debug_sections) |section| {
            try std.fmt.format(file.writer(),
                \\    PROVIDE(__{[section]s}_start = 0);
                \\    PROVIDE(__{[section]s}_end = 0);
                \\
            , .{
                .section = section,
            });
        }
    }

    try std.fmt.format(file.writer(),
        \\
        \\    /* Read-write data */
        \\    .data : ALIGN(16) {{
        \\        __data_start = .;
        \\        *(.data*);
        \\        *(vtable);
        \\
        \\        . = ALIGN(4);
        \\        /* preinit data */
        \\        PROVIDE_HIDDEN (__preinit_array_start = .);
        \\        KEEP(*(.preinit_array));
        \\        PROVIDE_HIDDEN (__preinit_array_end = .);
        \\
        \\        . = ALIGN(4);
        \\        /* init data */
        \\        PROVIDE_HIDDEN (__init_array_start = .);
        \\        KEEP(*(SORT(.init_array.*)));
        \\        KEEP(*(.init_array));
        \\        PROVIDE_HIDDEN (__init_array_end = .);
        \\
        \\        . = ALIGN(4);
        \\        /* finit data */
        \\        PROVIDE_HIDDEN (__fini_array_start = .);
        \\        KEEP(*(SORT(.fini_array.*)));
        \\        KEEP(*(.fini_array));
        \\        PROVIDE_HIDDEN (__fini_array_end = .);
        \\
        \\        . = ALIGN(16);
        \\        __data_end = .;
        \\    }} >load :{[rw_data_destination]s}
        \\
        \\    .bss : ALIGN(8) {{
        \\        PROVIDE(__bss_start = ABSOLUTE(.));
        \\        *(.bss* COMMON);
        \\        . = ALIGN(8);
        \\        PROVIDE(__bss_end = ABSOLUTE(.));
        \\    }}
        \\
    , .{
        .rw_data_destination = rw_data_destination,
    });
    if (gen.params.has_tls) {
        try file.writer().writeAll(
            \\    .tdata : {
            \\        PROVIDE_HIDDEN (__tdata_start = .);
            \\        *(.tdata .tdata.* .gnu.linkonce.td.*)
            \\    } :tls
            \\    .tbss : {
            \\        *(.tbss .tbss.* .gnu.linkonce.tb.*);
            \\        *(.tcommon);
            \\    } :tls
            \\
        );
    }
    if (gen.params.has_dynamic) {
        try file.writer().writeAll(
            \\    .dynamic : {{ *(.dynamic) }}
            \\
        );
    }
    try std.fmt.format(file.writer(),
        \\    _bss_gap_start = .;
        \\    . = ALIGN({[stack_align]});
        \\    PROVIDE(_bss_gap_size = (. - _bss_gap_start));
        \\    PROVIDE(_stack_start = ABSOLUTE(.));
        \\    . += {[stack_size]};
        \\    PROVIDE(_stack_end = ABSOLUTE(.));
        \\
        \\    __end__ = ABSOLUTE(.);
        \\}}
    , .{
        .stack_align = gen.params.stack_align,
        .stack_size = gen.params.stack_size,
    });
}

pub fn add(b: *Build, params: LinkscriptParams) *LinkscriptGenStep {
    const gen = b.allocator.create(LinkscriptGenStep) catch @panic("OOM");
    gen.* = .{
        .step = Step.init(.{
            .id = .custom,
            .name = "linkscript",
            .owner = b,
            .makeFn = make,
        }),
        .params = params,
        .generated_file = .{ .step = &gen.step },
    };
    return gen;
}

pub fn create(b: *Build, params: LinkscriptParams) Build.LazyPath {
    return add(b, params).getOutput();
}
