// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const Build = std.Build;
const Step = Build.Step;

const afbuild = @import("../afbuild.zig");

pub const PartCompileParams = struct {
    machine: *const afbuild.Machine,
    part: *const afbuild.Machine.Part,
    common_modules: *const afbuild.CommonModules,
    compile_options: afbuild.CompileOptions,
};

pub const SysModuleOptions = struct {
    root_source_file: Build.LazyPath,
};
pub const SysModuleSteps = struct {
    executable_compile: *Step.Compile,
    test_compile: *Step.Compile,
};

pub const SysModuleBuild = struct {
    machine: *const afbuild.Machine,
    part: *const afbuild.Machine.Part,
    common_modules: *const afbuild.CommonModules,
    compile_options: afbuild.CompileOptions,

    build: *Build,
    name: []const u8,
    source_path: []const u8,

    pub fn resolvedTarget(self: *const SysModuleBuild) Build.ResolvedTarget {
        return self.build.resolveTargetQuery(std.Target.Query.fromTarget(self.part.target));
    }

    pub fn path(self: *const SysModuleBuild, sub_path: []const u8) Build.LazyPath {
        return self.build.path(self.build.pathJoin(&.{ self.source_path, sub_path }));
    }

    pub fn addSysModule(self: *const SysModuleBuild, options: SysModuleOptions) SysModuleSteps {
        const executable_compile = self.build.addExecutable(Build.ExecutableOptions{
            // TODO actual binary name logic
            .name = self.build.fmt("{s}.bin", .{self.name}),
            .root_source_file = options.root_source_file,
            .target = self.resolvedTarget(),
            .optimize = self.compile_options.optimize,
            .linkage = .static,
            .pic = true,
            .code_model = self.part.code_model,
        });
        self.common_modules.addImports(executable_compile.root_module);
        const test_compile = self.build.addTest(.{
            .test_runner = .{ .path = self.build.path("afbuild/test_runner.zig"), .mode = .simple },
            .root_module = executable_compile.root_module,
        });
        return .{
            .executable_compile = executable_compile,
            .test_compile = test_compile,
        };
    }
};

const ModuleDef = struct {
    name: []const u8,
    build: ?*const fn (b: *SysModuleBuild) SysModuleSteps = null,
};
const modules = [_]ModuleDef{
    .{ .name = "ac" },
    .{ .name = "act" },
    .{ .name = "am" },
    .{ .name = "boss" },
    .{ .name = "cam" },
    .{ .name = "cdc" },
    .{ .name = "cfg" },
    .{ .name = "csnd" },
    .{ .name = "dlp" },
    .{ .name = "frd" },
    .{ .name = "gpio" },
    .{ .name = "gsp" },
    .{ .name = "hid" },
    .{ .name = "http" },
    .{ .name = "i2c" },
    .{ .name = "ir" },
    .{ .name = "loader", .build = @import("../src/modules/loader/afbuild.zig").build },
    .{ .name = "mcu" },
    .{ .name = "mic" },
    .{ .name = "mp" },
    .{ .name = "mvd" },
    .{ .name = "news" },
    .{ .name = "nfc" },
    .{ .name = "nim" },
    .{ .name = "ns" },
    .{ .name = "nwm" },
    .{ .name = "pdm" },
    .{ .name = "pm" },
    .{ .name = "ps" },
    .{ .name = "ptm" },
    .{ .name = "pxi" },
    .{ .name = "qtm" },
    .{ .name = "ro" },
    .{ .name = "sm" },
    .{ .name = "soc" },
    .{ .name = "spi" },
    .{ .name = "ssl" },

    .{ .name = "uart" },
};

pub fn addModules(b: *Build, part_compile_params: *const PartCompileParams) void {
    const modules_build_step = b.step("modules", "Build all modules");
    inline for (modules) |module| {
        if (module.build) |build| {
            var module_build: afbuild.modules.SysModuleBuild = .{
                .machine = part_compile_params.machine,
                .part = part_compile_params.part,
                .common_modules = part_compile_params.common_modules,
                .compile_options = part_compile_params.compile_options,

                .build = b,
                .name = module.name,
                .source_path = b.fmt("src/modules/{s}/", .{module.name}),
            };
            const steps = build(&module_build);

            const module_build_step = b.step(module.name, b.fmt("Build {s} module", .{module.name}));
            module_build_step.dependOn(&steps.executable_compile.step);
            modules_build_step.dependOn(module_build_step);

            const module_test_build_step = b.step(b.fmt("{s}-test-build", .{module.name}), b.fmt("Build {s} unit tests", .{module.name}));
            module_test_build_step.dependOn(&steps.test_compile.step);

            const module_test_run_step = b.step(b.fmt("{s}-test", .{module.name}), b.fmt("Run {s} unit tests", .{module.name}));
            module_test_run_step.dependOn(&b.addRunArtifact(steps.test_compile).step);
        }
    }
}
