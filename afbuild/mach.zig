// SPDX-License-Identifier: EUPL-1.2
const builtin = @import("builtin");
const std = @import("std");
const afbuild = @import("../afbuild.zig");
const Machine = afbuild.Machine;

pub fn buildMachList() []const Machine {
    return &(@import("mach/linux-usermode.zig").machs ++ [_]Machine{
        .{
            .id = "generic-riscv64",
            .name = "Generic RISC-V64",
            .parts = &.{
                .{
                    .id = "main",
                    .name = "main",
                    .target = .{
                        .cpu = std.Target.riscv.cpu.baseline_rv64.toCpu(.riscv64),
                        .os = afbuild.kernel_os,
                        .abi = .eabi,
                        .ofmt = .elf,
                    },
                    .code_model = .default,
                    .linkscript_params = .{
                        .output_format = "elf64-littleriscv",
                        .output_arch = "riscv",
                        .base_addr = 0x80200000,
                    },
                },
            },
        },
        @import("mach/rvvm.zig").mach,
    });
}
