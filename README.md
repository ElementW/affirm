# Affinis, a 3DS operating system reimplementation

Affinis aims to be a complete 3DS firmware replacement, including kernel and userspace services reimplementations.

## Building

To build Affinis's parts, you need:

* Zig 0.13
* The `firmtool` Python package

Run ``zig build`` at the root of this repository.

## Running

Build creates an ``zig-out/bin/affinis.firm`` file containing what's needed to launch the ARM9 and ARM11
kernels, and userspace in the future.

You can:

* Launch FIRM from SD card using Fastboot3DS, Luma3DS, etc; either:

  * Copy this file manually to an SD card
  * Upload this file to the 3DS through FTP and one of the FTP server homebrews

* Launch the FIRM from a booted 3DS using `A9NC <https://github.com/d0k3/A9NC>`_ and ``3dslink``
  (this is the recommended way as it requires no SD card swaps and is generally the quickest)

## Documentation

TODO

## Q & A

### You should be using Rust!
No
