// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const builtin = @import("builtin");
const Build = std.Build;
const Module = Build.Module;
const Target = std.Target;
const Step = Build.Step;
const Query = Target.Query;
const ResolvedTarget = Build.ResolvedTarget;

pub const linkscript = @import("afbuild/linkscript.zig");
pub const modules = @import("afbuild/modules.zig");

pub const CommonModules = struct {
    afu: *Module,
    afsys: *Module,

    pub fn addImports(self: *const CommonModules, module: *Module) void {
        module.addImport("afu", self.afu);
        module.addImport("afsys", self.afsys);
    }
};

pub const CompileOptions = struct {
    optimize: std.builtin.OptimizeMode,
};

pub const Machine = struct {
    pub const Part = struct {
        id: [:0]const u8,
        name: [:0]const u8,
        target: Target,
        linkscript_params: ?linkscript.LinkscriptParams,
        code_model: std.builtin.CodeModel = .kernel,
        compile_kernel: *const fn (
            b: *Build,
            common_modules: *const CommonModules,
            machine: *const Machine,
            part: *const Part,
            options: *const CompileOptions,
        ) *Step.Compile = compileKernel,
    };
    pub const PartOutputs = struct {
        part: *const Part,
        kernel: *Step.Compile,
    };

    const List = @import("afbuild/mach.zig").buildMachList();
    pub const Enum = blk: {
        var fields: [List.len]std.builtin.Type.EnumField = undefined;
        for (0.., List) |i, machine| {
            fields[i] = std.builtin.Type.EnumField{
                .name = machine.id,
                .value = i,
            };
        }
        const enum_type = @Type(std.builtin.Type{ .@"enum" = .{
            .tag_type = usize,
            .fields = &fields,
            .decls = &.{},
            .is_exhaustive = true,
        } });
        break :blk enum_type;
    };

    id: [:0]const u8,
    name: [:0]const u8,
    parts: []const Part,
    assemble_kernel: ?*const fn (b: *Build, outputs: []const PartOutputs) *Step = null,

    pub fn byEnum(e: Enum) *const Machine {
        return &List[@intFromEnum(e)];
    }
};

pub const kernel_os = Target.Os{
    .tag = .freestanding,
    .version_range = Target.Os.VersionRange{ .none = {} },
};
const userspace_os = Target.Os{
    .tag = .other,
    .version_range = Target.Os.VersionRange{ .none = {} },
};
pub const sysmodule_os = userspace_os;
pub const app_os = userspace_os;

fn compileKernel(
    b: *Build,
    common_modules: *const CommonModules,
    machine: *const Machine,
    part: *const Machine.Part,
    options: *const CompileOptions,
) *Step.Compile {
    const kernel = b.addExecutable(Build.ExecutableOptions{
        .name = b.fmt("kernel-{s}.bin", .{machine.id}),
        .root_source_file = b.path("src/afk/afk.zig"),
        .target = b.resolveTargetQuery(Target.Query.fromTarget(part.target)),
        .optimize = options.optimize,
        .linkage = .static,
        .pic = true,
        .code_model = part.code_model,
    });
    common_modules.addImports(kernel.root_module);
    if (part.linkscript_params) |params| {
        kernel.setLinkerScript(linkscript.create(b, params));
    }
    return kernel;
}

pub fn moduleAfu(b: *Build) *Module {
    const afu = b.addModule("afu", Module.CreateOptions{
        .root_source_file = b.path("src/afu/afu.zig"),
    });
    const afu_test_build = b.addTest(.{
        .root_source_file = afu.root_source_file.?,
        .test_runner = .{ .path = b.path("afbuild/test_runner.zig"), .mode = .simple },
    });
    const afu_test_build_step = b.step("afu-test-build", "Build afu unit tests");
    afu_test_build_step.dependOn(&afu_test_build.step);
    const afu_test_run_step = b.step("afu-test", "Run afu unit tests");
    afu_test_run_step.dependOn(&b.addRunArtifact(afu_test_build).step);
    return afu;
}

pub fn moduleAfsys(b: *Build, afu: *Module) *Module {
    const afsys = b.createModule(Module.CreateOptions{
        .root_source_file = b.path("src/afsys/afsys.zig"),
    });
    afsys.addImport("afu", afu);
    return afsys;
}
