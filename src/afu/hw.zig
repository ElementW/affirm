// SPDX-License-Identifier: EUPL-1.2
pub const register = @import("hw/register.zig");

pub const Register = register.Register;
pub const makeVolatile = register.makeVolatile;
pub const reg = register.reg;
pub const unkreg = register.unkreg;
pub const roreg = register.roreg;
pub const unkroreg = register.unkroreg;
