// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");

pub const dt = @import("dt.zig");
pub const encoding = @import("encoding.zig");
pub const gfx = @import("gfx.zig");
pub const hw = @import("hw.zig");
pub const io = @import("io.zig");
pub const ipc = @import("ipc.zig");
pub const os = @import("os.zig");
pub const services = @import("services.zig");
pub const units = @import("units.zig");

pub const Handle = @import("handle.zig").Handle;
pub const Result = @import("result.zig").Result;

test {
    std.testing.refAllDeclsRecursive(@This());
}
