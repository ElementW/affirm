// SPDX-License-Identifier: EUPL-1.2
pub const Message = @import("ipc/message.zig").Message;
const rpc = @import("ipc/rpc.zig");
const messagestruct = @import("ipc/messagestruct.zig");
pub const RPCRuntimeInfo = rpc.RPCRuntimeInfo;
pub const RPC = rpc.RPC;
pub const ServiceDeclaration = rpc.ServiceDeclaration;
pub const DeclareService = rpc.DeclareService;

pub const DuplicateHandle = messagestruct.DuplicateHandle;
pub const duplicateHandle = messagestruct.duplicateHandle;

pub const DuplicateHandles = messagestruct.DuplicateHandles;
pub const duplicateHandles = messagestruct.duplicateHandles;

pub const GiveHandle = messagestruct.GiveHandle;
pub const giveHandle = messagestruct.giveHandle;

pub const GiveHandles = messagestruct.GiveHandles;
pub const giveHandles = messagestruct.giveHandles;

pub const SendingProcessId = messagestruct.SendingProcessId;
pub const sendingProcessId = messagestruct.sendingProcessId;

pub const BufferCopy = messagestruct.BufferCopy;
pub const bufferCopy = messagestruct.bufferCopy;
