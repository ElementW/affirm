// SPDX-License-Identifier: EUPL-1.2
pub const RGB888 = packed struct {
    const Self = @This();
    pub const Black = Self.fromRGB(0, 0, 0);
    pub const White = Self.fromRGB(255, 255, 255);

    r: u8,
    g: u8,
    b: u8,

    pub fn fromRGB(r: u8, g: u8, b: u8) Self {
        return Self{ .r = r, .g = g, .b = b };
    }
};

pub const RGB565 = packed struct {
    const Self = @This();
    pub const Black = Self.fromRGB(0, 0, 0);
    pub const White = Self.fromRGB(31, 63, 31);

    r: u5,
    g: u6,
    b: u5,

    pub fn fromRGB(r: u5, g: u6, b: u5) Self {
        return Self{ .r = r, .g = g, .b = b };
    }

    pub fn fromU16(color: u16) Self {
        return @as(Self, @bitCast(color));
    }
    pub fn toU16(self: Self) u16 {
        return @as(u16, @bitCast(self));
    }
};

pub const RGBA8888 = packed struct {
    const Self = @This();
    pub const Black = Self.fromRGB(0, 0, 0);
    pub const White = Self.fromRGB(255, 255, 255);

    r: u8,
    g: u8,
    b: u8,
    a: u8,

    pub fn fromRGB(r: u8, g: u8, b: u8) Self {
        return Self{ .r = r, .g = g, .b = b, .a = 0xFF };
    }
    pub fn fromRGBA(r: u8, g: u8, b: u8, a: u8) Self {
        return Self{ .r = r, .g = g, .b = b, .a = a };
    }

    pub fn fromU32(color: u32) Self {
        return @as(Self, @bitCast(color));
    }
    pub fn toU32(self: Self) u32 {
        return @as(u32, @bitCast(self));
    }
};

pub const RGBA5551 = packed struct {
    const Self = @This();
    pub const Black = Self.fromRGB(0, 0, 0);
    pub const White = Self.fromRGB(31, 31, 31);

    r: u5,
    g: u5,
    b: u5,
    a: u1,

    pub fn fromRGB(r: u5, g: u5, b: u5) Self {
        return Self{ .r = r, .g = g, .b = b, .a = 1 };
    }
    pub fn fromRGBA(r: u5, g: u5, b: u5, a: u1) Self {
        return Self{ .r = r, .g = g, .b = b, .a = a };
    }

    pub fn fromU16(color: u16) Self {
        return @as(Self, @bitCast(color));
    }
    pub fn toU16(self: Self) u16 {
        return @as(u16, @bitCast(self));
    }
};

pub const RGBA4444 = packed struct {
    const Self = @This();
    pub const Black = Self.fromRGB(0, 0, 0);
    pub const White = Self.fromRGB(15, 15, 15);

    r: u4,
    g: u4,
    b: u4,
    a: u4,

    pub fn fromRGB(r: u4, g: u4, b: u4) Self {
        return Self{ .r = r, .g = g, .b = b, .a = 15 };
    }
    pub fn fromRGBA(r: u4, g: u4, b: u4, a: u4) Self {
        return Self{ .r = r, .g = g, .b = b, .a = a };
    }

    pub fn fromU16(color: u16) Self {
        return @as(Self, @bitCast(color));
    }
    pub fn toU16(self: Self) u16 {
        return @as(u16, @bitCast(self));
    }
};
