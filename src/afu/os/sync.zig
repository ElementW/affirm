// SPDX-License-Identifier: EUPL-1.2

/// https://www.3dbrew.org/wiki/SVC#enum_ResetType
pub const ResetType = enum(u32) {
    one_shot = 0,
    sticky = 1,
    pulse = 2,
};

pub const Mutex = opaque {};

pub const Semaphore = opaque {};

pub const Timer = opaque {};

pub const Event = opaque {};

pub const AddressArbiter = opaque {};
