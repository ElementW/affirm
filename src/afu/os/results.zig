// SPDX-License-Identifier: EUPL-1.2
const afu = @import("../afu.zig");
const Result = afu.Result;

pub const InvalidHandle = Result.failure(.permanent, .wrong_argument, .os, .invalid_handle);
pub const OutOfRange = Result.failure(.permanent, .wrong_argument, .os, .out_of_range);

pub const NotImplemented = Result.failure(.permanent, .not_supported, .os, .not_implemented);
