// SPDX-License-Identifier: EUPL-1.2
const afu = @import("../afu.zig");
const Handle = afu.Handle;

pub const Port = opaque {};

pub const Session = opaque {};

pub const SessionHandlePair = extern struct {
    server: Handle.Of(Session),
    client: Handle.Of(Session),
};

pub const MessageTap = opaque {};
