// SPDX-License-Identifier: EUPL-1.2
//! https://www.3dbrew.org/wiki/SVC#GetSystemInfo
const std = @import("std");
const afu = @import("../afu.zig");

const mm = afu.os.mm;
const pm = afu.os.pm;
const units = afu.units;

pub const HandleType = enum(u8) {
    process = 0,
    thread = 1,
};

pub const HandleInfo = union(Type) {
    pub const Type = enum(u32) {
        creation_time = 0,
        reference_count = 1,
        owning_process_id = 2,

        handle_type = 3,
    };

    creation_time: u64,
    reference_count: u32,
    owning_process_id: pm.Process.Id,

    handle_type: HandleType,
};

pub const SystemInfo = union(Type) {
    pub const Type = enum(u32) {
        memory_usage = 0,
        kernel_memory_usage = 2,
        peak_process_count = 25,
        bootstrap_process_count = 26,

        page_size = 32,
    };

    pub const Query = union(Type) {
        memory_usage: mm.MemoryRegion,
        kernel_memory_usage: void,
        peak_process_count: void,
        bootstrap_process_count: void,
        page_size: u32,
    };

    memory_usage: units.Bytes(u64),
    kernel_memory_usage: units.Bytes(u64),
    peak_process_count: u64,
    bootstrap_process_count: u64,
    page_size: u64,
};

pub const KernelOperation = union(Type) {
    pub const Type = enum(u32) {
        // TODO
    };
};
