// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afu = @import("../afu.zig");

/// See https://www.3dbrew.org/wiki/Multi-threading#enum_ArbitrationType
pub const ArbitrationType = enum(u32) {
    signal = 0,
    wait_if_less_than = 1,
    decrement_and_wait_if_less_than = 2,
    wait_if_less_than_timeout = 3,
    decrement_and_wait_if_less_than_timeout = 4,
};

pub const BreakType = enum(u32) {
    panic = 0,
    assert = 1,
    user = 2,
    load_ro = 3,
    unload_ro = 4,
};

pub const CodeSet = opaque {
    /// See https://www.3dbrew.org/wiki/Multi-threading#struct_CodeSetInfo
    /// See libctru include/3ds/svc.h struct CodeSetHeader
    pub const Info = extern struct {
        pub const Section = extern struct {
            /// Section start address
            addr: usize,
            /// Size of section in pages
            size: usize,
        };
        /// Codeset name, ASCII-encoded
        name: [8]u8,
        version: u16,
        _1: [6]u8,

        text: Section,
        ro: Section,
        rw: Section,
        /// Total page count for .text
        text_total_size: usize,
        /// Total page count for .rodata
        ro_total_size: usize,
        /// Total page count for .data and .bss
        rw_total_size: usize,

        _2: usize,
        /// Title ID
        total_id: afu.os.TitleID,

        comptime {
            std.debug.assert(@offsetOf(Info, "text") == 0x10);
        }
    };
};

// https://github.com/devkitPro/libctru/blob/a88b5af7a84d994fa24e258ce9dbf34e009c2c7b/libctru/include/3ds/svc.h#L117
pub const LimitableResource = union(Type) {
    pub const Type = enum(u32) {
        priority = 0,
        commit = 1,
        thread = 2,
        event = 3,
        mutex = 4,
        semaphore = 5,
        timer = 6,
        shared_memory = 7,
        address_arbiter = 8,
        cpu_time = 9,
    };
    pub const RawValue = u64;

    priority: u64,
    commit: u64,
    thread: u64,
    event: u64,
    mutex: u64,
    semaphore: u64,
    timer: u64,
    shared_memory: u64,
    address_arbiter: u64,
    cpu_time: u64,
};

pub const ResourceLimits = opaque {};

pub const Process = opaque {
    pub const Id = enum(u32) { _ };
    pub const Name = []const u8;
    pub const StartupInfo = extern struct {
        priority: i32,
        stack_size: usize,
        argc: u32,
        argv: [*]const [*:0]const u16,
        envp: [*:null]const ?[*:0]const u16,
    };
    /// See https://3dbrew.org/wiki/SVC#GetProcessInfo
    pub const Info = union(Type) {
        pub const Type = enum(u32) {
            resident_set_size = 0,
            unique_set_size = 2,
            handle_count = 4,
            peak_handle_count = 5,
            thread_count = 7,
            peak_thread_count = 8,
            memory_region = 19,
            linear_physical_offset = 20,
            qtm_block_physical_offset = 21,
            qtm_block_virtual_address = 22,
            qtm_block_size = 23,
        };

        resident_set_size: u64,
        unique_set_size: u64,
        handle_count: u64,
        peak_handle_count: u64,
        thread_count: u64,
        peak_thread_count: u64,
        memory_region: u64,
        linear_physical_offset: u64,
        qtm_block_physical_offset: u64,
        qtm_block_virtual_address: u64,
        qtm_block_size: u64,
    };
};

pub const Thread = opaque {
    pub const Id = enum(u32) { _ };
    pub const Func = *const fn () void;
    pub const Info = union(Type) {
        pub const Type = enum(u32) {
            // TODO find what these types are, the 3DS kernel doesn't use them
        };
    };
    pub const Priority = i32;
    pub const State = enum(u8) {
        wait = 0,
        runnable = 1,
        joined = 2,
    };
};

pub const Core = opaque {
    pub const Id = enum(i32) {
        none = -1,
        process_defaults = -2,
        _,
    };
};

/// https://www.3dbrew.org/wiki/NCCH/Extended_Header#ARM11_Kernel_Capabilities
pub const Capability = extern union {
    raw: u32,
    // interrupt: packed struct(u32) {
    //     0b1110xxxxxxxx  Interrupt info
    // },
    // syscall: packed struct(u32) {
    //     0b11110xxxxxxx  System call mask  Bits 24-26: System call mask table index; Bits 0-23: mask
    // },
    // kernel_version: packed struct(u32) {
    //     0b1111110xxxxx  Kernel release version  Bits 8-15: Major version; Bits 0-7: Minor version
    // },
    // handle_table_size: packed struct(u32) {
    //     0b11111110xxxx  Handle table size  Bits 0-18: size
    // },
    // kernel_flags: packed struct(u32) {
    //     0b111111110xxx  Kernel flags  See below
    // },
    // 0b11111111100x  Map IO/static address range  Describes a memory mapping like the 0b111111111110 descriptor, but an entire range rather than a single page is mapped. Another 0b11111111100x descriptor must follow this one to denote the (exclusive) end of the address range to map. Bit20 on the first descriptor: map read-only (otherwise RW), bit20 on the second descriptor: map static (cacheable, otherwise IO if the bit is not set)
    // 0b111111111110  Map IO memory page  Bits 0-19: page index to map (virtual address >> 12; the physical address is determined per-page according to Memory layout); Bit 20: Map read-only (otherwise read-write)
};
