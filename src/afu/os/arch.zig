// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const builtin = @import("builtin");

pub const desc = @import("arch/desc.zig");

pub const arm = @import("arch/arm.zig");
pub const @"linux-translated" = @import("arch/linux-translated.zig");
pub const x86_64 = @import("arch/x86_64.zig");

pub const target = if (builtin.target.os.tag == .linux) @"linux-translated" else switch (builtin.cpu.arch) {
    .arm => arm,
    .x86_64 => x86_64,
    else => |arch| @compileError("Unsupported architecture " ++ @tagName(arch)),
};
