// SPDX-License-Identifier: EUPL-1.2

pub const Interrupt = opaque {
    pub const Index = enum(u32) { _ };
    pub const Priority = i32;
};
