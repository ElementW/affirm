// SPDX-License-Identifier: EUPL-1.2

/// https://www.3dbrew.org/wiki/Corelink_DMA_Engines
pub const DMA = opaque {
    pub const Flags = packed struct(u8) {
        _: u8,
    };
    pub const Config = extern struct {
        pub const EndianSwapSize = enum(u8) {
            none = 0,
        };
        channel: i8,
        endian_swap_size: EndianSwapSize,
        flags: Flags,
        _: u8,
        src_config: SubConfig,
        dst_config: SubConfig,
    };
    pub const SubConfig = extern struct {
        peripheral_id: i8,
        allowed_burst_sizes: u8,
        gather_granule_size: i16,
        gather_stride: i16,
        scatter_granule_size: i16,
        scatter_stride: i16,
    };
    pub const State = extern struct {
        // TODO
    };
};

/// https://www.3dbrew.org/wiki/Memory_Management#struct_MemoryInfo
pub const MemoryInfo = extern struct {
    base_process_va: usize,
    size: usize,
    permissions: MemoryPermission,
    state: MemoryState,
};

pub const MemoryRegion = enum(u8) {
    all = 0,
    app = 1,
    system = 2,
    base = 3,
};

/// https://www.3dbrew.org/wiki/Memory_Management#enum_MemoryOperation
pub const MemoryOperation = packed struct(u32) {
    pub const Operation = enum(u8) {
        none = 0,
        free = 1,
        reserve = 2,
        commit = 3,
        map = 4,
        unmap = 5,
        protect = 6,
    };
    operation: Operation,
    region: MemoryRegion,
    linear: bool,
    _: u15,
};

/// https://www.3dbrew.org/wiki/Memory_Management#enum_MemoryPermission
pub const MemoryPermission = enum(u32) {
    none = 0,
    r = 1,
    w = 2,
    rw = 3,
    x = 4,
    rx = 5,
    wx = 6,
    rwx = 7,

    dont_care = 0x10000000,
};

/// https://www.3dbrew.org/wiki/Memory_Management#enum_MemoryState
pub const MemoryState = enum(u32) {
    free = 0,
    reserved = 1,
    io = 2,
    static = 3,
    code = 4,
    private = 5,
    shared = 6,
    continuous = 7,
    aliased = 8,
    alias = 9,
    alias_code = 10,
    locked = 11,
};

/// https://www.3dbrew.org/wiki/Memory_Management#enum_PageFlags
pub const PageFlags = packed struct(u32) {
    _: u30,
    changed: bool,
    locked: bool,
};

/// https://www.3dbrew.org/wiki/Memory_Management#struct_PageInfo
pub const PageInfo = extern struct {
    flags: PageFlags,
};

pub const MemoryQuery = extern struct {
    memory_info: MemoryInfo,
    page_info: PageInfo,
};

pub const MemoryBlock = opaque {};
