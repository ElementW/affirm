// SPDX-License-Identifier: EUPL-1.2
pub const dbg = @import("arm/dbg.zig");
pub const syscall = @import("arm/syscall.zig");
