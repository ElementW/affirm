// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afu = @import("../../../afu.zig");

const ipc = afu.os.ipc;
const mm = afu.os.mm;
const pm = afu.os.pm;
const results = afu.os.results;
const syscall = afu.os.syscall;

const Handle = afu.Handle;
const Result = afu.Result;

const LinuxProcess = struct {
    pub const Id = std.os.linux.pid_t;
    id: Id,
};

const LinuxHandleContentsTag = enum {
    process,

    pub fn of(comptime Contents: type) ?@This() {
        return switch (Contents) {
            LinuxProcess => .process,
            else => null,
        };
    }
};
const LinuxHandleContents = union(LinuxHandleContentsTag) {
    process: LinuxProcess,
};

var kernel_socket: std.os.linux.fd_t = -1;
var handles: std.AutoHashMap(Handle, LinuxHandleContents) = undefined;
var max_cpu: u32 = undefined;
var self_pid: std.os.linux.pid_t = undefined;
threadlocal var self_tid: std.os.linux.pid_t = -1;
fn init() void {
    const possible_fd = std.os.linux.open("/sys/devices/system/cpu/possible", .{ .ACCMODE = .RDONLY }, 0);
    if (possible_fd < 0) {
        @panic("Failed to open /sys/devices/system/cpu/possible");
    }
    var buf: [32]u8 = undefined;
    const size = std.os.linux.read(possible_fd, &buf, @sizeOf(buf));
    max_cpu = std.fmt.parseInt(u32, buf[0..size], 10) catch unreachable;
    std.os.linux.close(possible_fd);

    self_pid = std.os.linux.getpid();
    initThread(self_pid);
}
fn initThread(tid: std.os.linux.pid_t) void {
    self_tid = tid;
}
// socket(AF_UNIX, SOCK_DGRAM, 0);

fn resolveHandle(comptime Contents: type, handle: Handle) ?*Contents {
    const contents = handles.getPtr(handle) orelse return null;
    if (contents != LinuxHandleContentsTag.of(Contents)) {
        return null;
    }
    return &@field(contents, @tagName(Contents.HandleContentsTag));
}

const ControlMemory = struct {
    const Exchange = extern union {
        pub const Request = struct {
            op: mm.MemoryOperation,
            addr0: ?*anyopaque,
            addr1: ?*anyopaque,
            size: usize,
            permissions: mm.MemoryPermission,
        };
        pub const Response = Result.With(?*anyopaque);
        request: Request,
        response: Response,
    };
    inline fn func(op: mm.MemoryOperation, addr0: ?*anyopaque, addr1: ?*anyopaque, size: usize, permissions: mm.MemoryPermission) Result.With(?*anyopaque) {
        const exchange: Exchange = .{
            .request = .{
                .op = op,
                .addr0 = addr0,
                .addr1 = addr1,
                .size = size,
                .permissions = permissions,
            },
        };
        _ = exchange;
        @panic("NYI");
        // return exchange.response;
    }
};

const QueryMemory = struct {
    inline fn func(addr: ?*anyopaque) Result.With(mm.MemoryQuery) {
        _ = addr;
        @panic("NYI");
    }
};

const ExitProcess = struct {
    inline fn func() noreturn {
        std.os.exit(0);
    }
};

fn getProcessAffinityMask(process_handle: Handle.Of(pm.Process), affinity_mask: [*]u8, processor_count: i32) Result {
    if (processor_count > max_cpu) {
        return results.OutOfRange;
    }
    const process = resolveHandle(LinuxProcess, process_handle) orelse return results.InvalidHandle;
    _ = process;
    _ = affinity_mask;
    @panic("NYI");
    // return Result.success();
}

fn setProcessAffinityMask(process_handle: Handle.Of(pm.Process), affinity_mask: [*]const u8, processor_count: i32) Result {
    if (processor_count > max_cpu) {
        return results.OutOfRange;
    }
    const process = resolveHandle(LinuxProcess, process_handle) orelse return results.InvalidHandle;
    _ = process;
    _ = affinity_mask;
    @panic("NYI");
    // return Result.success();
}

//syscall(GetProcessIdealProcessor, int32 *idealprocessor, Handle process);
//
//syscall(SetProcessIdealProcessor, Handle process, int32 idealprocessor);
//
//syscall(CreateThread, Handle *thread, func entrypoint, uint32 arg, uint32 stacktop, int32 threadpriority, int32 processorid);

const ExitThread = struct {
    inline fn func() noreturn {
        std.os.linux.exit(0);
    }
};

const SleepThread = struct {
    inline fn func(nanoseconds: i64) noreturn {
        std.os.nanosleep(nanoseconds / 1000000000, nanoseconds % 1000000000);
    }
};

//syscall(GetThreadPriority, int32 *priority, Handle thread);
//
//syscall(SetThreadPriority, Handle thread, int32 priority);
//
//syscall(GetThreadAffinityMask, uint8 *affinitymask, Handle thread, int32 processorcount);
//
//syscall(SetThreadAffinityMask, Handle thread, uint8 *affinitymask, int32 processorcount);
//
//syscall(GetThreadIdealProcessor, int32 *processorid, Handle thread);
//
//syscall(SetThreadIdealProcessor, Handle thread, int32 processorid);
//
fn getCurrentProcessorID() u32 {
    return @truncate(std.os.linux.syscall0(.getcpu));
}
//
//syscall(Run, Handle process, pm::StartupInfo *info);
//
//syscall(CreateMutex, Handle *mutex, bool initialLocked);
//
//syscall(ReleaseMutex, Handle mutex);
//
//syscall(CreateSemaphore, Handle *semaphore, int32 initialCount, int32 maxCount);
//
//syscall(ReleaseSemaphore, int32 *count, Handle semaphore, int32 releaseCount);
//
//syscall(CreateEvent, Handle *event, pm::ResetType resettype);
//
fn signalEvent(handle: Handle) Result {
    _ = handle;
    @panic("NYI");
}

fn clearEvent(handle: Handle) Result {
    _ = handle;
    @panic("NYI");
}

//syscall(CreateTimer, Handle *timer, pm::ResetType resettype);
//
//syscall(SetTimer, Handle timer, int64 initial_nanoseconds, int64 interval);
//

fn cancelTimer(handle: Handle) Result {
    _ = handle;
    @panic("NYI");
}

fn clearTimer(handle: Handle) Result {
    _ = handle;
    @panic("NYI");
}

//syscall(CreateMemoryBlock, Handle *memblock, uint32 addr, uint32 size,
//    mm::MemoryPermission mypermission, mm::MemoryPermission otherpermission);
//
//syscall(MapMemoryBlock, Handle memblock, uint32 addr, mm::MemoryPermission mypermissions,
//    mm::MemoryPermission otherpermission);
//
//syscall(UnmapMemoryBlock, Handle memblock, uint32 addr);
//
//syscall(CreateAddressArbiter, Handle *arbiter);
//
//syscall(ArbitrateAddress, Handle arbiter, uint32 addr, pm::ArbitrationType type, int32 value,
//    int64 nanoseconds);
//
fn closeHandle(handle: Handle) Result {
    _ = handle;
    @panic("NYI");
}
//syscall(WaitSynchronization1, Handle handle, int64 timeout_nanoseconds);
//
//syscall(WaitSynchronizationN, int32 *out, Handle *handles, int32 handlecount, bool waitAll,
//    int64 timeoutNanoseconds);
//
//syscall(SignalAndWait, int32 *out, Handle signal, Handle *handles, int32 handleCount, bool waitAll,
//    int64 nanoseconds);
//
//syscall(DuplicateHandle, Handle *out, Handle original);
fn duplicateHandle(handle: Handle) Result.With(Handle) {
    _ = handle;
    @panic("NYI");
}

fn getSystemTick() u64 {
    @panic("NYI");
}
//syscall(GetHandleInfo, int64 *out, Handle handle, HandleInfoType type);
//
//syscall(GetSystemInfo, int64 *out, SystemInfoType type, int32 param);
//
//syscall(GetProcessInfo, int64 *out, Handle process, pm::ProcessInfoType type);
//
//syscall(GetThreadInfo,int64 *out, Handle thread, pm::ThreadInfoType type);*/
//
//syscall(ConnectToPort, Handle *out, const char *portName);
//
fn sendSyncRequest1(session_handle: Handle.Of(ipc.Session)) Result {
    _ = session_handle;
    @panic("NYI");
}
fn sendSyncRequest2(session_handle: Handle.Of(ipc.Session)) Result {
    _ = session_handle;
    @panic("NYI");
}
fn sendSyncRequest3(session_handle: Handle.Of(ipc.Session)) Result {
    _ = session_handle;
    @panic("NYI");
}
fn sendSyncRequest4(session_handle: Handle.Of(ipc.Session)) Result {
    _ = session_handle;
    @panic("NYI");
}
fn sendSyncRequest(session_handle: Handle.Of(ipc.Session)) Result {
    _ = session_handle;
    @panic("NYI");
}

fn openProcess(process: Handle.Of(pm.Process), process_id: pm.Process.Id) Result {
    _ = process;
    _ = process_id;
    // TODO
    @panic("NYI");
}
fn openThread(process_id: pm.Process.Id, thread_id: pm.Thread.Id) Result.With(Handle.Of(pm.Thread)) {
    _ = process_id;
    _ = thread_id;
    // TODO
    @panic("NYI");
}
//syscall(GetProcessId, uint32 *processId, Handle process);
//
//syscall(GetProcessIdOfThread, uint32 *processId, Handle thread);
//
//syscall(GetThreadId, uint32 *threadId, Handle thread);
//
//syscall(GetResourceLimit, Handle *resourceLimit, Handle process);
//
//syscall(GetResourceLimitLimitValues, int64 *values, Handle resourceLimit, pm::ResourceLimitType *names,
//    int32 nameCount);
//
//syscall(GetResourceLimitCurrentValues, int64 *values, Handle resourceLimit, pm::ResourceLimitType *names,
//    int32 nameCount);
//
////syscall(GetThreadContext, ThreadContext *context, Handle thread);
//
//syscall(Break, pm::BreakType type, const void *croInfo, uint32 croInfoSize);
//
const OutputDebugString = struct {
    inline fn func(string: []const u8) Result {
        std.os.write(std.os.linux.STDOUT_FILENO, string) catch |err| return switch (err) {
            .InputOutput => Result.failure(.temporary, .invalid_state, .os, .cancel_requested),
            .SystemResources => Result.failure(.status, .out_of_resource, .os, .out_of_memory),
            else => unreachable,
        };
        return Result.success();
    }
};
//syscall(ControlPerformanceCounter, unsigned long long, int, unsigned int, unsigned long long);
//
//syscall(CreatePort, Handle *portServer, Handle *portClient, const char *name, int16 maxSessions);
//
//syscall(CreateSessionToPort, Handle *session, Handle port);
//
//syscall(CreateSession, Handle *sessionServer, Handle *sessionClient);
//
//syscall(AcceptSession, Handle *session, Handle port);
//
//syscall(ReplyAndReceive1, int32 *index, Handle *handles, int32 handleCount, Handle replyTarget);
//
//syscall(ReplyAndReceive2, int32 *index, Handle *handles, int32 handleCount, Handle replyTarget);
//
//syscall(ReplyAndReceive3, int32 *index, Handle *handles, int32 handleCount, Handle replyTarget);
//
//syscall(ReplyAndReceive4, int32 *index, Handle *handles, int32 handleCount, Handle replyTarget);
//
//syscall(ReplyAndReceive, int32 *index, Handle *handles, int32 handleCount, Handle replyTarget);
//
//*syscall(BindInterrupt, Interrupt name, Handle eventOrSemaphore, int32 priority, bool isLevelHighActive);
//
//syscall(UnbindInterrupt, Interrupt name, Handle eventOrSemaphore);*/
//
//syscall(InvalidateProcessDataCache, Handle process, void *addr, uint32 size);
//
//syscall(StoreProcessDataCache, Handle process, void const *addr, uint32 size);
//
//syscall(FlushProcessDataCache, Handle process, void const *addr, uint32 size);
//
////syscall(StartInterProcessDma, Handle *dma, Handle dstProcess, void *dst, Handle srcProcess, const void *src, uint32 size, const DmaConfig *config);
//
//syscall(StopDma, Handle dma);
//
////syscall(GetDmaState, DmaState *state, Handle dma);
//
//syscall(RestartDma, Handle, void*, void const*, unsigned int, signed char);
//
//syscall(SetGpuProt, int8 flag);
//
//syscall(SetWifiEnabled, int8 flag);
//
//syscall(DebugActiveProcess, Handle *debug, uint32 processID);
//
//syscall(BreakDebugProcess, Handle debug);
//
//syscall(TerminateDebugProcess, Handle debug);
//
////syscall(GetProcessDebugEvent, DebugEventInfo *info, Handle debug);
//
//syscall(ContinueDebugEvent, Handle debug, uint32 flags);
//
//syscall(GetProcessList, int32 *processCount, uint32 *processIds, int32 processIdMaxCount);
//
//syscall(GetThreadList, int32 *threadCount, uint32 *threadIds, int32 threadIdMaxCount, Handle domain);
//
//*syscall(GetDebugThreadContext, ThreadContext *context, Handle debug, uint32 threadId, uint32 controlFlags);
//
//syscall(SetDebugThreadContext, Handle debug, uint32 threadId, const ThreadContext *context, uint32 controlFlags);
//
//syscall(QueryDebugProcessMemory, MemoryInfo *blockInfo, PageInfo *pageInfo, Handle debug, uint32 addr);*/
//
//syscall(ReadProcessMemory, void *buffer, Handle debug, uint32 addr, uint32 size);
//
//syscall(WriteProcessMemory, Handle debug, void const *buffer, uint32 addr, uint32 size);
//
//syscall(SetHardwareBreakPoint, int32 registerId, uint32 control, uint32 value);
//
////syscall(GetDebugThreadParam, int64 *unused, uint32 *out, Handle kdebug, uint32 threadId, DebugThreadParameter param);
//
//syscall(ControlProcessMemory, Handle KProcess, unsigned int Addr0, unsigned int Addr1, unsigned int Size, unsigned int Type, unsigned int Permissions);
//
//syscall(MapProcessMemory, Handle process, uint32 startAddr, uint32 size);
//
//syscall(UnmapProcessMemory, Handle process, uint32 startAddr, uint32 size);
//
//syscall(CreateCodeSet, Handle *handle_out, struct CodeSetInfo, uint32 code_ptr, uint32 ro_ptr, uint32 data_ptr);
//
//syscall(RandomStub, void);
//
//syscall(CreateProcess, Handle *handle_out, Handle codeset_handle, uint32 arm11kernelcaps_ptr, uint32 arm11kernelcaps_num);
//
//syscall(TerminateProcess, Handle);
//
//syscall(SetProcessResourceLimits, Handle KProcess, Handle KResourceLimit);
//
//syscall(CreateResourceLimit, Handle *KResourceLimit);
//
////syscall(SetResourceLimitValues, Handle res_limit, LimitableResource *resource_type_list, int64 *resource_list, uint32 count);
//
//syscall(AddCodeSegment, unsigned int Addr, unsigned int Size);
//
//syscall(Backdoor, unsigned int CodeAddress);
//
//syscall(KernelOperation, unsigned int Type, ...);
//
////syscall(QueryProcessMemory, MemInfo *Info, unsigned int *Out, Handle KProcess, unsigned int Addr);

pub const interface = syscall.Interface{
    .ControlMemory = ControlMemory.func,
    .QueryMemory = QueryMemory.func,
    .ExitProcess = ExitProcess.func,
    .ExitThread = ExitThread.func,
    .OutputDebugString = OutputDebugString.func,
};
