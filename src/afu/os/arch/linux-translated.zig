// SPDX-License-Identifier: EUPL-1.2
pub const dbg = struct {
    pub const BreakRegisterId = enum(u32) { _ };
    pub const ThreadContext = extern struct {
        pub const ControlFlags = u32;
    };
};
pub const syscalls = @import("linux-translated/syscalls.zig");
