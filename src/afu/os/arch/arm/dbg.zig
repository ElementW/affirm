// SPDX-License-Identifier: EUPL-1.2
pub const BreakRegisterId = enum(u32) {
    brp0 = 0,
    brp1 = 1,
    brp2 = 2,
    brp3 = 3,
    brp4 = 4,
    brp5 = 5,
    wrp0 = 0x100,
    wrp1 = 0x101,
};

pub const ThreadContext = extern struct {
    pub const ControlFlags = packed struct(u32) {
        pub const Architecture = enum(u12) { Thumb = 0, ARM = 1 };
        registers: bool,
        control: bool,
        fpu_registers: bool,
        fpu_control: bool,
        _: u16,
        architecture: Architecture,
    };

    registers: [13]u32,
    sp: u32,
    lr: u32,
    pc: u32,
    cpsr: u32,
    fpu_registers: [32]u32,
    fpscr: u32,
    fpexc: u32,
};
