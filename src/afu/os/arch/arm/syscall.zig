// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afu = @import("../../../afu.zig");

const dbg = afu.os.dbg;
const hw = afu.os.hw;
const ipc = afu.os.ipc;
const mm = afu.os.mm;
const pm = afu.os.pm;
const sync = afu.os.sync;
const sys = afu.os.sys;

const results = afu.os.results;

const Handle = afu.Handle;
const Result = afu.Result;

const Location = union {
    pub const Register = enum(u4) {
        r0 = 0,
        r1 = 1,
        r2 = 2,
        r3 = 3,
        r4 = 4,
        r5 = 5,
        r6 = 6,
        r7 = 7,
        r8 = 8,
        r9 = 9,
        r10 = 10,
        r11 = 11,
        r12 = 12,
    };
    pub const Stack = struct {
        offset: u8,
    };
    register: Register,
    stack: Stack,

    pub const r0 = @This(){ .register = .r0 };
    pub const r1 = @This(){ .register = .r1 };
    pub const r2 = @This(){ .register = .r2 };
    pub const r3 = @This(){ .register = .r3 };
    pub const r4 = @This(){ .register = .r4 };
    pub const r5 = @This(){ .register = .r5 };
    pub const r6 = @This(){ .register = .r6 };
    pub const r7 = @This(){ .register = .r7 };
    pub const r8 = @This(){ .register = .r8 };
    pub const r9 = @This(){ .register = .r9 };
    pub const r10 = @This(){ .register = .r10 };
    pub const r11 = @This(){ .register = .r11 };
    pub const r12 = @This(){ .register = .r12 };
    pub fn pc(comptime offset: u8) @This() {
        return .{ .stack = .{ .offset = offset } };
    }
};
pub const Description = afu.os.arch.desc.Description(Location);
const in = Description.LocDesc.in;
const out = Description.LocDesc.out;
const ignored = Description.LocDesc.ignored;
const syscall = Description.syscall;
const stubbed = Description.stubbed;

pub const ControlMemory = struct {
    pub const desc = syscall(0x01, .ControlMemory, &.{
        in(.r0, mm.MemoryOperation, "op"),
        in(.r1, ?*anyopaque, "addr0"),
        in(.r2, ?*anyopaque, "addr1"),
        in(.r3, u32, "size"),
        in(.r4, mm.MemoryPermission, "perm"),
        out(.r0, Result, "result"),
        out(.r1, ?*anyopaque, "addr_out"),
    });
    pub inline fn func(op: mm.MemoryOperation, addr0: ?*anyopaque, addr1: ?*anyopaque, size: usize, permissions: mm.MemoryPermission) Result.With(?*anyopaque) {
        var out_result: Result = undefined;
        var out_addr: ?*anyopaque = undefined;
        asm volatile (
            \\svc %c[svcidx]
            \\str r1, %[addrOut]
            : [out_result] "={r0}" (out_result),
              [out_addr] "={r1}" (out_addr),
            : [svcidx] "n" (desc.index),
              [op] "{r0}" (op),
              [addr0] "{r1}" (addr0),
              [addr1] "{r2}" (addr1),
              [size] "{r3}" (size),
              [permissions] "{r4}" (permissions),
        );
        return .{ .result = out_result, .maybe_value = out_addr };
    }
};

pub const QueryMemory = struct {
    pub const desc = syscall(0x02, .QueryMemory, &.{
        in(.r0, ?*anyopaque, "addr"),
        out(.r0, Result, "result"),
        out(.r1, u32, "base_process_va"),
        out(.r2, u32, "size"),
        out(.r3, mm.MemoryPermission, "permissions"),
        out(.r4, mm.MemoryState, "state"),
        out(.r5, mm.PageFlags, "page_flags"),
    });
    pub inline fn func(addr: ?*anyopaque) Result.With(mm.MemoryQuery) {
        var out_result: Result = undefined;
        var out_base_process_va: u32 = undefined;
        var out_size: u32 = undefined;
        var out_permissions: mm.MemoryPermission = undefined;
        var out_state: mm.MemoryState = undefined;
        var out_page_flags: mm.PageFlags = undefined;
        asm volatile (
            \\svc %c[svcidx]
            : [out_result] "={r0}" (out_result),
              [out_base_process_va] "={r1}" (out_base_process_va),
              [out_size] "={r2}" (out_size),
              [out_permissions] "={r3}" (out_permissions),
              [out_state] "={r4}" (out_state),
              [out_page_flags] "={r5}" (out_page_flags),
            : [svcidx] "n" (desc.index),
              [addr] "{r0}" (addr),
        );
        return .{ .result = out_result, .maybe_value = .{
            .memory_info = .{
                .base_process_va = out_base_process_va,
                .size = out_size,
                .permissions = out_permissions,
                .state = out_state,
            },
            .page_info = .{
                .flags = out_page_flags,
            },
        } };
    }
};

pub const ExitProcess = struct {
    pub const desc = syscall(0x03, .ExitProcess, &.{});
    pub inline fn func() noreturn {
        _ = asm volatile (
            \\svc %c[svcidx]
            :
            : [svcidx] "n" (desc.index),
        );
    }
};

pub const GetProcessAffinityMask = struct {
    pub const desc = syscall(0x04, .GetProcessAffinityMask, &.{
        in(.r0, [*]u8, "affinity_mask"),
        in(.r1, Handle.Of(pm.Process), "process_handle"),
        in(.r2, i32, "processor_count"),
        out(.r0, Result, "result"),
    });
    pub inline fn func(process_handle: Handle.Of(pm.Process), affinity_mask: [*]u8, processor_count: i32) Result {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> Result),
            : [svcidx] "n" (desc.index),
              [affinity_mask] "{r0}" (affinity_mask),
              [process_handle] "{r1}" (process_handle),
              [processor_count] "{r2}" (processor_count),
        );
    }
};

pub const SetProcessAffinityMask = struct {
    pub const desc = syscall(0x05, .SetProcessAffinityMask, &.{
        in(.r0, Handle.Of(pm.Process), "process_handle"),
        in(.r1, [*]const u8, "affinity_mask"),
        in(.r2, i32, "processor_count"),
        out(.r0, Result, "result"),
    });
    pub inline fn func(process_handle: Handle.Of(pm.Process), affinity_mask: [*]const u8, processor_count: i32) Result {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> Result),
            : [svcidx] "n" (desc.index),
              [process_handle] "{r0}" (process_handle),
              [affinity_mask] "{r1}" (affinity_mask),
              [processor_count] "{r2}" (processor_count),
        );
    }
};

pub const GetProcessIdealCore = struct {
    pub const desc = syscall(0x06, .GetProcessIdealCore, &.{
        ignored(.r0),
        in(.r1, Handle.Of(pm.Process), "process_handle"),
        out(.r0, Result, "result"),
        out(.r1, i32, "core_id"),
    });
    pub inline fn func(process_handle: Handle.Of(pm.Process)) Result.With(pm.Core.Id) {
        var out_result: Result = undefined;
        var out_core_id: pm.Core.Id = undefined;
        asm volatile (
            \\svc %c[svcidx]
            \\str r1, %[addrOut]
            : [out_result] "={r0}" (out_result),
              [out_core_id] "={r1}" (out_core_id),
            : [svcidx] "n" (desc.index),
              [process_handle] "{r1}" (process_handle),
        );
        return .{ .result = out_result, .maybe_value = out_core_id };
    }
};

pub const SetProcessIdealCore = struct {
    pub const desc = syscall(0x07, .SetProcessIdealCore, &.{
        in(.r0, Handle.Of(pm.Process), "process_handle"),
        in(.r1, i32, "core_id"),
        out(.r0, Result, "result"),
    });
    pub inline fn func(process_handle: Handle.Of(pm.Process), core_id: pm.Core.Id) Result {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> Result),
            : [svcidx] "n" (desc.index),
              [process_handle] "{r0}" (process_handle),
              [core_id] "{r1}" (core_id),
        );
    }
};

pub const CreateThread = struct {
    pub const desc = syscall(0x08, .CreateThread, &.{
        in(.r0, i32, "thread_prio"),
        in(.r1, pm.Thread.Func, "entrypoint"),
        in(.r2, u32, "arg"),
        in(.r3, *anyopaque, "stack_top"),
        in(.r4, i32, "processor_id"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(pm.Thread), "thread_handle"),
    });
    //TODO syscall(CreateThread, Handle *thread, func entrypoint, uint32 arg, uint32 stacktop, int32 threadpriority, int32 processorid);
};

pub const ExitThread = struct {
    pub const desc = syscall(0x09, .ExitThread, &.{});
    pub inline fn func() noreturn {
        _ = asm volatile (
            \\svc %c[svcidx]
            :
            : [svcidx] "n" (desc.index),
        );
    }
};

pub const SleepThread = struct {
    pub const desc = syscall(0x0A, .SleepThread, &.{
        in(.r0, i64, "nanoseconds"),
    });
    //TODO syscall(SleepThread, int64 nanoseconds);
};

pub const GetThreadPriority = struct {
    pub const desc = syscall(0x0B, .GetThreadPriority, &.{
        ignored(.r0),
        in(.r1, Handle.Of(pm.Thread), "thread_handle"),
        out(.r0, Result, "result"),
        out(.r1, pm.Thread.Priority, "thread_priority"),
    });
    //TODO syscall(GetThreadPriority, int32 *priority, Handle thread);
};

pub const SetThreadPriority = struct {
    pub const desc = syscall(0x0C, .SetThreadPriority, &.{
        in(.r0, Handle.Of(pm.Thread), "thread_handle"),
        in(.r1, pm.Thread.Priority, "thread_priority"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(SetThreadPriority, Handle thread, int32 priority);
};

pub const GetThreadAffinityMask = struct {
    pub const desc = syscall(0x0D, .GetThreadAffinityMask, &.{
        in(.r0, [*]u8, "affinity_mask"),
        in(.r1, Handle.Of(pm.Thread), "thread_handle"),
        in(.r2, i32, "processor_count"),
        out(.r0, Result, "result"),
    });
    // TODO pub inline fn func(thread_handle: Handle.Of(pm.Thread), affinity_mask: [*]u8, processor_count: i32) Result {}
};

pub const SetThreadAffinityMask = struct {
    pub const desc = syscall(0x0E, .SetThreadAffinityMask, &.{
        in(.r0, Handle.Of(pm.Thread), "thread_handle"),
        in(.r1, [*]const u8, "affinity_mask"),
        in(.r2, i32, "processor_count"),
        out(.r0, Result, "result"),
    });
    // TODO pub inline fn func(thread_handle: Handle.Of(pm.Thread), affinity_mask: [*]const u8, processor_count: i32) Result {}
};

pub const GetThreadIdealCore = struct {
    pub const desc = syscall(0x0F, .GetThreadIdealCore, &.{
        ignored(.r0),
        in(.r1, Handle.Of(pm.Thread), "thread_handle"),
        out(.r0, Result, "result"),
        out(.r1, i32, "processor_id"),
    });
    //TODO syscall(GetThreadIdealCore, int32 *processorid, Handle thread);
};

pub const SetThreadIdealCore = struct {
    pub const desc = syscall(0x10, .SetThreadIdealCore, &.{
        in(.r0, Handle.Of(pm.Thread), "thread_handle"),
        in(.r1, i32, "processor_id"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(SetThreadIdealCore, Handle thread, int32 processorid);
};

pub const GetCurrentCoreId = struct {
    pub const desc = syscall(0x11, .GetCurrentCoreId, &.{
        out(.r0, i32, "processor_id"),
    });
    //TODO syscall(GetCurrentCoreId, void);
};

pub const Run = struct {
    pub const desc = syscall(0x12, .Run, &.{
        in(.r0, Handle.Of(pm.Process), "process_handle"),
        in(.r1, i32, "priority"),
        in(.r2, u32, "stack_size"),
        in(.r3, i32, "argc"),
        in(.r4, [*][*:0]const u16, "argv"),
        in(.r5, [*:null]const ?[*:0]const u16, "envp"),
        out(.r0, Result, "result"),
    });
    //TODO pub inline fn func(process_handle: Handle.Of(pm.Process), startup_info: pm.Process.StartupInfo) Result {}
};

pub const CreateMutex = struct {
    pub const desc = syscall(0x13, .CreateMutex, &.{
        ignored(.r0),
        in(.r1, bool, "initially_locked"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(sync.Mutex), "mutex_handle"),
    });
    //TODO syscall(CreateMutex, Handle *mutex, bool initialLocked);
};

pub const ReleaseMutex = struct {
    pub const desc = syscall(0x14, .ReleaseMutex, &.{
        in(.r0, Handle.Of(sync.Mutex), "mutex_handle"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(ReleaseMutex, Handle mutex);
};

pub const CreateSemaphore = struct {
    pub const desc = syscall(0x15, .CreateSemaphore, &.{
        ignored(.r0),
        in(.r1, i32, "initial_count"),
        in(.r2, i32, "max_count"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(sync.Semaphore), "semaphore_handle"),
    });
    //TODO syscall(CreateSemaphore, Handle *semaphore, int32 initialCount, int32 maxCount);
};

pub const ReleaseSemaphore = struct {
    pub const desc = syscall(0x16, .ReleaseSemaphore, &.{
        ignored(.r0),
        in(.r1, Handle.Of(sync.Semaphore), "semaphore_handle"),
        in(.r2, i32, "release_count"),
        out(.r0, Result, "result"),
        out(.r1, i32, "current_count"),
    });
    //TODO syscall(ReleaseSemaphore, int32 *count, Handle semaphore, int32 releaseCount);
};

pub const CreateEvent = struct {
    pub const desc = syscall(0x17, .CreateEvent, &.{
        ignored(.r0),
        in(.r1, sync.ResetType, "reset_type"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(sync.Event), "event_handle"),
    });
    //TODO syscall(CreateEvent, Handle *event, pm::ResetType resettype);
};

pub const SignalEvent = struct {
    pub const desc = syscall(0x18, .SignalEvent, &.{
        in(.r0, Handle.Of(sync.Event), "event_handle"),
        out(.r0, Result, "result"),
    });
    pub inline fn func(handle: Handle) Result {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> Result),
            : [svcidx] "n" (desc.index),
              [handle] "{r0}" (handle),
        );
    }
};

pub const ClearEvent = struct {
    pub const desc = syscall(0x19, .ClearEvent, &.{
        in(.r0, Handle.Of(sync.Event), "event_handle"),
        out(.r0, Result, "result"),
    });
    pub inline fn func(handle: Handle) Result {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> Result),
            : [svcidx] "n" (desc.index),
              [handle] "{r0}" (handle),
        );
    }
};

pub const CreateTimer = struct {
    pub const desc = syscall(0x1A, .CreateTimer, &.{
        ignored(.r0),
        in(.r1, sync.ResetType, "reset_type"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(sync.Timer), "timer_handle"),
    });
    //TODO syscall(CreateTimer, Handle *timer, pm::ResetType resettype);
};

pub const SetTimer = struct {
    pub const desc = syscall(0x1B, .SetTimer, &.{
        // 64-bit data types are aligned to even numbered registers as per the AAPCS.
        // The 3DS syscall interface takes a weird approach in an effort to reduce register swaps.
        // Values in nanoseconds.
        in(.r0, Handle.Of(sync.Timer), "timer_handle"),
        in(.r1, u32, "interval_low"),
        in(.r2, i64, "initial"), // Spans on r3
        in(.r4, i32, "interval_high"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(SetTimer, Handle timer, int64 initial_nanoseconds, int64 interval);
};

pub const CancelTimer = struct {
    pub const desc = syscall(0x1C, .CancelTimer, &.{
        in(.r0, Handle.Of(sync.Timer), "timer_handle"),
        out(.r0, Result, "result"),
    });
    pub inline fn func(handle: Handle) Result {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> Result),
            : [svcidx] "n" (desc.index),
              [handle] "{r0}" (handle),
        );
    }
};

pub const ClearTimer = struct {
    pub const desc = syscall(0x1D, .ClearTimer, &.{
        in(.r0, Handle.Of(sync.Timer), "timer_handle"),
        out(.r0, Result, "result"),
    });
    pub inline fn func(handle: Handle) Result {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> Result),
            : [svcidx] "n" (desc.index),
              [handle] "{r0}" (handle),
        );
    }
};

pub const CreateMemoryBlock = struct {
    pub const desc = syscall(0x1E, .CreateMemoryBlock, &.{
        in(.r0, mm.MemoryPermission, "other_perm"),
        in(.r1, *anyopaque, "addr"),
        in(.r2, u32, "size"),
        in(.r3, mm.MemoryPermission, "self_perm"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(mm.MemoryBlock), "memblock_handle"),
    });
    //TODO syscall(CreateMemoryBlock, Handle *memblock, uint32 addr, uint32 size,
    //TODO     mm::MemoryPermission mypermission, mm::MemoryPermission otherpermission);
};

pub const MapMemoryBlock = struct {
    pub const desc = syscall(0x1F, .MapMemoryBlock, &.{
        in(.r0, Handle.Of(mm.MemoryBlock), "memblock_handle"),
        in(.r1, *anyopaque, "addr"),
        in(.r2, mm.MemoryPermission, "self_perm"),
        in(.r3, mm.MemoryPermission, "other_perm"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(MapMemoryBlock, Handle memblock, uint32 addr, mm::MemoryPermission mypermissions,
    //TODO     mm::MemoryPermission otherpermission);
};

pub const UnmapMemoryBlock = struct {
    pub const desc = syscall(0x20, .UnmapMemoryBlock, &.{
        in(.r0, Handle.Of(mm.MemoryBlock), "memblock_handle"),
        in(.r1, *anyopaque, "addr"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(UnmapMemoryBlock, Handle memblock, uint32 addr);
};

pub const CreateAddressArbiter = struct {
    pub const desc = syscall(0x21, .CreateAddressArbiter, &.{
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(sync.AddressArbiter), "arbiter_handle"),
    });
    //TODO syscall(CreateAddressArbiter, Handle *arbiter);
};

pub const ArbitrateAddress = struct {
    pub const desc = syscall(0x22, .ArbitrateAddress, &.{
        in(.r0, Handle.Of(sync.AddressArbiter), "arbiter_handle"),
        in(.r1, *anyopaque, "addr"),
        in(.r2, pm.ArbitrationType, "type"),
        in(.r3, i32, "value"),
        in(.r4, i64, "nanoseconds"), // Spans on r5
        out(.r0, Result, "result"),
    });
    //TODO syscall(ArbitrateAddress, Handle arbiter, uint32 addr, pm::ArbitrationType type, int32 value,
    //TODO     int64 nanoseconds);
};

pub const CloseHandle = struct {
    pub const desc = syscall(0x23, .CloseHandle, &.{
        in(.r0, Handle, "handle"),
        out(.r0, Result, "result"),
    });
    pub inline fn func(handle: Handle) Result {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> Result),
            : [svcidx] "n" (desc.index),
              [handle] "{r0}" (handle),
        );
    }
};

pub const WaitSynchronization = struct {
    pub const desc = syscall(0x24, .WaitSynchronization, &.{
        in(.r0, Handle, "handle"),
        ignored(.r1),
        in(.r2, i64, "timeout_nanoseconds"), // Spans on r3
        out(.r0, Result, "result"),
    });
    //TODO syscall(WaitSynchronization1, Handle handle, int64 timeout_nanoseconds);
};

pub const WaitSynchronizationN = struct {
    pub const desc = syscall(0x25, .WaitSynchronizationN, &.{
        in(.r0, u32, "nanoseconds_low"),
        in(.r1, [*]const Handle, "handles"),
        in(.r2, i32, "handle_count"),
        in(.r3, bool, "wait_all"),
        in(.r4, i32, "nanoseconds_high"),
        out(.r0, Result, "result"),
        out(.r1, i32, "out"),
    });
    //TODO syscall(WaitSynchronizationN, int32 *out, Handle *handles, int32 handlecount, bool waitAll,
    //    int64 timeoutNanoseconds);
};

pub const SignalAndWait = struct {
    // pub const desc = @compileError("TODO");
    //stubbed syscall(0x26, SignalAndWait)
    //  // i32 *out, Handle signal, Handle *handles, i32 handleCount, bool waitAll, int64 nanoseconds
    //});
    //TODO syscall(SignalAndWait, int32 *out, Handle signal, Handle *handles, int32 handleCount, bool waitAll,
    //    int64 nanoseconds);
};

pub const DuplicateHandle = struct {
    pub const desc = syscall(0x27, .DuplicateHandle, &.{
        in(.r0, Handle, "handle"),
        out(.r0, Result, "result"),
        out(.r1, Handle, "duplicated_handle"),
    });
    pub inline fn func(handle: Handle) Result.With(Handle) {
        var out_result: Result = undefined;
        var out_handle: Handle = undefined;
        asm volatile (
            \\svc %c[svcidx]
            : [out_result] "={r0}" (out_result),
              [out_handle] "={r1}" (out_handle),
            : [svcidx] "n" (desc.index),
              [handle] "{r0}" (handle),
        );
        return .{ .result = out_result, .value = .{ .present = out_handle } };
    }
};

pub const GetSystemTick = struct {
    pub const desc = syscall(0x28, .GetSystemTick, &.{
        out(.r0, u64, "ticks"),
    });
    pub inline fn func() u64 {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> u64),
            : [svcidx] "n" (desc.index),
        );
    }
};

pub const GetHandleInfo = struct {
    pub const desc = syscall(0x29, .GetHandleInfo, &.{
        ignored(.r0),
        in(.r1, Handle, "handle"),
        in(.r2, sys.HandleInfo.Type, "info_type"),
        out(.r0, Result, "result"),
        out(.r1, u32, "info0"),
        out(.r2, u32, "info1"),
    });
    //TODO syscall(GetHandleInfo, int64 *out, Handle handle, HandleInfoType type);
};

pub const GetSystemInfo = struct {
    pub const desc = syscall(0x2A, .GetSystemInfo, &.{
        ignored(.r0),
        in(.r1, sys.SystemInfo.Type, "info_type"),
        in(.r2, u32, "param"),
        out(.r0, Result, "result"),
        out(.r1, u32, "info0"),
        out(.r2, u32, "info1"),
    });
    //TODO syscall(GetSystemInfo, int64 *out, SystemInfoType type, int32 param);
};

pub const GetProcessInfo = struct {
    pub const desc = syscall(0x2B, .GetProcessInfo, &.{
        ignored(.r0),
        in(.r1, Handle.Of(pm.Process), "process_handle"),
        in(.r2, pm.Process.Info.Type, "type"),
        out(.r0, Result, "result"),
        out(.r1, i64, "out"), // Spans on r2
    });
    //TODO syscall(GetProcessInfo, int64 *out, Handle process, pm::ProcessInfoType type);
};

pub const GetThreadInfo = struct {
    pub const desc = syscall(0x2C, .GetThreadInfo, &.{
        ignored(.r0),
        in(.r1, Handle.Of(pm.Process), "process_handle"),
        in(.r2, pm.Thread.Info.Type, "type"),
        out(.r0, Result, "result"),
        out(.r1, i64, "out"), // Spans on r2
    });
    //TODO syscall(GetThreadInfo,int64 *out, Handle thread, pm::ThreadInfoType type);*/
};

pub const ConnectToPort = struct {
    pub const desc = syscall(0x2D, .ConnectToPort, &.{
        ignored(.r0),
        in(.r1, [*:0]const u8, "port_name"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(ipc.Port), "port_handle"),
    });
    pub inline fn func(name: []const u8) Result.With(Handle.Of(ipc.Session)) {
        var out_result: Result = undefined;
        var out_handle: Handle.Of(ipc.Session) = undefined;
        asm volatile (
            \\svc %c[svcidx]
            : [out_result] "={r0}" (out_result),
              [out_handle] "={r1}" (out_handle),
            : [svcidx] "n" (desc.index),
              [session_handle] "{r1}" (name.ptr),
        );
        return .{ .result = out_result, .maybe_value = out_handle };
    }
};

pub const SendSyncRequest1 = struct {
    pub const desc = stubbed(0x2E, .SendSyncRequest1, &.{
        in(.r0, Handle.Of(ipc.Session), "session_handle"),
        out(.r0, Result, "result"),
    });
};

pub const SendSyncRequest2 = struct {
    pub const desc = stubbed(0x2F, .SendSyncRequest2, &.{
        in(.r0, Handle.Of(ipc.Session), "session_handle"),
        out(.r0, Result, "result"),
    });
    pub inline fn func(session_handle: Handle.Of(ipc.Session)) Result {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> Result),
            : [svcidx] "n" (desc.index),
              [session_handle] "{r0}" (session_handle),
        );
    }
};

pub const SendSyncRequest3 = struct {
    pub const desc = stubbed(0x30, .SendSyncRequest3, &.{
        in(.r0, Handle.Of(ipc.Session), "session_handle"),
        out(.r0, Result, "result"),
    });
    pub inline fn func(session_handle: Handle.Of(ipc.Session)) Result {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> Result),
            : [svcidx] "n" (desc.index),
              [session_handle] "{r0}" (session_handle),
        );
    }
};

pub const SendSyncRequest4 = struct {
    pub const desc = stubbed(0x31, .SendSyncRequest4, &.{
        in(.r0, Handle.Of(ipc.Session), "session_handle"),
        out(.r0, Result, "result"),
    });
    pub inline fn func(session_handle: Handle.Of(ipc.Session)) Result {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> Result),
            : [svcidx] "n" (desc.index),
              [session_handle] "{r0}" (session_handle),
        );
    }
};

pub const SendSyncRequest = struct {
    pub const desc = syscall(0x32, .SendSyncRequest, &.{
        in(.r0, Handle.Of(ipc.Session), "session_handle"),
        out(.r0, Result, "result"),
    });
    pub inline fn func(session_handle: Handle.Of(ipc.Session)) Result {
        return asm volatile (
            \\svc %c[svcidx]
            : [ret] "={r0}" (-> Result),
            : [svcidx] "n" (desc.index),
              [session_handle] "{r0}" (session_handle),
        );
    }
};

pub const OpenProcess = struct {
    pub const desc = syscall(0x33, .OpenProcess, &.{
        ignored(.r0),
        in(.r1, pm.Process.Id, "process_id"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(pm.Process), "process_handle"),
    });
    pub inline fn func(process_id: pm.Process.Id) Result.With(Handle.Of(pm.Process)) {
        var out_result: Result = undefined;
        var out_handle: Handle.Of(pm.Process) = undefined;
        asm volatile (
            \\svc %c[svcidx]
            : [out_result] "={r0}" (out_result),
              [out_handle] "={r1}" (out_handle),
            : [svcidx] "n" (desc.index),
              [handle] "{r1}" (process_id),
        );
        return .{ .result = out_result, .maybe_value = out_handle };
    }
};

pub const OpenThread = struct {
    pub const desc = syscall(0x34, .OpenThread, &.{
        ignored(.r0),
        in(.r1, pm.Process.Id, "process_id"),
        in(.r1, pm.Thread.Id, "thread_id"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(pm.Thread), "thread_handle"),
    });
    pub inline fn func(process_id: pm.Process.Id, thread_id: pm.Thread.Id) Result.With(Handle.Of(pm.Thread)) {
        _ = process_id;
        _ = thread_id;
        // TODO
        @panic("NYI");
    }
};

pub const GetProcessId = struct {
    pub const desc = syscall(0x35, .GetProcessId, &.{
        ignored(.r0),
        in(.r1, Handle.Of(pm.Process), "process_handle"),
        out(.r0, Result, "result"),
        out(.r1, pm.Process.Id, "process_id"),
    });
    //TODO syscall(GetProcessId, uint32 *processId, Handle process);
};

pub const GetProcessIdOfThread = struct {
    pub const desc = syscall(0x36, .GetProcessIdOfThread, &.{
        ignored(.r0),
        in(.r1, Handle.Of(pm.Thread), "thread_handle"),
        out(.r0, Result, "result"),
        out(.r1, pm.Process.Id, "process_id"),
    });
    //TODO syscall(GetProcessIdOfThread, uint32 *processId, Handle thread);
};

pub const GetThreadId = struct {
    pub const desc = syscall(0x37, .GetThreadId, &.{
        ignored(.r0),
        in(.r1, Handle.Of(pm.Thread), "thread_handle"),
        out(.r0, Result, "result"),
        out(.r1, pm.Thread.Id, "thread_id"),
    });
    //TODO syscall(GetThreadId, uint32 *threadId, Handle thread);
};

pub const GetResourceLimits = struct {
    pub const desc = syscall(0x38, .GetResourceLimits, &.{
        ignored(.r0),
        in(.r1, Handle.Of(pm.Process), "process_handle"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(pm.ResourceLimits), "resource_limits_handle"),
    });
    //TODO syscall(GetResourceLimit, Handle *resourceLimit, Handle process);
};

pub const GetResourceLimitsLimitValues = struct {
    //  pub const desc = syscall(0x39, .GetResourceLimitsLimitValues)
    //  in(.r0, [*]i64, values)
    //  in(.r1, Handle.Of(pm.ResourceLimits), resource_limits_handle)
    //  in(.r2, [*]pm.ResourceLimitsType, types)
    //  in(.r3, i32, type_count)
    //  out(.r0, Result, result)
    //});
    //TODO syscall(GetResourceLimitsLimitValues, int64 *values, Handle resourceLimit, pm::ResourceLimitType *names,
    //    int32 nameCount);
};

pub const GetResourceLimitsCurrentValues = struct {
    //syscall(0x3A, GetResourceLimitsCurrentValues)
    //  in(.r0, [*]64, values)
    //  in(.r1, Handle.Of(pm.ResourceLimits), resource_limits_handle)
    //  in(.r2, [*]pm.ResourceLimitType, types)
    //  in(.r3, i32, type_count)
    //  out(.r0, Result, result)
    //});
    //TODO syscall(GetResourceLimitsCurrentValues, int64 *values, Handle resourceLimit, pm::ResourceLimitType *names,
    //    int32 nameCount);
};

pub const GetThreadContext = struct {
    //stubbed syscall(0x3B, GetThreadContext)
    //  in(.r0, dbg.ThreadContext*, context)
    //  in(.r1, Handle.Of(pm.Thread), thread_handle)
    //  out(.r0, Result, result)
    //});
    //TODO syscall(GetThreadContext, ThreadContext *context, Handle thread);
};

pub const Break = struct {
    pub const desc = syscall(0x3C, .Break, &.{
        in(.r0, pm.BreakType, "type"),
        in(.r1, *const anyopaque, "data"),
        in(.r2, usize, "length"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(Break, pm::BreakType type, const void *croInfo, uint32 croInfoSize);
};

pub const OutputDebugString = struct {
    pub const desc = syscall(0x3D, .OutputDebugString, &.{
        in(.r0, [*]const u8, "str"),
        in(.r1, i32, "length"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(OutputDebugString, const void*, int);
};

pub const ControlPerformanceCounter = struct {
    pub const desc = syscall(0x3E, .ControlPerformanceCounter, &.{
        in(.r0, u32, "param0_high"),
        in(.r1, dbg.PerformanceCounter.Operation, "operation"),
        in(.r3, u32, "param1"),
        in(.r3, u32, "param0_low"),
        out(.r0, Result, "result"),
        out(.r1, u64, "output"), // Spans on r2
    });
    //TODO syscall(ControlPerformanceCounter, unsigned long long, int, unsigned int, unsigned long long);
};

pub const CreatePort = struct {
    pub const desc = syscall(0x47, .CreatePort, &.{
        ignored(.r0),
        ignored(.r1),
        in(.r2, [*]const u8, "port_name"),
        in(.r3, i16, "max_sessions"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(ipc.Port), "server_port_handle"),
        out(.r2, Handle.Of(ipc.Port), "client_port_handle"),
    });
    //TODO syscall(CreatePort, Handle *portServer, Handle *portClient, const char *name, int16 maxSessions);
};

pub const CreateSessionToPort = struct {
    pub const desc = syscall(0x48, .CreateSessionToPort, &.{
        ignored(.r0),
        in(.r1, Handle.Of(ipc.Port), "client_port_handle"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(ipc.Session), "client_session_handle"),
    });
    //TODO syscall(CreateSessionToPort, Handle *session, Handle port);
};

pub const CreateSession = struct {
    pub const desc = syscall(0x49, .CreateSession, &.{
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(ipc.Session), "server_session_handle"),
        out(.r2, Handle.Of(ipc.Session), "client_session_handle"),
    });
    //TODO syscall(CreateSession, Handle *sessionServer, Handle *sessionClient);
};

pub const AcceptSession = struct {
    pub const desc = syscall(0x4A, .AcceptSession, &.{
        ignored(.r0),
        in(.r1, Handle.Of(ipc.Port), "port_handle"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(ipc.Session), "session_handle"),
    });
    //TODO syscall(AcceptSession, Handle *session, Handle port);
};

pub const ReplyAndReceive1 = struct {
    pub const desc = stubbed(0x4B, .ReplyAndReceive1, &.{
        ignored(.r0),
        in(.r1, [*]const Handle, "handles"),
        in(.r2, i32, "handle_count"),
        in(.r3, Handle, "reply_target"),
        out(.r0, Result, "result"),
        out(.r1, i32, "index"),
    });
    //TODO syscall(ReplyAndReceive1, int32 *index, Handle *handles, int32 handleCount, Handle replyTarget);
};

pub const ReplyAndReceive2 = struct {
    pub const desc = stubbed(0x4C, .ReplyAndReceive2, &.{
        ignored(.r0),
        in(.r1, [*]const Handle, "handles"),
        in(.r2, i32, "handle_count"),
        in(.r3, Handle, "reply_target"),
        out(.r0, Result, "result"),
        out(.r1, i32, "index"),
    });
    //TODO syscall(ReplyAndReceive2, int32 *index, Handle *handles, int32 handleCount, Handle replyTarget);
};

pub const ReplyAndReceive3 = struct {
    pub const desc = stubbed(0x4D, .ReplyAndReceive3, &.{
        ignored(.r0),
        in(.r1, [*]const Handle, "handles"),
        in(.r2, i32, "handle_count"),
        in(.r3, Handle, "reply_target"),
        out(.r0, Result, "result"),
        out(.r1, i32, "index"),
    });
    //TODO syscall(ReplyAndReceive3, int32 *index, Handle *handles, int32 handleCount, Handle replyTarget);
};

pub const ReplyAndReceive4 = struct {
    pub const desc = stubbed(0x4E, .ReplyAndReceive4, &.{
        ignored(.r0),
        in(.r1, [*]const Handle, "handles"),
        in(.r2, i32, "handle_count"),
        in(.r3, Handle, "reply_target"),
        out(.r0, Result, "result"),
        out(.r1, i32, "index"),
    });
    //TODO syscall(ReplyAndReceive4, int32 *index, Handle *handles, int32 handleCount, Handle replyTarget);
};

pub const ReplyAndReceive = struct {
    pub const desc = syscall(0x4F, .ReplyAndReceive, &.{
        ignored(.r0),
        in(.r1, [*]const Handle, "handles"),
        in(.r2, i32, "handle_count"),
        in(.r3, Handle, "reply_target"),
        out(.r0, Result, "result"),
        out(.r1, i32, "index"),
    });
    //TODO syscall(ReplyAndReceive, int32 *index, Handle *handles, int32 handleCount, Handle replyTarget);
};

pub const BindInterrupt = struct {
    pub const desc = syscall(0x50, .BindInterrupt, &.{
        in(.r0, hw.Interrupt.Index, "interrupt_index"),
        in(.r1, Handle, "wakeup_handle"),
        in(.r2, hw.Interrupt.Priority, "priority"),
        in(.r3, bool, "is_manual_clear"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(BindInterrupt, Interrupt name, Handle eventOrSemaphore, int32 priority, bool isLevelHighActive);
};

pub const UnbindInterrupt = struct {
    pub const desc = syscall(0x51, .UnbindInterrupt, &.{
        in(.r0, hw.Interrupt.Index, "interrupt_index"),
        in(.r1, Handle, "wakeup_handle"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(UnbindInterrupt, Interrupt name, Handle eventOrSemaphore);*/
};

pub const InvalidateProcessDataCache = struct {
    pub const desc = syscall(0x52, .InvalidateProcessDataCache, &.{
        in(.r0, Handle.Of(pm.Process), "process_handle"),
        in(.r1, *anyopaque, "addr"),
        in(.r2, u32, "size"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(InvalidateProcessDataCache, Handle process, void *addr, uint32 size);
};

pub const StoreProcessDataCache = struct {
    pub const desc = syscall(0x53, .StoreProcessDataCache, &.{
        in(.r0, Handle.Of(pm.Process), "process_handle"),
        in(.r1, *anyopaque, "addr"),
        in(.r2, u32, "size"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(StoreProcessDataCache, Handle process, void const *addr, uint32 size);
};

pub const FlushProcessDataCache = struct {
    pub const desc = syscall(0x54, .FlushProcessDataCache, &.{
        in(.r0, Handle.Of(pm.Process), "process_handle"),
        in(.r1, *anyopaque, "addr"),
        in(.r2, u32, "size"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(FlushProcessDataCache, Handle process, void const *addr, uint32 size);
};

pub const StartInterProcessDma = struct {
    pub const desc = syscall(0x55, .StartInterProcessDma, &.{
        in(.r0, *const anyopaque, "src"),
        in(.r1, Handle.Of(pm.Process), "dst_process_handle"),
        in(.r2, *anyopaque, "dst"),
        in(.r3, Handle.Of(pm.Process), "src_process_handle"),
        in(.r4, u32, "size"),
        in(.r5, *const mm.DMA.Config, "dma_config"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(mm.DMA), "dma_handle"),
    });
    //TODO syscall(StartInterProcessDma, Handle *dma, Handle dstProcess, void *dst, Handle srcProcess, const void *src, uint32 size, const DmaConfig *config);
};

pub const StopDma = struct {
    pub const desc = syscall(0x56, .StopDma, &.{
        in(.r0, Handle.Of(mm.DMA), "dma_handle"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(StopDma, Handle dma);
};

pub const GetDmaState = struct {
    pub const desc = syscall(0x57, .GetDmaState, &.{
        ignored(.r0),
        in(.r1, Handle.Of(mm.DMA), "dma_handle"),
        out(.r0, Result, "result"),
        out(.r1, mm.DMA.State, "dma_state"),
    });
    //TODO syscall(GetDmaState, DmaState *state, Handle dma);
};

pub const RestartDma = struct {
    pub const desc = syscall(0x58, .RestartDma, &.{
        // TODO
        // Handle, void*, void const*, unsigned int, signed char)
    });
    //TODO syscall(RestartDma, Handle, void*, void const*, unsigned int, signed char);
};

pub const SetGpuProt = struct {
    pub const desc = syscall(0x59, .SetGpuProt, &.{
        in(.r0, bool, "restrict_gpu_range"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(SetGpuProt, int8 flag);
};

pub const SetWifiEnabled = struct {
    pub const desc = syscall(0x5A, .SetWifiEnabled, &.{
        in(.r0, bool, "enabled"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(SetWifiEnabled, int8 flag);
};

pub const DebugActiveProces = struct {
    pub const desc = syscall(0x60, .DebugActiveProces, &.{
        ignored(.r0),
        in(.r1, pm.Process.Id, "process_id"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(dbg.Debug), "debug_handle"),
    });
    //TODO syscall(DebugActiveProcess, Handle *debug, uint32 processID);
};

pub const BreakDebugProcess = struct {
    pub const desc = syscall(0x61, .BreakDebugProcess, &.{
        in(.r0, Handle.Of(dbg.Debug), "debug_handle"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(BreakDebugProcess, Handle debug);
};

pub const TerminateDebugProcess = struct {
    pub const desc = syscall(0x62, .TerminateDebugProcess, &.{
        in(.r0, Handle.Of(dbg.Debug), "debug_handle"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(TerminateDebugProcess, Handle debug);
};

pub const GetProcessDebugEvent = struct {
    pub const desc = syscall(0x63, .GetProcessDebugEvent, &.{
        in(.r0, *dbg.EventInfo, "info"),
        in(.r1, Handle.Of(dbg.Debug), "debug_handle"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(GetProcessDebugEvent, DebugEventInfo *info, Handle debug);
};

pub const ContinueDebugEvent = struct {
    pub const desc = syscall(0x64, .ContinueDebugEvent, &.{
        in(.r0, Handle.Of(dbg.Debug), "debug_handle"),
        in(.r1, dbg.Flags, "flags"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(ContinueDebugEvent, Handle debug, uint32 flags);
};

pub const GetProcessList = struct {
    pub const desc = syscall(0x65, .GetProcessList, &.{
        ignored(.r0),
        in(.r1, [*]pm.Thread.Id, "process_ids"),
        in(.r2, i32, "process_id_max_count"),
        out(.r0, Result, "result"),
        out(.r1, i32, "process_count"),
    });
    //TODO syscall(GetProcessList, int32 *processCount, uint32 *processIds, int32 processIdMaxCount);
};

pub const GetThreadList = struct {
    pub const desc = syscall(0x66, .GetThreadList, &.{
        ignored(.r0),
        in(.r1, [*]pm.Thread.Id, "thread_ids"),
        in(.r2, i32, "thread_id_max_count"),
        in(.r3, Handle.Of(pm.Process), "process_handle"),
        out(.r0, Result, "result"),
        out(.r1, i32, "thread_count"),
    });
    //TODO syscall(GetThreadList, int32 *threadCount, uint32 *threadIds, int32 threadIdMaxCount, Handle domain);
};

pub const GetDebugThreadContext = struct {
    pub const desc = syscall(0x67, .GetDebugThreadContext, &.{
        in(.r0, Handle.Of(dbg.Debug), "debug_handle"),
        in(.r1, pm.Thread.Id, "thread_id"),
        in(.r2, *dbg.ThreadContext, "context"),
        in(.r3, dbg.ThreadContext.ControlFlags, "control_flags"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(GetDebugThreadContext, ThreadContext *context, Handle debug, uint32 threadId, uint32 controlFlags);
};

pub const SetDebugThreadContext = struct {
    pub const desc = syscall(0x68, .SetDebugThreadContext, &.{
        in(.r0, Handle.Of(dbg.Debug), "debug_handle"),
        in(.r1, pm.Thread.Id, "thread_id"),
        in(.r2, *const dbg.ThreadContext, "context"),
        in(.r3, dbg.ThreadContext.ControlFlags, "control_flags"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(SetDebugThreadContext, Handle debug, uint32 threadId, const ThreadContext *context, uint32 controlFlags);
};

pub const QueryDebugProcessMemory = struct {
    pub const desc = syscall(0x69, .QueryDebugProcessMemory, &.{
        ignored(.r0),
        ignored(.r1),
        in(.r2, Handle.Of(dbg.Debug), "debug_handle"),
        in(.r3, *anyopaque, "addr"),
        out(.r0, Result, "result"),
        out(.r1, u32, "base_process_va"),
        out(.r2, u32, "size"),
        out(.r3, mm.MemoryPermission, "perm"),
        out(.r4, mm.MemoryState, "state"),
        out(.r5, mm.PageFlags, "page_flags"),
    });
    //TODO syscall(QueryDebugProcessMemory, MemoryInfo *blockInfo, PageInfo *pageInfo, Handle debug, uint32 addr);*/
};

pub const ReadProcessMemory = struct {
    pub const desc = syscall(0x6A, .ReadProcessMemory, &.{
        in(.r0, *anyopaque, "buffer"),
        in(.r1, Handle.Of(dbg.Debug), "debug_handle"),
        in(.r2, *anyopaque, "addr"),
        in(.r3, u32, "size"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(ReadProcessMemory, void *buffer, Handle debug, uint32 addr, uint32 size);
};

pub const WriteProcessMemory = struct {
    pub const desc = syscall(0x6B, .WriteProcessMemory, &.{
        in(.r0, Handle.Of(dbg.Debug), "debug_handle"),
        in(.r1, *const anyopaque, "buffer"),
        in(.r2, *anyopaque, "addr"),
        in(.r3, u32, "size"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(WriteProcessMemory, Handle debug, void const *buffer, uint32 addr, uint32 size);
};

pub const SetHardwareBreakPoint = struct {
    pub const desc = syscall(0x6C, .SetHardwareBreakPoint, &.{
        in(.r0, dbg.BreakRegisterId, "register_id"),
        in(.r1, u32, "control"),
        in(.r2, u32, "value"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(SetHardwareBreakPoint, int32 registerId, uint32 control, uint32 value);
};

pub const GetDebugThreadParam = struct {
    pub const desc = syscall(0x6D, .GetDebugThreadParam, &.{
        in(.r0, dbg.ThreadParameter.Type, "parameter_type"),
        ignored(.r1),
        in(.r2, Handle.Of(dbg.Debug), "debug_handle"),
        in(.r3, pm.Thread.Id, "thread_id"),
        out(.r0, Result, "result"),
        out(.r1, i64, "unused"), // Spans on r2
        out(.r3, i32, "out"),
    });
    //TODO syscall(GetDebugThreadParam, int64 *unused, uint32 *out, Handle kdebug, uint32 threadId, DebugThreadParameter param);
};

pub const ControlProcessMemory = struct {
    pub const desc = syscall(0x70, .ControlProcessMemory, &.{
        in(.r0, Handle.Of(pm.Process), "process_handle"),
        in(.r1, *anyopaque, "addr0"),
        in(.r2, *anyopaque, "addr1"),
        in(.r3, u32, "size"),
        in(.r4, mm.MemoryOperation, "op"),
        in(.r5, mm.MemoryPermission, "perm"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(ControlProcessMemory, Handle KProcess, unsigned int Addr0, unsigned int Addr1, unsigned int Size, unsigned int Type, unsigned int Permissions);
};

pub const MapProcessMemory = struct {
    pub const desc = syscall(0x71, .MapProcessMemory, &.{
        in(.r0, Handle.Of(pm.Process), "process_handle"),
        in(.r1, *anyopaque, "addr"),
        in(.r2, u32, "size"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(MapProcessMemory, Handle process, uint32 startAddr, uint32 size);
};

pub const UnmapProcessMemory = struct {
    pub const desc = syscall(0x72, .UnmapProcessMemory, &.{
        in(.r0, Handle.Of(pm.Process), "process_handle"),
        in(.r1, *anyopaque, "addr"),
        in(.r2, u32, "size"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(UnmapProcessMemory, Handle process, uint32 startAddr, uint32 size);
};

const CreateCodeSet = struct {
    pub const desc = syscall(0x73, .CreateCodeSet, &.{
        in(.r0, *anyopaque, "data"),
        in(.r1, *const pm.CodeSetInfo, "codeset_info"),
        in(.r2, *anyopaque, "code"),
        in(.r3, *anyopaque, "ro"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(pm.CodeSet), "codeset_handle"),
    });
    //TODO syscall(CreateCodeSet, Handle *handle_out, struct CodeSetInfo, uint32 code_ptr, uint32 ro_ptr, uint32 data_ptr);
};

const RandomStub = struct {
    pub const desc = syscall(0x74, .RandomStub, &.{ // stubbed
        out(.r0, Result, "result"),
    });
    //TODO syscall(RandomStub, void);
};

const CreateProcess = struct {
    pub const desc = syscall(0x75, .CreateProcess, &.{
        ignored(.r0),
        in(.r1, Handle.Of(pm.CodeSet), "codeset_handle"),
        in(.r2, [*]const u32, "flags"),
        in(.r3, u32, "flags_count"),
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(pm.Process), "process_handle"),
    });
    //TODO syscall(CreateProcess, Handle *handle_out, Handle codeset_handle, uint32 arm11kernelcaps_ptr, uint32 arm11kernelcaps_num);
};

const TerminateProcess = struct {
    pub const desc = syscall(0x76, .TerminateProcess, &.{
        in(.r0, Handle.Of(pm.Process), "process_handle"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(TerminateProcess, Handle);
};

const SetProcessResourceLimits = struct {
    pub const desc = syscall(0x77, .SetProcessResourceLimits, &.{
        in(.r0, Handle.Of(pm.Process), "process_handle"),
        in(.r1, Handle.Of(pm.ResourceLimits), "resource_limits_handle"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(SetProcessResourceLimits, Handle KProcess, Handle KResourceLimit);
};

const CreateResourceLimits = struct {
    pub const desc = syscall(0x78, .CreateResourceLimits, &.{
        out(.r0, Result, "result"),
        out(.r1, Handle.Of(pm.ResourceLimits), "resource_limits_handle"),
    });
    //TODO syscall(CreateResourceLimit, Handle *KResourceLimit);
};

const SetResourceLimitValues = struct {
    pub const desc = syscall(0x79, .SetResourceLimitValues, &.{
        in(.r0, Handle.Of(pm.ResourceLimits), "resource_limits_handle"),
        in(.r1, *const pm.LimitableResource, "resources"),
        in(.r2, [*]i64, "values"),
        in(.r3, u32, "resource_count"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(SetResourceLimitValues, Handle res_limit, LimitableResource *resource_type_list, int64 *resource_list, uint32 count);
};

const AddCodeSegment = struct {
    pub const desc = syscall(0x7A, .AddCodeSegment, &.{
        // stubbed
        in(.r0, *anyopaque, "addr"),
        in(.r1, u32, "size"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(AddCodeSegment, unsigned int Addr, unsigned int Size);
};

const Backdoor = struct {
    pub const desc = syscall(0x7B, .Backdoor, &.{
        in(.r0, *anyopaque, "code_addr"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(Backdoor, unsigned int CodeAddress);
};

const KernelOperation = struct {
    pub const desc = syscall(0x7C, .KernelOperation, &.{
        in(.r0, u32, "type"),
        in(.r1, u32, "arg0"),
        in(.r2, u32, "arg1"),
        in(.r3, u32, "arg2"),
        out(.r0, Result, "result"),
    });
    //TODO syscall(KernelOperation, unsigned int Type, ...);
};

const QueryProcessMemory = struct {
    pub const desc = syscall(0x7D, .QueryProcessMemory, &.{
        ignored(.r0),
        ignored(.r1),
        in(.r2, Handle.Of(pm.Process), "process_handle"),
        in(.r3, *anyopaque, "addr"),
        out(.r0, Result, "result"),
        out(.r1, u32, "base_process_va"),
        out(.r2, u32, "size"),
        out(.r3, mm.MemoryPermission, "perm"),
        out(.r4, mm.MemoryState, "state"),
        out(.r5, mm.PageFlags, "page_flags"),
    });
    //TODO syscall(QueryProcessMemory, MemInfo *Info, unsigned int *Out, Handle KProcess, unsigned int Addr);
};

const StopPoint = struct {
    pub const desc = syscall(0xFF, .StopPoint, &.{});
};

pub const interface = afu.os.syscall.Interface{
    .ControlMemory = ControlMemory.func,
    // .QueryMemory = QueryMemory.func,
    .ExitProcess = ExitProcess.func,
    .ExitThread = ExitThread.func,
    // .OutputDebugString = OutputDebugString.func,
};
