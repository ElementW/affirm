// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afu = @import("../../afu.zig");

const Names = afu.os.syscall.Names;

pub fn Description(comptime LocationT: type) type {
    return struct {
        pub const Index = u24;
        pub const LocDesc = struct {
            pub const Use = enum {
                in,
                ignored,
                out,
                clobber,
                none,
            };
            pub const Location = LocationT;
            location: Location,
            use: Use,
            type_info: std.builtin.Type,
            name: []const u8,

            pub fn in(location: Location, comptime T: type, name: [:0]const u8) LocDesc {
                return LocDesc{
                    .location = location,
                    .use = .in,
                    .type_info = @typeInfo(T),
                    .name = name,
                };
            }
            pub fn out(location: Location, comptime T: type, name: [:0]const u8) LocDesc {
                return LocDesc{
                    .location = location,
                    .use = .out,
                    .type_info = @typeInfo(T),
                    .name = name,
                };
            }
            pub fn ignored(location: Location) LocDesc {
                return LocDesc{
                    .location = location,
                    .use = .ignored,
                    .type_info = @typeInfo(void),
                    .name = "",
                };
            }
        };
        index: Index,
        name: Names,
        locs: []const LocDesc,

        pub fn syscall(index: Index, name: Names, locs: []const LocDesc) @This() {
            return .{
                .index = index,
                .name = name,
                .locs = locs,
            };
        }
        pub fn stubbed(index: Index, name: Names, locs: []const LocDesc) @This() {
            return .{
                .index = index,
                .name = name,
                .locs = locs,
                // TODO mark stubbed
            };
        }
    };
}
