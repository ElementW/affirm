// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afu = @import("../afu.zig");

const dbg = afu.os.dbg;
const hw = afu.os.hw;
const ipc = afu.os.ipc;
const mm = afu.os.mm;
const pm = afu.os.pm;
const sync = afu.os.sync;
const sys = afu.os.sys;

const Handle = afu.Handle;
const Result = afu.Result;

pub const Names = enum {
    ControlMemory,
    QueryMemory,

    Run,
    ExitProcess,

    // Process affinities
    GetProcessAffinityMask,
    SetProcessAffinityMask,
    /// Called `GetProcessIdealProcessor` by the 3DS.
    GetProcessIdealCore,
    /// Called `SetProcessIdealProcessor` by the 3DS.
    SetProcessIdealCore,

    // Thread management
    CreateThread,
    ExitThread,
    SleepThread,
    GetThreadPriority,
    SetThreadPriority,
    GetThreadAffinityMask,
    SetThreadAffinityMask,
    /// Called `GetThreadIdealProcessor` by the 3DS.
    GetThreadIdealCore,
    /// Called `SetThreadIdealProcessor` by the 3DS.
    SetThreadIdealCore,
    /// Called `GetCurrentCoreProcessorID` by the 3DS.
    GetCurrentCoreId,

    // Mutexes
    CreateMutex,
    ReleaseMutex,

    // Semaphores
    CreateSemaphore,
    ReleaseSemaphore,

    // Events
    CreateEvent,
    SignalEvent,
    ClearEvent,

    // Timers
    CreateTimer,
    SetTimer,
    CancelTimer,
    ClearTimer,

    // Memory blocks
    CreateMemoryBlock,
    MapMemoryBlock,
    UnmapMemoryBlock,

    // Address arbiters
    CreateAddressArbiter,
    ArbitrateAddress,

    // Handle management
    DuplicateHandle,
    CloseHandle,

    // Synchronization
    WaitSynchronization,
    WaitSynchronizationN,
    SignalAndWait,
    SendSyncRequest1,
    SendSyncRequest2,
    SendSyncRequest3,
    SendSyncRequest4,
    SendSyncRequest,

    GetSystemTick,
    GetHandleInfo,
    GetSystemInfo,
    GetProcessInfo,
    GetThreadInfo,
    ConnectToPort,
    OpenProcess,
    OpenThread,
    GetProcessId,
    GetProcessIdOfThread,
    GetThreadId,

    // Resource limits
    CreateResourceLimits,
    SetProcessResourceLimits,
    SetResourceLimitValues,
    GetResourceLimits,
    GetResourceLimitsLimitValues,
    GetResourceLimitsCurrentValues,

    GetThreadContext,
    Break,
    OutputDebugString,
    ControlPerformanceCounter,
    CreatePort,
    CreateSessionToPort,
    CreateSession,
    AcceptSession,
    ReplyAndReceive1,
    ReplyAndReceive2,
    ReplyAndReceive3,
    ReplyAndReceive4,
    ReplyAndReceive,

    // Userspace interrupts
    BindInterrupt,
    UnbindInterrupt,

    // Caches
    InvalidateProcessDataCache,
    StoreProcessDataCache,
    FlushProcessDataCache,

    // DMA
    StartInterProcessDma,
    StopDma,
    GetDmaState,
    RestartDma,

    SetGpuProt,
    SetWifiEnabled,

    // Debugging
    DebugActiveProces,
    BreakDebugProcess,
    TerminateDebugProcess,
    GetProcessDebugEvent,
    ContinueDebugEvent,
    GetProcessList,
    GetThreadList,
    GetDebugThreadContext,
    SetDebugThreadContext,
    QueryDebugProcessMemory,
    ReadProcessMemory,
    WriteProcessMemory,
    SetHardwareBreakPoint,
    GetDebugThreadParam,
    ControlProcessMemory,
    MapProcessMemory,
    UnmapProcessMemory,
    QueryProcessMemory,

    // Process spawning
    CreateCodeSet,
    RandomStub,
    CreateProcess,
    TerminateProcess,
    //TODO AddCodeSegment,

    Backdoor,
    KernelOperation,
    //TODO StopPoint,

    // DeviceTreeControl,
    MessageTap,
};

const NI = afu.os.results.NotImplemented;
pub const Meta = struct {
    pub const ControlMemory = struct {
        pub const Fn = fn (
            op: mm.MemoryOperation,
            addr0: ?*anyopaque,
            addr1: ?*anyopaque,
            size: usize,
            permissions: mm.MemoryPermission,
        ) callconv(.@"inline") Result.With(?*anyopaque);
        inline fn defaultImpl(
            _: mm.MemoryOperation,
            _: ?*anyopaque,
            _: ?*anyopaque,
            _: usize,
            _: mm.MemoryPermission,
        ) Result.With(?*anyopaque) {
            return NI.without(?*anyopaque);
        }
    };
    pub const QueryMemory = struct {
        pub const Fn = fn (addr: ?*anyopaque) callconv(.@"inline") Result.With(mm.MemoryQuery);
        inline fn defaultImpl(_: ?*anyopaque) Result.With(mm.MemoryQuery) {
            return NI.without(mm.MemoryQuery);
        }
    };

    pub const Run = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            startup_info: pm.Process.StartupInfo,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: pm.Process.StartupInfo) Result {
            return NI.without(mm.MemoryQuery);
        }
    };
    pub const ExitProcess = struct {
        pub const Fn = fn () callconv(.@"inline") noreturn;
        inline fn defaultImpl() noreturn {
            while (true) {}
        }
    };

    pub const GetProcessAffinityMask = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            affinity_mask: [*]u8,
            processor_count: i32,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: [*]u8, _: i32) Result {
            return NI;
        }
    };
    pub const SetProcessAffinityMask = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            affinity_mask: [*]const u8,
            processor_count: i32,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: [*]const u8, _: i32) Result {
            return NI;
        }
    };
    pub const GetProcessIdealCore = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
        ) callconv(.@"inline") Result.With(pm.Core.Id);
        inline fn defaultImpl(_: Handle.Of(pm.Process)) Result.With(pm.Core.Id) {
            return NI.without(pm.Core.Id);
        }
    };
    pub const SetProcessIdealCore = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            core_id: pm.Core.Id,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: pm.Core.Id) Result {
            return NI;
        }
    };

    pub const CreateThread = struct {
        pub const Fn = fn (
            entrypoint: pm.Thread.Func,
            arg: usize,
            stack_top: *anyopaque,
            core_id: pm.Core.Id,
        ) callconv(.@"inline") Result.With(Handle.Of(pm.Thread));
        inline fn defaultImpl(
            _: pm.Thread.Func,
            _: usize,
            _: *anyopaque,
            _: pm.Core.Id,
        ) Result.With(Handle.Of(pm.Thread)) {
            return NI.without(Handle.Of(pm.Thread));
        }
    };
    pub const ExitThread = struct {
        pub const Fn = fn () callconv(.@"inline") noreturn;
        inline fn defaultImpl() noreturn {
            while (true) {}
        }
    };
    pub const SleepThread = struct {
        pub const Fn = fn (nanoseconds: i64) callconv(.@"inline") void;
        inline fn defaultImpl(_: i64) void {}
    };
    pub const GetThreadPriority = struct {
        pub const Fn = fn (
            thread_handle: Handle.Of(pm.Thread),
        ) callconv(.@"inline") Result.With(pm.Thread.Priority);
        inline fn defaultImpl(_: Handle.Of(pm.Thread)) Result.With(pm.Thread.Priority) {
            return NI.without(pm.Thread.Priority);
        }
    };
    pub const SetThreadPriority = struct {
        pub const Fn = fn (
            thread_handle: Handle.Of(pm.Thread),
            priority: pm.Thread.Priority,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Thread), _: pm.Thread.Priority) Result {
            return NI;
        }
    };
    pub const GetThreadAffinityMask = struct {
        pub const Fn = fn (
            thread_handle: Handle.Of(pm.Thread),
            affinity_mask: [*]u8,
            processor_count: i32,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Thread), _: [*]u8, _: i32) Result {
            return NI;
        }
    };
    pub const SetThreadAffinityMask = struct {
        pub const Fn = fn (
            thread_handle: Handle.Of(pm.Thread),
            affinity_mask: [*]const u8,
            processor_count: i32,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Thread), _: [*]const u8, _: i32) Result {
            return NI;
        }
    };
    pub const GetThreadIdealCore = struct {
        pub const Fn = fn (thread_handle: Handle.Of(pm.Thread)) callconv(.@"inline") Result.With(pm.Core.Id);
        inline fn defaultImpl(_: Handle.Of(pm.Thread)) Result.With(pm.Core.Id) {
            return NI.without(pm.Core.Id);
        }
    };
    pub const SetThreadIdealCore = struct {
        pub const Fn = fn (thread_handle: Handle.Of(pm.Thread), core_id: pm.Core.Id) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Thread), _: pm.Core.Id) Result {
            return NI;
        }
    };
    pub const GetCurrentCoreId = struct {
        pub const Fn = fn () callconv(.@"inline") pm.Core.Id;
        inline fn defaultImpl() pm.Core.Id {
            return @enumFromInt(0);
        }
    };

    pub const CreateMutex = struct {
        pub const Fn = fn (initially_locked: bool) callconv(.@"inline") Result.With(Handle.Of(sync.Mutex));
        inline fn defaultImpl(_: bool) Result.With(Handle.Of(sync.Mutex)) {
            return NI.without(Handle.Of(sync.Mutex));
        }
    };
    pub const ReleaseMutex = struct {
        pub const Fn = fn (mutex_handle: Handle.Of(sync.Mutex)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(sync.Mutex)) Result {
            return NI;
        }
    };

    pub const CreateSemaphore = struct {
        pub const Fn = fn (
            initial_count: u32,
            max_count: u32,
        ) callconv(.@"inline") Result.With(Handle.Of(sync.Semaphore));
        inline fn defaultImpl(_: u32, _: u32) Result.With(Handle.Of(sync.Semaphore)) {
            return NI.without(Handle.Of(sync.Semaphore));
        }
    };
    pub const ReleaseSemaphore = struct {
        pub const Fn = fn (
            semaphore_handle: Handle.Of(sync.Semaphore),
            release_count: i32,
        ) callconv(.@"inline") Result.With(i32);
        inline fn defaultImpl(_: Handle.Of(sync.Semaphore), _: i32) Result.With(i32) {
            return NI.without(i32);
        }
    };

    pub const CreateEvent = struct {
        pub const Fn = fn (reset_type: sync.ResetType) callconv(.@"inline") Result.With(Handle.Of(sync.Event));
        inline fn defaultImpl(_: sync.ResetType) Result.With(Handle.Of(sync.Event)) {
            return NI.without(Handle.Of(sync.Event));
        }
    };
    pub const SignalEvent = struct {
        pub const Fn = fn (event_handle: Handle.Of(sync.Event)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(sync.Event)) Result {
            return NI;
        }
    };
    pub const ClearEvent = struct {
        pub const Fn = fn (event_handle: Handle.Of(sync.Event)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(sync.Event)) Result {
            return NI;
        }
    };

    pub const CreateTimer = struct {
        pub const Fn = fn (reset_type: sync.ResetType) callconv(.@"inline") Result.With(Handle.Of(sync.Timer));
        inline fn defaultImpl(_: sync.ResetType) Result.With(Handle.Of(sync.Timer)) {
            return NI.without(Handle.Of(sync.Timer));
        }
    };
    pub const SetTimer = struct {
        pub const Fn = fn (
            timer_handle: Handle.Of(sync.Timer),
            initial: i64,
            interval: i64,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(sync.Timer), _: i64, _: i64) Result {
            return NI;
        }
    };
    pub const CancelTimer = struct {
        pub const Fn = fn (timer_handle: Handle.Of(sync.Timer)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(sync.Timer)) Result {
            return NI;
        }
    };
    pub const ClearTimer = struct {
        pub const Fn = fn (timer_handle: Handle.Of(sync.Timer)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(sync.Timer)) Result {
            return NI;
        }
    };

    pub const CreateMemoryBlock = struct {
        pub const Fn = fn (
            addr: *anyopaque,
            size: usize,
            self_perm: mm.MemoryPermission,
            other_perm: mm.MemoryPermission,
        ) callconv(.@"inline") Result.With(Handle.Of(mm.MemoryBlock));
        inline fn defaultImpl(
            _: *anyopaque,
            _: usize,
            _: mm.MemoryPermission,
            _: mm.MemoryPermission,
        ) Result.With(
            Handle.Of(mm.MemoryBlock),
        ) {
            return NI.without(Handle.Of(mm.MemoryBlock));
        }
    };
    pub const MapMemoryBlock = struct {
        pub const Fn = fn (
            memblock_handle: Handle.Of(mm.MemoryBlock),
            addr: *anyopaque,
            self_perm: mm.MemoryPermission,
            other_perm: mm.MemoryPermission,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(
            _: Handle.Of(mm.MemoryBlock),
            _: *anyopaque,
            _: mm.MemoryPermission,
            _: mm.MemoryPermission,
        ) Result {
            return NI;
        }
    };
    pub const UnmapMemoryBlock = struct {
        pub const Fn = fn (
            memblock_handle: Handle.Of(mm.MemoryBlock),
            addr: *anyopaque,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(mm.MemoryBlock), _: *anyopaque) Result {
            return NI;
        }
    };

    pub const CreateAddressArbiter = struct {
        pub const Fn = fn () callconv(.@"inline") Result.With(Handle.Of(sync.AddressArbiter));
        inline fn defaultImpl() Result.With(Handle.Of(sync.AddressArbiter)) {
            return NI.without(Handle.Of(sync.AddressArbiter));
        }
    };
    pub const ArbitrateAddress = struct {
        pub const Fn = fn (
            arbiter_handle: Handle.Of(sync.AddressArbiter),
            addr: *anyopaque,
            @"type": pm.ArbitrationType,
            value: i32,
            nanoseconds: i64,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(
            _: Handle.Of(sync.AddressArbiter),
            _: *anyopaque,
            _: pm.ArbitrationType,
            _: i32,
            _: i64,
        ) Result {
            return NI;
        }
    };

    pub const DuplicateHandle = struct {
        pub const Fn = fn (handle: Handle) callconv(.@"inline") Result.With(Handle);
        inline fn defaultImpl(_: Handle) Result.With(Handle) {
            return NI.without(Handle);
        }
    };
    pub const CloseHandle = struct {
        pub const Fn = fn (handle: Handle) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle) Result {
            return NI;
        }
    };

    pub const WaitSynchronization = struct {
        pub const Fn = fn (handle: Handle, nanoseconds: i64) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle, _: i64) Result {
            return NI;
        }
    };
    pub const WaitSynchronizationN = struct {
        pub const Fn = fn (
            handles: []const Handle,
            wait_all: bool,
            nanoseconds: i64,
        ) callconv(.@"inline") Result.With(i32);
        inline fn defaultImpl(_: []const Handle, _: bool, _: i64) Result.With(i32) {
            return NI.without(i32);
        }
    };
    pub const SignalAndWait = struct {
        pub const Fn = fn (
            signal_handle: Handle,
            wait_handles: []const Handle,
            wait_all: bool,
            nanoseconds: i64,
        ) callconv(.@"inline") Result.With(i32);
        inline fn defaultImpl(_: Handle, _: []const Handle, _: bool, _: i64) Result.With(i32) {
            return NI.without(i32);
        }
    };
    pub const SendSyncRequest1 = struct {
        pub const Fn = fn (session_handle: Handle.Of(ipc.Session)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(ipc.Session)) Result {
            return NI;
        }
    };
    pub const SendSyncRequest2 = struct {
        pub const Fn = fn (session_handle: Handle.Of(ipc.Session)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(ipc.Session)) Result {
            return NI;
        }
    };
    pub const SendSyncRequest3 = struct {
        pub const Fn = fn (session_handle: Handle.Of(ipc.Session)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(ipc.Session)) Result {
            return NI;
        }
    };
    pub const SendSyncRequest4 = struct {
        pub const Fn = fn (session_handle: Handle.Of(ipc.Session)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(ipc.Session)) Result {
            return NI;
        }
    };
    pub const SendSyncRequest = struct {
        pub const Fn = fn (session_handle: Handle.Of(ipc.Session)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(ipc.Session)) Result {
            return NI;
        }
    };

    pub const GetSystemTick = struct {
        pub const Fn = fn () callconv(.@"inline") i64;
        inline fn defaultImpl() i64 {
            return 0;
        }
    };
    pub const GetHandleInfo = struct {
        pub const Fn = fn (
            handle: Handle,
            info_type: sys.HandleInfo.Type,
        ) callconv(.@"inline") Result.With(sys.HandleInfo);
        inline fn defaultImpl(_: Handle, _: sys.HandleInfo.Type) Result.With(sys.HandleInfo) {
            return NI.without(sys.HandleInfo);
        }
    };
    pub const GetSystemInfo = struct {
        pub const Fn = fn (info_query: sys.SystemInfo.Query) callconv(.@"inline") Result.With(sys.SystemInfo);
        inline fn defaultImpl(_: sys.SystemInfo.Query) Result.With(sys.SystemInfo) {
            return NI.without(sys.SystemInfo);
        }
    };
    pub const GetProcessInfo = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            info_type: pm.Process.Info.Type,
        ) callconv(.@"inline") Result.With(pm.Process.Info);
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: pm.Process.Info.Type) Result.With(pm.Process.Info) {
            return NI.without(pm.Process.Info);
        }
    };
    pub const GetThreadInfo = struct {
        pub const Fn = fn (
            thread_handle: Handle.Of(pm.Thread),
            info_type: pm.Thread.Info.Type,
        ) callconv(.@"inline") Result.With(pm.Thread.Info);
        inline fn defaultImpl(_: Handle.Of(pm.Thread), _: pm.Thread.Info.Type) Result.With(pm.Thread.Info) {
            return NI.without(pm.Thread.Info);
        }
    };
    pub const ConnectToPort = struct {
        pub const Fn = fn (name: []const u8) callconv(.@"inline") Result.With(Handle.Of(ipc.Session));
        inline fn defaultImpl(_: []const u8) Result.With(Handle.Of(ipc.Session)) {
            return NI.without(Handle.Of(ipc.Session));
        }
    };
    pub const OpenProcess = struct {
        pub const Fn = fn (process_id: pm.Process.Id) callconv(.@"inline") Result.With(Handle.Of(pm.Process));
        inline fn defaultImpl(_: pm.Process.Id) Result.With(Handle.Of(pm.Process)) {
            return NI.without(Handle.Of(pm.Process));
        }
    };
    pub const OpenThread = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            thread_id: pm.Thread.Id,
        ) callconv(.@"inline") Result.With(Handle.Of(pm.Thread));
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: pm.Thread.Id) Result.With(Handle.Of(pm.Thread)) {
            return NI.without(Handle.Of(pm.Thread));
        }
    };
    pub const GetProcessId = struct {
        pub const Fn = fn (process_handle: Handle.Of(pm.Process)) callconv(.@"inline") Result.With(pm.Process.Id);
        inline fn defaultImpl(_: Handle.Of(pm.Process)) Result.With(pm.Process.Id) {
            return NI.without(pm.Process.Id);
        }
    };
    pub const GetProcessIdOfThread = struct {
        pub const Fn = fn (thread_handle: Handle.Of(pm.Thread)) callconv(.@"inline") Result.With(pm.Process.Id);
        inline fn defaultImpl(_: Handle.Of(pm.Thread)) Result.With(pm.Process.Id) {
            return NI.without(pm.Process.Id);
        }
    };
    pub const GetThreadId = struct {
        pub const Fn = fn (thread_handle: Handle.Of(pm.Thread)) callconv(.@"inline") Result.With(pm.Thread.Id);
        inline fn defaultImpl(_: Handle.Of(pm.Thread)) Result.With(pm.Thread.Id) {
            return NI.without(pm.Thread.Id);
        }
    };

    pub const CreateResourceLimits = struct {
        pub const Fn = fn () callconv(.@"inline") Result.With(Handle.Of(pm.ResourceLimits));
        inline fn defaultImpl() Result.With(Handle.Of(pm.ResourceLimits)) {
            return NI.without(Handle.Of(pm.ResourceLimits));
        }
    };
    pub const SetProcessResourceLimits = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            resource_limits_handle: Handle.Of(pm.ResourceLimits),
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: Handle.Of(pm.ResourceLimits)) Result {
            return NI;
        }
    };
    pub const SetResourceLimitValues = struct {
        pub const Fn = fn (
            resource_limits_handle: Handle.Of(pm.ResourceLimits),
            values: []const pm.LimitableResource,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.ResourceLimits), _: []const pm.LimitableResource) Result {
            return NI;
        }
    };
    pub const GetResourceLimits = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
        ) callconv(.@"inline") Result.With(Handle.Of(pm.ResourceLimits));
        inline fn defaultImpl(_: Handle.Of(pm.Process)) Result.With(Handle.Of(pm.ResourceLimits)) {
            return NI.without(Handle.Of(pm.ResourceLimits));
        }
    };
    pub const GetResourceLimitsLimitValues = struct {
        pub const Fn = fn (
            resource_limits_handle: Handle.Of(pm.ResourceLimits),
            values: []pm.LimitableResource,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.ResourceLimits), _: []pm.LimitableResource) Result {
            return NI;
        }
    };
    pub const GetResourceLimitsCurrentValues = struct {
        pub const Fn = fn (
            resource_limits_handle: Handle.Of(pm.ResourceLimits),
            values: []pm.LimitableResource,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.ResourceLimits), _: []pm.LimitableResource) Result {
            return NI;
        }
    };

    pub const GetThreadContext = struct {
        pub const Fn = fn (
            thread_handle: Handle.Of(pm.Thread),
        ) callconv(.@"inline") Result.With(dbg.ThreadContext);
        inline fn defaultImpl(_: Handle.Of(pm.Thread)) Result.With(dbg.ThreadContext) {
            return NI.without(dbg.ThreadContext);
        }
    };
    pub const Break = struct {
        pub const Fn = fn (
            @"type": pm.BreakType,
            data: *const anyopaque,
            length: usize,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: pm.BreakType, _: *const anyopaque, _: usize) Result {
            return NI;
        }
    };
    pub const OutputDebugString = struct {
        pub const Fn = fn (string: []const u8) callconv(.@"inline") Result;
        inline fn defaultImpl(_: []const u8) Result {
            return NI;
        }
    };
    pub const ControlPerformanceCounter = struct {
        pub const Fn = fn (
            operation: dbg.PerformanceCounter.Operation,
        ) callconv(.@"inline") Result.With(dbg.PerformanceCounter.Operation.Result);
        inline fn defaultImpl(_: dbg.PerformanceCounter.Operation) Result.With(dbg.PerformanceCounter.Operation.Result) {
            return NI.without(u64);
        }
    };
    pub const CreatePort = struct {
        pub const Fn = fn (
            name: []const u8,
            max_sessions: u32,
        ) callconv(.@"inline") Result.With(ipc.SessionHandlePair);
        inline fn defaultImpl(_: []const u8, _: u32) Result.With(ipc.SessionHandlePair) {
            return NI.without(Handle.Of(ipc.SessionHandlePair));
        }
    };
    pub const CreateSessionToPort = struct {
        pub const Fn = fn (
            port_handle: Handle.Of(ipc.Port),
        ) callconv(.@"inline") Result.With(Handle.Of(ipc.Session));
        inline fn defaultImpl(_: Handle.Of(ipc.Port)) Result.With(Handle.Of(ipc.Session)) {
            return NI.without(Handle.Of(ipc.Session));
        }
    };
    pub const CreateSession = struct {
        pub const Fn = fn () callconv(.@"inline") Result.With(ipc.SessionHandlePair);
        inline fn defaultImpl() Result.With(ipc.SessionHandlePair) {
            return NI.without(Handle.Of(ipc.SessionHandlePair));
        }
    };
    pub const AcceptSession = struct {
        pub const Fn = fn (
            port_handle: Handle.Of(ipc.Port),
        ) callconv(.@"inline") Result.With(Handle.Of(ipc.Session));
        inline fn defaultImpl(_: Handle.Of(ipc.Port)) Result.With(Handle.Of(ipc.Session)) {
            return NI.without(Handle.Of(ipc.Session));
        }
    };
    pub const ReplyAndReceive1 = struct {
        pub const Fn = fn (
            handles: []Handle,
            reply_target: Handle.Of(ipc.Session),
        ) callconv(.@"inline") Result.With(usize);
        inline fn defaultImpl(_: []Handle, _: Handle.Of(ipc.Session)) Result.With(usize) {
            return NI.without(usize);
        }
    };
    pub const ReplyAndReceive2 = struct {
        pub const Fn = fn (
            handles: []Handle,
            reply_target: Handle.Of(ipc.Session),
        ) callconv(.@"inline") Result.With(usize);
        inline fn defaultImpl(_: []Handle, _: Handle.Of(ipc.Session)) Result.With(usize) {
            return NI.without(usize);
        }
    };
    pub const ReplyAndReceive3 = struct {
        pub const Fn = fn (
            handles: []Handle,
            reply_target: Handle.Of(ipc.Session),
        ) callconv(.@"inline") Result.With(usize);
        inline fn defaultImpl(_: []Handle, _: Handle.Of(ipc.Session)) Result.With(usize) {
            return NI.without(usize);
        }
    };
    pub const ReplyAndReceive4 = struct {
        pub const Fn = fn (
            handles: []Handle,
            reply_target: Handle.Of(ipc.Session),
        ) callconv(.@"inline") Result.With(usize);
        inline fn defaultImpl(_: []Handle, _: Handle.Of(ipc.Session)) Result.With(usize) {
            return NI.without(usize);
        }
    };
    pub const ReplyAndReceive = struct {
        pub const Fn = fn (
            handles: []Handle,
            reply_target: Handle.Of(ipc.Session),
        ) callconv(.@"inline") Result.With(usize);
        inline fn defaultImpl(_: []Handle, _: Handle.Of(ipc.Session)) Result.With(usize) {
            return NI.without(usize);
        }
    };

    pub const BindInterrupt = struct {
        pub const Fn = fn (
            interrupt_index: hw.Interrupt.Index,
            wakeup_handle: Handle,
            priority: hw.Interrupt.Priority,
            is_manual_clear: bool,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: hw.Interrupt.Index, _: Handle, _: hw.Interrupt.Priority, _: bool) Result {
            return NI;
        }
    };
    pub const UnbindInterrupt = struct {
        pub const Fn = fn (interrupt_index: hw.Interrupt.Index, wakeup_handle: Handle) callconv(.@"inline") Result;
        inline fn defaultImpl(_: hw.Interrupt.Index, _: Handle) Result {
            return NI;
        }
    };

    pub const InvalidateProcessDataCache = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            addr: *anyopaque,
            size: usize,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: *anyopaque, _: usize) Result {
            return NI;
        }
    };
    pub const StoreProcessDataCache = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            addr: *const anyopaque,
            size: usize,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: *const anyopaque, _: usize) Result {
            return NI;
        }
    };
    pub const FlushProcessDataCache = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            addr: *const anyopaque,
            size: usize,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: *const anyopaque, _: usize) Result {
            return NI;
        }
    };

    pub const StartInterProcessDma = struct {
        pub const Fn = fn (
            dst_process_handle: Handle.Of(pm.Process),
            dst: *anyopaque,
            src_process_handle: Handle.Of(pm.Process),
            src: *const anyopaque,
            size: usize,
            dma_config: *const mm.DMA.Config,
        ) callconv(.@"inline") Result.With(Handle.Of(mm.DMA));
        inline fn defaultImpl(
            _: Handle.Of(pm.Process),
            _: *anyopaque,
            _: Handle.Of(pm.Process),
            _: *const anyopaque,
            _: usize,
            _: *const mm.DMA.Config,
        ) Result.With(Handle.Of(mm.DMA)) {
            return NI.without(Handle.Of(mm.DMA));
        }
    };
    pub const StopDma = struct {
        pub const Fn = fn (dma_handle: Handle.Of(mm.DMA)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(mm.DMA)) Result {
            return NI;
        }
    };
    pub const GetDmaState = struct {
        pub const Fn = fn (dma_handle: Handle.Of(mm.DMA)) callconv(.@"inline") Result.With(mm.DMA.State);
        inline fn defaultImpl(_: Handle.Of(mm.DMA)) Result.With(mm.DMA.State) {
            return NI.without(mm.DMA.State);
        }
    };
    pub const RestartDma = struct {
        pub const Fn = fn (
            dma_handle: Handle.Of(mm.DMA),
            dest: ?*anyopaque,
            source: ?*const anyopaque,
            size: usize,
            flags: mm.DMA.Flags,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(mm.DMA), _: ?*anyopaque, _: ?*const anyopaque, _: usize, _: mm.DMA.Flags) Result {
            return NI;
        }
    };

    pub const SetGpuProt = struct {
        pub const Fn = fn (use_application_restriction: bool) callconv(.@"inline") Result;
        inline fn defaultImpl(_: bool) Result {
            return NI;
        }
    };
    pub const SetWifiEnabled = struct {
        pub const Fn = fn (enable: bool) callconv(.@"inline") Result;
        inline fn defaultImpl(_: bool) Result {
            return NI;
        }
    };

    pub const DebugActiveProces = struct {
        pub const Fn = fn (process_id: pm.Process.Id) callconv(.@"inline") Result.With(Handle.Of(dbg.Debug));
        inline fn defaultImpl(_: pm.Process.Id) Result.With(Handle.Of(dbg.Debug)) {
            return NI.without(Handle.Of(dbg.Debug));
        }
    };
    pub const BreakDebugProcess = struct {
        pub const Fn = fn (debug_handle: Handle.Of(dbg.Debug)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(dbg.Debug)) Result {
            return NI;
        }
    };
    pub const TerminateDebugProcess = struct {
        pub const Fn = fn (debug_handle: Handle.Of(dbg.Debug)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(dbg.Debug)) Result {
            return NI;
        }
    };
    pub const GetProcessDebugEvent = struct {
        pub const Fn = fn (debug_handle: Handle.Of(dbg.Debug), info: *dbg.EventInfo) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(dbg.Debug), _: *dbg.EventInfo) Result {
            return NI;
        }
    };
    pub const ContinueDebugEvent = struct {
        pub const Fn = fn (debug_handle: Handle.Of(dbg.Debug), flags: dbg.Flags) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(dbg.Debug), _: dbg.Flags) Result {
            return NI;
        }
    };
    pub const GetProcessList = struct {
        pub const Fn = fn (process_ids: []pm.Process.Id) callconv(.@"inline") Result.With([]pm.Process.Id);
        inline fn defaultImpl(_: []pm.Process.Id) Result.With([]pm.Process.Id) {
            return NI.without([]pm.Process.Id);
        }
    };
    pub const GetThreadList = struct {
        pub const Fn = fn (
            thread_ids: []pm.Thread.Id,
            process_handle: Handle.Of(pm.Process),
        ) callconv(.@"inline") Result.With([]pm.Thread.Id);
        inline fn defaultImpl(_: []pm.Thread.Id, _: Handle.Of(pm.Process)) Result.With([]pm.Thread.Id) {
            return NI.without([]pm.Thread.Id);
        }
    };
    pub const GetDebugThreadContext = struct {
        pub const Fn = fn (
            debug_handle: Handle.Of(dbg.Debug),
            thread_id: pm.Thread.Id,
            context: *dbg.ThreadContext,
            control_flags: dbg.ThreadContext.ControlFlags,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(
            _: Handle.Of(dbg.Debug),
            _: pm.Thread.Id,
            _: *dbg.ThreadContext,
            _: dbg.ThreadContext.ControlFlags,
        ) Result {
            return NI;
        }
    };
    pub const SetDebugThreadContext = struct {
        pub const Fn = fn (
            debug_handle: Handle.Of(dbg.Debug),
            thread_id: pm.Thread.Id,
            context: *const dbg.ThreadContext,
            control_flags: dbg.ThreadContext.ControlFlags,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(
            _: Handle.Of(dbg.Debug),
            _: pm.Thread.Id,
            _: *const dbg.ThreadContext,
            _: dbg.ThreadContext.ControlFlags,
        ) Result {
            return NI;
        }
    };
    pub const QueryDebugProcessMemory = struct {
        pub const Fn = fn (
            debug_handle: Handle.Of(dbg.Debug),
            addr: *anyopaque,
        ) callconv(.@"inline") Result.With(mm.MemoryQuery);
        inline fn defaultImpl(_: Handle.Of(dbg.Debug), _: *anyopaque) Result.With(mm.MemoryQuery) {
            return NI.without(mm.MemoryQuery);
        }
    };
    pub const ReadProcessMemory = struct {
        pub const Fn = fn (
            debug_handle: Handle.Of(dbg.Debug),
            buffer: *anyopaque,
            addr: *anyopaque,
            size: usize,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(dbg.Debug), _: *anyopaque, _: *anyopaque, _: usize) Result {
            return NI;
        }
    };
    pub const WriteProcessMemory = struct {
        pub const Fn = fn (
            debug_handle: Handle.Of(dbg.Debug),
            buffer: *anyopaque,
            addr: *anyopaque,
            size: usize,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(dbg.Debug), _: *anyopaque, _: *anyopaque, _: usize) Result {
            return NI;
        }
    };
    pub const SetHardwareBreakPoint = struct {
        pub const Fn = fn (
            register_id: dbg.BreakRegisterId,
            control: u32,
            value: u32,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: dbg.BreakRegisterId, _: u32, _: u32) Result {
            return NI;
        }
    };
    pub const GetDebugThreadParam = struct {
        pub const Fn = fn (
            debug_handle: Handle.Of(dbg.Debug),
            thread_id: pm.Thread.Id,
            parameter_type: dbg.ThreadParameter.Type,
        ) callconv(.@"inline") Result.With(dbg.ThreadParameter);
        inline fn defaultImpl(
            _: Handle.Of(dbg.Debug),
            _: pm.Thread.Id,
            _: dbg.ThreadParameter.Type,
        ) Result.With(dbg.ThreadParameter) {
            return NI.without(dbg.ThreadParameter);
        }
    };
    pub const ControlProcessMemory = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            addr0: *anyopaque,
            addr1: *anyopaque,
            size: usize,
            op: mm.MemoryOperation,
            perm: mm.MemoryPermission,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(
            _: Handle.Of(pm.Process),
            _: *anyopaque,
            _: *anyopaque,
            _: usize,
            _: mm.MemoryOperation,
            _: mm.MemoryPermission,
        ) Result {
            return NI;
        }
    };
    pub const MapProcessMemory = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            addr: *anyopaque,
            size: usize,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: *anyopaque, _: usize) Result {
            return NI;
        }
    };
    pub const UnmapProcessMemory = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            addr: *anyopaque,
            size: usize,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: *anyopaque, _: usize) Result {
            return NI;
        }
    };
    pub const QueryProcessMemory = struct {
        pub const Fn = fn (
            process_handle: Handle.Of(pm.Process),
            addr: ?*anyopaque,
        ) callconv(.@"inline") Result.With(mm.MemoryQuery);
        inline fn defaultImpl(_: Handle.Of(pm.Process), _: ?*anyopaque) Result.With(mm.MemoryQuery) {
            return NI.without(mm.MemoryQuery);
        }
    };

    pub const CreateCodeSet = struct {
        pub const Fn = fn (
            codeset_info: *const pm.CodeSet.Info,
            text: *anyopaque,
            ro: *anyopaque,
            rw: *anyopaque,
        ) callconv(.@"inline") Result.With(Handle.Of(pm.CodeSet));
        inline fn defaultImpl(
            _: *const pm.CodeSet.Info,
            _: *anyopaque,
            _: *anyopaque,
            _: *anyopaque,
        ) Result.With(Handle.Of(pm.CodeSet)) {
            return NI.without(Handle.Of(pm.CodeSet));
        }
    };
    pub const RandomStub = struct {
        pub const Fn = fn () callconv(.@"inline") Result;
        inline fn defaultImpl() Result {
            return NI;
        }
    };
    pub const CreateProcess = struct {
        pub const Fn = fn (
            codeset_handle: Handle.Of(pm.CodeSet),
            capabilities: []const u32,
        ) callconv(.@"inline") Result.With(Handle.Of(pm.Process));
        inline fn defaultImpl(_: Handle.Of(pm.CodeSet), _: []const u32) Result.With(Handle.Of(pm.Process)) {
            return NI.without(Handle.Of(pm.Process));
        }
    };
    pub const TerminateProcess = struct {
        pub const Fn = fn (process_handle: Handle.Of(pm.Process)) callconv(.@"inline") Result;
        inline fn defaultImpl(_: Handle.Of(pm.Process)) Result {
            return NI;
        }
    };
    //TODO pub const AddCodeSegment = struct {};

    pub const Backdoor = struct {
        pub const Fn = fn (addr: *anyopaque) callconv(.@"inline") void;
        inline fn defaultImpl(_: *anyopaque) void {
            return NI;
        }
    };
    pub const KernelOperation = struct {
        pub const Fn = fn (
            operation: sys.KernelOperation,
            arg1: u32,
            arg2: u32,
            arg3: u32,
        ) callconv(.@"inline") Result;
        inline fn defaultImpl(_: sys.KernelOperation, _: u32, _: u32, _: u32) Result {
            return NI;
        }
    };
    //TODO pub const StopPoint = struct {};

    // pub const DeviceTreeControl = struct {};
    pub const MessageTap = struct {
        pub const Fn = fn () callconv(.@"inline") Result.With(Handle.Of(ipc.MessageTap));
        inline fn defaultImpl() Result.With(Handle.Of(ipc.MessageTap)) {
            return NI.without(Handle.Of(ipc.MessageTap));
        }
    };
};

pub const Interface = struct {
    ControlMemory: Meta.ControlMemory.Fn = Meta.ControlMemory.defaultImpl,
    QueryMemory: Meta.QueryMemory.Fn = Meta.QueryMemory.defaultImpl,

    Run: Meta.Run.Fn = Meta.Run.defaultImpl,
    ExitProcess: Meta.ExitProcess.Fn = Meta.ExitProcess.defaultImpl,

    GetProcessAffinityMask: Meta.GetProcessAffinityMask.Fn = Meta.GetProcessAffinityMask.defaultImpl,
    SetProcessAffinityMask: Meta.SetProcessAffinityMask.Fn = Meta.SetProcessAffinityMask.defaultImpl,
    GetProcessIdealCore: Meta.GetProcessIdealCore.Fn = Meta.GetProcessIdealCore.defaultImpl,
    SetProcessIdealCore: Meta.SetProcessIdealCore.Fn = Meta.SetProcessIdealCore.defaultImpl,

    CreateThread: Meta.CreateThread.Fn = Meta.CreateThread.defaultImpl,
    ExitThread: Meta.ExitThread.Fn = Meta.ExitThread.defaultImpl,
    SleepThread: Meta.SleepThread.Fn = Meta.SleepThread.defaultImpl,
    GetThreadPriority: Meta.GetThreadPriority.Fn = Meta.GetThreadPriority.defaultImpl,
    SetThreadPriority: Meta.SetThreadPriority.Fn = Meta.SetThreadPriority.defaultImpl,
    GetThreadAffinityMask: Meta.GetThreadAffinityMask.Fn = Meta.GetThreadAffinityMask.defaultImpl,
    SetThreadAffinityMask: Meta.SetThreadAffinityMask.Fn = Meta.SetThreadAffinityMask.defaultImpl,
    GetThreadIdealCore: Meta.GetThreadIdealCore.Fn = Meta.GetThreadIdealCore.defaultImpl,
    SetThreadIdealCore: Meta.SetThreadIdealCore.Fn = Meta.SetThreadIdealCore.defaultImpl,
    GetCurrentCoreId: Meta.GetCurrentCoreId.Fn = Meta.GetCurrentCoreId.defaultImpl,

    CreateMutex: Meta.CreateMutex.Fn = Meta.CreateMutex.defaultImpl,
    ReleaseMutex: Meta.ReleaseMutex.Fn = Meta.ReleaseMutex.defaultImpl,

    CreateSemaphore: Meta.CreateSemaphore.Fn = Meta.CreateSemaphore.defaultImpl,
    ReleaseSemaphore: Meta.ReleaseSemaphore.Fn = Meta.ReleaseSemaphore.defaultImpl,

    CreateEvent: Meta.CreateEvent.Fn = Meta.CreateEvent.defaultImpl,
    SignalEvent: Meta.SignalEvent.Fn = Meta.SignalEvent.defaultImpl,
    ClearEvent: Meta.ClearEvent.Fn = Meta.ClearEvent.defaultImpl,

    CreateTimer: Meta.CreateTimer.Fn = Meta.CreateTimer.defaultImpl,
    SetTimer: Meta.SetTimer.Fn = Meta.SetTimer.defaultImpl,
    CancelTimer: Meta.CancelTimer.Fn = Meta.CancelTimer.defaultImpl,
    ClearTimer: Meta.ClearTimer.Fn = Meta.ClearTimer.defaultImpl,

    CreateMemoryBlock: Meta.CreateMemoryBlock.Fn = Meta.CreateMemoryBlock.defaultImpl,
    MapMemoryBlock: Meta.MapMemoryBlock.Fn = Meta.MapMemoryBlock.defaultImpl,
    UnmapMemoryBlock: Meta.UnmapMemoryBlock.Fn = Meta.UnmapMemoryBlock.defaultImpl,

    CreateAddressArbiter: Meta.CreateAddressArbiter.Fn = Meta.CreateAddressArbiter.defaultImpl,
    ArbitrateAddress: Meta.ArbitrateAddress.Fn = Meta.ArbitrateAddress.defaultImpl,

    DuplicateHandle: Meta.DuplicateHandle.Fn = Meta.DuplicateHandle.defaultImpl,
    CloseHandle: Meta.CloseHandle.Fn = Meta.CloseHandle.defaultImpl,

    WaitSynchronization: Meta.WaitSynchronization.Fn = Meta.WaitSynchronization.defaultImpl,
    WaitSynchronizationN: Meta.WaitSynchronizationN.Fn = Meta.WaitSynchronizationN.defaultImpl,
    SignalAndWait: Meta.SignalAndWait.Fn = Meta.SignalAndWait.defaultImpl,
    SendSyncRequest1: Meta.SendSyncRequest1.Fn = Meta.SendSyncRequest1.defaultImpl,
    SendSyncRequest2: Meta.SendSyncRequest2.Fn = Meta.SendSyncRequest2.defaultImpl,
    SendSyncRequest3: Meta.SendSyncRequest3.Fn = Meta.SendSyncRequest3.defaultImpl,
    SendSyncRequest4: Meta.SendSyncRequest4.Fn = Meta.SendSyncRequest4.defaultImpl,
    SendSyncRequest: Meta.SendSyncRequest.Fn = Meta.SendSyncRequest.defaultImpl,

    GetSystemTick: Meta.GetSystemTick.Fn = Meta.GetSystemTick.defaultImpl,
    GetHandleInfo: Meta.GetHandleInfo.Fn = Meta.GetHandleInfo.defaultImpl,
    GetSystemInfo: Meta.GetSystemInfo.Fn = Meta.GetSystemInfo.defaultImpl,
    GetProcessInfo: Meta.GetProcessInfo.Fn = Meta.GetProcessInfo.defaultImpl,
    GetThreadInfo: Meta.GetThreadInfo.Fn = Meta.GetThreadInfo.defaultImpl,
    ConnectToPort: Meta.ConnectToPort.Fn = Meta.ConnectToPort.defaultImpl,
    OpenProcess: Meta.OpenProcess.Fn = Meta.OpenProcess.defaultImpl,
    OpenThread: Meta.OpenThread.Fn = Meta.OpenThread.defaultImpl,
    GetProcessId: Meta.GetProcessId.Fn = Meta.GetProcessId.defaultImpl,
    GetProcessIdOfThread: Meta.GetProcessIdOfThread.Fn = Meta.GetProcessIdOfThread.defaultImpl,
    GetThreadId: Meta.GetThreadId.Fn = Meta.GetThreadId.defaultImpl,

    CreateResourceLimits: Meta.CreateResourceLimits.Fn = Meta.CreateResourceLimits.defaultImpl,
    SetProcessResourceLimits: Meta.SetProcessResourceLimits.Fn = Meta.SetProcessResourceLimits.defaultImpl,
    SetResourceLimitValues: Meta.SetResourceLimitValues.Fn = Meta.SetResourceLimitValues.defaultImpl,
    GetResourceLimits: Meta.GetResourceLimits.Fn = Meta.GetResourceLimits.defaultImpl,
    GetResourceLimitsLimitValues: Meta.GetResourceLimitsLimitValues.Fn = Meta.GetResourceLimitsLimitValues.defaultImpl,
    GetResourceLimitsCurrentValues: Meta.GetResourceLimitsCurrentValues.Fn = Meta.GetResourceLimitsCurrentValues.defaultImpl,

    GetThreadContext: Meta.GetThreadContext.Fn = Meta.GetThreadContext.defaultImpl,
    Break: Meta.Break.Fn = Meta.Break.defaultImpl,
    OutputDebugString: Meta.OutputDebugString.Fn = Meta.OutputDebugString.defaultImpl,
    ControlPerformanceCounter: Meta.ControlPerformanceCounter.Fn = Meta.ControlPerformanceCounter.defaultImpl,
    CreatePort: Meta.CreatePort.Fn = Meta.CreatePort.defaultImpl,
    CreateSessionToPort: Meta.CreateSessionToPort.Fn = Meta.CreateSessionToPort.defaultImpl,
    CreateSession: Meta.CreateSession.Fn = Meta.CreateSession.defaultImpl,
    AcceptSession: Meta.AcceptSession.Fn = Meta.AcceptSession.defaultImpl,
    ReplyAndReceive1: Meta.ReplyAndReceive1.Fn = Meta.ReplyAndReceive1.defaultImpl,
    ReplyAndReceive2: Meta.ReplyAndReceive2.Fn = Meta.ReplyAndReceive2.defaultImpl,
    ReplyAndReceive3: Meta.ReplyAndReceive3.Fn = Meta.ReplyAndReceive3.defaultImpl,
    ReplyAndReceive4: Meta.ReplyAndReceive4.Fn = Meta.ReplyAndReceive4.defaultImpl,
    ReplyAndReceive: Meta.ReplyAndReceive.Fn = Meta.ReplyAndReceive.defaultImpl,

    BindInterrupt: Meta.BindInterrupt.Fn = Meta.BindInterrupt.defaultImpl,
    UnbindInterrupt: Meta.UnbindInterrupt.Fn = Meta.UnbindInterrupt.defaultImpl,

    InvalidateProcessDataCache: Meta.InvalidateProcessDataCache.Fn = Meta.InvalidateProcessDataCache.defaultImpl,
    StoreProcessDataCache: Meta.StoreProcessDataCache.Fn = Meta.StoreProcessDataCache.defaultImpl,
    FlushProcessDataCache: Meta.FlushProcessDataCache.Fn = Meta.FlushProcessDataCache.defaultImpl,

    StartInterProcessDma: Meta.StartInterProcessDma.Fn = Meta.StartInterProcessDma.defaultImpl,
    StopDma: Meta.StopDma.Fn = Meta.StopDma.defaultImpl,
    GetDmaState: Meta.GetDmaState.Fn = Meta.GetDmaState.defaultImpl,
    RestartDma: Meta.RestartDma.Fn = Meta.RestartDma.defaultImpl,

    SetGpuProt: Meta.SetGpuProt.Fn = Meta.SetGpuProt.defaultImpl,
    SetWifiEnabled: Meta.SetWifiEnabled.Fn = Meta.SetWifiEnabled.defaultImpl,

    DebugActiveProces: Meta.DebugActiveProces.Fn = Meta.DebugActiveProces.defaultImpl,
    BreakDebugProcess: Meta.BreakDebugProcess.Fn = Meta.BreakDebugProcess.defaultImpl,
    TerminateDebugProcess: Meta.TerminateDebugProcess.Fn = Meta.TerminateDebugProcess.defaultImpl,
    GetProcessDebugEvent: Meta.GetProcessDebugEvent.Fn = Meta.GetProcessDebugEvent.defaultImpl,
    ContinueDebugEvent: Meta.ContinueDebugEvent.Fn = Meta.ContinueDebugEvent.defaultImpl,
    GetProcessList: Meta.GetProcessList.Fn = Meta.GetProcessList.defaultImpl,
    GetThreadList: Meta.GetThreadList.Fn = Meta.GetThreadList.defaultImpl,
    GetDebugThreadContext: Meta.GetDebugThreadContext.Fn = Meta.GetDebugThreadContext.defaultImpl,
    SetDebugThreadContext: Meta.SetDebugThreadContext.Fn = Meta.SetDebugThreadContext.defaultImpl,
    QueryDebugProcessMemory: Meta.QueryDebugProcessMemory.Fn = Meta.QueryDebugProcessMemory.defaultImpl,
    ReadProcessMemory: Meta.ReadProcessMemory.Fn = Meta.ReadProcessMemory.defaultImpl,
    WriteProcessMemory: Meta.WriteProcessMemory.Fn = Meta.WriteProcessMemory.defaultImpl,
    SetHardwareBreakPoint: Meta.SetHardwareBreakPoint.Fn = Meta.SetHardwareBreakPoint.defaultImpl,
    GetDebugThreadParam: Meta.GetDebugThreadParam.Fn = Meta.GetDebugThreadParam.defaultImpl,
    ControlProcessMemory: Meta.ControlProcessMemory.Fn = Meta.ControlProcessMemory.defaultImpl,
    MapProcessMemory: Meta.MapProcessMemory.Fn = Meta.MapProcessMemory.defaultImpl,
    UnmapProcessMemory: Meta.UnmapProcessMemory.Fn = Meta.UnmapProcessMemory.defaultImpl,
    QueryProcessMemory: Meta.QueryProcessMemory.Fn = Meta.QueryProcessMemory.defaultImpl,

    CreateCodeSet: Meta.CreateCodeSet.Fn = Meta.CreateCodeSet.defaultImpl,
    RandomStub: Meta.RandomStub.Fn = Meta.RandomStub.defaultImpl,
    CreateProcess: Meta.CreateProcess.Fn = Meta.CreateProcess.defaultImpl,
    TerminateProcess: Meta.TerminateProcess.Fn = Meta.TerminateProcess.defaultImpl,
    // AddCodeSegment: Meta.AddCodeSegment.Fn = Meta.AddCodeSegment.defaultImpl,

    Backdoor: Meta.Backdoor.Fn = Meta.Backdoor.defaultImpl,

    KernelOperation: Meta.KernelOperation.Fn = Meta.KernelOperation.defaultImpl,
    // StopPoint: Meta.StopPoint.Fn= Meta.StopPoint.defaultImpl,

    // DeviceTreeControl: Meta.DeviceTreeControl.Fn = Meta.DeviceTreeControl.defaultImpl,
    MessageTap: Meta.MessageTap.Fn = Meta.MessageTap.defaultImpl,
};

comptime {
    for (@typeInfo(Names).@"enum".fields) |field| {
        if (!@hasField(Interface, field.name)) {
            @compileLog(field.name ++ " is missing from syscall.Interface");
        }
        if (!@hasDecl(Meta, field.name)) {
            @compileLog(field.name ++ " is missing from syscall.Meta");
        }
    }
    for (@typeInfo(Interface).@"struct".fields) |field| {
        if (!@hasField(Names, field.name)) {
            @compileLog("syscall.Interface contains undefined syscall " ++ field.name ++ ", update syscall.Names");
        }
    }
    for (@typeInfo(Meta).@"struct".decls) |decl| {
        if (!@hasField(Names, decl.name)) {
            @compileLog("syscall.Meta contains undefined syscall " ++ decl.name ++ ", update syscall.Names");
        }
    }
}
