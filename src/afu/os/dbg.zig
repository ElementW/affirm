// SPDX-License-Identifier: EUPL-1.2
const afu = @import("../afu.zig");

const pm = afu.os.pm;

/// https://www.3dbrew.org/wiki/SVC#struct_DebugEventInfo
pub const Event = enum(u32) {
    ProcessCreate,
    ThreadCreate,
    ThreadExit,
    ProcessExit,
    Exception,
    DynamicLibraryLoad,
    DynamicLibraryUnload,
    ScheduleStart,
    ScheduleEnd,
    SyscallStart,
    SyscallEnd,
    DebugOutput,
    MemoryMap,
};

pub const BreakRegisterId = afu.os.arch.target.dbg.BreakRegisterId;

pub const EventInfo = struct {
    // TODO
};

pub const Flags = enum {
    // TODO
};

pub const ThreadContext = afu.os.arch.target.dbg.ThreadContext;

/// https://www.3dbrew.org/wiki/SVC#enum_DebugThreadParameter
pub const ThreadParameter = union(Type) {
    pub const Type = enum(u32) {
        priority = 0,
        state = 1,
        ideal_core = 2,
        creator_core = 3,
    };

    priority: pm.Thread.Priority,
    state: pm.Thread.State,
    ideal_core: pm.Core.Id,
    creator_core: pm.Core.Id,
};

pub const Debug = opaque {};

pub const PerformanceCounter = struct {
    pub const Name = enum(u32) { _ };
    /// https://github.com/devkitPro/libctru/blob/a88b5af7a84d994fa24e258ce9dbf34e009c2c7b/libctru/include/3ds/svc.h#L198
    pub const Operation = union(Type) {
        pub const Type = enum(u32) {
            enable = 0,
            disable = 1,
            get_value = 2,
            set_value = 3,
            get_overflow_flags = 4,
            reset = 5,
            get_event = 6,
            set_event = 7,
            set_virtual_counter_enabled = 8,
        };
        pub const Result = union(Type) {
            enable: void,
            disable: void,
            get_value: u64,
            set_value: void,
            get_overflow_flags: u32,
            reset: void,
            get_event: PerformanceCounter.Event,
            set_event: void,
            set_virtual_counter_enabled: void,
        };
        /// Enable and lock performance monitoring functionality.
        enable: void,
        /// Disable and forcibly unlock performance monitoring functionality.
        disable: void,
        /// Get the value of a counter register.
        get_value: Name,
        /// Set the value of a counter register.
        set_value: struct { name: Name, value: u64 },
        /// Get the overflow flags for all CP15 and SCU counters.
        get_overflow_flags: void,
        /// Reset the value and/or overflow flags of selected counters.
        reset: void,
        /// Get the event ID associated to a particular counter.
        get_event: Name,
        /// Set the event ID associated to a paritcular counter.
        set_event: struct { name: Name, event: PerformanceCounter.Event },
        /// (Dis)allow the kernel to track counter overflows and to use 64-bit counter values.
        set_virtual_counter_enabled: bool,
    };
    pub const Event = enum(u32) {
        _,
    };
};
