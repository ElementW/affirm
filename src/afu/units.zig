// SPDX-License-Identifier: EUPL-1.2
pub fn typealias(comptime T: type) type {
    return T;
}

pub const Bytes = typealias;
pub const Kibibytes = typealias;
pub const Mebibytes = typealias;
pub const Gibibytes = typealias;
pub fn Kilobytes(comptime _: type) void {
    @compileError("Kilobytes use is not permitted");
}
pub fn Megabytes(comptime _: type) void {
    @compileError("Megabytes use is not permitted");
}
pub fn Gigabytes(comptime _: type) void {
    @compileError("Gigabytes use is not permitted");
}

pub const Seconds = typealias;
pub const Milliseconds = typealias;
pub const Microseconds = typealias;
pub const Nanoseconds = typealias;

pub const Ticks = typealias;
