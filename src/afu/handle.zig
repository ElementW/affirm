// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");

/// Opaque resource pointer.
pub const Handle = enum(u32) {
    const AliasHandleBase = 0xFFFF8000;
    invalid = 0,
    current_thread = AliasHandleBase,
    current_process = AliasHandleBase + 1,
    _,

    /// Specialisation of an opaque resource pointer for the given type.
    /// Used for lightweight type checking.
    pub fn Of(comptime T: type) type {
        return enum(u32) {
            pub const Type = T;
            invalid = 0,
            _,

            pub inline fn toHandle(self: @This()) Handle {
                return @enumFromInt(@intFromEnum(self));
            }
            pub inline fn raw(self: @This()) Handle {
                return @enumFromInt(@intFromEnum(self));
            }
        };
    }

    /// Specialise this handle into a typed version.
    /// Performs no type checking by itself because handles are opaque.
    pub inline fn of(self: Handle, comptime T: type) Of(T) {
        return @enumFromInt(@intFromEnum(self));
    }
};

comptime {
    std.debug.assert(std.meta.hasUniqueRepresentation(Handle));
}
