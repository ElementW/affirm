// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afu = @import("afu");

const assert = std.debug.assert;
const SDError = afu.io.sdmmc.SDError;

const ResponseType = enum(u3) {
    /// No response expected
    None = 3,
    /// R1: normal response command
    R1 = 4,
    /// R1b: R1 with busy flag set
    R1B = 5,
    R2 = 6,
    R3 = 7,

    pub const R4 = ResponseType.R3;
    pub const R5 = ResponseType.R1;
    pub const R6 = ResponseType.R1;
    pub const R7 = ResponseType.R1;
};

/// See SD Spec Table 4-40: Voltage Accepted in R7
const SupplyVoltage = packed struct(u4) {
    v2_7to3_6: bool,
    low_boltage: bool,
    _: u2,
};

/// Bits 15-23 in SD Spec Table 5-1: OCR Register Definition
const VoltageRange = packed struct(u9) {
    v2_7to2_8: bool,
    v2_8to2_9: bool,
    v2_9to3_0: bool,
    v3_0to3_1: bool,
    v3_1to3_2: bool,
    v3_2to3_3: bool,
    v3_3to3_4: bool,
    v3_4to3_5: bool,
    v3_5to3_6: bool,
};

const IOVoltageRange = packed struct(u24) {
    _: u8,
    v2_0to2_1: bool,
    v2_1to2_2: bool,
    v2_2to2_3: bool,
    v2_4to2_4: bool,
    v2_4to2_5: bool,
    v2_5to2_6: bool,
    v2_6to2_7: bool,
    v2_7to2_8: bool,
    v2_8to2_9: bool,
    v2_9to3_0: bool,
    v3_0to3_1: bool,
    v3_1to3_2: bool,
    v3_2to3_3: bool,
    v3_3to3_4: bool,
    v3_4to3_5: bool,
    v3_5to3_6: bool,
};

/// See SD Spec Table 4-41: Card Status
const State = enum(u4) {
    idle = 0,
    ready = 1,
    ident = 2,
    standby = 3,
    transfer = 4,
    data = 5,
    receive = 6,
    program = 7,
    disabled = 8,
    io = 15,
};

/// See SD Spec Table 4-41: Card Status
const CardStatus = packed struct(u32) {
    _1: u2,
    application_specific: bool,
    ake_seq_error: bool,
    _2: u1,
    app_cmd: bool,
    _3: u2,
    ready_for_data: bool,
    current_state: State,
    erase_reset: bool,
    card_ecc_disabled: bool,
    wp_erase_skip: bool,
    csd_overwrite: bool,
    _4: u2,
    @"error": bool,
    card_controller_error: bool,
    card_ecc_failed: bool,
    illegal_command: bool,
    com_crc_error: bool,
    lock_unlock_failed: bool,
    card_is_locked: bool,
    wp_violation: bool,
    erase_param: bool,
    erase_seq_error: bool,
    block_len_error: bool,
    address_error: bool,
    out_of_range: bool,
};

/// See SD Spec Table 5-1: OCR Register Definition
const OCR = packed struct(u32) {
    _1: u7,
    low_voltage_range: bool,
    _2: u7,
    voltage_range: VoltageRange,
    switch_1_8v_ccepted: bool,
    _3: u4,
    uhs2: bool,
    card_capacity_status: bool,
    power_up_busy: bool,
};

/// See SDIO Spec Table 3-1: OCR Values for CMD5
const IOOCR = packed struct(u24) {
    voltage_range: IOVoltageRange,
};
comptime {
    assert(@bitSizeOf(IOOCR) == 3 * 8);
}

const CID = extern union {
    const SD = extern struct {
        manufacturer_id: u8,
        oem_id: u16 align(1),
        product_name: [5]u8,
        product_revision: u8,
        product_serial_number: u32 align(1),
        manufacturing_date: u16 align(1),
        crc7: u8,
    };
    const MMC = extern struct {
        manufacturer_id: u8,
        oem_id: u16 align(1),
        product_name: [6]u8 align(1),
        product_revision: u8,
        product_serial_number: u32 align(1),
        manufacturing_date: u8,
        crc7: u8,
    };

    sd: SD,
    mmc: MMC,
};
comptime {
    assert(@sizeOf(CID.SD) == 16);
    assert(@sizeOf(CID.MMC) == 16);
}

const CSD = packed union {
    const Version = enum(u2) {
        v1 = 0b00,
        v2 = 0b01,
    };
    const Generic = packed struct {
        csd_version: Version,
        _1: u30,
        _2: u32,
        _3: u32,
        _4: u32,
    };
    const UnscaledValue = enum(u4) {
        reserved = 0,
        t1_0 = 1,
        t1_2 = 2,
        t1_3 = 3,
        t1_5 = 4,
        t2_0 = 5,
        t2_5 = 6,
        t3_0 = 7,
        t3_5 = 8,
        t4_0 = 9,
        t4_5 = 10,
        t5_0 = 11,
        t5_5 = 12,
        t6_0 = 13,
        t7_0 = 14,
        t8_0 = 15,

        pub fn actualTimes10(self: @This()) u8 {
            return switch (self) {
                .t1_0 => 10,
                .t1_2 => 12,
                .t1_3 => 13,
                .t1_5 => 15,
                .t2_0 => 20,
                .t2_5 => 25,
                .t3_0 => 30,
                .t3_5 => 35,
                .t4_0 => 40,
                .t4_5 => 45,
                .t5_0 => 50,
                .t5_5 => 55,
                .t6_0 => 60,
                .t7_0 => 70,
                .t8_0 => 80,
                else => 0,
            };
        }
    };
    const CurrentRange = packed struct(u6) {
        pub const RangeBound = enum(u3) {
            milliamp0_5 = 0,
            milliamp1 = 1,
            milliamp5 = 2,
            milliamp10 = 3,
            milliamp25 = 4,
            milliamp35 = 5,
            milliamp60 = 6,
            milliamp100 = 7,
        };
        min: RangeBound,
        max: RangeBound,
    };
    const V1 = packed struct {
        const Self = @This();

        csd_version: Version,
        mmc_spec_version: u4,
        _1: u2,
        data_read_access_time1: packed struct(u8) {
            unit: enum(u3) {
                nanosecond1 = 0,
                nanosecond10 = 1,
                nanosecond100 = 2,
                microsecond1 = 3,
                microsecond10 = 4,
                microsecond100 = 5,
                millisecond1 = 6,
                millisecond10 = 7,

                pub fn nanosecondFactor(self: @This()) u32 {
                    return switch (self.unit) {
                        .nanosecond1 => 1,
                        .nanosecond10 => 10,
                        .nanosecond100 => 100,
                        .microsecond1 => 1_000,
                        .microsecond10 => 10_000,
                        .microsecond100 => 100_000,
                        .millisecond1 => 1_000_000,
                        .millisecond1 => 10_000_000,
                    };
                }
            },
            value: UnscaledValue,
            _: u1 = 0,

            //pub fn nanoseconds(self: @This()) u32 {
            //    return self.unit.nanosecondFactor() * @compileError("Not implemented");
            //}
        },
        data_read_access_time2: u8,
        transfer_speed: packed struct(u8) {
            unit: enum(u3) {
                kbit_per_second100 = 0,
                mbit_per_second1 = 1,
                mbit_per_second10 = 2,
                mbit_per_second100 = 3,
            },
            value: UnscaledValue,
            _: u1 = 0,
        },
        card_command_classes: CardCommandClasses,
        /// Size of a read block.
        read_block_length: packed struct(u4) {
            log2: u4,

            pub fn actual(self: @This()) u32 {
                return 1 << self.log2;
            }
        },
        /// If data smaller than `read_block_length` can be read.
        partial_read_allowed: bool,
        /// If data not aligned to `write_block_length` size can be written.
        write_block_misalignment: bool,
        /// If data not aligned to `read_block_length` can be read.
        read_block_misalignment: bool,
        /// If the card features a configurable driver stage.
        dsr_implemented: bool,
        _2: u2,
        /// The usable storage capacity of the device.
        device_size: packed struct(u12) {
            c_size: u12,

            pub fn actual(csd: V1) u32 {
                const mult = 1 << (csd.device_size_multiplier + 2);
                const block_nr = (csd.device_size.c_size + 1) * mult;
                const block_len = csd.read_block_length.actual();
                return block_nr * block_len;
            }
        },
        /// Range for current comsumption during a read operation.
        vdd_r_current: CurrentRange,
        /// Range for current comsumption during a write operation.
        vdd_w_current: CurrentRange,
        /// Multiplier used for computation of device storage capacity.
        device_size_multiplier: u3,
        /// If individual 512 byte blocks can be erased, or only multiples of `sector_size`.
        erase_single_block: bool,
        /// Size of a sector.
        sector_size: packed struct(u7) {
            raw: u7,

            pub fn blocks(self: @This()) u8 {
                return self.raw + 1;
            }
            pub fn bytes(self: @This(), csd: V1) u32 {
                return csd.write_block_length * self.blocks();
            }
        },
        /// Size of a write protected group.
        write_protect_group_size: packed struct(u7) {
            raw: u7,

            pub fn sectors(self: @This()) u8 {
                return self.raw + 1;
            }
            pub fn bytes(self: @This(), csd: V1) u32 {
                return csd.sector_size.bytes() * self.sectors();
            }
        },
        /// If write protected groups are available.
        write_protect_group_enable: bool,
        // TODO
        default_ecc: u2,
        /// Typical block write time.
        write_speed: packed struct(u3) {
            raw: u3,

            pub fn factor(self: @This()) u8 {
                return 1 << self.raw;
            }
            //pub fn todo(self: @This(), csd: V1) u32 {
            //    _ = csd;
            //    return self.factor() * @compileError("Not implemented"); //csd.data_read_access_time1.
            //}
        },
        max_write_block_length: u4,
        partial_write_allowed: bool,
        _3: u5,
        file_format_group: bool,
        copy_glag: bool,
        permanent_write_protection: bool,
        temporary_write_protection: bool,
        file_format: u2,
        ecc_code: u2,
        crc: u7,
        _4: u1,

        pub fn cardCommandClasses(self: Self) u16 {
            return self.cardCommandClasses_hi << 4 | self.cardCommandClasses_lo;
        }
        pub fn deviceSize(self: Self) u16 {
            return self.deviceSize_hi << 8 | self.deviceSize_lo;
        }
    };
    const V2 = packed struct {
        csd_version: Version,
        _1: u30,
        _2: u32,
        _3: u32,
        _4: u32,
    };
    generic: Generic,
    v1: V1,
    v2: V2,
};
comptime {
    assert(@sizeOf(CSD.Generic) == 16);
    assert(@sizeOf(CSD.V1) == 16);
    assert(@sizeOf(CSD.V2) == 16);
}

const DSR = packed struct(u16) {
    pre_driver_switch_time: enum(u8) {
        @"500ns_to_200ns" = 0,
        @"50ns_to_100ns" = 1,
        @"10ns_to_20ns" = 2,
        @"2ns_to_5ns" = 3,
    },
    driver_transistor_current: enum(u8) {
        @"1mA_to_2mA" = 0,
        @"5mA_to_10mA" = 1,
        @"20mA_to_50mA" = 2,
        @"100mA_to_200mA" = 3,
    },
};

const SDSpecVersion = struct {
    major: u8,
    minor: u8,
};

const SCR = packed struct(u64) {
    _1: u32, // Reserved
    cmd_support: packed struct(u5) {
        secure_receive_send: bool,
        extension_register_multi_block: bool,
        extension_reguster_single_block: bool,
        set_block_count: bool,
        speed_class_control: bool,
    },
    _2: u1, // Reserved
    spec_version_5: u4,
    spec_version_4: u1,
    /// If Extended Security (Part A4 Data Protection System Specification) is supported.
    extended_security_supported: u4,
    spec_version_3: u1,
    dat_bus_widths_supported: packed struct(u4) {
        bit1: bool,
        _1: bool,
        bit4: bool,
        _2: bool,
    },
    sd_security_support: enum(u3) {
        no_security = 0,
        sdsc = 2,
        sdhc = 3,
        sdxc = 4,
    },
    data_status_after_erases: bool,
    spec_version_1: u4,
    scr_structure: enum(u4) {
        version1 = 0,
    },

    pub fn specVersion(self: @This()) SDSpecVersion {
        if (self.spec_version_1 == 0) {
            return SDSpecVersion{ .major = 1, .minor = 0 };
        } else if (self.spec_version_1 == 1) {
            return SDSpecVersion{ .major = 1, .minor = 10 };
        } else if (self.spec_version_3 == 0) {
            return SDSpecVersion{ .major = 2, .minor = 0 };
        } else if (self.spec_version_4 == 0 and self.spec_version_5 == 0) {
            return SDSpecVersion{ .major = 3, .minor = 0 };
        } else if (self.spec_version_4 == 1 and self.spec_version_5 == 0) {
            return SDSpecVersion{ .major = 4, .minor = 0 };
        }
        return SDSpecVersion{ .major = self.spec_version_5 + 4, .minor = 0 };
    }
};

const RCA = u16;

/// See Linux ``drivers/mmc/host/tmio_mmc_core.c``
const ControllerCommand = packed struct(u16) {
    const Type = enum(u1) {
        CMD = 0,
        ACMD = 1,
    };

    /// SD Command index
    index: Command.Index,
    /// Whether the command is regular or an application command
    type: Type,
    _1: u1 = 0,
    response_type: ResponseType,
    data_present: bool,
    transfer_read: bool,
    transfer_multi: bool,
    security_cmd: bool,
    _2: u1 = 0,
};

/// See SD Spec Table 4-21: Card Command Classes (CCCs) in SD Mode
const CardCommandClasses = packed struct(u12) {
    basic: bool = false,
    command_queue: bool = false,
    block_read: bool = false,
    _: bool = false,
    block_write: bool = false,
    erase: bool = false,
    write_protection: bool = false,
    lock_card: bool = false,
    application_specific: bool = false,
    io_mode: bool = false,
    @"switch": bool = false,
    extension: bool = false,
};

const DataTransfer = packed struct(u2) {
    rx: bool,
    tx: bool,

    pub const none: DataTransfer = .{ .rx = false, .tx = false };
    pub const send: DataTransfer = .{ .rx = false, .tx = true };
    pub const recv: DataTransfer = .{ .rx = true, .tx = false };
    pub const both: DataTransfer = .{ .rx = true, .tx = true };
};

pub const Command = struct {
    const Self = @This();
    const Type = enum {
        broadcast,
        broadcast_response,
        addressed,
        addressed_data_transfer,
    };
    const Classes = CardCommandClasses;
    /// See SD Spec Table 4-20: Command Format
    const Index = u6;
    const Response = packed union {
        /// See SD Spec Table 4-35: Response R1
        const R1 = packed union {
            status: CardStatus,
            uint32: u32,
            uint16: [2]u16,
        };
        /// See SD Spec Table 4-36: Response R2
        const R2 = packed union {
            cid: CID,
            csd: CSD,
            csdv1: CSD.V1,
            csdv2: CSD.V2,
            uint32: [4]u32,
            uint16: [8]u16,
        };
        /// See SD Spec Table 4-37: Response R3
        const R3 = packed union {
            ocr: OCR,
            uint32: u32,
            uint16: [2]u16,
        };
        /// See SDIO Spec §3.3 The IO_SEND_OP_COND Response (R4)
        const R4 = packed union {
            uint32: u32,
            uint16: [2]u16,
        };
        /// See SDIO Spec §5.2 IO_RW_DIRECT Response (R5)
        const R5 = packed union {
            const Arg = packed struct {
                data: u8,
                flags: u8,
                _: u16,
            };
            arg: Arg,
            uint32: u32,
            uint16: [2]u16,
        };
        /// See SD Spec Table 4-38: Response R6
        const R6 = packed union {
            const Arg = packed struct(u32) {
                card_status_bits: u16,
                new_pblished_rca: u16,
            };
            arg: Arg,
            uint32: u32,
            uint16: [2]u16,
        };
        // See SD Spec Table 4-39: Response R7
        const R7 = packed union {
            const Arg = packed struct(u32) {
                check_pattern: u8,
                voltage_accepted: SupplyVoltage,
                _: u20,
            };
            arg: Arg,
            uint32: u32,
            uint16: [2]u16,
        };

        r1: R1,
        r2: R2,
        r3: R3,
        r4: R4,
        r5: R5,
        r6: R6,
        r7: R7,

        pub fn clear(self: *@This()) void {
            @memset(@as([*]u8, self)[0..@sizeOf(Self)], 0);
        }
    };

    name: []const u8,
    type: Type,
    classes: Classes,
    data_transfer: DataTransfer,
    controller_cmd: ControllerCommand,

    fn make(name: []const u8, idx: Index, @"type": Type, rsp: ResponseType, classes: Classes, data_transfer: DataTransfer) Self {
        return Self{
            .name = name,
            .type = @"type",
            .classes = classes,
            .data_transfer = data_transfer,
            .controller_cmd = ControllerCommand{
                .index = idx,
                .type = if (classes.application_specific) .ACMD else .CMD,
                .response_type = rsp,
                .data_present = data_transfer.tx or data_transfer.rx,
                .transfer_read = false,
                .transfer_multi = false,
                .security_cmd = false,
            },
        };
    }
};

// SDIO commands: See SDIO Spec Table C-1
const Specification = enum {
    MMC,
    SD,
    SDIO,
};
pub const Commands = struct {
    /// Broadcast Command
    fn bc(spec: Specification, name: []const u8, idx: Command.Index, classes: Command.Classes) Command {
        _ = spec;
        return Command.make(name, idx, .broadcast, .None, classes, .{ .rx = false, .tx = false });
    }
    /// Broadcast Command with Response
    fn bcr(spec: Specification, name: []const u8, idx: Command.Index, rsp: ResponseType, classes: Command.Classes) Command {
        _ = spec;
        return Command.make(name, idx, .broadcast_response, rsp, classes, .{ .rx = false, .tx = false });
    }
    /// Addressed Command
    fn ac(spec: Specification, name: []const u8, idx: Command.Index, rsp: ResponseType, classes: Command.Classes) Command {
        _ = spec;
        return Command.make(name, idx, .addressed, rsp, classes, .{ .rx = false, .tx = false });
    }
    /// Addressed Data Transfer Command
    fn adtc(spec: Specification, name: []const u8, idx: Command.Index, transfer: DataTransfer, rsp: ResponseType, classes: Command.Classes) Command {
        _ = spec;
        return Command.make(name, idx, .addressed_data_transfer, rsp, classes, transfer);
    }

    // Basic commands. See SD Spec Table 4-22
    pub const GoIdleState = packed struct(u32) {
        pub const Info = bc(.MMC, "GO_IDLE_STATE", 0, .{ .basic = true });
        _: u32,
    };
    pub const SendOpCond = packed struct(u32) {
        // Reserved in SD and SDIO
        pub const Info = bcr(.MMC, "SEND_OP_COND", 1, .R3, .{ .basic = true });
        ocr: OCR,
    };
    pub const AllSendCid = packed struct(u32) {
        pub const Info = bcr(.MMC, "ALL_SEND_CID", 2, .R2, .{ .basic = true });
        _: u32,
    };
    pub const SetRelativeAddr = packed struct(u32) {
        pub const Info = ac(.MMC, "SET_RELATIVE_ADDR", 3, .R3, .{ .basic = true });
        _: u16,
        rca: RCA,
    };
    pub const SendRelativeAddr = packed struct(u32) {
        pub const Info = bc(.SD, "SEND_RELATIVE_ADDR", 3, .{ .basic = true });
        _: u32,
    };
    pub const SetDSR = packed struct(u32) {
        pub const Info = bc(.MMC, "SET_DSR", 4, .{ .basic = true });
        _: u16,
        dsr: DSR,
    };
    pub const IOSendOpCond = packed struct(u32) {
        pub const Info = bcr(.SDIO, "IO_SEND_OP_COND", 5, .R4, .{ .io_mode = true });
        voltage_range: IOVoltageRange,
        switch1V8_request: bool,
        _: u7,
    };
    pub const SwitchFunc = packed struct(u32) {
        pub const Info = adtc(.SD, "SWITCH_FUNC", 6, .none, .R1, .{ .@"switch" = true });
        access_mode: u4,
        command_system: u4,
        drive_strength: u4,
        power_limit: u4,
        function_group5: u4,
        function_group6: u4,
        _: u7,
        mode: enum(u1) {
            Check = 0,
            Switch = 1,
        },
    };
    pub const SelectCard = packed struct(u32) {
        pub const Info = ac(.MMC, "SELECT_CARD", 7, .R1B, .{ .basic = true });
        _: u16,
        rca: RCA,
    };
    pub const SendIfCond = packed struct(u32) {
        pub const Info = bcr(.SD, "SEND_IF_COND", 8, .R7, .{ .basic = true });
        check_pattern: u8,
        supply_voltage: SupplyVoltage,
        PCIe_availability: bool,
        PCIe_1V2_availability: bool,
        _: u18,
    };
    pub const SendCSD = packed struct(u32) {
        pub const Info = ac(.MMC, "SEND_CSD", 9, .R2, .{ .basic = true });
        _: u16,
        rca: RCA,
    };
    pub const SendCID = packed struct(u32) {
        pub const Info = ac(.MMC, "SEND_CID", 10, .R2, .{ .basic = true });
        _: u16,
        rca: RCA,
    };
    pub const ReadDatUntilStop = packed struct(u32) {
        pub const Info = adtc(.MMC, "READ_DAT_UNTIL_STOP", 11, .recv, .R1, .{ .basic = true });
        address: u32,
    };
    pub const VoltageSwitch = packed struct(u32) {
        pub const Info = ac(.SD, "VOLTAGE_SWITCH", 11, .R1, .{ .basic = true });
        _: u32,
    };
    pub const StopTransmission = packed struct(u32) {
        pub const Info = ac(.MMC, "STOP_TRANSMISSION", 12, .R1B, .{ .basic = true });
        _: u32,
    };
    pub const SendStatus = packed struct(u32) {
        pub const Info = ac(.MMC, "SEND_STATUS", 13, .R1, .{ .basic = true });
        _: u15,
        send_task_status_register: bool,
        rca: RCA,
    };
    // 14: Reserved (EMMC)
    pub const GoInactiveState = packed struct(u32) {
        pub const Info = ac(.MMC, "GO_INACTIVE_STATE", 15, .None, .{ .basic = true });
        _: u16,
        rca: RCA,
    };

    // Block-oriented commands. See SD Spec Table 4-23 & 4-24
    pub const SetBlocklen = packed struct(u32) {
        pub const Info = ac(.MMC, "SET_BLOCKLEN", 16, .R1, .{ .block_read = true, .block_write = true, .erase = true });
        block_length: u32,
    };
    pub const ReadSingleBlock = packed struct(u32) {
        pub const Info = adtc(.MMC, "READ_SINGLE_BLOCK", 17, .recv, .R1, .{ .block_read = true });
        address: u32,
    };
    pub const ReadMultipleBlock = packed struct(u32) {
        pub const Info = adtc(.MMC, "READ_MULTIPLE_BLOCK", 18, .recv, .R1, .{ .block_read = true });
        address: u32,
    };
    pub const SendTuningBlock = packed struct(u32) {
        pub const Info = adtc(.SD, "SEND_TUNING_BLOCK", 19, .recv, .R1, .{ .block_read = true });
        _: u32,
    };
    pub const WriteDatUntilStop = packed struct(u32) {
        pub const Info = adtc(.MMC, "WRITE_DAT_UNTIL_STOP", 20, .send, .R1, .{ .block_read = true, .block_write = true });
        address: u32,
    };
    pub const SpeedClassControl = packed struct(u32) {
        pub const Info = ac(.SD, "SPEED_CLASS_CONTROL", 20, .R1B, .{ .block_read = true, .block_write = true });
        _: u32, // TODO
    };
    // 21: Reserved for DPS Specification
    // 22: Reserved
    pub const SetBlockCount = packed struct(u32) {
        pub const Info = ac(.MMC, "SET_BLOCK_COUNT", 23, .R1, .{ .block_read = true, .block_write = true });
        number_of_blocks: u16,
        _: u16,
    };
    pub const WriteBlock = packed struct(u32) {
        pub const Info = adtc(.MMC, "WRITE_BLOCK", 24, .send, .R1, .{ .block_write = true });
        address: u32,
    };
    pub const WriteMultipleBlock = packed struct(u32) {
        pub const Info = adtc(.MMC, "WRITE_MULTIPLE_BLOCK", 25, .send, .R1, .{ .block_write = true });
        address: u32,
    };
    // 26: TODO adtc(.MMC, "PROGRAM_CID", 26, .send, .R1, .???);
    pub const ProgramCSD = packed struct(u32) {
        pub const Info = adtc(.MMC, "PROGRAM_CSD", 27, .send, .R1, .{ .block_write = true });
        _: u32,
    };
    // 28: TODO SD SET_WRITE_PROT
    // 29: TODO SD CLR_WRITE_PROT
    // 30: TODO SD SEND_WRITE_PROT
    // 31: SD Reserved
    // 32: TODO SD ERASE_WR_BLK_START
    // 33: TODO SD ERASE_WR_BLK_END
    // 34-37: Depends on function switch (CMD6)
    // 38: TODO SD ERASE
    // 39: TODO SD SELECT_CARD_PARTITION
    // 40: TODO DPS Spec
    // 41: SD Reserved
    // 42: TODO SD LOCK_UNLOCK

    pub const ReadExtrSingle = packed struct(u32) {
        pub const Info = adtc(.SD, "READ_EXTR_SINGLE", 48, .recv, .R1, .{ .extension = true });
        length: u9,
        address: u17,
        _: u1,
        fno: u4,
        mio: bool,
    };
    pub const WriteExtrSingle = packed struct(u32) {
        pub const Info = adtc(.SD, "WRITE_EXTR_SINGLE", 49, .send, .R1, .{ .extension = true });
        length: u9,
        address: u17,
        mw: bool,
        fno: u4,
        mio: bool,
    };

    // 50: Depends on function switch (CMD6)
    // 51: SD Reserved
    pub const IORWDirect = packed struct(u32) {
        pub const Info = ac(.SDIO, "IO_RW_DIRECT", 52, .R5, .{ .io_mode = true });
        pub const Direction = enum(u1) {
            read = 0,
            write = 1,
        };
        write_data: u8,
        _1: bool,
        register_address: u17,
        _2: bool,
        read_after_write: bool,
        function_number: u3,
        direction: Direction,
    };
    pub const IORWExtended = packed struct(u32) {
        pub const Info = adtc(.SDIO, "IO_RW_EXTENDED", 53, .both, .R5, .{ .io_mode = true });
        pub const OpCode = enum(u1) {
            write_fixed_addr = 0,
            write_incrementing_addr = 1,
        };
        pub const Direction = enum(u1) {
            read = 0,
            write = 1,
        };
        unit_count: u9,
        register_address: u17,
        opcode: OpCode,
        block_mode: bool,
        function_number: u3,
        direction: Direction,
    };
    // 54: TODO SDIO

    // Application-specific commands. See SD Spec Table 4-28
    pub const AppCmd = packed struct(u32) {
        pub const Info = ac(.MMC, "APP_CMD", 55, .R1, .{ .application_specific = true });
        _: u16,
        rca: RCA,
    };
    pub const GenCmd = packed struct(u32) {
        // R1b response in MMC, what to do with this?
        pub const Info = adtc(.MMC, "GEN_CMD", 56, .both, .R1, .{ .application_specific = true });
        pub const Direction = enum(u1) {
            read = 1,
            write = 0,
        };
        direction: Direction,
        _: u31,
    };

    // 57: Depends on function switch (CMD6)
    // 58: TODO SD READ_EXTR_MULTI
    // 59: TODO SD WRITE_EXTR_MULTI
    // 60-63: MMC reserved for manufacturer

    //pub const ReadOCR = packed struct(u32) {
    //    pub const Info = adtc(.MMC, "READ_OCR", 58?, .?, .R3, .extension);
    //};
};

pub const ApplicationCommands = struct {
    /// Broadcast Command
    fn bc(spec: Specification, name: []const u8, idx: Command.Index) Command {
        _ = spec;
        return Command.make(name, idx, .broadcast, .None, .{ .application_specific = true }, .none);
    }
    /// Broadcast Command with Response
    fn bcr(spec: Specification, name: []const u8, idx: Command.Index, rsp: ResponseType) Command {
        _ = spec;
        return Command.make(name, idx, .broadcast_response, rsp, .{ .application_specific = true }, .none);
    }
    /// Addressed Command
    fn ac(spec: Specification, name: []const u8, idx: Command.Index, rsp: ResponseType) Command {
        _ = spec;
        return Command.make(name, idx, .addressed, rsp, .{ .application_specific = true }, .none);
    }
    /// Addressed Data Transfer Command
    fn adtc(spec: Specification, name: []const u8, idx: Command.Index, transfer: DataTransfer, rsp: ResponseType) Command {
        _ = spec;
        return Command.make(name, idx, .addressed_data_transfer, rsp, .{ .application_specific = true }, transfer);
    }

    // SD memory card commands. See SD Spec Table 4-31
    // 1-5: Reserved
    pub const SetBusWidth = packed struct(u32) {
        pub const Info = ac(.SD, "SET_BUS_WIDTH", 6, .R1);
        width: enum(u2) {
            bit1 = 0b00,
            bit4 = 0b10,
        },
        _: u30,
    };
    // 7-12: Reserved
    pub const SDStatus = packed struct(u32) {
        pub const Info = adtc(.SD, "SD_STATUS", 13, .recv, .R1);
        _: u32,
    };
    // 14-16: Reserved for DPS Specification
    // 17: Reserved
    // 18: Reserved for SD security applications
    // 19-21: Reserved
    pub const SendNumWrBlocks = packed struct(u32) {
        pub const Info = adtc(.SD, "SEND_NUM_WR_BLOCKS", 22, .recv, .R1);
        _: u32,
    };
    pub const SetWrBlkEraseCount = packed struct(u32) {
        pub const Info = ac(.SD, "SET_WR_BLK_ERASE_COUNT", 23, .R1);
        number_of_blocks: u23,
        _: u9,
    };
    // 24: Reserved
    // 25-26: Reserved for SD security applications
    // 27: Shall not be used
    // 28: Reserved for DPS Specification
    // 29: Reserved
    // 30-35: Reserved for Security Specification
    // 36-37: Reserved
    // 38: Reserved for SD security applications
    // 39-40: Reserved
    pub const SDSendOpCond = packed struct(u32) {
        pub const Info = bcr(.SD, "SD_SEND_OP_COND", 41, .R3);
        _: u32, // TODO
    };
    pub const SetClrCardDetect = packed struct(u32) {
        pub const Info = ac(.SD, "SET_CLR_CARD_DETECT", 42, .R1);
        connect_detection_resistor: bool,
        _: u31,
    };
    // 43-49: Reserved for SD security applications
    // 50: ???
    pub const SendSCR = packed struct(u32) {
        pub const Info = adtc(.SD, "SEND_SCR", 51, .recv, .R1);
        _: u32,
    };
    // 52: Reserved for Security Specification
    // 53: TODO SD SECURE_RECEIVE
    // 54: TODO SD SECURE_SEND
    // 56-59: Reserved for Security Specification
};
