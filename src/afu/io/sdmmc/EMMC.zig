const FourCC = @import("../util/FourCC.zig");

const Error = error {
  SendCIDFailed,
  SendRCAFailed,
  SendCSDFailed
};

pub const Port: u8 = 1;

pub fn init() !void {
  const Tag = FourCC.fromString("NAND");

  hw.registers.Config9.SDMCControl.* = 0;
  hw.registers.Config9.SDMCControl.* = 0x200;
  EMMC.setDataMode(hw.SD.DataMode.Data32);

  var resp: hw.SD.Command.Response;

  hw.EMMC.selectPort(Port);
  hw.EMMC.setBusWidth(hw.SD.Controller.CardOption.BusWidth.Bit1);
  hw.EMMC.setClockDivider(hw.SD.Controller.ClkControl.Div256);

  try(hw.EMMC.sendCommand(hw.SD.Commands.GoIdleState, 0, &resp));
  Log.d(Tag, "Init CMD0 result: %08X", resp.r1_32);

  hw.SD.OCR ocr;
  ocr.voltageRange = hw.SD.VoltageRange.V3_2To3_3;
  do {
    if (hw.EMMC.sendCommand(hw.SD.Commands.SendOpCond, ocr, &resp).failed()) {
      continue;
    } else {
      Log.d(Tag, "Init CMD1 result: %08X", resp.r3_32);;
    }
  } while(!resp.r3.powerUpBusy);

  if (hw.EMMC.sendCommand(hw.SD.Commands.AllSendCid, 0, &resp).ok()) {
    /*uint32 tmp = resp.r2_32[0], tmp2 = resp.r2_32[1];
    resp.r2_32[0] = resp.r2_32[3];
    resp.r2_32[1] = resp.r2_32[2];
    resp.r2_32[2] = tmp2;
    resp.r2_32[3] = tmp;*/
    Log.d(Tag, "CID %02X %08X %08X %08X", resp.r2_32[0], resp.r2_32[1], resp.r2_32[2], resp.r2_32[3]);
    Log.d(Tag, "CID: m=%02X o=%04X p=%6s rv=%02X sn=%04X mdt=%02X",
           resp.r2CID_MMC.manufacturerID, resp.r2CID_MMC.oemID, resp.r2CID_MMC.productName,
           resp.r2CID_MMC.productRevision, resp.r2CID_MMC.productSerialNumber,
           resp.r2CID_MMC.manufacturingDate);
  } else {
    raise Errors.SendCIDFailed;
  }

  if (hw.EMMC.sendCommand(hw.SD.Commands.SendRelativeAddr, 0, &resp).ok()) {
    Log.d(Tag, "RCA: %04X", resp.r6.newPublishedRCA);
  } else {
    raise Errors.SendRCAFailed;
  }

  if (hw.EMMC.sendCommand(hw.SD.Commands.SendCSD, 0, &resp).ok()) {
    Log.d(Tag, "CSDver: %X", resp.r2CSD.csdVersion);
    Log.d(Tag, "CSD %08X %08X %08X %08X", resp.r2_32[0], resp.r2_32[1], resp.r2_32[2], resp.r2_32[3]);
    //assert(resp.r2CSD.csdVersion == hw.SD.CSDVersion.V1);
    Log.d(Tag, "DART %X %X", resp.r2CSDv1.dataReadAccessTime1, resp.r2CSDv1.dataReadAccessTime2);
    Log.d(Tag, "TRAN_SPEED %X", resp.r2CSDv1.transferSpeed);
    Log.d(Tag, "%X %X", resp.r2CSDv1.cardCommandClasses_hi, resp.r2CSDv1.cardCommandClasses_lo);
    Log.d(Tag, "CCC %X BLEN %X", resp.r2CSDv1.cardCommandClasses(), resp.r2CSDv1.maxReadBlockLength);

    Log.d(Tag, "BLOCK_LEN 2^%i C_SIZE %i * %i = BLOCKNR %i",
           resp.r2CSDv1.maxReadBlockLength,
           resp.r2CSDv1.deviceSize(),
           resp.r2CSDv1.deviceSizeMultiplier,
           (resp.r2CSDv1.deviceSize() + 1) * (1 << (resp.r2CSDv1.deviceSizeMultiplier + 2)));
    Log.d(Tag, "SZ %i",
           (resp.r2CSDv1.deviceSize() + 1) << (resp.r2CSDv1.maxReadBlockLength + resp.r2CSDv1.deviceSizeMultiplier + 2));
    Log.d(Tag, "logme!");
  } else {
    raise Errors.SendCSDFailed;
  }

  hw.EMMC.setClockDivider(.Div4);
}
