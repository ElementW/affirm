// SPDX-License-Identifier: EUPL-1.2
pub const protocol = @import("sdmmc/protocol.zig");

pub const SDError = error{
    CommandFailed,
    IllegalAccess,
    ResponseTimeout,
    TXUnderrun,
    RXOverflow,
    DataTimeout,
    StopBitError,
    CRCFail,
    CmdIndexError,
    EarlyResponseEnd,
    NonalignedTransferLength,
};
