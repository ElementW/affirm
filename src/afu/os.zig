// SPDX-License-Identifier: EUPL-1.2
pub const dbg = @import("os/dbg.zig");
pub const hw = @import("os/hw.zig");
pub const ipc = @import("os/ipc.zig");
pub const mm = @import("os/mm.zig");
pub const pm = @import("os/pm.zig");
pub const sync = @import("os/sync.zig");
pub const sys = @import("os/sys.zig");

pub const arch = @import("os/arch.zig");
pub const results = @import("os/results.zig");
pub const syscall = @import("os/syscall.zig");

pub const TitleID = u64;

const i = arch.target.syscalls.interface;
