// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const expect = std.testing.expect;
const assert = std.debug.assert;

fn MakeVolatileType(comptime T: type) type {
    var ti = @typeInfo(T);
    ti.Pointer.is_volatile = true;
    return @Type(ti);
}
pub inline fn makeVolatile(ptr: anytype) MakeVolatileType(@TypeOf(ptr)) {
    return @ptrCast(ptr);
}

pub const RegisterAccess = enum { r, w, rw };

pub fn Register(comptime T: type, comptime A: RegisterAccess) type {
    const U =
        if (T == anyopaque or T == void) anyopaque else if (@sizeOf(T) == 1) u8 else if (@sizeOf(T) == 2) u16 else if (@sizeOf(T) == 4) u32 else {
        @compileError(std.fmt.comptimePrint("No primitive of size {}", .{@sizeOf(T)}));
    };
    return struct {
        const Self = @This();
        const IsRegister = true;
        pub const Access = A;
        pub const Value = T;
        pub const Align = @max(@alignOf(Value), 1);
        pub const PointerType = if (A == .r) *align(Align) const volatile T else *align(Align) volatile T;
        pub const OpaquePointerType = if (A == .r) *align(Align) const volatile anyopaque else *align(Align) volatile anyopaque;
        pub const Equivalent = U;
        pub const EquivalentPointerType = if (A == .r) *align(Align) const volatile U else *align(Align) volatile U;
        const EPT = EquivalentPointerType;

        ptr: PointerType,

        pub inline fn of(ptr: OpaquePointerType) Self {
            return Self{ .ptr = @ptrCast(ptr) };
        }

        pub inline fn at(loc: usize) Self {
            return Self{ .ptr = @ptrFromInt(loc) };
        }

        pub inline fn read(self: Self) Value {
            comptime {
                if (Access == .w) {
                    @compileError("Can't read write-only register");
                }
                if (Equivalent == anyopaque) {
                    @compileError("Can't read " ++ @typeName(Value) ++ " register");
                }
            }
            return @bitCast(@as(EPT, @ptrCast(self.ptr)).*);
        }

        pub inline fn write(self: Self, value: anytype) void {
            comptime {
                if (Access == .r) {
                    @compileError("Can't write to read-only register");
                }
                if (Equivalent == anyopaque) {
                    @compileError("Can't write to " ++ @typeName(Value) ++ " register");
                }
            }
            if (@TypeOf(value) == comptime_int) {
                @as(EPT, @ptrCast(self.ptr)).* = value;
            } else {
                comptime {
                    if (@sizeOf(@TypeOf(value)) != @sizeOf(T)) {
                        @compileError(std.fmt.comptimePrint("Can't write {}-byte value to {}-byte register", .{ @sizeOf(@TypeOf(value)), @sizeOf(T) }));
                    }
                }
                @as(EPT, @ptrCast(self.ptr)).* = @bitCast(value);
            }
        }

        pub inline fn as(self: Self, comptime V: type) V {
            if (@typeInfo(V) == .@"struct" and @hasDecl(V, "IsRegister")) {
                return V.at(@as(usize, self.ptr));
            } else {
                comptime {
                    const reqType = if (Access == .r) "const volatile" else "volatile";
                    if (@typeInfo(V) != .pointer) {
                        @compileError("Register.as() type must be a " ++ reqType ++ " pointer, and " ++ @typeName(V) ++ " is not a pointer");
                    }
                    if (!@typeInfo(V).pointer.is_volatile) {
                        @compileError("Register.as() type must be a " ++ reqType ++ " pointer, and " ++ @typeName(V) ++ " is not volatile");
                    }
                    if (Access == .r and !@typeInfo(V).pointer.is_const) {
                        @compileError("Register.as() type must be a " ++ reqType ++ " pointer, and " ++ @typeName(V) ++ " is not const");
                    }
                }
                return @ptrCast(self.ptr);
            }
        }

        pub inline fn next(self: Self, i: usize) Self {
            return Self.at(@intFromPtr(self.ptr) + i * @sizeOf(Value));
        }

        pub inline fn changeFields(self: Self, fields: anytype) void {
            comptime {
                if (Access != .rw) {
                    @compileError("Can't change fields (RMW) on a read-onlt/write-only register");
                }
            }
            var value = self.read();
            inline for (@typeInfo(@TypeOf(fields)).@"struct".fields) |field| {
                @field(value, field.name) = @field(fields, field.name);
            }
            self.write(value);
        }
    };
}
comptime {
    assert(@sizeOf(Register(u32, .rw)) == @sizeOf(*volatile u32));
}

pub inline fn reg(comptime T: type, addr: usize) Register(T, .rw) {
    return Register(T, .rw).at(addr);
}

pub inline fn unkreg(comptime T: type, addr: usize) Register(T, .rw) {
    // TODO mark use with "Use of unknown register"
    return Register(T, .rw).at(addr);
}

pub inline fn roreg(comptime T: type, addr: usize) Register(T, .r) {
    return Register(T, .r).at(addr);
}

pub inline fn unkroreg(comptime T: type, addr: usize) Register(T, .r) {
    // TODO mark use with "Use of unknown register"
    return Register(T, .r).at(addr);
}

test "basic usage" {
    var mmio: u32 = 0;
    const reg1 = Register(u32, .rw).of(&mmio);
    try expect(reg1.read() == 0);
    reg1.write(5);
    try expect(reg1.read() == 5);
}

test "from address" {
    var mmio: u32 = 1337;
    const addr = @intFromPtr(&mmio);
    const reg1 = Register(u32, .rw).at(addr);
    try expect(reg1.read() == 1337);
    reg1.write(0);
    try expect(reg1.read() == 0);
}

test "as pointer" {
    var mmio: u32 = 0;
    const reg1 = Register(u32, .rw).of(&mmio);
    //const ptr = reg1.as(u32); // MUST NOT COMPILE
    //const ptr = reg1.as(*u32); // MUST NOT COMPILE
    const ptr = reg1.as(*volatile u32);
    try expect(ptr == &mmio);
}

test "next register" {
    var mmio: [2]u32 = [2]u32{ 0, 0 };
    const reg1 = Register(u32, .rw).of(&mmio);
    const reg2 = reg1.next(1);
    try expect(reg1.ptr == &mmio[0]);
    try expect(reg2.ptr == &mmio[1]);
}

test "cast write" {
    const Data = packed struct(u32) { a: u16, b: u16 };
    const initial = Data{ .a = 42069, .b = 1337 };
    const wanted = Data{ .a = 31415, .b = 123 };
    var mmio: u32 = @bitCast(initial);
    const addr = @intFromPtr(&mmio);
    const reg1 = Register(u32, .rw).at(addr);
    try expect(reg1.read() == @as(u32, @bitCast(initial)));
    reg1.write(wanted);
    try expect(reg1.read() == @as(u32, @bitCast(wanted)));
}
