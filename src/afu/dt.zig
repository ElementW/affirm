// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
pub const FDT = @import("dt/fdt.zig").FDT;
pub const LDT = @import("dt/ldt.zig");

pub const Cell = u32;

/// Big endian cell.
pub const BECell = extern struct {
    raw: u32,

    pub fn get(self: BECell) Cell {
        return std.mem.bigToNative(Cell, self.raw);
    }
    pub fn set(self: *BECell, value: Cell) void {
        self.raw = std.mem.nativeToBig(Cell, value);
    }
};

pub const Interpretation = enum {
    bool_true,
    cell,
    string,
    string_sequence,

    pub fn guess(data: []const u8) Interpretation {
        if (data.len == 0) {
            return .bool_true;
        }
        if (data[data.len - 1] == 0) {
            isAlphanum: {
                var nul_count: u32 = 0;
                for (data) |byte| {
                    if (byte == 0) {
                        nul_count += 1;
                    } else {
                        if (byte < ' ' or byte > '~') {
                            break :isAlphanum;
                        }
                    }
                }
                return if (nul_count * 2 >= data.len) .cell else s: {
                    break :s if (nul_count > 1) .string_sequence else .string;
                };
            }
        }
        return .cell;
    }
};
