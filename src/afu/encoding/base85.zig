const std = @import("std");
const expect = std.testing.expect;
const mem = std.mem;

pub fn decode(noalias encoded: []const u8, noalias decoded: []u8) []u8 {
    const n = encoded.len / 5 * 5;
    var i: usize = 0;
    var o: usize = 0;

    while (i < n) : ({
        i += 5;
        o += 4;
    }) {
        while (encoded[i] == 'z') {
            decoded[o] = 0;
            decoded[o + 1] = 0;
            decoded[o + 2] = 0;
            decoded[o + 3] = 0;
            i += 1;
            o += 4;
        }
        var p: u32 = (encoded[i] - '!');
        p = (p * 85) + (encoded[i + 1] - '!');
        p = (p * 85) + (encoded[i + 2] - '!');
        p = (p * 85) + (encoded[i + 3] - '!');
        p = (p * 85) + (encoded[i + 4] - '!');

        decoded[o] = @as(u8, @truncate(p >> 24));
        decoded[o + 1] = @as(u8, @truncate(p >> 16));
        decoded[o + 2] = @as(u8, @truncate(p >> 8));
        decoded[o + 3] = @as(u8, @truncate(p));
    }

    var remaining = encoded.len - i;
    while (remaining > 0 and encoded[i] == 'z') {
        decoded[o] = 0;
        decoded[o + 1] = 0;
        decoded[o + 2] = 0;
        decoded[o + 3] = 0;
        i += 1;
        o += 4;
        remaining -= 1;
    }
    if (remaining > 0) {
        var padded: [5]u8 = undefined;
        @memcpy(padded[0..remaining], encoded[i..]);
        @memset(padded[remaining..5], 'u');

        var p: u32 = (padded[0] - '!');
        p = (p * 85) + (padded[1] - '!');
        p = (p * 85) + (padded[2] - '!');
        p = (p * 85) + (padded[3] - '!');
        p = (p * 85) + (padded[4] - '!');

        decoded[o] = @as(u8, @truncate(p >> 24));
        decoded[o + 1] = @as(u8, @truncate(p >> 16));
        decoded[o + 2] = @as(u8, @truncate(p >> 8));
        decoded[o + 3] = @as(u8, @truncate(p));

        o += remaining - 1;
    }

    return decoded[0..o];
}

test "ascii85 decode ascii aligned" {
    var output: [1024]u8 = undefined;
    const slice = decode("7W3Ei7W3Ei7VR$W7VR$W", &output);
    try expect(mem.eql(u8, slice, "FourFourFiveFive"));
}

test "ascii85 decode ascii unaligned" {
    var output: [1024]u8 = undefined;
    const slice = decode("7W3Ei7W3Ei7VR$W7VR$W;e^I", &output);
    try expect(mem.eql(u8, slice, "FourFourFiveFiveSix"));
}
