// SPDX-License-Identifier: EUPL-1.2

const Self = @This();
pub const Primitive = u32;

value: Primitive,

pub inline fn fromU32(value: Primitive) Self {
    return Self{ .value = value };
}

pub inline fn toU32(self: Self) Primitive {
    return self.value;
}

pub inline fn fromU8(a: u8, b: u8, c: u8, d: u8) Self {
    return Self{
        .value = @as(Primitive, a) |
            @as(Primitive, b) << 8 |
            @as(Primitive, c) << 16 |
            @as(Primitive, d) << 24,
    };
}

pub fn fromString(s: *const [4]u8) Self {
    return fromU8(s[0], s[1], s[2], s[3]);
}

pub fn toString(self: Self) [4]u8 {
    return [4]u8{
        @as(u8, @truncate(self.value)),
        @as(u8, @truncate(self.value >> 8)),
        @as(u8, @truncate(self.value >> 16)),
        @as(u8, @truncate(self.value >> 24)),
    };
}
