// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");

pub const Result = packed struct(u32) {
    const FailureBit: u32 = 1 << 31;
    pub const Level = enum(u5) {
        pub const Fatal = 0x1F;
        success = 0,
        info = 1,
        fatal = Fatal,
        reset = Fatal - 1,
        reinitialize = Fatal - 2,
        usage = Fatal - 3,
        permanent = Fatal - 4,
        temporary = Fatal - 5,
        status = Fatal - 6,
    };
    pub const Summary = enum(u6) {
        success = 0,
        nop = 1,
        would_block = 2,
        out_of_resource = 3,
        not_found = 4,
        invalid_state = 5,
        not_supported = 6,
        invalid_argument = 7,
        wrong_argument = 8,
        canceled = 9,
        status_changed = 10,
        internal = 11,
        invalid_result_value = 63,
    };
    pub const Module = enum(u8) {
        common = 0,
        kernel = 1,
        util = 2,
        file_server = 3,
        loader_server = 4,
        tcb = 5,
        os = 6,
        dbg = 7,
        dmnt = 8,
        pdn = 9,
        gsp = 10,
        i2c = 11,
        gpio = 12,
        dd = 13,
        codec = 14,
        spi = 15,
        pxi = 16,
        fs = 17,
        di = 18,
        hid = 19,
        cam = 20,
        pi = 21,
        pm = 22,
        pm_low = 23,
        fsi = 24,
        srv = 25,
        ndm = 26,
        nwm = 27,
        soc = 28,
        ldr = 29,
        acc = 30,
        rom_fs = 31,
        am = 32,
        hio = 33,
        updater = 34,
        mic = 35,
        fnd = 36,
        mp = 37,
        mpwl = 38,
        ac = 39,
        http = 40,
        dsp = 41,
        snd = 42,
        dlp = 43,
        hio_low = 44,
        csnd = 45,
        ssl = 46,
        am_low = 47,
        nex = 48,
        friends = 49,
        rdt = 50,
        applet = 51,
        nim = 52,
        ptm = 53,
        midi = 54,
        mc = 55,
        swc = 56,
        fat_fs = 57,
        ngc = 58,
        card = 59,
        card_nor = 60,
        sdmc = 61,
        boss = 62,
        dbm = 63,
        config = 64,
        ps = 65,
        cec = 66,
        ir = 67,
        uds = 68,
        pl = 69,
        cup = 70,
        gyroscope = 71,
        mcu = 72,
        ns = 73,
        news = 74,
        ro = 75,
        gd = 76,
        card_spi = 77,
        ec = 78,
        web_browser = 79,
        @"test" = 80,
        enc = 81,
        pia = 82,
        act = 83,
        vctl = 84,
        olv = 85,
        neia = 86,
        npns = 87,
        avd = 90,
        l2b = 91,
        mvd = 92,
        nfc = 93,
        uart = 94,
        spm = 95,
        qtm = 96,
        nfp = 97,

        pci = 98,
        usb = 99,
        gnss = 100,

        application = 254,
        invalid_result_val = 255,
    };
    pub const Description = enum(u10) {
        pub const Invalid = 0x3ff;
        invalid_result_value = 0x3ff,
        timeout = Invalid - 1,
        out_of_range = Invalid - 2,
        already_exists = Invalid - 3,
        cancel_requested = Invalid - 4,
        not_found = Invalid - 5,
        already_initialized = Invalid - 6,
        not_initialized = Invalid - 7,
        invalid_handle = Invalid - 8,
        invalid_pointer = Invalid - 9,
        invalid_address = Invalid - 10,
        not_implemented = Invalid - 11,
        out_of_memory = Invalid - 12,
        misaligned_size = Invalid - 13,
        misaligned_address = Invalid - 14,
        busy = Invalid - 15,
        no_data = Invalid - 16,
        invalid_combination = Invalid - 17,
        invalid_enum_value = Invalid - 18,
        invalid_size = Invalid - 19,
        already_done = Invalid - 20,
        not_authorized = Invalid - 21,
        too_large = Invalid - 22,
        invalid_selection = Invalid - 23,
        success = 0,
        _,
    };

    pub fn With(comptime T: type) type {
        return struct {
            const Self = @This();
            pub const Type = T;

            result: Result,
            /// This is left uninitialised if this result is a failure.
            /// Use `get()` to obtain the value contained.
            maybe_value: T = undefined,

            pub inline fn success(value: T) Self {
                return Self{ .result = Result.success(), .maybe_value = value };
            }
            pub inline fn info(s: Summary, m: Module, d: Description, value: T) Self {
                return Self{ .result = Result.info(s, m, d), .maybe_value = value };
            }
            pub inline fn failure(l: Level, s: Summary, m: Module, d: Description) Self {
                return Self{ .result = Result.failure(l, s, m, d) };
            }

            pub inline fn isFailure(self: Self) bool {
                return self.result.isFailure();
            }
            pub inline fn isSuccess(self: Self) bool {
                return self.result.isSuccess();
            }

            pub inline fn get(self: Self) ?T {
                return if (self.isFailure()) null else self.maybe_value;
            }
        };
    }

    description: Description = .invalid_result_value,
    module: Module = .common,
    _: u3 = 0,
    summary: Summary = .invalid_result_value,
    level: Level = .fatal,

    pub inline fn success() Result {
        return Result{ .level = .success, .summary = .success, .module = .common, .description = .success };
    }
    pub inline fn info(s: Summary, m: Module, d: Description) Result {
        return Result{ .level = .info, .summary = s, .module = m, .description = d };
    }
    pub inline fn failure(l: Level, s: Summary, m: Module, d: Description) Result {
        if (l == .success or l == .info) {
            @panic("Tried to create failure Result using " ++ @tagName(l) ++ " level");
        }
        return Result{ .level = l, .summary = s, .module = m, .description = d };
    }

    pub inline fn with(comptime self: Result, value: anytype) Result.With(@TypeOf(value)) {
        comptime if (self.level != .success and self.level != .info) {
            @compileError("Tried to Result.with() when Result.level = " ++ @tagName(self.level) ++ " (must be success or info)");
        };
        return Result.With(@TypeOf(value)){
            .result = self,
            .maybe_value = value,
        };
    }
    pub inline fn without(comptime self: Result, comptime ValueT: type) Result.With(ValueT) {
        comptime if (self.level == .success or self.level == .info) {
            @compileError("Tried to Result.without() when Result.level = " ++ @tagName(self.level));
        };
        return Result.With(ValueT){
            .result = self,
            .maybe_value = undefined,
        };
    }

    pub inline fn fromU32(value: u32) Result {
        return @as(Result, @bitCast(value));
    }
    pub inline fn toU32(self: Result) u32 {
        return @as(u32, @bitCast(self));
    }

    pub inline fn isFailure(self: Result) bool {
        return (self.toU32() & FailureBit) != 0;
    }
    pub inline fn isSuccess(self: Result) bool {
        return !self.isFailure();
    }
};

pub fn ServiceResult(comptime name: []const u8, DescriptionT: type) type {
    _ = name;

    const descriptionTag = std.meta.Tag(Result.Description);
    if (@typeInfo(DescriptionT) != .@"enum" or @typeInfo(DescriptionT).@"enum".tag_type != descriptionTag) {
        @compileError("Description must be an enum(" ++ @typeName(descriptionTag) ++ ")");
    }

    const numFields = @typeInfo(Result.Description).@"enum".fields.len + @typeInfo(DescriptionT).@"enum".fields.len;
    comptime var fields: [numFields]std.builtin.Type.EnumField = undefined;
    comptime var index = 0;
    for (@typeInfo(Result.Description).@"enum".fields) |field| {
        fields[index] = field;
        index += 1;
    }
    for (@typeInfo(DescriptionT).@"enum".fields) |field| {
        fields[index] = field;
        index += 1;
    }
    const enumInfo = std.builtin.Type.Enum{
        .tag_type = u10,
        .fields = &fields,
        .decls = &[0]std.builtin.Type.Declaration{},
        .is_exhaustive = true,
    };
    const SpecialisedDescription = blk: {
        const Description = @Type(std.builtin.Type{ .@"enum" = enumInfo });
        break :blk Description;
    };

    return packed struct(u32) {
        const ResultSelf = @This();
        const FailureBit = Result.FailureBit;
        pub const Level = Result.Level;
        pub const Summary = Result.Summary;
        pub const Module = Result.Module;
        pub const Description = SpecialisedDescription;

        pub fn With(comptime T: type) type {
            return struct {
                const ResultWithSelf = @This();
                pub const Type = T;

                result: Result,
                /// This is left uninitialised if this result is a failure.
                /// Use `get()` to obtain the value contained.
                maybe_value: T = undefined,

                pub inline fn success(value: T) ResultWithSelf {
                    return ResultWithSelf{ .result = Result.success(), .maybe_value = value };
                }
                pub inline fn info(s: Summary, m: Module, d: Description, value: T) ResultWithSelf {
                    return ResultWithSelf{ .result = Result.info(s, m, d), .maybe_value = value };
                }
                pub inline fn failure(l: Level, s: Summary, m: Module, d: Description) ResultWithSelf {
                    return ResultWithSelf{ .result = Result.failure(l, s, m, d) };
                }

                pub inline fn isFailure(self: ResultWithSelf) bool {
                    return self.result.isFailure();
                }
                pub inline fn isSuccess(self: ResultWithSelf) bool {
                    return self.result.isSuccess();
                }

                pub inline fn get(self: ResultWithSelf) ?T {
                    return if (self.isFailure()) null else self.value.present;
                }
            };
        }

        description: Description = .invalid_result_value,
        module: Module = .common,
        _: u3 = 0,
        summary: Summary = .invalid_result_value,
        level: Level = .fatal,

        pub inline fn success() ResultSelf {
            return ResultSelf{ .level = .success, .summary = .success, .module = .common, .description = .success };
        }
        pub inline fn info(s: Summary, m: Module, d: Description) ResultSelf {
            return ResultSelf{ .level = .info, .summary = s, .module = m, .description = d };
        }
        pub inline fn failure(l: Level, s: Summary, m: Module, d: Description) ResultSelf {
            if (l == .success or l == .info) {
                @panic("Tried to create failure Result using " ++ @tagName(l) ++ " level");
            }
            return ResultSelf{ .level = l, .summary = s, .module = m, .description = d };
        }

        pub inline fn with(comptime self: ResultSelf, value: anytype) With(@TypeOf(value)) {
            comptime if (self.level != .success and self.level != .info) {
                @compileError("Tried to Result.with() when Result.level = " ++ @tagName(self.level) ++ " (must be success or info)");
            };
            return With(@TypeOf(value)){
                .result = self,
                .maybe_value = value,
            };
        }
        pub inline fn without(comptime self: ResultSelf, comptime ValueT: type) With(ValueT) {
            comptime if (self.level == .success or self.level == .info) {
                @compileError("Tried to Result.without() when Result.level = " ++ @tagName(self.level));
            };
            return With(ValueT){
                .result = self,
                .maybe_value = undefined,
            };
        }

        pub inline fn fromU32(value: u32) ResultSelf {
            return @as(ResultSelf, @bitCast(value));
        }
        pub inline fn toU32(self: ResultSelf) u32 {
            return @as(u32, @bitCast(self));
        }

        pub inline fn isFailure(self: ResultSelf) bool {
            return (self.toU32() & FailureBit) != 0;
        }
        pub inline fn isSuccess(self: ResultSelf) bool {
            return !self.isFailure();
        }
    };
}

fn rwpEquals(left: anytype, right: @TypeOf(left)) bool {
    const r_left: Result = @field(left, "result");
    const r_right: Result = @field(right, "result");
    if (!std.meta.eql(r_left, r_right))
        return false;
    if (!r_left.isSuccess())
        return true;
    return std.meta.eql(@field(left, "maybe_value"), @field(right, "maybe_value"));
}

test ServiceResult {
    const TestResult = ServiceResult("Test", enum(u10) {
        too_spicy = 1,
        too_sugary,
        bland,
    });
    // Test if the service result's Description enum was merged properly
    try std.testing.expectEqual(1, @intFromEnum(TestResult.Description.too_spicy));
    try std.testing.expectEqual(@intFromEnum(Result.Description.invalid_pointer), @intFromEnum(TestResult.Description.invalid_pointer));
    try std.testing.expect(TestResult.failure(.permanent, .invalid_argument, .@"test", .invalid_size).description == .invalid_size);
    try std.testing.expect(TestResult.failure(.permanent, .invalid_argument, .@"test", .too_sugary).description == .too_sugary);

    // When Zig supports testing compile errors, test description type
    // _ = ServiceResult("", struct {});
    // _ = ServiceResult("", enum { a });
    // _ = ServiceResult("", enum(u8) { a = 1 });
    // _ = ServiceResult("", enum(u10) { a = 0 });
}

test "Result with value" {
    const Payload = extern struct { a: u32, b: u64 };
    const PayloadExample: Payload = .{ .a = 1, .b = 2 };
    const RWP = Result.With(Payload);
    const Test = struct {
        fn ok() RWP {
            return RWP.success(PayloadExample);
        }
        fn info() RWP {
            return RWP.info(.status_changed, .application, .already_done, PayloadExample);
        }
        fn fail() RWP {
            return RWP.failure(.usage, .invalid_argument, .application, .invalid_handle);
        }
        fn okWith() RWP {
            return Result.success().with(PayloadExample);
        }
        fn failWith() RWP {
            return Result.failure(.usage, .invalid_argument, .application, .invalid_handle).without(Payload);
        }
    };
    try std.testing.expect(rwpEquals(RWP{ .result = Result.success(), .maybe_value = PayloadExample }, Test.ok()));
    try std.testing.expect(rwpEquals(Test.ok(), Test.okWith()));
    try std.testing.expect(rwpEquals(Test.fail(), Test.failWith()));
    try std.testing.expect(std.meta.eql(Test.info().get(), PayloadExample));
    try std.testing.expectEqual(Test.fail().get(), null);
}
