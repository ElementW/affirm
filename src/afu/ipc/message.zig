// SPDX-License-Identifier: EUPL-1.2
//! See also:
//! * https://www.3dbrew.org/wiki/IPC#Message_Structure
//! * https://github.com/devkitPro/libctru/blob/master/libctru/include/3ds/ipc.h

pub const Message = extern struct {
    /// Unit size through which message data is read and written.
    pub const Unit = u32;

    pub const MaxBytes = 0x100;
    pub const MaxUnits = MaxBytes / @sizeOf(Unit);

    pub const Tag = u16;
    pub const Flags = packed struct(u4) {
        _: u4 = 0,
    };

    /// Message header informing the kernel of the message structure, and the receiver of how what
    /// call the message represents.
    pub const Header = packed struct(Unit) {
        /// Number of units in the message section the kernel should apply a translation to during
        /// transport. Usually larger than the number of *parameters* in the section.
        translate_units: u6,
        /// Number of data  units in the untranslated section of the message.
        /// Must be set to `@sizeOf(all data) / @sizeOf(Unit)`.
        data_units: u6,
        flags: Flags = .{},
        /// Application-specific tag, identifying what function the message was sent to
        /// perform.
        tag: Tag,
    };

    pub const Data = Unit;

    /// Buffer descriptors point to areas of memory in the address space of processes receiving
    /// messages to receive the data sent by `.buffer_copy` and `.pxi_buffer_{rw,read}` translations.
    /// The kernel expects to see a `Table` of them at Thread Local Storage address 0x180, which
    /// is indexed by the `.index` field of translate headers.
    pub const TranslateBufferDescriptor = extern struct {
        pub const Index = u4;
        pub const Table = [1 << 4]TranslateBufferDescriptor;
        header: packed struct(Unit) {
            _: u14 = 0,
            size: u18,
        },
        address: ?*anyopaque,
    };
    pub const TranslateHeader = packed struct(Unit) {
        pub const Kind = enum(u3) {
            /// Perform `Handle` duplication, transfer, or process ID replacement.
            handle = 0,
            /// Copy a buffer from the sender to a buffer in the receiver.
            buffer_copy = 1,
            pxi_buffer_rw = 2,
            pxi_buffer_read = 3,
            // 4 is invalid (buffer map without permissions)
            /// Map sender read-only buffer pages into the receiver as read-write pages for the
            /// duration of the message exchange.
            buffer_map_read = 5,
            /// Map sender write-only buffer pages into the receiver as read-write pages for the
            /// duration of the message exchange.
            buffer_map_write = 6,
            /// Map sender read-write buffer pages into the receiver as read-write pages for the
            /// duration of the message exchange.
            buffer_map_rw = 7,
        };
        pub const HandleTranslateMethod = enum(u2) {
            /// Copies the set of handles into new handles the receving process owns; the
            /// handles of the sender stay open. This is similar to sending file descriptors
            /// over a UNIX socket using `SCM_RIGHTS`.
            duplicate_handle = 0,
            /// Gives the set of handles away to the receiving process; the handles on the
            /// sender side become invalid after this, similar to calling `CloseHandle()`
            /// on them.
            give_handle = 1,
            /// Fill in the following units with the sending process ID.
            process_id = 2,
        };
        _: u1 = 0,
        /// What kind of translation to apply to the N units following this header. N is 1 except
        /// for `.handle` translation, whose `.count` field dictates how many handles to translate.
        kind: Kind,
        data: packed union {
            handle: packed struct(u28) {
                method: HandleTranslateMethod,
                _: u20 = 0,
                /// Number of following handles to translate, or process IDs to fill. This is offset
                /// by 1, so `.count_minus_one = 0` means 1 handle will be translated.
                count_minus_one: u6,
            },
            buffer_copy: packed struct(u28) {
                _: u6 = 0,
                index: TranslateBufferDescriptor.Index,
                size: u18,
            },
            pxi_buffer: packed struct(u28) {
                index: TranslateBufferDescriptor.Index,
                size: u24,
            },
            buffer_map: packed struct(u28) {
                size: u28,
            },
        },
    };
    pub const TranslateData = Unit;

    units: [MaxUnits]Unit,

    pub fn header(self: *const @This()) Header {
        return @bitCast(self.units[0]);
    }

    pub fn dataUnits(self: *@This()) []Unit {
        return self.units[1..][0..self.header().data_units];
    }

    pub fn translateUnits(self: *@This()) []Unit {
        return self.units[1 + self.header().data_units ..][0..self.header().translate_units];
    }
};
