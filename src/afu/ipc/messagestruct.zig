// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afu = @import("../afu.zig");

const Message = afu.ipc.Message;
const Header = Message.Header;
const Result = afu.Result;
const Handle = afu.Handle;
const Process = afu.os.pm.Process;

const SingleHandle = extern struct {
    pub const TranslateUnits = 2;
    header: Message.TranslateHeader,
    handle: Handle,
};
inline fn singleHandle(comptime method: Message.TranslateHeader.HandleTranslateMethod, handle: Handle) SingleHandle {
    return .{
        .header = .{
            .kind = .handle,
            .data = .{
                .handle = .{ .method = method, .count_minus_one = 0 },
            },
        },
        .handle = handle,
    };
}
fn MultiHandles(count: u6) type {
    return extern struct {
        pub const TranslateUnits = 1 + count;
        header: Message.TranslateHeader,
        handles: [count]Handle,
    };
}
inline fn multiHandles(comptime method: Message.TranslateHeader.HandleTranslateMethod, comptime N: u6, handles: [N]Handle) MultiHandles(N) {
    return .{
        .header = .{
            .kind = .handle,
            .data = .{
                .handle = .{ .method = method, .count_minus_one = N - 1 },
            },
        },
        .handles = handles,
    };
}

pub const DuplicateHandle = SingleHandle;
pub inline fn duplicateHandle(handle: Handle) DuplicateHandle {
    return singleHandle(.duplicate_handle, handle);
}
pub const DuplicateHandles = MultiHandles;
pub inline fn duplicateHandles(comptime N: u6, handles: [N]Handle) DuplicateHandles(N) {
    return multiHandles(.duplicate_handle, N, handles);
}

pub const GiveHandle = SingleHandle;
pub inline fn giveHandle(handle: Handle) GiveHandle {
    return singleHandle(.give_handle, handle);
}
pub const GiveHandles = MultiHandles;
pub inline fn giveHandles(comptime N: u6, handles: [N]Handle) GiveHandles(N) {
    return multiHandles(.give_handle, N, handles);
}

pub const SendingProcessId = extern struct {
    pub const TranslateUnits = 2;
    header: Message.TranslateHeader = .{
        .kind = .handle,
        .data = .{
            .handle = .{ .method = .process_id, .count_minus_one = 0 },
        },
    },
    process_id: Process.Id,
};
pub fn sendingProcessId() SendingProcessId {
    return .{
        .process_id = @enumFromInt(0),
    };
}

fn bufferElementSanityCheck(comptime info: std.builtin.Type) void {
    switch (info) {
        .array => |ar| {
            bufferElementSanityCheck(@typeInfo(ar.child));
        },
        .@"struct" => |st| {
            if (st.layout != .@"extern" and st.layout != .@"packed") {
                @compileError("Buffer structure type must be extern or packed");
            }
            for (st.fields) |field| {
                bufferElementSanityCheck(@typeInfo(field.type));
            }
        },
        .@"union" => |un| {
            if (un.layout != .@"extern" and un.layout != .@"packed") {
                @compileError("Buffer union type must be extern or packed");
            }
            for (un.fields) |field| {
                bufferElementSanityCheck(@typeInfo(field.type));
            }
        },
        else => {},
    }
}
pub fn BufferElement(comptime BufferT: type) type {
    const info = @typeInfo(BufferT);
    switch (info) {
        .pointer => |ptr| {
            if (ptr.size == .many or ptr.size == .c) {
                @compileError("Pointers to many do not encode item count");
            }
            bufferElementSanityCheck(@typeInfo(ptr.child));
            return switch (@typeInfo(ptr.child)) {
                .array => |ar| ar.child,
                else => ptr.child,
            };
        },
        else => @compileError("Unsupported type"),
    }
}
test "BufferElement gives correct type" {
    const S = extern struct {};
    // Should error BufferElement(S)
    try std.testing.expectEqual(S, BufferElement(*S));
    // Should error BufferElement([*]S)
    // Should error BufferElement([*]const S)
    // Should error BufferElement([3]S)
    try std.testing.expectEqual(S, BufferElement(*[3]S));
    try std.testing.expectEqual(S, BufferElement([]S));
    try std.testing.expectEqual(S, BufferElement([]const S));
}
fn bufferElementCount(buffer: anytype) usize {
    const info = @typeInfo(@TypeOf(buffer));
    switch (info) {
        .pointer => |ptr| {
            if (ptr.size == .many or ptr.size == .c) {
                @compileError("Pointers to many do not encode item count");
            }
            if (ptr.size == .slice) {
                return @field(buffer, "len");
            }
            return switch (@typeInfo(ptr.child)) {
                .array => |ar| ar.len,
                else => 1,
            };
        },
        else => @compileError("Unsupported type"),
    }
}
test "bufferElementCount gives correct number of items in buffer" {
    const S = extern struct { _: u32 = 0 };
    const s_arr = [_]S{.{}} ** 3;
    const s_slice: []const S = &s_arr;
    try std.testing.expectEqual(1, bufferElementCount(@as(*const S, &s_arr[0])));
    try std.testing.expectEqual(3, bufferElementCount(@as(*const [3]S, &s_arr)));
    try std.testing.expectEqual(3, bufferElementCount(s_slice));
    try std.testing.expectEqual(2, bufferElementCount(s_slice[0..2]));
}

pub fn BufferCopy(comptime ElementT: type) type {
    return extern struct {
        pub const TranslateUnits = if (@bitSizeOf(usize) == 32) 2 else 4;
        pub const SliceType = []ElementT;
        header: Message.TranslateHeader,
        buffer: *anyopaque,
    };
}
pub fn bufferCopy(buffer: anytype, index: Message.TranslateBufferDescriptor.Index) BufferCopy(BufferElement(@TypeOf(buffer))) {
    return .{
        .header = .{
            .kind = .buffer_copy,
            .data = .{
                .buffer_copy = .{
                    .index = index,
                    .size = @sizeOf(BufferElement(@TypeOf(buffer))) * bufferElementCount(buffer),
                },
            },
        },
        .buffer = @as(*anyopaque, @constCast(@ptrCast(buffer))),
    };
}

// TODO add PXI buffers, buffer mapping

pub fn checkStructAndBuildHeader(tag: u16, T: type, comptime name: []const u8, comptime side: []const u8) Header {
    const info = @typeInfo(T);
    if (info != .@"struct") {
        @compileError(name ++ " " ++ side ++ " type must be a struct");
    }
    const st = info.@"struct";
    if (st.layout != .@"extern") {
        @compileError(name ++ " " ++ side ++ " struct must be extern");
    }
    if (st.fields.len == 0) {
        @compileError(name ++ " " ++ side ++ " struct must not be empty");
    }
    if (!comptime std.mem.eql(u8, st.fields[0].name, "header") or st.fields[0].type != Header) {
        @compileError(name ++ " " ++ side ++ " must begin with a `header: " ++
            @typeName(Header) ++ "` field, not `" ++ st.fields[0].name ++ ": " ++
            @typeName(st.fields[0].type) ++ "`");
    }
    if (st.fields.len == 1) {
        return .{ .tag = tag, .data_units = 0, .translate_units = 0 };
    }
    comptime var in_translate_part = false;
    comptime var last_data_field: ?std.builtin.Type.StructField = null;
    var translate_units: u6 = 0;
    inline for (st.fields[0 .. st.fields.len - 1], st.fields[1..]) |previous_field, field| {
        const previous_field_offset = @offsetOf(T, previous_field.name);
        const previous_field_end = previous_field_offset + @sizeOf(previous_field.type);
        const field_offset = @offsetOf(T, field.name);
        if (field_offset != previous_field_end) {
            @compileError(std.fmt.comptimePrint("{s} {s} has {} bytes implicit padding " ++
                "between `{s}` and `{s}` ({} bytes aligned)", .{
                name,
                side,
                field_offset - previous_field_end,
                previous_field.name,
                field.name,
                @alignOf(field.type),
            }));
        }
        const is_translate = @typeInfo(field.type) == .@"struct" and @hasDecl(field.type, "TranslateUnits");
        if (is_translate) {
            in_translate_part = true;
            const computed_units = @sizeOf(field.type) / @sizeOf(Message.Unit);
            const declared_units: usize = @field(field.type, "TranslateUnits");
            if (declared_units != computed_units) {
                @compileError(std.fmt.comptimePrint("{s} has wrong TranslateUnits of {}, " ++
                    "should be {} ({} bytes)", .{
                    @typeName(field.type),
                    declared_units,
                    computed_units,
                    @sizeOf(field.type),
                }));
            }
            translate_units += computed_units;
        } else {
            if (in_translate_part) {
                @compileError("RPC data fields cannot appear after translate fields yet " ++
                    name ++ " " ++ side ++ " field `" ++ field.name ++ ": " ++
                    @typeName(field.type) ++ "` is data");
            }
            last_data_field = field;
        }
    }
    const data_units: u6 = comptime blk: {
        if (last_data_field) |last| {
            const last_offset = @offsetOf(T, last.name);
            const last_size = @sizeOf(last.type);
            const byte_past = last_offset + last_size;
            if (@mod(byte_past, @sizeOf(Message.Unit)) != 0) {
                @compileError(std.fmt.comptimePrint("{s} {s} struct data section end offset ({}) is " ++
                    "not an integer multiple of {s} ({}). The last field is `{s}: {s}` at offset {} size {} align {}", .{
                    name,
                    side,
                    byte_past,
                    @typeName(Message.Unit),
                    @sizeOf(Message.Unit),
                    @typeName(last.type),
                    last.name,
                    last_offset,
                    last_size,
                    last.alignment,
                }));
            }
            break :blk (byte_past - @sizeOf(Header)) / @sizeOf(Message.Unit);
        } else {
            break :blk 0;
        }
    };
    return .{
        .tag = tag,
        .data_units = data_units,
        .translate_units = translate_units,
    };
}
test "checkStructAndBuildHeader knows how to count" {
    // Empty message
    try std.testing.expectEqual(Header{
        .tag = 0,
        .data_units = 0,
        .translate_units = 0,
    }, checkStructAndBuildHeader(0, extern struct {
        header: Header,
    }, "empty", "struct"));
    // 1 unit big data section
    try std.testing.expectEqual(Header{
        .tag = 0,
        .data_units = 1,
        .translate_units = 0,
    }, checkStructAndBuildHeader(0, extern struct {
        header: Header,
        a: Message.Unit,
    }, "1-data-unit", "struct"));
    // 1 byte + 1 unit big data section
    try std.testing.expectEqual(Header{
        .tag = 0,
        .data_units = 2,
        .translate_units = 0,
    }, checkStructAndBuildHeader(0, extern struct {
        header: Header,
        selector: u8,
        _: [3]u8,
        index: u32,
    }, "u8-pad-u32", "struct"));
    // 2 units big translate (handle duplication)
    try std.testing.expectEqual(Header{
        .tag = 0,
        .data_units = 0,
        .translate_units = 2,
    }, checkStructAndBuildHeader(0, extern struct {
        header: Header,
        h: DuplicateHandle,
    }, "duplicate-handle", "struct"));
    // Combo of naturally aligned data and translate units
    try std.testing.expectEqual(Header{
        .tag = 0,
        .data_units = 3,
        .translate_units = 5,
    }, checkStructAndBuildHeader(0, extern struct {
        header: Header,
        table: u32,
        seed_bits: u64,
        dispatch: GiveHandles(4),
    }, "aligned-data-handle-combo", "struct"));
}
