// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afu = @import("../afu.zig");

const Message = afu.ipc.Message;
const Header = Message.Header;
const Result = afu.Result;
const Handle = afu.Handle;
const Process = afu.os.pm.Process;
const Session = afu.os.ipc.Session;

const messagestruct = @import("messagestruct.zig");
const checkStructAndBuildHeader = messagestruct.checkStructAndBuildHeader;
const SendingProcessId = messagestruct.SendingProcessId;

const RPCMarker = struct {};

pub const RPCRuntimeInfo = struct {
    name: []const u8,
};
pub fn RPC(comptime tag: anytype, RequestT: type, ResponseT: type) type {
    const tagType = @TypeOf(tag);
    if (@typeInfo(tagType) != .@"enum") {
        @compileError("RPC tag must be an enum(Message.tag) value");
    }
    const name = @tagName(tag);
    const tag_value = @intFromEnum(tag);
    const request_header = comptime checkStructAndBuildHeader(tag_value, RequestT, name, "request");
    const response_header = comptime checkStructAndBuildHeader(tag_value, ResponseT, name, "response");
    return struct {
        pub const _RPCMarker = RPCMarker;
        pub const Tag = tag;
        pub const Name = name;
        pub const RuntimeInfo = RPCRuntimeInfo{
            .name = name,
        };
        pub const Request = RequestT;
        pub const RequestHeader = request_header;
        pub const Response = ResponseT;
        pub const ResponseHeader = response_header;
        pub fn callSync(session_handle: Handle.Of(Session), request: Request) Response {
            _ = session_handle;
            _ = request;
            // TODO
            @panic("NYI");
        }
    };
}

pub const ServiceDeclaration = struct {
    name: []const u8,
};
fn Service(comptime ServiceNamespace: type, comptime decl: ServiceDeclaration) type {
    _ = decl;
    const type_info = @typeInfo(ServiceNamespace);
    const rpcs = blk: {
        var result: [type_info.@"struct".decls.len]type = undefined;
        var i = 0;
        for (type_info.@"struct".decls) |decl2| {
            if (std.mem.eql(u8, decl2.name, "Service")) {
                continue;
            }
            const field = @field(ServiceNamespace, decl2.name);
            if (@TypeOf(field) != type) {
                continue;
            }
            if (@hasDecl(field, "_RPCMarker") and @field(field, "_RPCMarker") == RPCMarker) {
                result[i] = field;
                i += 1;
            }
        }
        break :blk result[0..i];
    };
    if (rpcs.len == 0) {
        @compileError("No RPC decls found in " ++ @typeName(ServiceNamespace) ++ ", did you forget to make them pub?");
    }
    const RequestUnion = blk: {
        var fields: [rpcs.len]std.builtin.Type.UnionField = undefined;
        for (0.., rpcs) |i, rpc| {
            fields[i] = std.builtin.Type.UnionField{
                .name = @field(rpc, "Name"),
                .type = @field(rpc, "Request"),
                .alignment = @alignOf(@field(rpc, "Request")),
            };
        }
        const Request = @Type(std.builtin.Type{ .@"union" = .{
            .layout = .auto,
            .tag_type = @TypeOf(@field(rpcs[0], "Tag")),
            .decls = &.{},
            .fields = &fields,
        } });
        break :blk Request;
    };
    const ResponseUnion = blk: {
        var fields: [rpcs.len]std.builtin.Type.UnionField = undefined;
        for (0.., rpcs) |i, rpc| {
            fields[i] = std.builtin.Type.UnionField{
                .name = @field(rpc, "Name"),
                .type = @field(rpc, "Response"),
                .alignment = @alignOf(@field(rpc, "Response")),
            };
        }
        const Response = @Type(std.builtin.Type{ .@"union" = .{
            .layout = .auto,
            .tag_type = @TypeOf(@field(rpcs[0], "Tag")),
            .decls = &.{},
            .fields = &fields,
        } });
        break :blk Response;
    };

    return struct {
        pub const Request = RequestUnion;
        pub const Response = ResponseUnion;
    };
}
pub fn DeclareService(comptime ServiceNamespace: type, comptime decl: ServiceDeclaration) type {
    return Service(ServiceNamespace, decl);
}

test "Service" {
    const Test = struct {
        pub const Tag = enum(Message.Tag) {
            Initialize = 0x0801,
            DoSomething = 0x0400,
            _,
        };
        pub const Initialize = RPC(
            Tag.Initialize,
            extern struct {
                header: Header,
                process_id: SendingProcessId,
            },
            extern struct {
                header: Header,
                result: Result,
            },
        );
        pub const DoSomething = RPC(
            Tag.DoSomething,
            extern struct {
                header: Header,
                data: u32,
            },
            extern struct {
                header: Header,
                result: Result,
            },
        );

        pub const Service = DeclareService(@This(), .{
            .name = "test:U",
        });
    };

    try std.testing.expectEqual("Initialize", Test.Initialize.Name);
    try std.testing.expectEqual("DoSomething", Test.DoSomething.Name);

    comptime {
        std.debug.assert(std.meta.FieldType(Test.Service.Request, .Initialize) == Test.Initialize.Request);
    }

    const TestServer = struct {
        pub fn handle(request: Test.Service.Request) void {
            switch (request) {
                .DoSomething => |r| {
                    _ = r;
                },
            }
        }
    };
    _ = TestServer;
}
