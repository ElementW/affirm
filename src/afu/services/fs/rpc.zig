// SPDX-License-Identifier: EUPL-1.2
const afu = @import("../../afu.zig");

const ipc = afu.ipc;
const RPC = ipc.RPC;
const Message = ipc.Message;
const Result = afu.Result;

pub const Tag = enum(Message.Tag) {
    Initialize = 0x0801,
    OpenFile = 0x0802,
};

pub const Initialize = RPC(
    Tag.Initialize,
    extern struct {
        header: Message.Header,
        process_id: ipc.SendingProcessId,
    },
    extern struct {
        header: Message.Header,
        result: Result,
    },
);

const fs = struct {
    pub const Transaction = struct {
        pub const Id = u32;
    };
    pub const Archive = struct {
        pub const Handle = u32;
    };
    pub const Path = struct {
        pub const Type = u32;
    };
    pub const File = struct {
        pub const OpenFlags = u32;
    };
    pub const Attributes = u32;
};

pub const OpenFile = RPC(
    Tag.OpenFile,
    extern struct {
        header: Message.Header,
        transaction: fs.Transaction.Id,
        archive_handle: fs.Archive.Handle,
        path_type: fs.Path.Type,
        path_size: u32,
        open_flags: fs.File.OpenFlags,
        attributes: fs.Attributes,
        _: if (@bitSizeOf(usize) == 32) void else u32,
        path: ipc.BufferCopy(u8),
    },
    extern struct {
        header: Message.Header,
        result: Result,
        file_handle: ipc.DuplicateHandle,
    },
);
