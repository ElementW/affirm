// SPDX-License-Identifier: EUPL-1.2
pub const rpc = @import("fs/rpc.zig");

pub const Transaction = opaque {
    pub const Id = enum(u32) { _ };
};

pub const Archive = opaque {
    pub const Handle = enum(u64) { _ };
};

pub const Path = struct {
    pub const Type = enum(u32) { _ };
};

pub const File = struct {
    pub const OpenFlags = enum(u32) { _ };
};

pub const Attributes = packed struct(u32) {
    _: u32,
};
