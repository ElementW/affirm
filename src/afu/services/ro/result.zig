// SPDX-License-Identifier: EUPL-1.2
const Result = @import("../../result.zig").Result;

pub const RoResult = packed struct(u32) {
  enum Description {
    AlreadyLoaded = 1
  };

  constexpr Description description() const { return D(parts.description); }
};