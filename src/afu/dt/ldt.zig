// SPDX-License-Identifier: EUPL-1.2
/// LDT: Live Device Tree
const std = @import("std");
const afu = @import("../afu.zig");
const Cell = afu.dt.Cell;
const BECell = afu.dt.BECell;
const LDT = @This();

pub const Error = std.mem.Allocator.Error || error{NameTooLong};

pub const Property = struct {
    pub const Name = []const u8;
    pub const NameIndex = u32;
    const DirectCells = 3;
    const DirectBytes = @sizeOf(Cell) * DirectCells;

    byte_count: Cell,
    storage: extern union {
        direct: [DirectCells]Cell,
        heap: [*]const Cell,
    },

    inline fn isHeap(prop: *const Property) bool {
        return prop.byte_count > DirectBytes;
    }
    inline fn directU8Slice(prop: *const Property) []const u8 {
        return @as([*]const u8, @ptrCast(&prop.storage.direct))[0..prop.byteCount()];
    }
    inline fn heapU8Slice(prop: *const Property) []const u8 {
        return @as([*]const u8, @ptrCast(prop.storage.heap))[0..prop.byteCount()];
    }
    fn u8Slice(prop: *const Property) []const u8 {
        if (prop.isHeap()) {
            return prop.heapU8Slice();
        } else {
            return prop.directU8Slice();
        }
    }
    inline fn directCellSlice(prop: *const Property) []const Cell {
        return prop.storage.direct[0..prop.cellCount()];
    }
    inline fn heapCellSlice(prop: *const Property) []const Cell {
        return prop.storage.heap[0..prop.cellCount()];
    }
    fn cellSlice(prop: *const Property) []const Cell {
        if (prop.isHeap()) {
            return prop.heapCellSlice();
        } else {
            return prop.directCellSlice();
        }
    }

    pub fn deinit(prop: *const Property, ldt: *LDT) void {
        if (prop.isHeap()) {
            ldt.allocator.free(prop.heapCellSlice());
        }
    }

    /// Number of bytes this Property's value takes.
    pub fn byteCount(prop: *const Property) u32 {
        return prop.byte_count;
    }
    /// Number of cells this Property's value takes.
    pub fn cellCount(prop: *const Property) u32 {
        return (prop.byte_count + (@sizeOf(Cell) - 1)) / @sizeOf(Cell);
    }
    /// Count how many cells it would take to encode this property into an FDT, including the tag.
    pub fn encodedCellCount(prop: *const Property) usize {
        return 1 + // Tag
            2 + // Data length & name offset
            prop.cellCount();
    }

    // pub fn getType(prop: *const Property) Type {
    //     return switch (prop.raw_value) {
    //         .empty => .empty,
    //         .cell_1 => .cell,
    //         .cell_2, .cell_3, .cell_4, .cell_sequence => .cell_sequence,
    //     };
    // }
    // pub fn value(prop: *const Property) Value {
    //     return switch (prop.raw_value) {
    //         .empty => .{ .empty = {} },
    //         .cell_1 => |cell| .{ .cell = cell },
    //         .cell_2 => |cellseq| .{ .cell_sequence = &cellseq },
    //         .cell_3 => |cellseq| .{ .cell_sequence = &cellseq },
    //         .cell_4 => |cellseq| .{ .cell_sequence = &cellseq },
    //         .cell_sequence => |cellseq| .{ .cell_sequence = cellseq },
    //         .string => |str| .{ .string = str },
    //         .string_sequence => |strseq| .{ .string_sequence = strseq.strings() },
    //     };
    // }

    pub fn copy(prop: *const Property, ldt: *LDT) Error!Property {
        return if (prop.isHeap()) fromCellSequence(ldt, prop.asCellSequence()) else prop.*;
    }

    fn empty() Property {
        return .{ .byte_count = 0, .storage = .{ .direct = [3]Cell{ 0, 0, 0 } } };
    }

    fn fromCell(cell: Cell) Property {
        return .{ .byte_count = @sizeOf(Cell), .storage = .{ .direct = [3]Cell{ cell, 0, 0 } } };
    }
    pub fn asCell(prop: *const Property) ?Cell {
        return if (prop.byte_count >= @sizeOf(Cell)) prop.storage.direct[0] else null;
    }

    fn fromBECellSequence(ldt: *LDT, cells: []const BECell) Error!Property {
        if (cells.len <= DirectCells) {
            var buf: [DirectCells]Cell = undefined;
            for (buf[0..cells.len], cells) |*dest, src| {
                dest.* = src.get();
            }
            return .{
                .byte_count = @intCast(@sizeOf(Cell) * cells.len),
                .storage = .{ .direct = buf },
            };
        } else {
            const buf = try ldt.allocator.alloc(Cell, cells.len);
            for (buf, cells) |*dest, src| {
                dest.* = src.get();
            }
            return .{
                .byte_count = @intCast(@sizeOf(Cell) * cells.len),
                .storage = .{ .heap = buf.ptr },
            };
        }
    }
    fn fromCellSequence(ldt: *LDT, cells: []const Cell) Error!Property {
        if (cells.len <= DirectCells) {
            var buf: [DirectCells]Cell = undefined;
            @memcpy(buf[0..cells.len], cells);
            return .{
                .byte_count = @intCast(@sizeOf(Cell) * cells.len),
                .storage = .{ .direct = buf },
            };
        } else {
            return .{
                .byte_count = @intCast(@sizeOf(Cell) * cells.len),
                .storage = .{ .heap = (try ldt.allocator.dupe(Cell, cells)).ptr },
            };
        }
    }
    pub fn asCellSequence(prop: *const Property) []const Cell {
        return prop.cellSlice();
    }

    fn fromString(ldt: *LDT, string: []const u8) Error!Property {
        if (string.len <= DirectBytes) {
            var buf: [DirectCells]Cell = undefined;
            @memcpy(@as([*]u8, @ptrCast(&buf))[0..string.len], string);
            return .{
                .byte_count = @intCast(string.len),
                .storage = .{ .direct = buf },
            };
        } else {
            const buf = try ldt.allocator.allocWithOptions(u8, string.len, @alignOf(Cell), null);
            @memcpy(buf, string);
            return .{
                .byte_count = @intCast(string.len),
                .storage = .{ .heap = @ptrCast(buf.ptr) },
            };
        }
    }
    pub fn asString(prop: *const Property) []const u8 {
        return prop.u8Slice();
    }

    // fn fromStringSequence(ldt: *LDT, strings: []const []const u8) Error!Property {
    //     return .{};
    // }
    pub fn asStringSequence(prop: *const Property) std.mem.SplitIterator(u8, .scalar) {
        return std.mem.splitScalar(u8, prop.u8Slice(), 0);
    }

    pub fn guessInterpretation(prop: *const Property) afu.dt.Interpretation {
        return afu.dt.Interpretation.guess(prop.u8Slice());
    }
};

pub const Node = struct {
    properties: std.StringArrayHashMapUnmanaged(Property) = .{},
    nodes: std.StringArrayHashMapUnmanaged(Node) = .{},

    pub fn deinit(self: *Node, ldt: *LDT) void {
        // Property names are in ldt.name_pool and intentionally not freed here
        for (self.properties.values()) |prop| {
            prop.deinit(ldt);
        }
        self.properties.deinit(ldt.allocator);
        for (self.nodes.keys()) |node_name| {
            ldt.allocator.free(node_name);
        }
        for (self.nodes.values()) |*node| {
            node.deinit(ldt);
        }
        self.nodes.deinit(ldt.allocator);
    }

    // Nodes

    pub fn ensureNodeCapacity(self: *Node, ldt: *LDT, count: usize) Error!void {
        try self.nodes.ensureTotalCapacity(ldt.allocator, count);
    }

    pub inline fn addNode(self: *Node, ldt: *LDT, name: []const u8) Error!*Node {
        const res = try self.nodes.getOrPut(ldt.allocator, try ldt.allocator.dupe(u8, name));
        if (res.found_existing) {
            // If this node already exists, erase it.
            res.value_ptr.deinit(ldt);
        }
        res.value_ptr.* = .{};
        return res.value_ptr;
    }
    pub fn getNode(self: *Node, name: []const u8) ?*Node {
        return self.nodes.getPtr(name);
    }
    pub inline fn hasNode(self: *Node, name: []const u8) bool {
        return self.nodes.contains(name);
    }

    // Properties

    pub fn ensurePropertyCapacity(self: *Node, ldt: *LDT, count: usize) Error!void {
        try self.properties.ensureTotalCapacity(ldt.allocator, count);
    }

    fn putProperty(self: *Node, ldt: *LDT, name: []const u8, prop: Property) Error!void {
        const new_prop = if (self.properties.getPtr(name)) |existing_prop| brk: {
            existing_prop.deinit(ldt);
            break :brk existing_prop;
        } else (try self.properties.getOrPut(ldt.allocator, try ldt.name_pool.intern(ldt.allocator, name))).value_ptr;
        new_prop.* = prop;
    }

    pub fn setProperty(self: *Node, ldt: *LDT, name: []const u8, prop: *const Property) Error!void {
        try self.putProperty(ldt, name, try prop.copy(ldt));
    }
    pub inline fn getProperty(self: *const Node, name: []const u8) ?*const Property {
        return self.properties.getPtr(name);
    }
    pub inline fn hasProperty(self: *const Node, name: []const u8) bool {
        return self.properties.contains(name);
    }
    pub fn unsetProperty(self: *Node, ldt: *LDT, name: []const u8) void {
        if (self.properties.fetchSwapRemove(name)) |old_prop| {
            old_prop.value.deinit(ldt);
        }
    }

    pub fn setEmpty(self: *Node, ldt: *LDT, name: []const u8) Error!void {
        try self.putProperty(ldt, name, Property.empty());
    }

    pub fn setBool(self: *Node, ldt: *LDT, name: []const u8, value: bool) Error!void {
        if (value) {
            try self.setEmpty(ldt, name);
        } else {
            self.unsetProperty(ldt, name);
        }
    }
    pub fn getBool(self: *const Node, name: []const u8) bool {
        return self.hasProperty(name);
    }

    pub fn setCell(self: *Node, ldt: *LDT, name: []const u8, value: Cell) Error!void {
        try self.putProperty(ldt, name, Property.fromCell(value));
    }
    pub fn getCell(self: *const Node, name: []const u8) ?Cell {
        return (self.getProperty(name) orelse return null).asCell();
    }

    pub fn setBECellSequence(self: *Node, ldt: *LDT, name: []const u8, value: []const BECell) Error!void {
        try self.putProperty(ldt, name, try Property.fromBECellSequence(ldt, value));
    }
    pub fn setCellSequence(self: *Node, ldt: *LDT, name: []const u8, value: []const Cell) Error!void {
        try self.putProperty(ldt, name, try Property.fromCellSequence(ldt, value));
    }
    pub fn getCellSequence(self: *const Node, name: []const u8) ?[]const Cell {
        return (self.getProperty(name) orelse return null).asCellSequence();
    }

    pub fn setString(self: *Node, ldt: *LDT, name: []const u8, value: []const u8) Error!void {
        try self.putProperty(ldt, name, try Property.fromString(ldt, value));
    }
    pub fn getString(self: *const Node, name: []const u8) ?[]const u8 {
        return (self.getProperty(name) orelse return null).asString();
    }

    // pub fn setStringSequence(self: *Node, ldt: *LDT, name: []const u8, value: []const []const u8) Error!void {
    //     try self.putProperty(ldt, name, try Property.fromStringSequence(ldt, value));
    // }
    pub fn getStringSequence(self: *const Node, name: []const u8) ?std.mem.SplitIterator(u8, .scalar) {
        return (self.getProperty(name) orelse return null).asStringSequence();
    }
};

const NamePool = struct {
    pub const Header = extern struct {
        length: u8,

        comptime {
            if (@alignOf(@This()) != 1) {
                @compileError("Alignment of " ++ @typeName(@This()) ++ "is not 1");
            }
        }
    };
    const max_length = std.math.maxInt(@FieldType(Header, "length"));
    const constant_empty = "";

    blocks: [][*]u8,
    block_size: u32,
    last_block_usage: u32,

    fn init(allocator: std.mem.Allocator) Error!@This() {
        const block_size = 4096;
        const first_block = try allocator.alloc(u8, block_size);
        errdefer allocator.free(first_block);
        const blocks = try allocator.dupe([*]u8, &.{first_block.ptr});
        @memset(first_block, 0);
        return .{
            .blocks = blocks,
            .block_size = block_size,
            .last_block_usage = 0,
        };
    }

    fn deinit(pool: *@This(), allocator: std.mem.Allocator) void {
        for (pool.blocks) |block| {
            allocator.free(block[0..pool.block_size]);
        }
        allocator.free(pool.blocks);
    }

    fn find(pool: *const @This(), string: []const u8) Error!?[]const u8 {
        if (string.len == 0) {
            return constant_empty;
        } else if (string.len > max_length) {
            return Error.NameTooLong;
        }
        for (pool.blocks) |block| {
            var offset: u32 = 0;
            while (offset < pool.block_size) {
                const header = @as(*const Header, @ptrCast(&block[offset]));
                if (header.length == 0) {
                    break;
                }
                const interned = block[offset + @sizeOf(Header) ..][0..header.length];
                if (header.length == string.len and std.mem.eql(u8, string, interned)) {
                    return interned;
                }
                offset += @sizeOf(Header) + header.length;
            }
        }
        return null;
    }

    fn intern(pool: *@This(), allocator: std.mem.Allocator, string: []const u8) Error![]const u8 {
        if (try pool.find(string)) |interned| {
            return interned;
        }
        var last_block = pool.blocks[pool.blocks.len - 1];
        const end = pool.last_block_usage + @sizeOf(Header) + string.len;
        // If there is not enough memory for the new string, allocate a new block
        if (end >= pool.block_size) {
            const new_block = try allocator.alloc(u8, pool.block_size);
            errdefer allocator.free(new_block);
            pool.blocks = try allocator.realloc(pool.blocks, pool.blocks.len + 1);
            pool.blocks[pool.blocks.len - 1] = new_block.ptr;
            @memset(new_block, 0);
            pool.last_block_usage = 0;
            last_block = new_block.ptr;
        }
        // Insert new interned string
        const string_len: u8 = @intCast(string.len);
        @as(*Header, @ptrCast(&last_block[pool.last_block_usage])).* = .{ .length = string_len };
        const interned = last_block[pool.last_block_usage + @sizeOf(Header) ..][0..string.len];
        std.mem.copyForwards(u8, interned, string);
        pool.last_block_usage += @sizeOf(Header) + string_len;
        return interned;
    }
};

allocator: std.mem.Allocator,
name_pool: NamePool,
root: Node,

pub fn init(allocator: std.mem.Allocator) Error!LDT {
    return .{
        .allocator = allocator,
        .name_pool = try NamePool.init(allocator),
        .root = .{},
    };
}

pub fn deinit(ldt: *LDT) void {
    ldt.root.deinit(ldt);
    ldt.name_pool.deinit(ldt.allocator);
}

fn printRecursive(node_name: []const u8, node: *const Node, writer: std.io.AnyWriter, depth: usize) anyerror!void {
    try writer.writeByteNTimes(' ', depth * 2);
    try writer.print("{s} {{\n", .{node_name});
    for (node.properties.keys(), node.properties.values()) |name, prop| {
        try writer.writeByteNTimes(' ', (depth + 1) * 2);
        switch (prop.guessInterpretation()) {
            .bool_true => {
                try writer.print("{s};\n", .{name});
            },
            .cell => {
                const cellseq = prop.asCellSequence();
                try writer.print("{s} = <", .{name});
                for (0.., cellseq) |i, cell| {
                    if (cell >= 0x1000000) {
                        try writer.print("0x{X:0>8}", .{cell});
                    } else if (cell >= 0x10000) {
                        try writer.print("0x{X:0>6}", .{cell});
                    } else if (cell >= 0x100) {
                        try writer.print("0x{X:0>4}", .{cell});
                    } else {
                        try writer.print("0x{X:0>2}", .{cell});
                    }
                    if (i >= cellseq.len - 1) {
                        try writer.writeAll(">;\n");
                        break;
                    }
                    try writer.writeByte(' ');
                }
            },
            .string => {
                try writer.print("{s} = \"{s}\";\n", .{ name, prop.asString() });
            },
            .string_sequence => {
                try writer.print("{s} = \"", .{name});
                var strseq = prop.asStringSequence();
                while (strseq.next()) |str| {
                    try writer.writeAll(str);
                    if (strseq.rest().len != 0) {
                        try writer.writeAll("\", \"");
                    }
                }
                try writer.writeAll("\";\n");
            },
        }
    }
    for (node.nodes.keys(), node.nodes.values()) |child_name, *child| {
        try printRecursive(child_name, child, writer, depth + 1);
    }
    try writer.writeByteNTimes(' ', depth * 2);
    try writer.writeAll("}\n");
}

pub fn print(self: *const LDT, writer: std.io.AnyWriter) anyerror!void {
    try printRecursive("/", &self.root, writer, 0);
}

// pub const Alias = struct {
//     alias: []const u8,
//     node: []const u8,
// };

// const fdt: *align(8) const FDT = @ptrCast(&dtb);

// var buf: [1024 * 32]u8 align(16) = undefined;
// var a = std.heap.FixedBufferAllocator.init(&buf);
// //var b = std.heap.loggingAllocator(a.allocator());
// var ldt = try fdt.toLDT(a.allocator());
// try ldt.print(std.io.getStdOut().writer().any());

// var counter = std.io.countingWriter(std.io.null_writer);
// var inp = std.io.fixedBufferStream(&buf);
// try std.compress.zlib.compress(inp.reader(), counter.writer(), .{ .level = .best });
// std.log.info("{} -> {} ({d:.2}%)\n", .{ a.end_index, counter.bytes_written, @as(f32, @floatFromInt(counter.bytes_written)) / @as(f32, @floatFromInt(a.end_index)) * 100 });
