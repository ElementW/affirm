// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afu = @import("../afu.zig");
const Cell = afu.dt.Cell;
const BECell = afu.dt.BECell;
const LDT = afu.dt.LDT;

pub const FDT = opaque {
    pub const Error = error{
        /// Number of strings in a property's value exceed the allowed maximum.
        PropertyStringListTooLong,
        /// An unknown Flattened Device Tree token was encountered.
        UnknownToken,
        /// An End token was encountered early.
        EndToken,
        /// An end of stream was encountered early.
        EndOfStream,
        /// No root node was found.
        NoRoot,
        /// Encountered an unexpected token when looking for the tree root node.
        UnexpectedToken,
    };

    pub const Property = struct {
        pub const Guessed = struct {
            pub const Value = union(afu.dt.Interpretation) {
                bool_true: void,
                cell: []const BECell,
                string: []const u8,
                string_sequence: []const u8,
            };
            name: [:0]const u8,
            value: Value,

            pub fn bytesForValue(self: @This()) usize {
                return switch (self.value) {
                    .bool_true => 0,
                    .cell => |cells| cells.len * @sizeOf(BECell),
                    .string => |str| str.len,
                    .string_sequence => |str| str.len,
                };
            }
        };
        pub const Raw = struct {
            name: [:0]const u8,
            length: u32,
            value: []const BECell,

            pub fn guess(raw: *const Raw) Guessed {
                const data8 = @as([*]const u8, @ptrCast(raw.value))[0..raw.length];
                return switch (afu.dt.Interpretation.guess(data8)) {
                    .bool_true => .{
                        .name = raw.name,
                        .value = .{ .bool_true = {} },
                    },
                    .cell => .{
                        .name = raw.name,
                        .value = .{ .cell = raw.value },
                    },
                    .string, .string_sequence => .{
                        .name = raw.name,
                        .value = .{ .string = data8 },
                    },
                };
            }
        };
    };

    pub const Header = extern struct {
        const magic_constant = 0xD00DFEED;
        magic: BECell,
        totalsize: BECell,
        off_dt_struct: BECell,
        off_dt_strings: BECell,
        off_mem_rsvmap: BECell,
        version: BECell,
        last_comp_version: BECell,
        boot_cpuid_phys: BECell,
        size_dt_strings: BECell,
        size_dt_struct: BECell,
    };

    pub const ReserveEntry = extern struct {
        address: u64,
        size: u64,
    };

    pub const Token = enum(Cell) {
        begin_node = 1,
        end_node = 2,
        prop = 3,
        nop = 4,
        end = 9,
        _,
    };

    pub const Iterator = struct {
        pub const Item = union(enum) {
            begin_node: [:0]const u8,
            end_node: void,
            prop: Property.Raw,
            end: void,
        };
        fdt: *align(8) const FDT,
        index: usize,

        pub fn next(self: *@This()) !?Item {
            const st = self.fdt.structure();
            while (self.index < st.len) {
                switch (@as(Token, @enumFromInt(st[self.index].get()))) {
                    .begin_node => {
                        const name = std.mem.span(@as([*:0]const u8, @ptrCast(&st[self.index + 1])));
                        self.index += 1 + ((name.len + 4) >> 2);
                        return .{ .begin_node = name };
                    },
                    .end_node => {
                        self.index += 1;
                        return .{ .end_node = {} };
                    },
                    .prop => {
                        const length_bytes = st[self.index + 1].get();
                        const length_cells = (length_bytes + 3) >> 2;
                        const name = std.mem.span(@as([*:0]const u8, @ptrCast(self.fdt.strings()[st[self.index + 2].get()..])));
                        const item: Item = .{ .prop = .{
                            .name = name,
                            .length = length_bytes,
                            .value = st[self.index + 3 ..][0..length_cells],
                        } };
                        self.index += 3 + length_cells;
                        return item;
                    },
                    .end => {
                        return .{ .end = {} };
                    },
                    .nop => {
                        self.index += 1;
                    },
                    _ => {
                        return Error.UnknownToken;
                    },
                }
            }
            return null;
        }
    };

    pub fn isValid(self: *align(8) const FDT) bool {
        return self.header().magic.get() == Header.magic_constant;
    }

    fn header(self: *align(8) const FDT) *const Header {
        return @ptrCast(self);
    }

    fn reserves(self: *align(8) const FDT) [*]const ReserveEntry {
        return @ptrFromInt(@intFromPtr(self) + self.header().off_mem_rsvmap.get());
    }

    fn structure(self: *align(8) const FDT) []const BECell {
        return @as([*]const BECell, @ptrFromInt(@intFromPtr(self) + self.header().off_dt_struct.get()))[0 .. self.header().size_dt_struct.get() >> 2];
    }

    pub inline fn stringsTotalSize(self: *align(8) const FDT) u32 {
        return self.header().size_dt_strings.get();
    }

    fn strings(self: *align(8) const FDT) []const u8 {
        return @as([*]const u8, @ptrFromInt(@intFromPtr(self) + self.header().off_dt_strings.get()))[0..self.stringsTotalSize()];
    }

    // pub fn root(self: *align(8) const FDT) Error!*const Node {
    //     for (self.structure()) |*cell| {
    //         const token = @as(Token, @enumFromInt(std.mem.bigToNative(Cell, cell.*)));
    //         if (token == .begin_node) {
    //             return @ptrCast(cell);
    //         } else if (token != .nop) {
    //             return Error.UnexpectedToken;
    //         }
    //     }
    //     return Error.NoRoot;
    // }

    pub fn iterator(self: *align(8) const FDT) Iterator {
        return .{
            .fdt = self,
            .index = 0,
        };
    }

    pub fn print(self: *align(8) const FDT, writer: std.io.AnyWriter) (std.io.AnyWriter.Error || Error)!void {
        var it = self.iterator();
        var depth: u32 = 0;
        while (try it.next()) |item| {
            switch (item) {
                .begin_node => |name| {
                    try writer.writeByteNTimes(' ', depth * 2);
                    try writer.print("{s} {{\n", .{if (name.len == 0) "/" else name});
                    depth += 1;
                },
                .end_node => {
                    if (depth == 0) {
                        _ = try writer.write("};\n");
                        break;
                    }
                    depth -= 1;
                    try writer.writeByteNTimes(' ', depth * 2);
                    _ = try writer.write("};\n");
                },
                .prop => |raw_prop| {
                    try writer.writeByteNTimes(' ', depth * 2);
                    const prop = raw_prop.guess();
                    switch (prop.value) {
                        .bool_true => {
                            try writer.print("{s};\n", .{prop.name});
                        },
                        .cell => |cells| {
                            try writer.print("{s} = <", .{prop.name});
                            if (cells.len > 1) {
                                for (cells[0 .. cells.len - 1]) |cell| {
                                    try writer.print("0x{X:0>8} ", .{std.mem.bigToNative(Cell, cell)});
                                }
                            }
                            if (cells.len != 0) {
                                try writer.print("0x{X:0>8}", .{std.mem.bigToNative(Cell, cells[cells.len - 1])});
                            }
                            _ = try writer.write(">;\n");
                        },
                        .string => |str| {
                            try writer.print("{s} = \"{s}\";\n", .{ prop.name, str });
                        },
                        .string_sequence => |str| {
                            try writer.print("{s} = \"", .{prop.name});
                            var valueI: usize = 0;
                            while (true) {
                                const string = std.mem.span(@as([*:0]const u8, @ptrCast(str[valueI..])));
                                _ = try writer.write(string);
                                valueI += string.len + 1;
                                if (valueI >= str.len) {
                                    _ = try writer.write("\";\n");
                                    break;
                                }
                                _ = try writer.write("\", \"");
                            }
                        },
                    }
                },
                .end => if (depth == 0) {
                    return;
                } else {
                    return Error.EndToken;
                },
            }
        }
        return Error.EndOfStream;
    }

    fn toLDTNode(fdt: *align(8) const FDT, it: *FDT.Iterator, ldt: *LDT, ldt_node: *LDT.Node, depth: usize) (Error || LDT.Error)!void {
        // const pad = "                                      ";
        while (try it.next()) |item| {
            switch (item) {
                .begin_node => |name| {
                    // std.log.info("{s}BEGIN_NODE {s}", .{ pad[0 .. depth * 2], name });
                    const node = try ldt_node.addNode(ldt, name);
                    try toLDTNode(fdt, it, ldt, node, depth + 1);
                },
                .end_node => {
                    // std.log.info("{s}END_NODE nodes {}, props {}", .{ pad[0 .. depth * 2], ldt_node.nodes.count(), ldt_node.properties.count() });
                    return;
                },
                .prop => |prop_raw| {
                    const prop = prop_raw.guess();
                    // std.log.info("{s}PROP \"{s}\" {} bytes of {s}", .{ pad[0 .. depth * 2], prop.name, prop.bytesForValue(), @tagName(prop.value) });
                    switch (prop.value) {
                        .bool_true => {
                            try ldt_node.setEmpty(ldt, prop.name);
                        },
                        .cell => |cells| {
                            try ldt_node.setBECellSequence(ldt, prop.name, cells);
                        },
                        .string, .string_sequence => |str| {
                            try ldt_node.setString(ldt, prop.name, str);
                        },
                    }
                },
                .end => if (depth == 0) {
                    return;
                } else {
                    return Error.EndToken;
                },
            }
        }
        return Error.EndOfStream;
    }

    pub fn toLDT(fdt: *align(8) const FDT, allocator: std.mem.Allocator) (Error || LDT.Error)!LDT {
        var it = fdt.iterator();
        var ldt: LDT = try LDT.init(allocator);
        errdefer ldt.deinit();

        const next = try it.next();
        if (next) |item| switch (item) {
            .begin_node => {
                try toLDTNode(fdt, &it, &ldt, &ldt.root, 0);
                return ldt;
            },
            .end => {
                return Error.NoRoot;
            },
            else => {
                return Error.UnexpectedToken;
            },
        } else {
            return Error.NoRoot;
        }
    }
};
