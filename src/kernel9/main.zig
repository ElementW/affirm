// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afk = @import("afk");
const afsys = @import("afsys");
const afu = @import("afu");
const IRQControl = afk.arch.arm.mach.nintendo3ds.arm9.IRQControl;
const Timer = afk.arch.arm.mach.nintendo3ds.Timer;
const PSR = afk.arch.arm.PSR;
const I2C = afsys.plat.nintendo3ds.I2C;
const PXI = afsys.plat.nintendo3ds.PXI;
const Screen = afsys.plat.nintendo3ds.gfx.Screen;
const MCU = afsys.plat.nintendo3ds.MCU;
const EarlyPXI = afk.arch.arm.mach.nintendo3ds.EarlyPXI;
const Framebuffer = afsys.plat.nintendo3ds.gfx.Framebuffer;
const FourCC = afk.util.FourCC;
const LogScreen = afk.util.logging.LogScreen;
const Panic = afk.util.Panic;

const options = @import("options");

pub const std_options = std.Options{
    .logFn = afk.util.logging.LogScreen.zigLogFn,
};

extern const __start__: u32;
extern const __end__: u32;

fn log(str: []const u8) void {
    _ = str;
    //for ("[Kernel9] ") |c| {
    //    @intToPtr(*volatile u8, 0x10108000).* = c;
    //}
    //for (str) |c| {
    //    @intToPtr(*volatile u8, 0x10108000).* = c;
    //}
    //@intToPtr(*volatile u8, 0x10108000).* = 0;
}
pub fn panic(msg: []const u8, error_return_trace: ?*std.builtin.StackTrace, ret_addr: ?usize) noreturn {
    var buf: [128]u8 = undefined;
    log(std.fmt.bufPrint(&buf, "{s} from {X:0>8} (trace?{s})\n", .{ msg, @returnAddress(), if (error_return_trace == null) "no" else "yes" }) catch @trap());
    if (ret_addr) |r| {
        log(std.fmt.bufPrint(&buf, "Ret {X:0>8}\n", .{r}) catch @trap());
    }
    if (error_return_trace) |t| {
        for (t.instruction_addresses) |i| {
            log(std.fmt.bufPrint(&buf, "{X:0>8}\n", .{i}) catch @trap());
        }
    }
    // TODO use std.debug.writeCurrentStackTrace?
    var it = std.debug.StackIterator.init(@returnAddress(), null);
    while (it.next()) |return_address| {
        log(std.fmt.bufPrint(&buf, "at {X:0>8}\n", .{return_address}) catch @trap());
    }

    while (true) {
        @breakpoint();
    }
}

export fn kmain() noreturn {
    _ = &afk.arch.arm.syscall.af_SVCHandler;
    _ = &afk.arch.arm.irq.Acknowledger.af_zig_IRQAcknowledge;

    log("disable IRQs\n");
    IRQControl.disableAll();
    log("clear IRQs\n");
    IRQControl.clearAllPending();

    const cardstatus = @as(*volatile u8, @ptrFromInt(0x10000010));
    // Cartridge-slot power supply (0=off, 1=prepare power regulator, 2=enable output, 3=request power down)
    if (@as(u2, @truncate(cardstatus.* >> 2)) == 0) {
        cardstatus.* = 1 << 2;
        afk.chrono.busyWaitMs(20);
        cardstatus.* = 2 << 2;
    }

    log("Wait kernel\n");
    EarlyPXI.waitForOtherKernel();
    PXI.setFifoEnabled(true);
    PXI.clearSendFifo();

    log("I2C init\n");
    I2C.init();
    //MCU.setPowerLEDsBrightness(255) catch unreachable;
    //MCU.setWifiLEDOn(false) catch unreachable;

    log("Init screens\n");
    PXI.sendOne(@intFromEnum(EarlyPXI.Command.init_screens));
    const top_screen = @as([*]align(4) u8, @ptrFromInt(PXI.recvOne()));
    const bottom_screen = @as([*]align(4) u8, @ptrFromInt(PXI.recvOne()));
    _ = bottom_screen;

    //MCU.setWifiLEDOn(true) catch unreachable;
    log("Set screen color\n");
    PXI.send(&[_]u32{
        @intFromEnum(EarlyPXI.Command.set_bottom_screen_color),
        afu.gfx.Color.RGBA8888.fromRGB(255, 0, 0).toU32(),
    });

    log("Exit early PXI\n");
    PXI.sendOne(@intFromEnum(EarlyPXI.Command.exit_early_pxi));

    log("LogScreen\n");
    var ls = LogScreen.init(
        top_screen,
        Screen.TopWidth,
        Screen.Height,
        Framebuffer.ColorFormat.rgba8888,
    );
    LogScreen.default = &ls;

    log("Start\n");
    ls.logLine("Affinis v1/+inf");
    ls.logLine("For Nintendo \xBD\xBE\xBF");

    log("MCU\n");
    {
        const mcuVer = MCU.version();
        std.log.info("MCU {}.{} vcc={}", .{ mcuVer.high, mcuVer.low, MCU.voltage() });
        var mask = MCU.interruptMask();
        mask.power_button_press = true;
        MCU.setInterruptMask(mask) catch std.log.warn("setInterruptMask fail", .{});
    }
    std.log.info("0x{X:0>8} - 0x{X:0>8}; binary size = 0x{X:0>8} = {d}", .{
        @intFromPtr(&__start__),
        @intFromPtr(&__end__),
        @intFromPtr(&__end__) - @intFromPtr(&__start__),
        @intFromPtr(&__end__) - @intFromPtr(&__start__),
    });

    log("IRQ ON!\n");
    PSR.enableInterrupts();
    IRQControl.enable(.{ .timer0 = true, .timer1 = true });

    log("Timer0\n");
    const timer = Timer.get(0);
    timer.configure(.div64, false, true);
    std.log.info("timer.control().enableIRQ = {}", .{timer.controlReg().read().enable_irq});
    timer.setValue(0);
    timer.start();

    const timer2 = Timer.get(1);
    timer2.configure(.div1024, false, false);
    timer2.setValue(0x7FFF / 2);
    timer2.start();
    std.log.info("Timers running: {} {}", .{ timer.running(), timer2.running() });

    //EMMC.reset();
    //NAND.init() catch |err| {
    //  std.log.info("{s}", .{ @errorName(err) });
    //};

    //{
    //  //*reinterpret_cast<volatile uint16*>(0x10147028) = 0;
    //  *registers.Config11.WifiControl = 0;
    //  *registers.Config11.WifiUnknown = 0;
    //
    //  //*reinterpret_cast<volatile uint16*>(0x10147028) = 1;  // Actually enable wifi power
    //  *registers.Config11.WifiControl = 1;
    //  *registers.Config11.WifiUnknown = (1 << 4);
    //
    //  WiFiSDIO->reset();
    //  WiFiSDIO->setDataMode(DataMode.Data32);
    //  MCU.setWifiLEDOn(true);
    //  //util.Time.busyWaitMs(5);
    //  Command.Response resp;
    //  discard WiFiSDIO->sendCommand(Commands.GoIdleState, 0, &resp);
    //  //using (auto parm = zero_init<Commands.SendIfCond.Arg>()) {
    //  //  parm.checkPattern = 0xAF;
    //  //  parm.supplyVoltage = SupplyVoltage.V2_7To3_6;
    //  //  discard sendCommand(Commands.SendIfCond, parm, &resp);
    //  //  Log.raw("init: {X:0>8} {X:0>8} {X:0>8} {X:0>8}", .{ resp.r2_32[0], resp.r2_32[1], resp.r2_32[2], resp.r2_32[3] });
    //  //}
    //  do {
    //    while (!HID.isButtonDown(HID.PadIdx.Left));
    //    using (auto parm = zero_init<Commands.IOSendOpCond.Arg>()) {
    //      parm.voltageRange = IOVoltageRange.V3_2To3_3;
    //      if (auto res = WiFiSDIO->sendCommand(Commands.IOSendOpCond, parm, &resp); res.error()) {
    //        Log.raw("{s}.{s}", .{ res.error()->errorNamespace->name, res.error()->name });
    //        continue;
    //      }
    //      Log.raw("opcond: {X:0>8}", .{ resp.r4_32 });
    //      if (!(resp.r4_32 & 0x80000000)) {
    //        continue;
    //      }
    //    }
    //    break;
    //  } while (true);
    //  while (!HID.isButtonDown(HID.PadIdx.Right));
    //  discard WiFiSDIO->sendCommand(Commands.SendRelativeAddr, 0, &resp);
    //  Log.raw("reladdr: rca={X:0>4} status={X:0>4} {X:0>8}", .{ resp.r6.newPublishedRCA, resp.r6.cardStatusBits, resp.r6_32 });
    //  discard WiFiSDIO->sendCommand(Commands.SelectCard, resp.r6.newPublishedRCA << 16, &resp);
    //  Log.raw("sel: {X:0>8}", .{ resp.r1_32 });
    //  while (!HID.isButtonDown(HID.PadIdx.Left));
    //  using (auto parm = zero_init<Commands.IORWDirect.Arg>()) {
    //    parm.registerAddress = 4;
    //    parm.functionNumber = 0;
    //    parm.direction = parm.Read;
    //    discard WiFiSDIO->sendCommand(Commands.IORWDirect, parm, &resp);
    //    Log.raw("f0r4: {X:0>8} f={X:0>8}", .{ resp.r5.data, resp.r5.flags });
    //  }
    //}

    std.log.info("WFI loop begin", .{});
    while (true) {
        const zero: u32 = 0;
        asm volatile ("MCR p15, 0, %[zero], c7, c0, 4"
            :
            : [zero] "r" (zero),
        );
    }

    Panic.panic(.KMain, .KMainExited);
}
