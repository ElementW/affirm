// SPDX-License-Identifier: EUPL-1.2
//! WiFi SDIO registers namespace.
//! See :3dbrew:`EMMC_Registers`, Linux ``drivers/mmc/host/tmio_mmc.h``, and
//! https://problemkaputt.de/gbatek.htm#dsisdmmcioportscommandparamresponsedata
const reg = @import("Helpers.zig").reg;

pub const Controller = reg(void, 0x10122000);
