// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const Build = std.Build;
const Step = Build.Step;
const modules_build = @import("../build.zig");
const ModuleCompileParams = modules_build.ModuleCompileParams;

pub fn compile(b: *Build, params: *const ModuleCompileParams) *Step.Compile {
    _ = b;
    _ = params;
    // b.addExecutable(options: ExecutableOptions);
    @panic("NYI");
}
