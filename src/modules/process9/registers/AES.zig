// SPDX-License-Identifier: EUPL-1.2
//! AES control registers namespace.
//! See https://www.3dbrew.org/wiki/AES_Registers
const assert = @import("std").debug.assert;
const Only = @import("../../platform/Only.zig");
const reg = @import("Helpers.zig").reg;
const roreg = @import("Helpers.zig").roreg;

comptime {
    Only.core(.ARM9);
}

const Base: usize = 0x10009000;
pub const Control = reg(u32, Base);
pub const MACBlockControl = reg(u16, Base + 0x4);
pub const BlockControl = reg(u16, Base + 0x6);
pub const WriteFIFO = reg(u32, Base + 0x8);
pub const ReadFIFO = roreg(u32, Base + 0xC);
pub const KeySelect = reg(u8, Base + 0x10);
pub const KeyControl = reg(u8, Base + 0x11);

pub const CTR = reg(extern union {
    ctr_counter: [4]u32 align(16),
    ccm_nonce: [4]u32 align(16),
    cbc_iv: [4]u32 align(16),
}, Base + 0x20);
comptime {
    assert(@sizeOf(CTR.Value) == 16);
}

pub const MAC = reg(Base + 0x30, extern struct {
    ccm_mac: [4]u32 align(16),
});
comptime {
    assert(@sizeOf(MAC.Value) == 16);
}

pub const Key = extern struct {
    key: [4]u32 align(16),
};
comptime {
    assert(@sizeOf(Key) == 16);
}

pub const KeySet = extern struct {
    normalkey: Key,
    key_x: Key,
    key_y: Key,
};
comptime {
    assert(@sizeOf(KeySet) == 48);
}
pub const Key0 = reg(Base + 0x40, KeySet);
pub const Key1 = reg(Base + 0x70, KeySet);
pub const Key2 = reg(Base + 0xA0, KeySet);
pub const Key3 = reg(Base + 0xD0, KeySet);

pub const KeyFIFO = reg(Base + 0x100, u32);
pub const KeyXFIFO = reg(Base + 0x104, u32);
pub const KeyYFIFO = reg(Base + 0x108, u32);
