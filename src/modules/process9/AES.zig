// SPDX-License-Identifier: EUPL-1.2
const Only = @import("../platform/Only.zig");
const reg = @import("registers/AES.zig");

comptime {
  Only.core(.ARM9);
}

pub const Key = reg.Key;

/// List of typical AES keyslot usages.
/// See https://www.3dbrew.org/wiki/AES_Registers#Keyslots
/// Keyslots used only temporarily by processes skipped under Affinis (such as arm9loader)
/// are not (and should not be) part of this list.
pub const Keyslot = enum(u6) {
  TWL0 = 0x00,
  TWL1 = 0x01,
  TWL2 = 0x02,
  TWL3 = 0x03,
  NAND0 = 0x04,
  NAND1 = 0x05,
  NAND2 = 0x06,
  NAND3 = 0x07,
  DSiWareExportZero = 0x0A,
  NANDdbs = 0x0B,
  MovableSedCMAC = 0x0B,
  ClCertA = 0x0D,
  Temp0 = 0x11,
  New3DSNCCH0x0A = 0x18,
  New3DSSavedataCMAC = 0x19,
  New3DSSavedata = 0x1A,
  New3DSNCCH0x0B = 0X1B,
  AGB_FIRMSavegameCMAC = 0x24,
  NCCH0x01 = 0x25,
  NCCH0x00 = 0x2C,
  UDSLocalWLANCCMP = 0x2D,
  StreetPass = 0x2E,
  V6_0Save = 0x2F,
  SDNANDCMAC = 0x30,
  APTWrap = 0x31,
  Unknown0x32 = 0x32,
  GamecardSavedataCMAC = 0x33,
  SD = 0x34,
  MovableSed = 0x35,
  GamecardSavedata = 0x37,
  BOSS = 0x38,
  DownloadPlay = 0x39,
  Amiibo = 0x39,
  DSiWareExport = 0x3A,
  CTRCARDSeed = 0x3B,
  CommonKey = 0x3D,
};

pub const MACInputSource = enum(u1) {
  fifo = 0,
  register = 1,
};
pub const Endianness = enum(u1) {
  little_endian = 0,
  big_endian = 1,
};
pub const WordOrder = enum(u1) {
  reversed_order = 0,
  normal_order = 1,
};
pub const Mode = enum(u3) {
  ccm_decrypt = 0,
  ccm_encrypt = 1,
  ctr_decrypt = 2,
  ctr_encrypt = 3,
  cbc_decrypt = 4,
  cbc_encrypt = 5,
  ecb_decrypt = 6,
  ecb_encrypt = 7,
};

pub const AESControl = packed struct(u32) {
  write_fifo_count: u5, // const
  read_fifo_count: u5, // const
  flush_write_fifo: bool,
  flush_read_fifo: bool,
  write_fifo_dma_size: u2, // const
  read_fifo_dma_size: u2, // const
  mac_size: u3,
  _: u1,
  mac_input_control: MACInputSource,
  mac_verified: bool, // const
  output_endianness: Endianness,
  input_endianness: Endianness,
  output_word_order: WordOrder,
  input_word_order: WordOrder,
  update_keyslot: bool,
  mode: Mode,
  interrupt_enable: bool,
  start: bool,
};

pub const KeyGeneratorType = enum(u1) {
  N3DS = 0,
  DSi = 1,
};

pub const KeyControl = packed struct(u8) {
  keyslot: Keyslot,
  key_generator_type: KeyGeneratorType,
  enable_key_fifo_flush: bool,
};

pub fn setKeyslot(keyslot: Keyslot) void {
  registers::AES::KeyControl.as<volatile KeyControl*>()->keyslot = keyslot;
}

pub fn updateKeyslot() void {
  registers::AES::Control.as<volatile AESControl*>()->updateKeyslot = true;
}

pub fn setKeyX(keyslot: Keyslot, keyX: Key) void {
  change_v_register(registers::AES::Control.as<volatile AESControl*>(), ctl) {
    ctl.macInputControl = 1;
  }
  //*REG_AESKEYCNT = (*REG_AESKEYCNT >> 6 << 6) | keyslot | 0x80;
  //if (keyslot > Keyslots::TWL3) {
  //    *REG_AESKEYXFIFO = _keyx[0];
  //    *REG_AESKEYXFIFO = _keyx[1];
  //    *REG_AESKEYXFIFO = _keyx[2];
  //    *REG_AESKEYXFIFO = _keyx[3];
  //} else {
  //    uint32_t old_aescnt = *REG_AESCNT;
  //    volatile uint32_t* reg_aeskeyx = REG_AESKEY0123 + (((0x30u * keyslot) + 0x10u)/4u);
  //    *REG_AESCNT = (*REG_AESCNT s& ~(AES_CNT_INPUT_ENDIAN | AES_CNT_INPUT_ORDER));
  //    for (uint32_t i = 0; i < 4u; i++)
  //        reg_aeskeyx[i] = _keyx[i];
  //    *REG_AESCNT = old_aescnt;
  //}
}