// SPDX-License-Identifier: EUPL-1.2
const uefi = @import("std").os.uefi;

fn a() void {
    var graphics_output_protocol: ?*uefi.protocol.GraphicsOutput = undefined;
    if (uefi.system_table.boot_services.?.locateProtocol(
        &uefi.protocol.GraphicsOutput.guid,
        null,
        @as(*?*anyopaque, @ptrCast(&graphics_output_protocol)),
    ) == .Success) {
        // Check supported resolutions:
        var i: u32 = 0;
        while (i < graphics_output_protocol.?.mode.max_mode) : (i += 1) {
            var info: *uefi.protocols.GraphicsOutputModeInformation = undefined;
            var info_size: usize = undefined;
            _ = graphics_output_protocol.?.queryMode(i, &info_size, &info);
            printf(buf[0..], "    mode {} = {}x{}\r\n", .{ i, info.horizontal_resolution, info.vertical_resolution });
        }

        printf(buf[0..], "    current mode = {}\r\n", .{graphics_output_protocol.?.mode.mode});
    } else {
        puts("*** graphics output protocol is NOT supported :(\r\n");
    }
}
