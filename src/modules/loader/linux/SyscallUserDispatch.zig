// SPDX-License-Identifier: EUPL-1.2
const builtin = @import("builtin");
const std = @import("std");

const PR_SET_SYSCALL_USER_DISPATCH = 59;
const PR_SYS_DISPATCH_OFF=0;
const PR_SYS_DISPATCH_ON=1;
const SYSCALL_DISPATCH_FILTER_ALLOW=0;
const SYSCALL_DISPATCH_FILTER_BLOCK=1;

prctl_enabled: bool,
enable_flag: u8,

fn sigsysHandler(_: i32, si: *const std.os.linux.siginfo_t, opaque_uctx: ?*anyopaque) callconv(.c) void {
    const uctx: *std.os.linux.ucontext_t = @ptrCast(opaque_uctx);
    switch (builtin.target.cpu.arch) {
        .x86_64 => {
            uctx.mcontext.gregs[std.os.linux.REG.EDI]
        },
        else => |a| @compileError("Architecture " ++ @tagName(a) ++ " not supported")
    }
    @type
}

fn setup(self: *@This()) void {
    std.os.linux.prctl(PR_SET_SYSCALL_USER_DISPATCH, PR_SYS_DISPATCH_ON, arg3: usize, arg4: usize, arg5: usize);
    // int prctl(PR_SET_SYSCALL_USER_DISPATCH, PR_SYS_DISPATCH_ON, unsigned long off, unsigned long size, int8_t *switch);

    var sa: std.posix.Sigaction = .{
        .handler = .{ .sigaction = &sigsysHandler },
        .mask = std.posix.empty_sigset,
        .flags = std.posix.SA.RESTART,
    };
    try std.posix.sigaction(std.posix.SIG.SYS, &sa, null);
}
