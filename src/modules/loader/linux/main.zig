// SPDX-License-Identifier: EUPL-1.2
const builtin = @import("builtin");
const std = @import("std");

pub fn main() !void {
    comptime std.debug.assert(!builtin.link_libc);
    comptime std.debug.assert(builtin.link_mode == .static);
    comptime std.debug.assert(builtin.target.os.tag == .linux);

    const kernel_socket_fd = 3;
    var kernel_socket_addr: std.posix.sockaddr = undefined;
    var kernel_socket_addr_len: std.posix.socklen_t = @sizeOf(kernel_socket_addr);
    std.posix.getsockname(kernel_socket_fd, &kernel_socket_addr, &kernel_socket_addr_len) catch |e| {
        std.log.err("loader must be launched by the kernel ({!})", .{e});
    };
}
