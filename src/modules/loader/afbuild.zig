// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const Build = std.Build;
const Step = Build.Step;

const afbuild = @import("../../../afbuild.zig");
const SysModuleBuild = afbuild.modules.SysModuleBuild;
const SysModuleSteps = afbuild.modules.SysModuleSteps;

const decl = .{
    .name = "loader",
    .id = "affinis.loader",
    .app_type = .system,
    .stack_size = 0x1000,
    .resource_limit_category = .system,
    .run_in_sleep_mode = true,
    .syscalls = .{},
    .services = .{
        .access = &.{
            "fs:LDR",
        },
        .provide = &.{
            "Loader",
        },
    },
    .machine_specific = .{
        .nintendo3ds = .{
            .title_id = 0x0004013000001302, // 0x0004013000001303 SAFE_MODE
        },
    },
};

const source_path = "src/modules/loader/";
pub fn build(b: *SysModuleBuild) SysModuleSteps {
    const loader = b.addSysModule(.{
        .root_source_file = b.path("main.zig"),
    });
    return loader;
}
