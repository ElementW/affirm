// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afk = @import("afk");
const afsys = @import("afsys");
const PXI = afsys.plat.nintendo3ds.PXI;
const EarlyPXI = afk.arch.arm.mach.nintendo3ds.EarlyPXI;
const ScreenInit = afsys.plat.nintendo3ds.gfx.ScreenInit;
const panic2 = afk.util.Panic.panic;

pub const std_options = std.Options{
    .logFn = afk.util.logging.LogScreen.zigLogFn,
};

var top_screen: usize = undefined;
var bottom_screen: usize = undefined;

fn log(str: []const u8) void {
    _ = str;
    //for ("[Kernel11] ") |c| {
    //    @intToPtr(*volatile u8, 0x10108000).* = c;
    //}
    //for (str) |c| {
    //    @intToPtr(*volatile u8, 0x10108000).* = c;
    //}
    //@intToPtr(*volatile u8, 0x10108000).* = 0;
}

pub fn panic(msg: []const u8, error_return_trace: ?*std.builtin.StackTrace, ret_addr: ?usize) noreturn {
    var buf: [128]u8 = undefined;
    log(std.fmt.bufPrint(&buf, "{s} from {X:0>8} (trace?{s})\n", .{ msg, @returnAddress(), if (error_return_trace == null) "no" else "yes" }) catch @trap());
    if (ret_addr) |r| {
        log(std.fmt.bufPrint(&buf, "Ret {X:0>8}\n", .{r}) catch @trap());
    }
    if (error_return_trace) |t| {
        for (t.instruction_addresses) |i| {
            log(std.fmt.bufPrint(&buf, "{X:0>8}\n", .{i}) catch @trap());
        }
    }
    // TODO use std.debug.writeCurrentStackTrace?
    var it = std.debug.StackIterator.init(@returnAddress(), null);
    while (it.next()) |return_address| {
        log(std.fmt.bufPrint(&buf, "at {X:0>8}\n", .{return_address}) catch @trap());
    }

    while (true) {
        @breakpoint();
    }
}

fn enableGpu() void {
    afk.arch.arm.mach.nintendo3ds.arm11.Config11.GPUControl.write(0x1007F);
}
fn enableBacklight() void {
    afsys.plat.nintendo3ds.MCU.setLCDBacklight(true, true) catch {};
}

fn arm9CommandLoop() void {
    log("Enter early PXI command loop\n");
    while (true) {
        PXI.busyWaitForRecvFifoNotEmpty();
        switch (@as(EarlyPXI.Command, @enumFromInt(PXI.recvOne()))) {
            .init_screens => {
                log("EarlyPXI: InitScreens\n");
                ScreenInit.init(
                    @as([*]u8, @ptrFromInt(top_screen)),
                    @as([*]u8, @ptrFromInt(top_screen)),
                    @as([*]u8, @ptrFromInt(bottom_screen)),
                    &enableGpu,
                    &enableBacklight,
                );
                PXI.sendOne(top_screen);
                PXI.sendOne(bottom_screen);
            },
            .draw_bottom_screen => {
                const bscreen = @as([*]u32, @ptrFromInt(bottom_screen));
                for (0..320 * 240) |i| {
                    bscreen[i] = PXI.recvOne();
                }
            },
            .set_bottom_screen_color => {
                const bscreen = @as([*]u32, @ptrFromInt(bottom_screen));
                const color = PXI.recvOne();
                log("EarlyPXI: SetBottomScreenColor\n");
                for (0..320 * 240) |i| {
                    bscreen[i] = color;
                }
            },
            .exit_early_pxi => return,
        }
    }
    log("Exit early PXI command loop\n");
}

export fn kmain() noreturn {
    @as(*volatile u32, @ptrFromInt(0x17E00100)).* = 0; // Disable GIC
    @as(*volatile u32, @ptrFromInt(0x17E01000)).* = 0; // Disable DIC

    log("Wait kernel\n");
    EarlyPXI.waitForOtherKernel();
    PXI.setFifoEnabled(true);
    PXI.clearSendFifo();

    top_screen = 0x18300000;
    //util::zeroMemory(top_screen, 400*240*3);
    bottom_screen = 0x1835DC00;
    //util::zeroMemory(bottom_screen, 320*240*3);

    const bs = @as([*]u8, @ptrFromInt(bottom_screen));
    for (0..320) |x| {
        const column = x * 240;
        for (0..240) |y| {
            const offset = (column + y) * 4;
            bs[offset + 0] = 0;
            bs[offset + 1] = 255;
            bs[offset + 2] = @as(u8, @truncate(x));
            bs[offset + 3] = @as(u8, @truncate(y));
        }
    }

    arm9CommandLoop();

    while (true) {
        _ = asm volatile ("wfi");
    }

    panic2(.KMain, .KMainExited, null, null);
}
