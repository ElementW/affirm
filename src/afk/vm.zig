// SPDX-License-Identifier: EUPL-1.2
//! Pg: Page
//! Fo: Folio
//! Co: Codex
//! ...-----------------------------------------------------------------------...
//!                                AddressSpace #1
//! +---------------------------------------------------------------------------+
//! |                                   Co #1                                   |
//! |          :          +---------+                                           |
//! | Fo #2 *1 | Fo #1 *1 |         |                  Fo #0 *3                 |
//! |          |          |   ...   |          :                     :          |
//! | 4K Pg #4 | 4K Pg #3 |         | 4K Pg #0 |       8K Pg #1      | 4K Pg #2 |
//! +----------+----------+         +----------+---------------------+----------+
const std = @import("std");
const afk = @import("afk.zig");
const afu = @import("afu");

pub const AddressSpace = @import("vm/AddressSpace.zig");

pub const Page = @import("vm/page.zig").Page;
pub const Folio = @import("vm/Folio.zig");
pub const Codex = @import("vm/Codex.zig");

pub const RawUserAddress = enum(usize) {
    _,

    pub fn toPhysical(self: RawUserAddress, space: AddressSpace) afk.mm.PhysicalAddress {
        return space.translate(@intFromEnum(self));
    }

    pub fn format(self: RawUserAddress, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = fmt;
        _ = options;
        return std.fmt.format(writer, if (@bitSizeOf(usize) == 32) "0x{X:0>8}@user" else "0x{X:0>16}@user", .{@intFromEnum(self)});
    }
};
pub const RUAaddr = RawUserAddress;

pub const UserAddress = struct {
    process_id: afu.os.pm.Process.Id,
    address: RawUserAddress,

    pub fn toPhysical(self: UserAddress) afk.mm.PhysicalAddress {
        return self.address.toPhysical(
            self.value,
        );
    }

    pub fn format(self: UserAddress, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = fmt;
        _ = options;
        return std.fmt.format(writer, if (@bitSizeOf(usize) == 32) "0x{X:0>8}@{d:0>4}" else "0x{X:0>16}@{d:0>4}", .{@intFromEnum(self)});
    }
};
pub const UAddr = UserAddress;

pub fn UserPointer(comptime T: type) type {
    if (@typeInfo(T) != .Pointer) {
        @compileError("Type passed to UserPointer() is expected to be a pointer");
    }
    // TODO read from/write to userspace
    return struct {
        user_address: UserAddress,

        pub fn format(self: UserPointer, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
            return self.user_address.format(fmt, options, writer);
        }
    };
}
pub const UPtr = UserPointer;
