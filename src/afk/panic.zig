// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const builtin = @import("builtin");
const afk = @import("afk.zig");

const arch = afk.arch;

var panic_depth = std.atomic.Value(u8).init(0);

pub fn panic(msg: []const u8, ret_addr: ?usize) noreturn {
    @branchHint(.cold);
    const first_trace_addr = ret_addr orelse @returnAddress();
    const writer = arch.target.default_output.writer();
    const tty_config = std.io.tty.Config.escape_codes;

    nosuspend switch (panic_depth.fetchAdd(1, .seq_cst)) {
        0 => {
            _ = writer.write("============= KERNEL PANIC =============\n") catch {};
            writer.print("Core {}\n", .{@intFromEnum(afk.Kernel.currentCoreId())}) catch {};
            _ = writer.write(msg) catch {};
            writer.print("\nra = {X:0>" ++ std.fmt.comptimePrint("{}", .{@sizeOf(usize) * 2}) ++ "}\n", .{first_trace_addr}) catch {};

            if (builtin.strip_debug_info) {
                _ = writer.write("Unable to dump stack trace: debug info stripped\n") catch {};
            } else {
                if (builtin.os.tag == .linux) {
                    if (std.debug.getSelfDebugInfo()) |debug_info| {
                        if (@errorReturnTrace()) |t| {
                            std.debug.writeStackTrace(t.*, writer, debug_info, tty_config) catch |err| {
                                writer.print("Unable to dump stack trace: {s}\n", .{@errorName(err)}) catch {};
                            };
                        }
                        std.debug.writeCurrentStackTrace(writer, debug_info, tty_config, first_trace_addr) catch |err| {
                            writer.print("Unable to dump stack trace: {s}\n", .{@errorName(err)}) catch {};
                        };
                    } else |err| {
                        writer.print("Unable to dump stack trace: Unable to open debug info: {s}\n", .{@errorName(err)}) catch {};
                    }
                } else {
                    if (getKernelDebugInfo()) |debug_info| {
                        // if (@errorReturnTrace()) |t| {
                        //     std.debug.writeStackTrace(t.*, writer, debug_info, tty_config) catch |err| {
                        //         writer.print("Unable to dump stack trace: {s}\n", .{@errorName(err)}) catch {};
                        //     };
                        // }
                        writeCurrentStackTrace(writer, debug_info, tty_config, first_trace_addr) catch |err| {
                            writer.print("Unable to dump stack trace: {s}\n", .{@errorName(err)}) catch {};
                        };
                    } else |err| {
                        writer.print("Unable to dump stack trace: Unable to open debug info: {s}\n", .{@errorName(err)}) catch {};
                    }
                }
            }

            _ = writer.write("\n========================================\n") catch {};
        },
        1 => {
            // Double panic
            _ = writer.write("Panicked during a panic. Aborting.\n") catch {};
        },
        else => {
            // Give up
        },
    };

    arch.target.hang();
}

var debug_info_allocator: ?std.mem.Allocator = null;
var debug_info_buffer: [128 * 1024]u8 = undefined;
var debug_info_buffer_allocator: std.heap.FixedBufferAllocator = undefined;
var debug_info_arena_allocator: std.heap.ArenaAllocator = undefined;
fn getDebugInfoAllocator() std.mem.Allocator {
    if (debug_info_allocator) |a| return a;

    debug_info_buffer_allocator = std.heap.FixedBufferAllocator.init(&debug_info_buffer);
    debug_info_arena_allocator = std.heap.ArenaAllocator.init(debug_info_buffer_allocator.allocator());
    const allocator = debug_info_arena_allocator.allocator();
    debug_info_allocator = allocator;
    return allocator;
}

const KernelDebugInfo = struct {
    allocator: std.mem.Allocator,
    base_address: usize,
    dwarf: std.debug.Dwarf,

    pub fn deinit(self: *@This()) void {
        self.dwarf.deinit(self.allocator);
    }

    pub fn getSymbolAtAddress(self: *@This(), allocator: std.mem.Allocator, address: usize) !std.debug.Symbol {
        // Translate the VA into an address into this object
        const relocated_address = address - self.base_address;
        if (nosuspend self.dwarf.findCompileUnit(address)) |compile_unit| {
            return .{
                .name = nosuspend self.dwarf.getSymbolName(relocated_address) orelse "???",
                .compile_unit_name = compile_unit.die.getAttrString(&self.dwarf, std.dwarf.AT.name, self.dwarf.section(.debug_str), compile_unit.*) catch |err| switch (err) {
                    error.MissingDebugInfo, error.InvalidDebugInfo => "???",
                },
                .source_location = nosuspend self.dwarf.getLineNumberInfo(allocator, compile_unit, relocated_address) catch |err| switch (err) {
                    error.MissingDebugInfo, error.InvalidDebugInfo => null,
                    else => return err,
                },
            };
        } else |err| switch (err) {
            error.MissingDebugInfo, error.InvalidDebugInfo => {
                return .{};
            },
            else => return err,
        }
    }

    pub fn getDwarfInfoForAddress(self: *@This(), allocator: std.mem.Allocator, address: usize) !?*const std.debug.Dwarf {
        _ = allocator;
        _ = address;
        return &self.dwarf;
    }
};

extern const __debug_info_start: u8;
extern const __debug_info_end: u8;
extern const __debug_abbrev_start: u8;
extern const __debug_abbrev_end: u8;
extern const __debug_str_start: u8;
extern const __debug_str_end: u8;
extern const __debug_line_start: u8;
extern const __debug_line_end: u8;
extern const __debug_ranges_start: u8;
extern const __debug_ranges_end: u8;
fn dwarfSection(start: *const u8, end: *const u8) std.debug.Dwarf.Section {
    return .{
        .data = @as([*]const u8, @ptrCast(start))[0..(@intFromPtr(end) - @intFromPtr(start))],
        .owned = false,
    };
}

var kernel_debug_info: ?KernelDebugInfo = null;
fn getKernelDebugInfo() !*KernelDebugInfo {
    if (kernel_debug_info != null) return &kernel_debug_info.?;
    if (&__debug_info_start == &__debug_info_end) return error.MissingDebugInfo;
    const DS = std.debug.Dwarf.Section.Id;
    var sections = std.debug.Dwarf.null_section_array;
    sections[@intFromEnum(DS.debug_info)] = dwarfSection(&__debug_info_start, &__debug_info_end);
    sections[@intFromEnum(DS.debug_abbrev)] = dwarfSection(&__debug_abbrev_start, &__debug_abbrev_end);
    sections[@intFromEnum(DS.debug_str)] = dwarfSection(&__debug_str_start, &__debug_str_end);
    sections[@intFromEnum(DS.debug_line)] = dwarfSection(&__debug_line_start, &__debug_line_end);
    sections[@intFromEnum(DS.debug_ranges)] = dwarfSection(&__debug_ranges_start, &__debug_ranges_end);
    kernel_debug_info = .{
        .allocator = getDebugInfoAllocator(),
        .base_address = 0,
        .dwarf = .{
            .endian = .little,
            .sections = sections,
            .is_macho = false,
        },
    };
    return &kernel_debug_info.?;
}

fn writeCurrentStackTrace(
    out_stream: std.io.AnyWriter,
    debug_info: *KernelDebugInfo,
    tty_config: std.io.tty.Config,
    start_addr: ?usize,
) !void {
    var context: std.debug.ThreadContext = undefined;
    const has_context = false; // std.debug.getContext(&context);

    var it = (if (has_context) blk: {
        break :blk std.debug.StackIterator.initWithContext(start_addr, debug_info, &context) catch null;
    } else null) orelse std.debug.StackIterator.init(start_addr, null);
    defer it.deinit();

    while (it.next()) |return_address| {
        const address = if (return_address == 0) return_address else return_address - 1;
        try printSourceAtAddress(debug_info, out_stream, address, tty_config);
    }
}

fn printSourceAtAddress(debug_info: *KernelDebugInfo, out_stream: anytype, address: usize, tty_config: std.io.tty.Config) !void {
    const symbol_info = debug_info.getSymbolAtAddress(debug_info.allocator, address) catch |err| switch (err) {
        error.MissingDebugInfo, error.InvalidDebugInfo => return printLineInfo(
            out_stream,
            null,
            address,
            "<no debug info>",
            tty_config,
        ),
        else => return err,
    };

    return printLineInfo(
        out_stream,
        symbol_info.source_location,
        address,
        symbol_info.name,
        tty_config,
    );
}

fn printLineInfo(
    out_stream: std.io.AnyWriter,
    source_location: ?std.debug.SourceLocation,
    address: usize,
    symbol_name: []const u8,
    tty_config: std.io.tty.Config,
) !void {
    nosuspend {
        try tty_config.setColor(out_stream, .bold);

        if (source_location) |*sl| {
            try out_stream.print("{s}:{d}:{d}", .{ sl.file_name, sl.line, sl.column });
        } else {
            try out_stream.writeAll("<no debug info>:?:?");
        }

        try tty_config.setColor(out_stream, .reset);
        try out_stream.writeAll(": ");
        try tty_config.setColor(out_stream, .dim);
        try out_stream.print("0x{x} in {s}", .{ address, symbol_name });
        try tty_config.setColor(out_stream, .reset);
        try out_stream.writeAll("\n");
    }
}
