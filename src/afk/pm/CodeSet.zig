// SPDX-License-Identifier: EUPL-1.2
//! https://www.3dbrew.org/wiki/KCodeSet
const afk = @import("../afk.zig");
const afu = @import("afu");

const Self = @This();

pub const Section = struct {
    process: *afk.pm.Process,
};

text: Section,
ro: Section,
rw: Section,

name: [63:0]u8,
title_id: afu.os.TitleID,

pub fn initFromUserspace(process: *afk.pm.Process, info: afu.os.pm.CodeSet.Info, text: afk.mm.RawVirtualAddress, ro: afk.mm.RawVirtualAddress, rw: afk.mm.RawVirtualAddress) Self {
    _ = process;
    _ = text;
    _ = ro;
    _ = rw;
    // KCodeSegment::Initialize(&this->text, &codeSetInfo->text, pgTable, textPtr);
    // KCodeSegment::Initialize(&this->rodata, &codeSetInfo->rodata, pgTable, rodataPtr);
    // KCodeSegment::Initialize(&this->data, &codeSetInfo->rwdata, pgTable, dataPtr);
    // KCodeSegment::EnsureCacheCoherencyForCode(&this->text);
    // this->numCodePages = codeSetInfo->text_size_total;
    // this->numConstPages = codeSetInfo->ro_size_total;
    // this->numDataPages = codeSetInfo->rw_size_total;
    // *(_QWORD *)this->processName = codeSetInfo->name;
    // this->processName[8] = 0;
    // this->unk1_from_codesetinfo = codeSetInfo->unk1;
    // this->titleId = codeSetInfo->program_id;
    return .{
        .name = info.name,
        .title_id = info.program_id,
    };
}
