// SPDX-License-Identifier: EUPL-1.2
const afk = @import("../afk.zig");
const afu = @import("afu");

const Self = @This();

arch_extra: afk.arch.target.pm.ThreadExtra,

process: *afk.pm.Process,
id: afu.os.pm.Thread.Id,

// af-only
name: afu.os.pm.Thread.Name,

context: afk.arch.target.pm.ThreadContext,

pub fn init() Self {
    @panic("NYI");
}
