// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afk = @import("../afk.zig");
const afu = @import("afu");

pub const HandleTable = struct {};

arch_extra: afk.arch.target.pm.ProcessExtra,

id: afu.os.pm.Process.Id,

memory: struct {
    space: afk.vm.AddressSpace,
},

scheduling: struct {
    priority: afu.os.pm.Thread.Priority,

    /// Default scheduling parameters to apply to newly created `Thread`s
    /// electing to use them. Has no influence on the process itself.
    ///
    /// These values are used when Horizon KThread::Initialize()'s `idealCore`
    /// parameter is -2, i.e. `afu.os.pm.Core.Id.process_defaults`.
    defaults: struct {
        ideal_core: afu.os.pm.Core.Id,
        core_mask: afk.pm.CoreMask,
    },
},

limits: afk.pm.ResourceLimits,
handles: HandleTable,

threads: []*afk.pm.Thread,
