// SPDX-License-Identifier: EUPL-1.2
const afk = @import("../afk.zig");

const Page = packed struct(u16) {
    pub const MoveToken = struct {
        page: *Page,

        pub fn finish(self: MoveToken) void {
            self.page.finishMove(self);
        }
    };

    pub const FragmentToken = struct {
        page: *Page,

        pub fn finish(self: MoveToken) void {
            self.page.finishFragment(self);
        }
    };

    pub const CoalesceToken = struct {
        page: *Page,

        pub fn finish(self: MoveToken) void {
            self.page.finishCoalesce(self);
        }
    };

    pub const MinSizeLog2 = 12;
    pub const MinSizeBytes = 1 << MinSizeLog2;

    /// Log2 of the page's size.
    size_log2: u8, // Actually u6
    /// If this page is the head (first) page of the folio it belongs to.
    head: bool,
    /// If this page is the tail (last) page of the folio it belongs to.
    tail: bool,
    /// Prevent use of this page because it is being moved, fragmented or coalesced by the kernel.
    use_inhibit: bool,
    /// Is this page a guard (no access bits) page?
    guard: bool,

    pub inline fn sizeBytes(self: Page) usize {
        return 1 << self.size_log2;
    }

    pub fn move(self: *Page) ?MoveToken {
        self.use_inhibit = true;
        return .{ .page = self };
    }

    fn finishMove(self: *Page, token: MoveToken) void {
        _ = token;
        self.use_inhibit = false;
    }

    pub fn fragment(self: *Page) ?FragmentToken {
        self.use_inhibit = true;
        return .{ .page = self };
    }

    fn finishFragment(self: *Page, token: FragmentToken) void {
        _ = token;
        self.use_inhibit = false;
    }

    pub fn coalesce(self: *Page) ?CoalesceToken {
        self.use_inhibit = true;
        return .{ .page = self };
    }

    fn finishCoalesce(self: *Page, token: CoalesceToken) void {
        _ = token;
        self.use_inhibit = false;
    }
};
