const builtin = @import("builtin");
const std = @import("std");
const afk = @import("../../afk.zig");

const cache = afk.arch.arm.cache;

const Mode = enum(u5) {
    usr = 0b10000,
    fiq = 0b10001,
    irq = 0b10010,
    svc = 0b10011,
    mon = 0b10110,
    abt = 0b10111,
    und = 0b11011,
    sys = 0b11111,
};

const ThumbShift = 5;
const Thumb = 1 << ThumbShift;
const FiqDisableShift = 6;
const FiqDisable = 1 << FiqDisableShift;
const IrqDisableShift = 7;
const IrqDisable = 1 << IrqDisableShift;

inline fn setMode(comptime mode: Mode, comptime enable_interrupts: bool) void {
    // zig fmt: off
    if (std.Target.arm.featureSetHas(builtin.cpu.model.features, .v6)) {
        if (enable_interrupts) {
            _ = asm volatile("cpsid aif, %c[mode]" :: [mode] "r" (@intFromEnum(mode)));
        } else {
            _ = asm volatile("cpsie aif, %c[mode]" :: [mode] "r" (@intFromEnum(mode)));
        }
    } else {
        if (enable_interrupts) {
            _ = asm volatile("msr cpsr_c, %c[mode]" :: [mode] "r" (@intFromEnum(mode)));
        } else {
            _ = asm volatile("msr cpsr_c, %c[mode]" :: [mode] "r" (FiqDisable | IrqDisable | @intFromEnum(mode)));
        }
    }
    // zig fmt: on
}

inline fn setC1Control(comptime params: struct {
    mmu: bool,
    l1cache: bool,
    l2cache: bool,
    branch_prediction: bool,
}) void {
    if (std.Target.arm.featureSetHas(builtin.cpu.model.features, .has_v6)) {
        afk.arch.arm.cp15.c1.Control.set(.{ .v5 = .{
            .enable_mmu = params.mmu,
            .enable_data_l1_cache = params.l1cache,
            .endianness = .little,
            .enable_instruction_l1_cache = params.l1cache,
            .exception_vectors_location = .low,
            .cache_replacement_strategy = .default,
            .inhibit_thumb_interworking = false,
            .enable_dtcm = true,
            .dtcm_load_mode = false,
            .enable_itcm = true,
            .itcm_load_mode = false,
        } });
    } else {
        afk.arch.arm.cp15.c1.Control.set(.{ .v6 = .{
            .enable_mmu = params.mmu,
            .enforce_strict_alignment = true,
            .enable_data_l1_cache = params.l1cache,
            .enable_write_buffer = true,
            .endianness = .little,
            .system_protection = false,
            .rom_protection = false,
            .enable_branch_prediction = params.branch_prediction,
            .enable_instruction_l1_cache = params.l1cache,
            .exception_vectors_location = .high,
            .cache_replacement_strategy = .default,
            .inhibit_thumb_interworking = false,
            .interrupt_performance_mode = .default,
            .enable_unaligned_mixed_endian = false,
            .disable_subpage_ap_bits = true,
            .enable_alternative_irq_fiq_vectors = false,
            .endianness_on_exception = .little,
            .enable_unified_l2_cache = params.l2cache,
        } });
    }
}

fn enableVfp() void {
    var co = afk.arch.arm.cp15.c1.CoprocessorAccess.get();
    // Enable access to coprocessors 10 and 11, i.e. the FPU
    co.cp10 = .full;
    co.cp11 = .full;
    afk.arch.arm.cp15.c1.CoprocessorAccess.set(co);
    //   mov r4, #(1 << 30)
    //   fmxr fpexc, r4
}

export fn _start() linksection(".text.start") callconv(.Naked) void {
    _ = arch_kmain;
    asm volatile (
        \\mov sp, _stack_end
        \\mov fp, #0
        \\mov lr, #0
        \\ldr r1, =__bss_start
        \\ldr r2, =__bss_end
        \\.Lbss_clr:
        \\  cmp r1, r2
        \\  strlt r0, [r1], #4
        \\  blt .Lbss_clr
        \\tail arch_kmain
    );
}

export fn arch_kmain() noreturn {
    setMode(.sys, false);
    // Disable caches / mpu
    setC1Control(.{
        .mmu = false,
        .l1cache = false,
        .l2cache = false,
        .branch_prediction = false,
    });
    afk.arch.arm.mm.MPU.setTable(table: Table);

    cache.flushDIBranchCaches();
    cache.syncBarrier();
    cache.flushPrefetchBuffer();

    setC1Control(.{
        .mmu = true,
        .l1cache = true,
        .l2cache = true,
        .branch_prediction = true,
    });

    afk.arch.arm.cp15.c1.AuxControl.set(.{ .mpcore = .{
        .enable_return_stack = true,
        .enable_dynamic_branch_prediction = true,
        .enable_static_branch_prediction = true,
        .enable_instruction_folding = true,
        .l1_l2_cache_exclusion = .inclusive,
        .cache_coherence = .smp,
        .enable_l1_partity_checking = false,
    } });

    afk.arch.arm.cp15.c1.CoprocessorAccess.set(.{
        .cp0 = .none,
        .cp1 = .none,
        .cp2 = .none,
        .cp3 = .none,
        .cp4 = .none,
        .cp5 = .none,
        .cp6 = .none,
        .cp7 = .none,
        .cp8 = .none,
        .cp9 = .none,
        .cp10 = .none,
        .cp11 = .none,
        .cp12 = .none,
        .cp13 = .none,
    });

    //   @ Copy the XRQ table and data/instructions following it
    //   @ mov r0, #0
    //   @@@ldr r1, =XRQStart
    //   @@@ldmia r1!, {r2-r9}  @ Load XRQ handling instructions
    //   @@@stmia r0!, {r2-r9}  @ Save them back
    //   @@@ldmia r1, {r2-r9}   @ Load XRQ handler pointers
    //   @@@stmia r0, {r2-r9}   @ Save them back as well

    afk.Kernel.kmain();
}

// .pool
// .size _start, . - _start
//
// #ifdef AF_ASM_ARM_HAS_MPU
// __mpu_regions:
//   .word 0x08000029 @ 08000000 2M   | arm9 mem (O3DS / N3DS)
//   .word 0x08000025 @ 08000000 512k | kernel9 mem
//   .word 0x20000037 @ 20000000 256M | fcram (O3DS / N3DS)
//   .word 0xFFFF001F @ FFFF0000 64k  | bootrom (unprotected / protected)
//   .word 0x07FF001D @ 07FF0000 32k  | dtcm (16k) + itcm (32k)
//   .word 0x10000029 @ 10000000 2M   | io mem (ARM9 / first 2MB)
//   .word 0x1FF00027 @ 1FF00000 1M   | dsp / axi wram
//   .word 0x1800002D @ 18000000 8M   | vram (+ 2MB)
// .size __mpu_regions, . - __mpu_regions
//
// __regions_perms_data:
//   .word CP15_AP_PRIV_RW_USER_RW << 0 | \
//         CP15_AP_PRIV_RW_USER_NO << 4 | \
//         CP15_AP_PRIV_RW_USER_RW << 8 | \
//         CP15_AP_PRIV_RO_USER_RO << 12 | \
//         CP15_AP_PRIV_RW_USER_NO << 16 | \
//         CP15_AP_PRIV_RW_USER_RW << 20 | \
//         CP15_AP_PRIV_RW_USER_RW << 24 | \
//         CP15_AP_PRIV_RW_USER_RW << 28
//
// __regions_perms_instr:
//   .word CP15_AP_PRIV_RO_USER_RO << 0 | \
//         CP15_AP_PRIV_RO_USER_NO << 4 | \
//         CP15_AP_NONE            << 8 | \
//         CP15_AP_PRIV_RO_USER_NO << 12 | \
//         CP15_AP_PRIV_RO_USER_NO << 16 | \
//         CP15_AP_NONE            << 20 | \
//         CP15_AP_NONE            << 24 | \
//         CP15_AP_NONE            << 28
// .size __regions_perms_instr, . - __regions_perms_instr
// #endif
