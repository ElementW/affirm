// SPDX-License-Identifier: EUPL-1.2

pub const Type = enum(u3) {
    reset = 0,
    undefined = 1,
    svc = 2,
    prefetch_abort = 3,
    data_abort = 4,
    reserved = 5,
    irq = 6,
    fiq = 7,
};

const Nintendo3DSTable = extern struct {
    irq_instruction: u32 = 0xE51FF004, // ldr pc, [pc, #-4]
    irq_handler: *anyopaque,
    fiq_instruction: u32 = 0xE51FF004,
    fiq_handler: *anyopaque,
    svc_instruction: u32 = 0xE51FF004,
    svc_handler: *anyopaque,
    undefined_instruction: u32 = 0xE51FF004,
    undefined_handler: *anyopaque,
    prefetch_abort_instruction: u32 = 0xE51FF004,
    prefetch_abort_handler: *anyopaque,
    data_abort_instruction: u32 = 0xE51FF004,
    data_abort_handler: *anyopaque,
};

extern fn af_IRQHandler() void;
extern fn af_SVCHandler() void;

pub fn init() void {
    const table: *Nintendo3DSTable = @ptrFromInt(0x8000000);
    table.* = .{
        .irq_handler = &af_IRQHandler,
        .svc_handler = &af_SVCHandler,
    };
}
