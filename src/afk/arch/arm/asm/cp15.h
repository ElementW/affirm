// SPDX-License-Identifier: EUPL-1.2
#ifndef AF_ASM_CP15_H
#define AF_ASM_CP15_H

// ARM9: ARM946E-S TRM § 2.3.5
// ARM11: ARM11 MPCore TRM § 3.4.6
#define CP15_REG_CR c1
#ifdef AF_ASSEMBLY
.macro get_cr reg:req
mrc p15, 0, \reg, CP15_REG_CR, c0, 0
.endm
.macro set_cr reg:req
mcr p15, 0, \reg, CP15_REG_CR, c0, 0
.endm
#endif
#ifdef AF_ARM9
#define CP15_CR_ENABLE_MPU       (1<<0)
#else
#define CP15_CR_ENABLE_MMU       (1<<0)
#endif
#define CP15_CR_ENABLE_DCACHE    (1<<2)
#ifdef AF_ARM9
#define CP15_CR_ENABLE_BIGENDIAN (1<<7)
#endif
#define CP15_CR_ENABLE_ICACHE    (1<<12)
#define CP15_CR_ALT_VECTORS      (1<<13)
#ifdef AF_ARM9
#define CP15_CR_CACHE_RROBIN     (1<<14)
#endif
#define CP15_CR_DISABLE_TBIT     (1<<15)
#ifdef AF_ARM9
#define CP15_CR_ENABLE_DTCM      (1<<16)
#define CP15_CR_DTCM_LMODE       (1<<17)
#define CP15_CR_ENABLE_ITCM      (1<<18)
#define CP15_CR_ITCM_LMODE       (1<<19)
#endif

#ifdef AF_ARM9
// ARM9: ARM946E-S TRM § 2.3.6
#define CP15_REG_CACHE_CFG c2

// ARM9: ARM946E-S TRM § 2.3.7
#define CP15_REG_WRBUF_CTRL c3

// ARM9: ARM946E-S TRM § 2.3.8
#define CP15_REG_ACCESS_PERM c5
#define CP15_AP_NONE            0b0000
#define CP15_AP_PRIV_RW_USER_NO 0b0001
#define CP15_AP_PRIV_RW_USER_RO 0b0010
#define CP15_AP_PRIV_RW_USER_RW 0b0011
#define CP15_AP_PRIV_RO_USER_NO 0b0101
#define CP15_AP_PRIV_RO_USER_RO 0b0110

// ARM9: ARM946E-S TRM § 2.3.9
#define CP15_REG_REGION c6
#endif

// ARM9: ARM946E-S TRM § 2.3.10
// ARM11: ARM11 MPCore TRM § 3.4.18
#define CP15_REG_CACHE_OP c7
#ifdef AF_ASSEMBLY
.macro cache_op reg:req creg:req
mcr p15, 0, \reg, CP15_REG_CACHE_OP, \creg, 0
.endm
#endif

#endif /* AF_ASM_CP15_H */
