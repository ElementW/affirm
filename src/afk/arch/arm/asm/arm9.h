// SPDX-License-Identifier: EUPL-1.2
#ifndef af_ASM_ARM9_H
#define af_ASM_ARM9_H

#define ARM9_IRQ_BASE 0x10001000
#define ARM9_IRQ_IE_OFFSET 0x0
#define ARM9_IRQ_IE (ARM9_IRQ_BASE + ARM9_IRQ_IE_OFFSET)
#define ARM9_IRQ_IF_OFFSET 0x4
#define ARM9_IRQ_IF (ARM9_IRQ_BASE + ARM9_IRQ_IF_OFFSET)

#endif /* af_ASM_ARM9_H */
