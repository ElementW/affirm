// SPDX-License-Identifier: EUPL-1.2
#ifndef af_ASM_SR_H
#define af_ASM_SR_H

// ARM11: MPCore TRM § 2.9
//  ARM9: ARMv5 ARM § A2.5

#define SR_MODE_USR 0x10
#define SR_MODE_FIQ 0x11
#define SR_MODE_IRQ 0x12
#define SR_MODE_SVC 0x13
#define SR_MODE_ABT 0x17
#define SR_MODE_UND 0x1B
#define SR_MODE_SYS 0x1F
#define SR_MODE_MASK 0x1F

#define SR_THUMB_SHIFT 5
#define SR_THUMB (1<<SR_THUMB_SHIFT)
#define SR_FIQ_DISABLE_SHIFT 6
#define SR_FIQ_DISABLE (1<<SR_FIQ_DISABLE_SHIFT)
#define SR_IRQ_DISABLE_SHIFT 7
#define SR_IRQ_DISABLE (1<<SR_IRQ_DISABLE_SHIFT)

#ifdef AF_ARM11
#define SR_ABORT_DISABLE_SHIFT 8
#define SR_ABORT_DISABLE (1<<SR_IRQ_DISABLE_SHIFT)
#endif

#ifdef AF_ASSEMBLY
.macro setmode mode:req, ie:req
#ifdef AF_ARM11
  .if \ie
    cpsie i, #(\mode)
  .else
    cpsid i, #(\mode)
  .endif
#else  // ARM9
  .if \ie
    msr cpsr_c, #(SR_FIQ_DISABLE | \mode)
  .else
    msr cpsr_c, #(SR_IRQ_DISABLE | SR_FIQ_DISABLE | \mode)
  .endif
#endif
.endm
#endif

#endif /* af_ASM_SR_H */
