// SPDX-License-Identifier: EUPL-1.2
//! ARM Processor cache manipulation.
//! See ARM11 MPCore TRM § 3.4.18 "c7, Cache Operations Register"
//! and ARM9 TRM § 2.3.10 "Register 7, Cache Operations Register"
const std = @import("std");
const builtin = @import("builtin");

// zig fmt: off
pub inline fn flushICache() void {
    _ = asm volatile("mcr p15, 0, %[z], c7, c5, 0" :: [z] "r" (0));
}

pub inline fn flushPrefetchBuffer() void {
    comptime std.debug.assert(std.Target.arm.featureSetHas(builtin.cpu.model.features, .has_v6));
    _ = asm volatile("mcr p15, 0, %[z], c7, c5, 4" :: [z] "r" (0));
}

pub inline fn flushBranchTargetCache() void {
    comptime std.debug.assert(std.Target.arm.featureSetHas(builtin.cpu.model.features, .has_v6));
    _ = asm volatile("mcr p15, 0, %[z], c7, c5, 6" :: [z] "r" (0));
}

pub inline fn flushDCache() void {
    _ = asm volatile("mcr p15, 0, %[z], c7, c6, 0" :: [z] "r" (0));
}

pub inline fn flushDIBranchCaches() void {
    if (std.Target.arm.featureSetHas(builtin.cpu.model.features, .has_v6)) {
        _ = asm volatile("mcr p15, 0, %[z], c7, c7, 0" :: [z] "r" (0));
    } else {
        flushDCache();
        flushICache();
    }
}

pub inline fn syncBarrier() void {
    _ = asm volatile("mcr p15, 0, %[z], c7, c10, 4" :: [z] "r" (0));
}

pub inline fn memoryBarrier() void {
    comptime std.debug.assert(std.Target.arm.featureSetHas(builtin.cpu.model.features, .has_v6));
    _ = asm volatile("mcr p15, 0, %[z], c7, c10, 5" :: [z] "r" (0));
}
// zig fmt: on
