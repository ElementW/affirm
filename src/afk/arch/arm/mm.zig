// SPDX-License-Identifier: EUPL-1.2
pub const MMU = @import("mm/MMU.zig");
pub const MPU = @import("mm/MPU.zig");
