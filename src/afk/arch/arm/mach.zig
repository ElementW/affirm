// SPDX-License-Identifier: EUPL-1.2
pub const nintendo3ds = @import("mach/nintendo3ds.zig");
pub const raspberrypi = @import("mach/raspberrypi.zig");
