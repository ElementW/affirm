// SPDX-License-Identifier: EUPL-1.2

const Syscall = *const fn (regs: [*]u32, index: u32) callconv(.C) void;

// TODO
fn svcControlMemory(regs: [*]u32, index: u32) callconv(.C) void {
    _ = regs;
    _ = index;
}
fn svcQueryMemory(regs: [*]u32, index: u32) callconv(.C) void {
    _ = regs;
    _ = index;
}

const handlers = [_]?Syscall{
    null, // 0x00
    svcControlMemory, // 0x01
    svcQueryMemory, // 0x02
    // /* 0x03 */ H(ExitProcess),
    // /* 0x04 */ H(GetProcessAffinityMask),
    // /* 0x05 */ H(SetProcessAffinityMask),
    // /* 0x06 */ H(GetProcessIdealProcessor),
    // /* 0x07 */ H(SetProcessIdealProcessor),
    // /* 0x08 */ H(CreateThread),
    // /* 0x09 */ H(ExitThread),
    // /* 0x0A */ H(SleepThread),
    // /* 0x0B */ H(GetThreadPriority),
    // /* 0x0C */ H(SetThreadPriority),
    // /* 0x0D */ H(GetThreadAffinityMask),
    // /* 0x0E */ H(SetThreadAffinityMask),
    // /* 0x0F */ H(GetThreadIdealProcessor),
    // /* 0x10 */ H(SetThreadIdealProcessor),
    // /* 0x11 */ H(GetCurrentProcessorNumber),
    // /* 0x12 */ H(Run),
    // /* 0x13 */ H(CreateMutex),
    // /* 0x14 */ H(ReleaseMutex),
    // /* 0x15 */ H(CreateSemaphore),
    // /* 0x16 */ H(ReleaseSemaphore),
    // /* 0x17 */ H(CreateEvent),
    // /* 0x18 */ H(SignalEvent),
    // /* 0x19 */ H(ClearEvent),
    // /* 0x1A */ H(CreateTimer),
    // /* 0x1B */ H(SetTimer),
    // /* 0x1C */ H(CancelTimer),
    // /* 0x1D */ H(ClearTimer),
    // /* 0x1E */ H(CreateMemoryBlock),
    // /* 0x1F */ H(MapMemoryBlock),
    // /* 0x20 */ H(UnmapMemoryBlock),
    // /* 0x21 */ H(CreateAddressArbiter),
    // /* 0x22 */ H(ArbitrateAddress),
    // /* 0x23 */ H(CloseHandle),
    // /* 0x24 */ H(WaitSynchronization1),
    // /* 0x25 */ H(WaitSynchronizationN),
    // /* 0x26 */ H(SignalAndWait),
    // /* 0x27 */ H(DuplicateHandle),
    // /* 0x28 */ H(GetSystemTick),
    // /* 0x29 */ H(GetHandleInfo),
    // /* 0x2A */ H(GetSystemInfo),
    // /* 0x2B */ H(GetProcessInfo),
    // /* 0x2C */ H(GetThreadInfo),
    // /* 0x2D */ H(ConnectToPort),
    // /* 0x2E */ H(SendSyncRequest1),
    // /* 0x2F */ H(SendSyncRequest2),
    // /* 0x30 */ H(SendSyncRequest3),
    // /* 0x31 */ H(SendSyncRequest4),
    // /* 0x32 */ H(SendSyncRequest),
    // /* 0x33 */ H(OpenProcess),
    // /* 0x34 */ H(OpenThread),
    // /* 0x35 */ H(GetProcessId),
    // /* 0x36 */ H(GetProcessIdOfThread),
    // /* 0x37 */ H(GetThreadId),
    // /* 0x38 */ H(GetResourceLimit),
    // /* 0x39 */ H(GetResourceLimitLimitValues),
    // /* 0x3A */ H(GetResourceLimitCurrentValues),
    // /* 0x3B */ H(GetThreadContext),
    // /* 0x3C */ H(Break),
    // /* 0x3D */ H(OutputDebugString),
    // /* 0x3E */ H(ControlPerformanceCounter),
    // /* 0x3F */ nullptr,
    // /* 0x40 */ nullptr,
    // /* 0x41 */ nullptr,
    // /* 0x42 */ nullptr,
    // /* 0x43 */ nullptr,
    // /* 0x44 */ nullptr,
    // /* 0x45 */ nullptr,
    // /* 0x46 */ nullptr,
    // /* 0x47 */ H(CreatePort),
    // /* 0x48 */ H(CreateSessionToPort),
    // /* 0x49 */ H(CreateSession),
    // /* 0x4A */ H(AcceptSession),
    // /* 0x4B */ H(ReplyAndReceive1),
    // /* 0x4C */ H(ReplyAndReceive2),
    // /* 0x4D */ H(ReplyAndReceive3),
    // /* 0x4E */ H(ReplyAndReceive4),
    // /* 0x4F */ H(ReplyAndReceive),
    // /* 0x50 */ H(BindInterrupt),
    // /* 0x51 */ H(UnbindInterrupt),
    // /* 0x52 */ H(InvalidateProcessDataCache),
    // /* 0x53 */ H(StoreProcessDataCache),
    // /* 0x54 */ H(FlushProcessDataCache),
    // /* 0x55 */ H(StartInterProcessDma),
    // /* 0x56 */ H(StopDma),
    // /* 0x57 */ H(GetDmaState),
    // /* 0x58 */ H(GetDmaState),
    // /* 0x59 */ H(SetGpuProt),
    // /* 0x5A */ H(SetWifiEnabled),
    // /* 0x5B */ nullptr,
    // /* 0x5C */ nullptr,
    // /* 0x5D */ nullptr,
    // /* 0x5E */ nullptr,
    // /* 0x5F */ nullptr,
    // /* 0x60 */ H(DebugActiveProcess),
    // /* 0x61 */ H(BreakDebugProcess),
    // /* 0x62 */ H(TerminateDebugProcess),
    // /* 0x63 */ H(GetProcessDebugEvent),
    // /* 0x64 */ H(ContinueDebugEvent),
    // /* 0x65 */ H(GetProcessList),
    // /* 0x66 */ H(GetThreadList),
    // /* 0x67 */ H(GetDebugThreadContext),
    // /* 0x68 */ H(SetDebugThreadContext),
    // /* 0x69 */ H(QueryDebugProcessMemory),
    // /* 0x6A */ H(ReadProcessMemory),
    // /* 0x6B */ H(WriteProcessMemory),
    // /* 0x6C */ H(SetHardwareBreakPoint),
    // /* 0x6D */ H(GetDebugThreadParam),
    // /* 0x6E */ nullptr,
    // /* 0x6F */ nullptr,
    // /* 0x70 */ H(ControlProcessMemory),
    // /* 0x71 */ H(MapProcessMemory),
    // /* 0x72 */ H(UnmapProcessMemory),
    // /* 0x73 */ H(CreateCodeSet),
    // /* 0x74 */ H(RandomStub),
    // /* 0x75 */ H(CreateProcess),
    // /* 0x76 */ H(TerminateProcess),
    // /* 0x77 */ H(SetProcessResourceLimits),
    // /* 0x78 */ H(CreateResourceLimit),
    // /* 0x79 */ H(SetResourceLimitValues),
    // /* 0x7A */ H(AddCodeSegment),
    // /* 0x7B */ H(Backdoor),
    // /* 0x7C */ H(KernelOperation),
    // /* 0x7D */ H(QueryProcessMemory),
    // /* 0x7E */ nullptr,
    // /* 0x7F */ nullptr
};

export fn af_BadSVCHandler(regs: [*]u32, index: u32) void {
    _ = index;
    _ = regs;
    // TODO crash program
    //#ifdef AF_ARM9
    //logScreen->log("SVC 0x%X from 0x%08X = 0x%08X, regs @ 0x%08X", index, regs[13]-4, *reinterpret_cast<volatile uint32*>(regs[13]-4), regs);
    //logScreen->log("r0 %08X  r1 %08X  r2 %08X", regs[0], regs[1], regs[2]);
    //logScreen->log("r3 %08X  r4 %08X  r5 %08X", regs[3], regs[4], regs[5]);
    //logScreen->log("r6 %08X  r7 %08X  r8 %08X", regs[6], regs[7], regs[8]);
    //logScreen->log("r9 %08X r10 %08X r11 %08X", regs[9], regs[10], regs[11]);
    //logScreen->log("            r12 %08X  lr %08X", regs[12], regs[13]);*/
    //#endif
}

export fn af_zig_SVCHandler(regs: [*]u32, index: u32) void {
    @setRuntimeSafety(false);
    if (index >= handlers.len) {
        @call(.always_tail, af_BadSVCHandler, .{ regs, index });
    } else {
        if (handlers[index]) |handler| {
            @call(.always_tail, handler, .{ regs, index });
        } else {
            @call(.always_tail, af_BadSVCHandler, .{ regs, index });
        }
    }
}

pub inline fn handle(regs: [*]u32, index: u32) void {
    @call(.always_tail, af_zig_SVCHandler, .{ regs, index });
}

/// http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0056d/ch05s04s02.html
pub export fn af_SVCHandler() callconv(.Naked) noreturn {
    _ = asm volatile (
        \\stmfd sp!, {r0-r12, lr}      // Save all the unbanked registers in SVC stack.
        \\ldr r1, [lr, #-4]            // Get the calling SVC instruction
        \\bic r1, r1, #0xff000000      // Extract the SVC number
        // TODO: calls from thumb code? check spsr
        \\mov r0, sp                   // Set the stack pointer as 1st param to get passed userspace context
        // The good thing with using register saves as param is we can read and return values the way the
        // 3DS kernel ABI wants them without additional overhead to what SVC handling incurs
        \\bl %[zig_handler]            // Call the Zig side handler; after this, assume r0-r12 are trashed
        \\ldmfd sp!, {r0-r12, pc}^     // Restore registers from SVC stack & CPSR = SPSR
        :
        : [zig_handler] "i" (af_zig_SVCHandler),
    );
    @trap();
}
