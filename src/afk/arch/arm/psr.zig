// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const cpu = @import("builtin").cpu;

pub const PSR = packed struct(u32) {
    pub const Mode = enum(u5) {
        user = 0b10000,
        fiq = 0b10001,
        irq = 0b10010,
        supervisor = 0b10011,
        abort = 0b10111,
        undefined = 0b11011,
        system = 0b11111,
    };

    pub const Endianness = enum(u1) {
        little = 0,
        big = 1,
    };

    const ThumbShift = 5;
    const Thumb = 1 << ThumbShift;
    const FIQDisableShift = 6;
    const FIQDisable = 1 << FIQDisableShift;
    const IRQDisableShift = 7;
    const IRQDisable = 1 << IRQDisableShift;

    const Self = @This();

    mode: Mode,
    fiq: bool,
    irq: bool,
    abort: bool,
    endianness: Endianness, // ARM 11 ONLY
    _1: u5 = 0,
    GE: u4, // ARM 11 ONLY
    _2: u3,
    jazelle: bool,
    u: u2,
    Q: bool,
    overflow: bool,
    carry: bool,
    zero: bool,
    negative: bool,

    pub inline fn fromU32(val: u32) Self {
        return @as(Self, @bitCast(val));
    }
    pub inline fn toU32(self: Self) u32 {
        return @as(u32, @bitCast(self));
    }

    pub inline fn get() Self {
        return fromU32(asm volatile ("mrs %[ret], cpsr"
            : [ret] "=r" (-> u32),
        ));
    }

    pub inline fn setC(self: Self) void {
        _ = asm volatile ("msr cpsr_c, %[psr]"
            :
            : [psr] "r" (self.toU32()),
        );
    }

    pub inline fn enableInterrupts() void {
        if (std.Target.arm.featureSetHas(cpu.model.features, .has_v6)) {
            _ = asm volatile ("cpsie i");
        } else {
            _ = asm volatile (
                \\mrs %[temp], cpsr
                \\bic %[temp], %[temp], %[mask]
                \\msr cpsr_c, %[temp]
                : [temp] "=&r" (-> u32),
                : [mask] "n" (IRQDisable),
            );
        }
    }

    pub inline fn disableInterrupts() void {
        if (std.Target.arm.featureSetHas(cpu.model.features, .has_v6)) {
            _ = asm volatile ("cpsid i");
        } else {
            _ = asm volatile (
                \\mrs %[temp], cpsr
                \\orr %[temp], %[temp], %[mask]
                \\msr cpsr_c, %[temp]
                : [temp] "=&r" (-> u32),
                : [mask] "n" (IRQDisable),
            );
        }
    }

    pub fn setInterruptsEnabled(enable: bool) void {
        if (std.Target.arm.featureSetHas(cpu.model.features, .has_v6)) {
            if (enable) {
                _ = asm volatile ("cpsie i");
            } else {
                _ = asm volatile ("cpsid i");
            }
        } else {
            _ = asm volatile (
                \\mrs %[temp], cpsr
                \\bic %[temp], %[temp], %[mask]
                \\orr %[temp], %[temp], %[en], LSL %[shift]
                \\msr cpsr_c, %[temp]
                : [temp] "=&r" (-> u32),
                : [en] "r" (if (enable) 1 else 0),
                  [mask] "n" (IRQDisable),
                  [shift] "n" (IRQDisableShift),
            );
        }
    }
};
