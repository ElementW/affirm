// SPDX-License-Identifier: EUPL-1.2
//! ARM9 IRQ control registers namespace.
//! See https://www.3dbrew.org/wiki/IRQ_Registers
const afu = @import("afu");
const reg = afu.hw.reg;

pub const Bits = packed struct(u32) {
    dmac10: bool = false,
    dmac11: bool = false,
    dmac12: bool = false,
    dmac13: bool = false,
    dmac14: bool = false,
    dmac15: bool = false,
    dmac16: bool = false,
    dmac17: bool = false,

    timer0: bool = false,
    timer1: bool = false,
    timer2: bool = false,
    timer3: bool = false,

    pxi_sync: bool = false,
    pxi_not_full: bool = false,
    pxi_not_empty: bool = false,

    aes: bool = false,

    sdio1: bool = false,
    sdio1async: bool = false,
    sdio3: bool = false,
    sdio3async: bool = false,

    debug_recv: bool = false,
    debug_send: bool = false,

    rsa: bool = false,

    ctrcard1: bool = false,
    ctrcard2: bool = false,
    cgc: bool = false,
    cgc_detect: bool = false,
    dscard: bool = false,

    dmac2: bool = false,
    dmac2abort: bool = false,

    _: u2 = 0,
};

const Base: usize = 0x10001000;
const Control = reg(Bits, Base);
const Pending = reg(Bits, Base + 0x4);

pub fn enabled() Bits {
    return Control.read();
}

pub fn enable(bits: Bits) void {
    Control.write(@as(u32, @bitCast(Control.read())) | @as(u32, @bitCast(bits)));
}

pub fn enableAll() void {
    Control.write(0xFFFFFFFF);
}

pub fn disable(bits: Bits) void {
    Control.write(@as(u32, @bitCast(Control.read())) & ~@as(u32, @bitCast(bits)));
}

pub fn disableAll() void {
    Control.write(0);
}

pub fn pending() Bits {
    return Pending.read();
}

pub fn clearPending(bits: Bits) void {
    Pending.write(bits);
}

pub fn clearAllPending() void {
    Pending.write(0xFFFFFFFF);
}
