// SPDX-License-Identifier: EUPL-1.2
//! CONFIG9 registers namespace.
//! See https://www.3dbrew.org/wiki/CONFIG9_Registers
const afu = @import("afu");
const reg = afu.hw.reg;
const unkreg = afu.hw.unkreg;

const Base: usize = 0x10000000;
pub const SysProt9 = reg(u8, Base);
pub const SysProt11 = reg(u8, Base + 0x1);
pub const Reset11 = reg(u8, Base + 0x2);
pub const DebugControl = reg(u32, Base + 0x4);
pub const unk0x8 = unkreg(u8, Base + 0x8);
pub const CardControl = reg(u16, Base + 0xC);
pub const CardStatus = reg(u8, Base + 0x10);
pub const CardCycles0 = reg(u16, Base + 0x12);
pub const CardCycles1 = reg(u16, Base + 0x14);
pub const SDMCControl = reg(u16, Base + 0x20);
pub const unk0x100 = unkreg(u16, Base + 0x100);
pub const ExtMemControl9 = reg(u8, Base + 0x200);
pub const MPCoreConfig = reg(u32, Base + 0xFFC);
pub const BootEnvironment = reg(u32, 0x10010000);
pub const UnitInfo = reg(u8, 0x10010010);
pub const TWLUnitInfo = reg(u8, 0x10010014);
