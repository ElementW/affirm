// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afu = @import("afu");
const Register = afu.hw.Register;
const assert = std.debug.assert;

const TimerRegs = struct {
    value: u16,
    control: u16,
};
comptime {
    assert(@sizeOf(TimerRegs) == @sizeOf(u32));
}

const ControlRegs = @as(*volatile [4]TimerRegs, @ptrFromInt(0x10003000));

pub const Prescaler = enum(u2) {
    div1 = 0,
    div64 = 1,
    div256 = 2,
    div1024 = 3,
};

pub const TimerControl = packed struct(u16) {
    prescaler: Prescaler,
    count_up: bool,
    _1: u3 = 0,
    enable_irq: bool,
    start: bool,
    _2: u8 = 0,
};

const Timer = @This();

index: u2, // const

pub inline fn controlReg(self: Timer) Register(TimerControl, .RW) {
    return Register(TimerControl, .RW).of(&ControlRegs[self.index].control);
}
pub inline fn valueReg(self: Timer) Register(u16, .RW) {
    return Register(u16, .RW).of(&ControlRegs[self.index].value);
}

pub inline fn value(self: Timer) u16 {
    return self.valueReg().read();
}
pub inline fn setValue(self: Timer, v: u16) void {
    return self.valueReg().write(v);
}

pub fn start(self: Timer) void {
    var control = self.controlReg().read();
    control.start = true;
    self.controlReg().write(control);
}
pub fn stop(self: Timer) void {
    var control = self.controlReg().read();
    control.start = false;
    self.controlReg().write(control);
}
pub fn running(self: Timer) bool {
    return self.controlReg().read().start;
}

pub fn configure(self: Timer, prescaler: Prescaler, count_up: bool, enable_irq: bool) void {
    self.controlReg().write(TimerControl{
        .prescaler = prescaler,
        .count_up = count_up,
        .enable_irq = enable_irq,
        .start = false,
    });
}

pub fn get(idx: u2) Timer {
    return Timer{ .index = idx };
}
