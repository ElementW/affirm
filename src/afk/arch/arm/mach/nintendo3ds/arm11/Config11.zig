// SPDX-License-Identifier: EUPL-1.2
//! CONFIG11 registers namespace.
//! https://www.3dbrew.org/wiki/CONFIG11_Registers
const afu = @import("afu");
const reg = afu.hw.reg;
const unkreg = afu.hw.unkreg;
const roreg = afu.hw.roreg;

const Base: usize = 0x10140000;
pub const unk0x100 = reg(u16, Base + 0x100);
pub const unk0x102 = reg(u16, Base + 0x102);
pub const FIQControl = reg(u8, Base + 0x104);
pub const unk0x105 = unkreg(u8, Base + 0x105);
pub const unk0x108 = unkreg(u16, Base + 0x108);
pub const unk0x10C = unkreg(u16, Base + 0x10C);
pub const GPUProt = reg(u32, Base + 0x140);
pub const WifiControl = reg(u8, Base + 0x180);
pub const SPIControl = reg(u32, Base + 0x1C0);
pub const unk0x200 = unkreg(u32, Base + 0x200);

pub const unk0x400 = unkreg(u8, Base + 0x400);
pub const unk0x410 = unkreg(u32, Base + 0x410);
pub const BootromOverlayControl = reg(u8, Base + 0x420);
pub const BootromOverlayValue = reg(u32, Base + 0x424);
pub const unk0x428 = unkreg(u32, Base + 0x428);

pub const SOCInfo = roreg(u16, Base + 0xFFC);

pub const GPUStatus = roreg(u16, Base + 0x1000);
pub const PTM0 = reg(u32, Base + 0x1008);
pub const PTM1 = reg(u32, Base + 0x100C);

// TODO: TWL regs

pub const WifiUnknown = reg(u8, Base + 0x110C);

pub const GPUControl = reg(u32, Base + 0x1200);
pub const GPUControl2 = reg(u32, Base + 0x1204);
pub const GPUFCRAMControl = reg(u16, Base + 0x1210);
pub const CodecControl = reg(u8, Base + 0x1220);
pub const CameraControl = reg(u8, Base + 0x1224);
pub const DSPControl = reg(u8, Base + 0x1230);

pub const MPCoreClkControl = reg(u16, Base + 0x1300);
pub const MPCoreControl = reg(u16, Base + 0x1304);
pub const MPCoreBootControl0 = reg(u8, Base + 0x1310);
pub const MPCoreBootControl1 = reg(u8, Base + 0x1311);
pub const MPCoreBootControl2 = reg(u8, Base + 0x1312);
pub const MPCoreBootControl3 = reg(u8, Base + 0x1313);
