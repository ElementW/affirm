// SPDX-License-Identifier: EUPL-1.2
const builtin = @import("builtin");
const std = @import("std");
const afk = @import("../../../../afk.zig");
const afu = @import("afu");
const afsys = @import("afsys");

const cc = afu.encoding.FourCC.fromString;
const PXI = afsys.plat.nintendo3ds.PXI;

pub const Command = enum(u32) {
    init_screens = cc("Scrn").toU32(),
    draw_bottom_screen = cc("DrwB").toU32(),
    set_bottom_screen_color = cc("ColB").toU32(),
    exit_early_pxi = cc("!PXI").toU32(),
};

inline fn tx(x: u8) void {
    PXI.setRemoteByte(x);
}

inline fn rx(x: u8) void {
    while (PXI.getLocalByte() != x) {}
}

pub fn waitForOtherKernel() void {
    const isArm11 = comptime std.Target.arm.featureSetHas(builtin.cpu.model.features, .has_v6);
    const x9to11 = if (!isArm11) tx else rx;
    const x11to9 = if (!isArm11) rx else tx;
    x9to11(0x00);
    x11to9(0x00);
    x9to11(0xAF);
    x11to9(0xAF);
}
