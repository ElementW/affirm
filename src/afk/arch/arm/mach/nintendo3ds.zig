// SPDX-License-Identifier: EUPL-1.2
pub const arm9 = struct {
    pub const Config9 = @import("nintendo3ds/arm9/Config9.zig");
    pub const IRQControl = @import("nintendo3ds/arm9/IRQControl.zig");
};
pub const arm11 = struct {
    pub const Config11 = @import("nintendo3ds/arm11/Config11.zig");
};

pub const EarlyPXI = @import("nintendo3ds/EarlyPXI.zig");
pub const Timer = @import("nintendo3ds/Timer.zig");
