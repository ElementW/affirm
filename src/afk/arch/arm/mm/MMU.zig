// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const Control = @import("../cp15/c1.zig").Control;
const TTBR0 = @import("../cp15/c2.zig").TTBR0;
const TTBR1 = @import("../cp15/c2.zig").TTBR1;
const cache = @import("../cache.zig");

inline fn setEnabled(state: bool) void {
    var c1 = Control.get();
    c1.v6.enable_data_l1_cache = false;
    c1.v6.enable_instruction_l1_cache = false;
    Control.set(c1);
    cache.flushDIBranchCaches();
    cache.flushPrefetchBuffer();
    c1.v6.enable_mmu = state;
    Control.set(c1);
}

/// Translation Table Base Control Register's (3.4.12) N value
/// By using a value of 1, the 4GB address space is split in half,
/// its lower part refering to TTBR0, and its higher one to TTBR1.
const N = 1;

pub inline fn setProcessTTBaseAddress(tlb: *anyopaque) void {
    std.debug.assert(@intFromPtr(tlb) % 0x4000 == 0);
    var ttbr = TTBR0.get();
    ttbr.table_base = @intFromPtr(tlb) >> TTBR0.Value.TableBaseShift;
    TTBR0.set(ttbr);
}

pub inline fn setKernelTTBaseAddress(tlb: *anyopaque) void {
    std.debug.assert(@intFromPtr(tlb) % 0x4000 == 0);
    var ttbr = TTBR1.get();
    ttbr.table_base = @intFromPtr(tlb) >> TTBR1.Value.TableBaseShift;
    TTBR1.set(ttbr);
}

pub inline fn invalidateTLB() void {
    _ = asm volatile ("mcr p15, 0, %[zero], c8, c7, 0"
        :
        : [zero] "r" (0),
        : "memory"
    );
}

pub inline fn enable() void {
    setEnabled(true);
}

pub inline fn disable() void {
    setEnabled(false);
}
