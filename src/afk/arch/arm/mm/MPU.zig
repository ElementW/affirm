// SPDX-License-Identifier: EUPL-1.2
const assert = @import("std").debug.assert;
const afk = @import("../../../afk.zig");
const CriticalSection = afk.sync.CriticalSection;
const C1Control = afk.arch.arm.cp15.c1.Control;

// ARM946E-S TRM, Table 4-2 "Region size encoding"
pub const RegionSize = enum(u5) {
    kb4 = 0b01011,
    kb8 = 0b01100,
    kb16 = 0b01101,
    kb32 = 0b01011,
    kb64 = 0b01111,
    kb128 = 0b10000,
    kb256 = 0b10001,
    kb512 = 0b10010,
    mb1 = 0b10011,
    mb2 = 0b10100,
    mb4 = 0b10101,
    mb8 = 0b10110,
    mb16 = 0b10111,
    mb32 = 0b11000,
    mb64 = 0b11001,
    mb128 = 0b11010,
    mb256 = 0b11011,
    mb512 = 0b11100,
    gb1 = 0b11101,
    gb2 = 0b11110,
    gb4 = 0b11111,
};

/// ARM946E-S TRM, Table 4-1 "Protection Register format"
pub const RegionDefinition = packed struct(u32) {
    enable: bool,
    size: RegionSize,
    _: u6,
    base: u20,
};

/// ARM946E-S TRM, Table 2-13 "Access permission encoding (extended)"
pub const Permissions = enum(u4) {
    n_n = 0b0000,
    rw_n = 0b0001,
    rw_ro = 0b0010,
    rw_rw = 0b0011,
    ro_n = 0b0101,
    ro_ro = 0b0110,
};

pub const Region = struct {
    enable: bool,
    bufferable: bool,
    i_cacheable: bool,
    d_cacheable: bool,
    base: usize,
    size: RegionSize,
    i_permissions: Permissions,
    d_permissions: Permissions,
};

pub const Table = struct {
    regions: [8]Region,

    pub fn toRawTable(self: Table) RawTable {
        var raw: RawTable = .{
            .regions = undefined,
            .bufferable = 0,
            .i_permissions = 0,
            .d_permissions = 0,
            .i_cacheable = 0,
            .d_cacheable = 0,
        };
        for (self.regions, 0..) |region, i| {
            raw.regions[i] = RegionDefinition{
                .enable = region.enable,
                .size = region.size,
                .base = region.base >> 12,
            };
            raw.i_permissions |= @intFromEnum(region.i_permissions) << (i * 4);
            raw.d_permissions |= @intFromEnum(region.d_permissions) << (i * 4);
            raw.bufferable |= @intFromBool(region.bufferable) << i;
            raw.i_cacheable |= @intFromBool(region.i_cacheable) << i;
            raw.d_cacheable |= @intFromBool(region.d_cacheable) << i;
        }
        return raw;
    }
};
pub const RawTable = struct {
    regions: [8]RegionDefinition,
    bufferable: u32,
    i_permissions: u32,
    d_permissions: u32,
    i_cacheable: u32,
    d_cacheable: u32,
};

pub fn enabled() bool {
    return C1Control.get().v5.enable_mmu;
}
pub fn setEnabled(enable: bool) void {
    const cs = CriticalSection.enter();
    defer cs.exit();
    var c1 = C1Control.get();
    c1.v5.enable_mmu = enable;
    C1Control.set(c1);
}

pub fn setTable(table: Table) void {
    setRawTable(table.toRawTable());
}

pub fn setRawTable(raw: RawTable) void {
    const cs = CriticalSection.enter();
    defer cs.exit();
    // Write region definition bits
    asm volatile (
        \\mcr p15, 0, %[r0], c6, c0, 0
        \\mcr p15, 0, %[r1], c6, c1, 0
        \\mcr p15, 0, %[r2], c6, c2, 0
        \\mcr p15, 0, %[r3], c6, c3, 0
        \\mcr p15, 0, %[r4], c6, c4, 0
        \\mcr p15, 0, %[r5], c6, c5, 0
        \\mcr p15, 0, %[r6], c6, c6, 0
        \\mcr p15, 0, %[r7], c6, c7, 0
        :
        : [r0] "r0" (raw.regions[0]),
          [r1] "r1" (raw.regions[1]),
          [r2] "r2" (raw.regions[2]),
          [r3] "r3" (raw.regions[3]),
          [r4] "r4" (raw.regions[4]),
          [r5] "r5" (raw.regions[5]),
          [r6] "r6" (raw.regions[6]),
          [r7] "r7" (raw.regions[7]),
    );
    // zig fmt: off
    // Write instruction access permission bits
    asm volatile("mcr p15, 0, %[out], c5, c0, 2" :: [out] "r" (raw.i_permissions));
    // Write data access permission bits
    asm volatile("mcr p15, 0, %[out], c5, c0, 3" :: [out] "r" (raw.d_permissions));
    // Write data bufferable bits
    asm volatile("mcr p15, 0, %[out], c3, c0, 0" :: [out] "r" (raw.bufferable));
    // Write instruction cachable bits
    asm volatile("mcr p15, 0, %[out], c2, c0, 1" :: [out] "r" (raw.i_cacheable));
    // Write data cachable bits
    asm volatile("mcr p15, 0, %[out], c2, c0, 0" :: [out] "r" (raw.d_cacheable));
    // zig fmt: on
}

pub fn getTable() Table {
    //var region_defs: union {
    //  array: [8]RegionDefinition,
    //  fields: struct {
    //    r0, r1, r2, r3, r4, r5, r6, r7: RegionDefinition
    //  }
    //} = undefined;
    var region_defs: RegionDefinition = undefined;
    var bufferable: u32 = 0;
    var i_permissions: u32 = 0;
    var d_permissions: u32 = 0;
    var i_cacheable: u32 = 0;
    var d_cacheable: u32 = 0;
    {
        const cs = CriticalSection.enter();
        defer cs.exit();
        // zig fmt: off
        // Read region definition bits
        region_defs[0] = asm volatile("mrc p15, 0, %[in], c6, c0" : [in] "=r" (-> RegionDefinition));
        region_defs[1] = asm volatile("mrc p15, 0, %[in], c6, c1" : [in] "=r" (-> RegionDefinition));
        region_defs[2] = asm volatile("mrc p15, 0, %[in], c6, c2" : [in] "=r" (-> RegionDefinition));
        region_defs[3] = asm volatile("mrc p15, 0, %[in], c6, c3" : [in] "=r" (-> RegionDefinition));
        region_defs[4] = asm volatile("mrc p15, 0, %[in], c6, c4" : [in] "=r" (-> RegionDefinition));
        region_defs[5] = asm volatile("mrc p15, 0, %[in], c6, c5" : [in] "=r" (-> RegionDefinition));
        region_defs[6] = asm volatile("mrc p15, 0, %[in], c6, c6" : [in] "=r" (-> RegionDefinition));
        region_defs[7] = asm volatile("mrc p15, 0, %[in], c6, c7" : [in] "=r" (-> RegionDefinition));
        // Read instruction access permission bits
        asm volatile("mrc p15, 0, %[in], c5, c0, 2" : [in] "=r" (i_permissions));
        // Read data access permission bits
        asm volatile("mrc p15, 0, %[in], c5, c0, 3" : [in] "=r" (d_permissions));
        // Read data bufferable bits
        asm volatile("mrc p15, 0, %[in], c3, c0, 0" : [in] "=r" (bufferable));
        // Read instruction cachable bits
        asm volatile("mrc p15, 0, %[in], c2, c0, 1" : [in] "=r" (i_cacheable));
        // Read data cachable bits
        asm volatile("mrc p15, 0, %[in], c2, c0, 0" : [in] "=r" (d_cacheable));
        // zig fmt: on
    }
    var table: Table = undefined;
    for (region_defs, 0..) |rdef, i| {
        table.regions[i] = Region{
            .enable = rdef.enable,
            .bufferable = (bufferable >> i) & 1,
            .i_cacheable = (i_cacheable >> i) & 1,
            .d_cacheable = (d_cacheable >> i) & 1,
            .base = rdef.base << 12,
            .size = rdef.size,
            .i_permissions = @as(Permissions, @enumFromInt((i_permissions >> (i * 4)) & 0b1111)),
            .d_permissions = @as(Permissions, @enumFromInt((d_permissions >> (i * 4)) & 0b1111)),
        };
    }
    return table;
}
