// SPDX-License-Identifier: EUPL-1.2
const builtin = @import("builtin");
const std = @import("std");
const afk = @import("../../../afk.zig");
const afu = @import("afu");
const afsys = @import("afsys");

pub const IRQHandler = ?*const fn () callconv(.C) void;

var kiko: u21 = 0;
fn testirq() callconv(.C) void {
    const LS = afk.util.logging.LogScreen;
    const palette1 = LS.Palette.compute(afu.gfx.Color.RGBA8888.fromRGB(0, 255, 0), afu.gfx.Color.RGBA8888.Black);
    const palette2 = LS.Palette.compute(afu.gfx.Color.RGBA8888.fromRGB(255, 0, 255), afu.gfx.Color.RGBA8888.Black);
    LS.default.?.drawCharacter(0xC0 + (kiko >> 3), 0, 0, palette1);
    LS.default.?.drawCharacter(0xC0 + (kiko & (8 - 1)), 8, 0, palette2);
    kiko = (kiko + 1) & (64 - 1);
}

fn log(str: []const u8) void {
    _ = str;
    //for ("[Kernel9] ") |c| {
    //    @intToPtr(*volatile u8, 0x10108000).* = c;
    //}
    //for (str) |c| {
    //    @intToPtr(*volatile u8, 0x10108000).* = c;
    //}
    //@intToPtr(*volatile u8, 0x10108000).* = 0;
}

pub export fn af_zig_IRQAcknowledge(_: ?*anyopaque) IRQHandler {
    log("IRQ begin\n");
    if (std.Target.arm.featureSetHas(builtin.cpu.model.features, .has_v6)) {
        return null;
    }
    const pendingBefore = afk.arch.arm.mach.nintendo3ds.arm9.IRQControl.pending();
    afk.arch.arm.mach.nintendo3ds.arm9.IRQControl.clearPending(.{ .timer0 = true });
    //if (vregs != nullptr) {
    //  uint32 stacktop;
    //  hw::IRQControl::Mask pendingAfter = hw::IRQControl::pending();
    //  uint32 *regs = reinterpret_cast<uint32*>(vregs);
    //  logScreen->lock();
    //  logScreen->log("IRQ! regs @ 0x%08X 0x%08X -> 0x%08X", vregs, pendingBefore, pendingAfter);
    //  logScreen->log("stk %08X spsr_irq %08X lr_irq %08X", &stacktop, regs[14], regs[13]);
    //  logScreen->log(" r0 %08X  r1 %08X  r2 %08X  r3 %08X", regs[0], regs[1], regs[2], regs[3]);
    //  logScreen->log(" r4 %08X  r5 %08X  r6 %08X  r7 %08X", regs[4], regs[5], regs[6], regs[7]);
    //  logScreen->log(" r8 %08X  r9 %08X r10 %08X r11 %08X", regs[8], regs[9], regs[10], regs[11]);
    //  logScreen->unlock();
    //  logScreen->log("r12 %08X", regs[12]);
    //} else {
    //  logScreen->log("IRQ! 0x%08X -> 0x%08X", pendingBefore, pendingAfter);
    //}
    const MCU = afsys.plat.nintendo3ds.MCU;
    const mcuInterrupts = MCU.readInterrupts();
    var buf: [32]u8 = undefined;
    const LS = afk.util.logging.LogScreen;
    const palette = LS.Palette.compute(afu.gfx.Color.RGBA8888.White, afu.gfx.Color.RGBA8888.Black);
    LS.default.?.drawTextLine(std.fmt.bufPrint(&buf, "{X:0>8}", .{@as(u32, @bitCast(mcuInterrupts))}) catch unreachable, 64, 0, palette);
    LS.default.?.drawTextLine(std.fmt.bufPrint(&buf, "{X:0>4}", .{afsys.plat.nintendo3ds.RNG.int(u16)}) catch unreachable, 128, 0, palette);
    LS.default.?.drawTextLine(std.fmt.bufPrint(&buf, "{X:0>8}", .{afsys.plat.nintendo3ds.RNG.int(u32)}) catch unreachable, 128 + 64, 0, palette);

    LS.default.?.drawTextLine(std.fmt.bufPrint(&buf, "CFG9_CARDSTATUS: {X:0>2}", .{@as(*volatile u8, @ptrFromInt(0x10000010)).*}) catch unreachable, 32, 32, palette);

    if (mcuInterrupts.power_button_press) {
        LS.default.?.logLine("\xEA Poweroff");
        MCU.poweroffLCD() catch LS.default.?.logLine("LCD Poweroff failed"); // MCU hangs if screens are on at poweroff
        afk.arch.arm.cache.flushDCache();
        MCU.poweroff() catch LS.default.?.logLine("Poweroff failed");
        while (true) {}
    }
    //hw::Timer::get(0).stop();
    return if (@as(u32, @bitCast(pendingBefore)) != 0) &testirq else null;
}
