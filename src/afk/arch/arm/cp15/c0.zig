// SPDX-License-Identifier: EUPL-1.2
const CP15Register = @import("register.zig").CP15Register;

/// CP15 C0 Main ID Register.
/// See ARM11 MPCore TRM § 3.4.1 "c0, Main ID Register"
pub const MainId = CP15Register(0, 0, 0, 0, .{}, packed struct(u32) {
    revision: u4,
    part_number: u12,
    architecture: u4,
    variant: u4,
    implementor: u8,
});

/// CP15 C0 Cache Type Register.
/// See ARM ARM § B3.3 "Register 0: ID codes"
/// See ARM11 MPCore TRM § 3.4.2 "c0, Cache Type Register"
pub const CacheType = CP15Register(0, 0, 0, 1, .{
    .needCpuFeatures = .{.v5},
}, packed struct(u32) {
    /// See ARM ARM Table B6-1 "Cache type values".
    pub const Type = enum(u4) {
        write_through = 0b0000,
        write_back = 0b0001,
        write_back_r7 = 0b0010,
        write_back_a = 0b0110,
        write_back_b = 0b0111,
        write_back_c = 0b1110,
        write_back_d = 0b0101,
    };

    pub const Size = packed struct(u12) {
        raw_line_length: u2,
        m: bool,
        raw_assoc: u3,
        raw_size: u4,
        _1: u1,
        bits_12_13_restricted: bool,
        _2: u4,

        // The length (in bytes) of a cache line.
        pub fn lineLength(self: Size) u7 {
            return switch (self.raw_line_length) {
                0b00 => 8,
                0b01 => 16,
                0b10 => 32,
                0b11 => 64,
            };
        }

        // The associativity (in ways) of the cache.
        pub fn assoc(self: Size) u8 {
            return if (self.m) {
                switch (self.raw_assoc) {
                    0b000 => 0,
                    0b001 => 3,
                    0b010 => 6,
                    0b011 => 12,
                    0b100 => 24,
                    0b101 => 48,
                    0b110 => 96,
                    0b101 => 192,
                    else => @panic("Invalid cp15 c0 cache type associativity"),
                }
            } else {
                switch (self.raw_assoc) {
                    0b000 => 1,
                    0b001 => 2,
                    0b010 => 4,
                    0b011 => 8,
                    0b100 => 16,
                    0b101 => 32,
                    0b110 => 64,
                    0b101 => 128,
                    else => @panic("Invalid cp15 c0 cache type associativity"),
                }
            };
        }

        // The size (in bytes) of the cache.
        pub fn cacheSize(self: Size) u19 {
            return if (self.m) {
                switch (self.raw_size) {
                    0b0000 => 768,
                    0b0001 => 1536,
                    0b0010 => 3072,
                    0b0011 => 6144,
                    0b0100 => 12288,
                    0b0101 => 24576,
                    0b0110 => 49152,
                    0b0111 => 98304,
                    0b1000 => 196608,
                    else => @panic("Invalid cp15 c0 cache type associativity"),
                }
            } else {
                switch (self.raw_size) {
                    0b0000 => 512,
                    0b0001 => 1024,
                    0b0010 => 2048,
                    0b0011 => 4096,
                    0b0100 => 8192,
                    0b0101 => 16384,
                    0b0110 => 32768,
                    0b0111 => 65536,
                    0b1000 => 131072,
                    else => @panic("Invalid cp15 c0 cache type associativity"),
                }
            };
        }
    };

    i_size: Size,
    d_size: Size,
    unified: bool,
    cache_type: Type,
    _: u3 = 0,
});

/// CP15 C0 TCM Size Register.
/// See ARM9 TRM § 2.3.4 "Register 0, Tightly-coupled Memory Size Register"
pub const TCMSize = CP15Register(0, 0, 0, 2, packed struct(u32) {
    pub const Size = enum(u4) {
        kb0 = 0b0000,
        kb4 = 0b0011,
        kb8 = 0b0100,
        kb16 = 0b0101,
        kb32 = 0b0110,
        kb64 = 0b0111,
        kb128 = 0b1000,
        kb256 = 0b1001,
        kb512 = 0b1010,
        mb1 = 0b1011,
    };

    _5: u2 = 0,
    itcm_absent: bool,
    _4: u3 = 0,
    itcm_size: Size,
    _3: u4 = 0,
    dtcm_absent: bool,
    _2: u3 = 0,
    dtcm_size: Size,
    _1: u10 = 0,
});
