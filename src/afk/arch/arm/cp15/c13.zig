// SPDX-License-Identifier: EUPL-1.2
const CP15Register = @import("register.zig").CP15Register;

/// CP15 C13 FCSE PID Register.
/// See ARM11 MPCore TRM § 3.4.24 "c13, FCSE PID Register"
pub const PID = CP15Register(0, 13, 0, 0, .{
    .dataSyncBarrierBeforeSet = true,
}, packed struct(u32) {
    _: u24 = 0,
    pid: u8,
});

/// CP15 C13 Context ID Register.
pub const ContextID = CP15Register(0, 13, 0, 1, .{
    .dataSyncBarrierBeforeSet = true,
}, packed struct(u32) {
    asid: u8,
    proc_id: u24,
});

/// CP15 C13 Thread ID Registers.
/// See ARM11 MPCore TRM § 3.4.26 "c13, c13, Thread ID registers"
pub const ThreadID = struct {
    pub const UserWritable = CP15Register(0, 13, 0, 2, .{}, u32);
    pub const UserVisible = CP15Register(0, 13, 0, 3, .{}, u32);
    pub const Privileged = CP15Register(0, 13, 0, 4, .{}, u32);
};
