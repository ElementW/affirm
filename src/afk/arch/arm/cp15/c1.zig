// SPDX-License-Identifier: EUPL-1.2
const CP15Register = @import("register.zig").CP15Register;

/// CP15 C1 Control Register.
/// See ARM ARM § B3.4 "Register 1: Control registers"
/// See ARM11 MPCore TRM § 3.4.7 "c1, Control Register"
/// See ARM9 TRM § 2.3.5 "Register 1, Control Register"
pub const Control = CP15Register(0, 1, 0, 0, .{
    .needCpuFeatures = .{.v5},
}, packed union {
    pub const Endianness = enum(u1) {
        little = 0,
        big = 1,
    };
    pub const ExceptionVectorsLocation = enum(u1) {
        low = 0,
        high = 1,
    };
    pub const CacheReplacementStrategy = enum(u1) {
        default = 0,
        predictable = 1,
    };
    pub const V5 = packed struct(u32) {
        enable_mmu: bool,
        _1: u1 = 0,
        enable_data_l1_cache: bool,
        _2: u4 = 0,
        endianness: Endianness,
        _3: u4 = 0,
        enable_instruction_l1_cache: bool,
        exception_vectors_location: ExceptionVectorsLocation,
        cache_replacement_strategy: CacheReplacementStrategy,
        inhibit_thumb_interworking: bool,
        enable_dtcm: bool,
        dtcm_load_mode: bool,
        enable_itcm: bool,
        itcm_load_mode: bool,
        _4: u12 = 0,
    };
    pub const V6 = packed struct(u32) {
        pub const InterruptPerformanceMode = enum(u1) {
            default = 0,
            low_latency = 1,
        };
        enable_mmu: bool,
        enforce_strict_alignment: bool,
        enable_data_l1_cache: bool,
        enable_write_buffer: bool,
        _: u3 = 0,
        endianness: Endianness,
        system_protection: bool,
        rom_protection: bool,
        f: bool,
        enable_branch_prediction: bool,
        enable_instruction_l1_cache: bool,
        exception_vectors_location: ExceptionVectorsLocation,
        cache_replacement_strategy: CacheReplacementStrategy,
        inhibit_thumb_interworking: bool,
        _1: u4 = 0,
        interrupt_performance_mode: InterruptPerformanceMode,
        enable_unaligned_mixed_endian: bool,
        disable_subpage_ap_bits: bool,
        enable_alternative_irq_fiq_vectors: bool,
        endianness_on_exception: Endianness,
        enable_unified_l2_cache: bool,
        _2: u6 = 0,
    };
    v5: V5,
    v6: V6,
});

pub const AuxControl = CP15Register(0, 1, 0, 1, .{}, packed union {
    pub const MPCore = packed struct(u32) {
        pub const L1L2CacheExclusion = enum(u1) {
            inclusive = 0,
            exclusive = 1,
        };
        pub const CacheCoherence = enum(u1) {
            amp = 0,
            smp = 1,
        };
        enable_return_stack: bool,
        enable_dynamic_branch_prediction: bool,
        enable_static_branch_prediction: bool,
        enable_instruction_folding: bool,
        l1_l2_cache_exclusion: L1L2CacheExclusion,
        cache_coherence: CacheCoherence,
        enable_l1_partity_checking: bool,
        _: u25 = 0,
    };
    mpcore: MPCore,
});

pub const CoprocessorAccess = CP15Register(0, 1, 0, 1, .{}, packed struct {
    pub const Access = enum(u2) {
        none = 0,
        privileged = 1,
        full = 3,
    };
    cp0: Access,
    cp1: Access,
    cp2: Access,
    cp3: Access,
    cp4: Access,
    cp5: Access,
    cp6: Access,
    cp7: Access,
    cp8: Access,
    cp9: Access,
    cp10: Access,
    cp11: Access,
    cp12: Access,
    cp13: Access,
    _: u4 = 0,
});
