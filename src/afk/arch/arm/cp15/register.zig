// SPDX-License-Identifier: EUPL-1.2
pub const std = @import("std");
pub const afk = @import("../../../afk.zig");

pub const RegisterAttrib = struct {
    memClobber: bool = false,
    dataSyncBarrierBeforeSet: bool = false,
    needCpuFeatures: []const std.Target.arm.Feature = &.{},
};

pub fn CP15Register(OpCode1: u3, CpReg: u4, CpReg2: u4, OpCode2: u3, comptime attr: RegisterAttrib, comptime T: type) type {
    if (@bitSizeOf(T) != 32) {
        @compileError(std.fmt.comptimePrint("{s} needs to be {} bits to be used as CSR value, but is {} bits", .{
            @typeName(T),
            32,
            @bitSizeOf(T),
        }));
    }
    if (@typeInfo(T) == .Struct and @typeInfo(T).Struct.layout != .@"packed") {
        @compileError(@typeName(T) ++ " is not packed");
    }
    return struct {
        pub const Value = T;

        pub inline fn get() Value {
            return asm volatile ("mrc p15, %c[op1], %[out], c%c[reg], c%c[reg2], %c[op2]"
                : [out] "=r" (-> Value),
                : [op1] "n" (OpCode1),
                  [reg] "n" (CpReg),
                  [reg2] "n" (CpReg2),
                  [op2] "n" (OpCode2),
            );
        }

        pub inline fn set(v: Value) void {
            if (attr.dataSyncBarrierBeforeSet) {
                afk.arch.arm.cache.syncBarrier();
            }
            if (attr.memClobber) {
                _ = asm volatile ("mcr p15, %c[op1], %[in], c%c[reg], c%c[reg2], %c[op2]"
                    :
                    : [in] "r" (v),
                      [op1] "n" (OpCode1),
                      [reg] "n" (CpReg),
                      [reg2] "n" (CpReg2),
                      [op2] "n" (OpCode2),
                    : "memory"
                );
            } else {
                _ = asm volatile ("mcr p15, %c[op1], %[in], c%c[reg], c%c[reg2], %c[op2]"
                    :
                    : [in] "r" (v),
                      [op1] "n" (OpCode1),
                      [reg] "n" (CpReg),
                      [reg2] "n" (CpReg2),
                      [op2] "n" (OpCode2),
                );
            }
        }
    };
}
