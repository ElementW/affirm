// SPDX-License-Identifier: EUPL-1.2
const CP15Register = @import("register.zig").CP15Register;

pub const OuterCachableAttr = enum(u2) {
    noncachable = 0b00,
    writeback_write_allocate = 0b01,
    writethrough_no_allocate = 0b10,
    writeback_no_allocate = 0b11,
};

/// CP15 C2 Translation Table Base Register 0.
/// See ARM11 MPCore TRM § 3.4.10 "c2, Translation Table Base Register 0"
pub const TTBR0 = CP15Register(0, 2, 0, 0, .{}, packed struct(u32) {
    pub const TableBaseShift = 5;
    pub const TableBaseLength = 32 - TableBaseShift;
    pub const TableBase = @Type(.{ .Int = .{ .signedness = .unsigned, .bits = TableBaseLength } });

    _1: u1 = 0,
    shared: bool,
    _2: u1 = 0,
    cachable_attr: OuterCachableAttr,
    table_base: TableBase,
});

/// CP15 C2 Translation Table Base Register 1.
/// See ARM11 MPCore TRM § 3.4.11 "c2, Translation Table Base Register 1"
pub const TTBR1 = CP15Register(0, 2, 0, 1, .{}, packed struct(u32) {
    pub const TableBaseShift = 18;
    pub const TableBaseLength = 32 - TableBaseShift;
    pub const TableBase = @Type(.{ .Int = .{ .signedness = .unsigned, .bits = TableBaseLength } });

    _1: u1 = 0,
    shared: bool,
    _2: u1 = 0,
    cachable_attr: OuterCachableAttr,
    _3: u9 = 0,
    table_base: TableBase,
});
