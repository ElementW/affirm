// SPDX-License-Identifier: EUPL-1.2
const CP15Register = @import("register.zig").CP15Register;

/// CP15 C15 Performance Monitor Control Register.
/// See ARM11 MPCore TRM § 3.4.27 "c15, Performance Monitor Control Register (PMNC)"
pub const PerfMonitor = CP15Register(0, 15, 12, 0, .{}, packed struct(u32) {
    enable: bool,
    count_reg_reset: bool,
    cycle_counter_reg_reset: bool,
    cycle_counter_div64: bool,
    count_reg0_irq_enable: bool,
    count_reg1_irq_enable: bool,
    cycle_counter_irq_enable: bool,
    count_reg0_overflow: bool,
    count_reg1_overflow: bool,
    cycle_counter_overflow: bool,
    _1: bool = 0,
    evt_count1: u8,
    evt_count0: u8,
    _2: u4 = 0,
});

/// CP15 C15 Cycle Counter Register.
/// See ARM11 MPCore TRM § 3.4.28 "c15, Cycle Counter Register (CCNT)"
pub const CycleCounter = CP15Register(0, 15, 12, 1, .{}, packed struct(u32) {
    value: u32,
});
