// SPDX-License-Identifier: EUPL-1.2
pub const c0 = @import("cp15/c0.zig");
pub const c1 = @import("cp15/c1.zig");
pub const c2 = @import("cp15/c2.zig");
pub const c13 = @import("cp15/c13.zig");
pub const c15 = @import("cp15/c15.zig");
