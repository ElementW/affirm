// SPDX-License-Identifier: EUPL-1.2
pub const CriticalSection = @import("sync/CriticalSection.zig");
