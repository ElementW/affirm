// SPDX-License-Identifier: EUPL-1.2
const PSR = @import("../../../../afk.zig").arch.arm.PSR;

const Self = @This();

state: bool,

pub inline fn enter() Self {
    const state: bool = PSR.get().irq;
    // Interrupts are disabled when the I bit is set
    if (state == false) {
        PSR.disableInterrupts();
    }
    return Self{ .state = state };
}

pub inline fn exit(self: Self) void {
    if (self.state == false) {
        PSR.enableInterrupts();
    }
}

comptime {
    const assert = @import("std").debug.assert;
    assert(@sizeOf(Self) <= @sizeOf(u8));
}

// TODO SCOPED Zig 0.13? macro/inline pub fn critical_section
