// SPDX-License-Identifier: EUPL-1.2

/// ARM11 MMU L1 Page Table Entry.
/// See ARM11 MPCore TRM § 5.11.2 "ARMv6 page table translation subpage AP bits disabled",
/// Figure 5-7 "ARMv6 first-level descriptor formats with subpages disabled"
pub const L1 = packed struct(u32) {
    pub const Type = enum(u2) {
        /// Causes either a Prefetch Abort or Data Abort.
        translation_fault = 0b00,
        coarse_page_table = 0b01,
        section = 0b10,
        reserved = 0b11,
    };
    pub const CoarsePageTable = packed struct(u30) {
        pub const BaseAddrShift = 10;
        pub const BaseAddrLength = 32 - BaseAddrShift;
        /// SBZ
        _: u3,
        domain: u4,
        ecc_enable: bool,
        base_address: u22,
    };
    pub const Section = packed struct(u30) {
        pub const BaseAddrShift = 20;
        pub const BaseAddrLength = 32 - BaseAddrShift;
        B: bool,
        C: bool,
        XN: bool,
        domain: u4,
        ecc_enable: bool,
        AP: u2,
        TEX: u3,
        APX: bool,
        shared: bool,
        /// If set, the translation is inserted into the TLB using the current ASID.
        not_global: bool,
        _: u1,
        supersection: bool,
        base_address: u12,
    };
    type: Type,
    value: packed union {
        coarse_page_table: CoarsePageTable,
        section: Section,
    },
};

/// ARM11 MMU L2 Page Table Entry.
/// See ARM11 MPCore TRM § 5.11.2 "ARMv6 page table translation subpage AP bits disabled",
/// Figure 5-8 "ARMv6 second-level descriptor format"
pub const L2 = packed struct(u32) {
    pub const Type = enum(u2) {
        /// Causes either a Prefetch Abort or Data Abort.
        translation_fault = 0b00,
        large_page = 0b01,
        small_page = 0b10,
        small_page_xn = 0b11,
    };
    pub const LargePage = packed struct(u30) {
        pub const TableBaseShift = 16;
        pub const TableBaseLength = 32 - TableBaseShift;
        B: bool,
        C: bool,
        AP: u2,
        /// SBZ
        _: u3,
        APX: bool,
        shared: bool,
        /// If set, the translation is inserted into the TLB using the current ASID.
        not_global: bool,
        TEX: u3,
        xn: bool,
        table_base: u16,
    };
    const SmallPage = packed struct(u30) {
        pub const TableBaseShift = 12;
        pub const TableBaseLength = 32 - TableBaseShift;
        B: bool,
        C: bool,
        AP: u2,
        TEX: u3,
        APX: bool,
        shared: bool,
        /// If set, the translation is inserted into the TLB using the current ASID.
        not_global: bool,
        table_base: u20,
    };
    type: Type,
    value: packed union {
        large_page: LargePage,
        small_page: SmallPage,
    },
};

// Table 5-3 "TEX field, and C and B bit encodings used in page table formats"
const CacheConfig = struct {
    const Self = @This();
    // Table 5-4 "Cache policy bits"
    pub const Policy = enum(u2) {
        non_cachable_unbuffered = 0b00,
        writeback_allocate_buffered = 0b01,
        writethrough_no_allocate_buffered = 0b10,
        writeback_no_allocate_buffered = 0b11,
    };
    pub const StronglyOrdered = Self{ .TEX = 0b000, .C = false, .B = false };
    pub const SharedDevice = Self{ .TEX = 0b000, .C = false, .B = true };
    pub const WriteThroughNoAllocate = Self{ .TEX = 0b000, .C = true, .B = false };
    pub const WriteBackNoAllocate = Self{ .TEX = 0b000, .C = true, .B = true };
    pub const NonCachable = Self{ .TEX = 0b001, .C = false, .B = false };
    // Reserved             { .TEX = 0b001, .C = false, .B = true }
    // Reserved             { .TEX = 0b001, .C = true,  .B = false }
    pub const WriteBackAllocate = Self{ .TEX = 0b001, .C = true, .B = true };
    pub const NonSharedDevice = Self{ .TEX = 0b010, .C = false, .B = false };
    // Reserved             { .TEX = 0b010, .C = false, .B = true }
    // Reserved             { .TEX = 0b010, .C = true,  .B = ? }
    // Reserved             { .TEX = 0b011, .C = ?,     .B = ? }
    pub fn Cached(inner: Policy, outer: Policy) Self {
        const innerBits = @intFromEnum(inner);
        return Self{ .TEX = 0b100 | @intFromEnum(outer), .C = innerBits >> 1, .B = innerBits & 1 };
    }

    TEX: u3,
    C: bool,
    B: bool,
};
