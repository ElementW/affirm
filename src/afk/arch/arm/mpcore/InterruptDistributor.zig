// SPDX-License-Identifier: EUPL-1.2
//! ARM11 interrupt distributor control registers namespace.
//! See ARM11 MPCore TRM §10.5 "Interrupt Distributor Registers"
const Only = @import("../platform/Only.zig");
const reg = @import("registers/Helpers.zig").reg;
const roreg = @import("registers/Helpers.zig").roreg;

comptime {
    Only.core(.ARM11);
}

const Base: usize = 0x17E01000;
/// 10.5.1 Interrupt Distributor Control Register, 0x000
const Control = reg(packed struct(u32) { enable: bool, _: u31 }, Base + 0x0);
/// 10.5.2 Interrupt Controller Type Register, 0x004
const Type = roreg(packed struct(u32) { interrupt_lines: u5, cpu_number: u2, _: u25 }, Base + 0x4);
/// 10.5.3 Interrupt Enable clear and Enable set registers, 0x100-0x11C and 0x180-0x19C
const EnableClear = reg(u32, Base + 0x100);
const EnableSet = reg(u32, Base + 0x180);
/// 10.5.4 Interrupt Pending clear and Pending set registers, 0x200-0x21C and 0x280-0x29C
const PendingClear = reg(u32, Base + 0x200);
const PendingSet = reg(u32, Base + 0x280);
/// 10.5.5 Active Bit Registers, 0x300-0x31C
const ActiveBit = roreg(u32, Base + 0x300);
/// 10.5.6 Interrupt Priority Registers, 0x400-0x4FC
const Priority = reg(u32, Base + 0x400);
/// 10.5.7 Interrupt CPU Targets Registers, 0x800-0x8FC
const Targets = reg(u32, Base + 0x800);
/// 10.5.8 Interrupt Configuration Registers, 0xC00-0xC3C
const Configuration = reg(u32, Base + 0xC00);
/// 10.5.9 Interrupt Line Level Registers, 0xD00-0xD1C
const LineLevel = roreg(u32, Base + 0xD00);

pub fn setEnabled(enable: bool) void {
    Control.changeFields(.{ .enable = enable });
}
pub fn enabled() bool {
    return Control.read().enable;
}

pub fn cpuNumber() u8 {
    return Type.read().cpu_number;
}
pub fn interruptLinesNumber() u8 {
    return Type.read().interrupt_lines;
}

pub fn disableAllInterrupts() void {
    for (0..8) |i| {
        EnableClear.next(i).write(0xFFFFFFFF);
        Priority.next(i).write(0);
        PendingClear.next(i).write(0xFFFFFFFF);
    }
}
