// SPDX-License-Identifier: EUPL-1.2
//! ARM11 interrupt interface control.
//! See ARM11 MPCore TRM §10.6 "CPU Interrupt Interface Registers"
const std = @import("std");
const Only = @import("../platform/Only.zig");
const reg = @import("registers/Helpers.zig").reg;

comptime {
    Only.core(.ARM11);
}

const Base: usize = 0x17E00000;
/// 10.6.1 CPU Interface Control Register, 0x00
pub const ControlReg = reg(packed struct(u32) {
    enable: bool,
    _: u31 = 0,
}, Base + 0x100);
/// 10.6.2 Priority Mask Register, 0x04
pub const Priority = packed struct(u32) {
    _1: u4 = 0,
    priority_mask: u4,
    _2: u24 = 0,
};
const PriorityMaskReg = reg(Priority, Base + 0x104);
/// 10.6.3 Binary Point Register, 0x08
const BinaryPointReg = reg(packed struct(u32) {
    binary_point: u3,
    _: u29 = 0,
}, Base + 0x108);
pub const InterruptAcknowledge = packed struct(u32) {
    id: u10,
    source: u3,
    _: u19 = 0,
};
/// 10.6.4 Interrupt Acknowledge Register, 0x0C
const AcknowledgeReg = reg(InterruptAcknowledge, Base + 0x10C);
/// 10.6.5 End of Interrupt (EOI) Register, 0x10
const EOIReg = reg(InterruptAcknowledge, Base + 0x110);
/// 10.6.6 Running Priority Register, 0x14
const RunningPriorityReg = reg(Priority, Base + 0x114);
/// 10.6.7 Highest Pending Interrupt Register, 0x18
const HighestPendingReg = reg(InterruptAcknowledge, Base + 0x118);

pub fn setEnabled(enable: bool) void {
    ControlReg.changeFields(.{
        .enable = enable,
    });
}
pub fn enabled() bool {
    return ControlReg.read().enable;
}

pub fn setPriorityMask(mask: u4) void {
    PriorityMaskReg.changeFields(.{
        .priority_mask = mask,
    });
}
pub fn priorityMask() u4 {
    return PriorityMaskReg.read().priority_mask;
}

pub fn setBinaryPoint(point: u3) void {
    BinaryPointReg.changeFields(.{
        .binary_point = point,
    });
}
pub fn binaryPoint() u3 {
    return BinaryPointReg.read().binary_point;
}

pub const SpuriousInterruptID: u16 = 1023;
pub fn acknowledge() InterruptAcknowledge {
    return AcknowledgeReg.read();
}

pub fn endOfInterrupt(source: u3, id: u9) void {
    AcknowledgeReg.write(.{ .source = source, .id = id });
}

pub fn runningPriority() u4 {
    return RunningPriorityReg.read().priority_mask;
}

pub fn highestPending() InterruptAcknowledge {
    return HighestPendingReg.read();
}
