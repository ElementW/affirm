// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");

pub const pm = struct {
    pub const ProcessExtra = @import("linux/pm/ProcessExtra.zig");
    pub const ThreadExtra = @import("linux/pm/ThreadExtra.zig");
};

const start = @import("linux/start.zig");

pub const default_output = struct {
    var writer_object = std.io.getStdOut().writer();

    pub fn writer() std.io.AnyWriter {
        return writer_object.any();
    }
};

pub fn waitForEvents() void {
    _ = std.os.linux.nanosleep(&.{
        .tv_sec = 1,
        .tv_nsec = 0,
    }, null);
}

pub fn hang() noreturn {
    std.os.linux.exit(1);
}

pub const main = start.main;

pub fn referenceSymbols() void {}
