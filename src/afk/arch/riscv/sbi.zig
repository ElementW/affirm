// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");

pub const Error = error{
    Failed,
    NotSupported,
    InvalidParam,
    Denied,
    InvalidAddress,
    AlreadyAvailable,
    AlreadyStarted,
    AlreadyStopped,
    NoShmem,
    InvalidState,
    BadRange,
    Timeout,
    IO,
};

const SBIReturn = extern struct {
    pub const ErrorEnum = enum(isize) {
        success = 0,
        failed = -1,
        not_supported = -2,
        invalid_param = -3,
        denied = -4,
        invalid_address = -5,
        already_available = -6,
        already_started = -7,
        already_stopped = -8,
        no_shmem = -9,
        invalid_state = -10,
        bad_range = -11,
        timeout = -12,
        io = -13,
    };
    pub const Value = isize;

    fn unwrap(err: ErrorEnum, value: Value) Error!Value {
        return switch (err) {
            .success => value,
            .failed => Error.Failed,
            .not_supported => Error.NotSupported,
            .invalid_param => Error.InvalidParam,
            .denied => Error.Denied,
            .invalid_address => Error.InvalidAddress,
            .already_available => Error.AlreadyAvailable,
            .already_started => Error.AlreadyStarted,
            .already_stopped => Error.AlreadyStopped,
            .no_shmem => Error.NoShmem,
            .invalid_state => Error.InvalidState,
            .bad_range => Error.BadRange,
            .timeout => Error.Timeout,
            .io => Error.IO,
        };
    }
};

const EID = enum(i32) {
    legacy_set_timer = 0x0,
    legacy_console_putchar = 0x1,
    legacy_console_getchar = 0x2,
    legacy_clear_ipi = 0x3,
    legacy_send_ipi = 0x4,
    legacy_remote_fence_i = 0x5,
    legacy_remote_sfence_vma = 0x6,
    legacy_remote_sfence_vma_asid = 0x7,
    legacy_shutdown = 0x8,
    base = 0x10,
    time = 0x54494d45,
    ipi = 0x735049,
    rfence = 0x52464e43,
    hsm = 0x48534d,
    srst = 0x53525354,
    pmu = 0x504d55,
    dbcn = 0x4442434e,
    _,
};

inline fn ecall1(eid: EID, fid: i32, arg0: isize) Error!isize {
    var err: SBIReturn.ErrorEnum = undefined;
    var value: SBIReturn.Value = undefined;
    asm volatile ("ecall"
        : [err] "={a0}" (err),
          [value] "={a1}" (value),
        : [eid] "{a7}" (@intFromEnum(eid)),
          [fid] "{a6}" (fid),
          [arg0] "{a0}" (arg0),
    );
    return SBIReturn.unwrap(err, value);
}

inline fn ecall3(eid: EID, fid: i32, arg0: isize, arg1: isize, arg2: isize) Error!isize {
    var err: SBIReturn.ErrorEnum = undefined;
    var value: SBIReturn.Value = undefined;
    asm volatile ("ecall"
        : [err] "={a0}" (err),
          [value] "={a1}" (value),
        : [eid] "{a7}" (@intFromEnum(eid)),
          [fid] "{a6}" (fid),
          [arg0] "{a0}" (arg0),
          [arg1] "{a1}" (arg1),
          [arg2] "{a2}" (arg2),
    );
    return SBIReturn.unwrap(err, value);
}

pub const DebugConsole = struct {
    pub const Writer = std.io.GenericWriter(void, Error, writeFn);

    fn writeFn(context: void, bytes: []const u8) Error!usize {
        _ = context;
        return write(bytes);
    }

    pub fn writer() Writer {
        return .{ .context = {} };
    }

    pub fn write(bytes: []const u8) Error!usize {
        return @bitCast(try ecall3(.dbcn, 0, @bitCast(bytes.len), @bitCast(@intFromPtr(bytes.ptr)), 0));
    }

    pub fn read(bytes: []u8) Error!usize {
        return @bitCast(try ecall3(.dbcn, 1, @bitCast(bytes.len), @bitCast(@intFromPtr(bytes.ptr)), 0));
    }
};
