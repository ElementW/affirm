// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afk = @import("../../afk.zig");
const afu = @import("afu");

const satp = @import("csr/supervisor.zig").AddressTranslation;
const sbi = @import("sbi.zig");

const log = std.log.scoped(.riscv_start);

/// Linux RISC-V boot image header
const BootImageHeader = extern struct {
    const Magic = "RISCV\x00\x00\x00";
    const Magic2 = "RSC\x05";
    const Version = extern struct {
        major: u16,
        minor: u16,
    };
    const Flags = packed struct(u64) {
        big_endian: bool = false,
        _: u63 = 0,
    };

    /// Executable code
    code0: u32,
    /// Executable code
    code1: u32,
    /// Image load offset, little endian
    text_offset: u64,
    /// Effective Image size, little endian
    image_size: u64,
    /// Kernel flags, little endian
    flags: u64,
    /// Version of this header
    version: Version,
    /// Reserved
    res1: u32 = 0,
    /// Reserved
    res2: u64 = 0,
    /// Magic number, little endian
    magic: u64 = Magic,
    /// Magic number 2, little endian
    magic2: u32 = Magic2,
    /// Reserved for PE COFF offset
    res3: u32,
};

pub fn _header() linksection(".text.header") callconv(.Naked) void {
    _ = _start;
    const m_mode = false;
    const efi = false;
    if (comptime efi) {
        // The first instruction encodes to "MZ" which is required
        // for the image to be a valid PE/COFF executable.
        asm volatile (
            \\c.li s4, -13
            \\j _start
        );
    } else {
        asm volatile (
            \\j _start
        );
    }
    const text_offset: usize = if (comptime m_mode)
        0
    else if (@bitSizeOf(usize) == 64)
        0x200000
    else
        0x400000;
    const flags: BootImageHeader.Flags = .{
        .big_endian = false,
    };
    const version: BootImageHeader.Version = .{
        .major = 0,
        .minor = 2,
    };
    asm volatile (
        \\.balign 8
        \\.dword %c[text_offset]
        \\.dword __end__ - __start__
        \\.dword %c[flags]
        \\.word %c[version]
        \\.word 0
        \\.dword 0
        \\.dword %c[magic]
        \\.balign 4
        \\.word %c[magic2]
        \\.word 0
        :
        : [text_offset] "i" (text_offset),
          [flags] "i" (flags),
          [version] "i" (version),
          [magic] "i" (std.mem.readInt(u32, BootImageHeader.Magic, .little)),
          [magic2] "i" (std.mem.readInt(u64, BootImageHeader.Magic2, .little)),
    );
}

export fn _start() linksection(".text.start") callconv(.Naked) void {
    _ = arch_kmain;
    asm volatile (
        \\mv ra, zero
        \\la sp, _stack_end
        \\la t0, __bss_start
        \\la t1, __bss_end
        \\la gp, _stack_start
        \\csrw satp, zero
        \\clear_bss:
        \\  sd zero, 0(t0)
        \\  addi t0, t0, 8
        \\  blt t0, t1, clear_bss
        \\tail arch_kmain
    );
}

const FDT = afu.dt.FDT;
export fn arch_kmain(hart_id: usize, maybe_fdt: usize) noreturn {
    satp.set(.{
        .ppn = 0,
        .asid = 0,
        .mode = .bare,
    });
    var fdt: ?*align(8) const FDT = null;
    if (maybe_fdt != 0 and std.mem.isAligned(maybe_fdt, 8)) {
        const check_fdt = @as(*align(8) const FDT, @ptrFromInt(maybe_fdt));
        if (check_fdt.isValid()) {
            fdt = check_fdt;
        }
    }
    log.info("Hart #{}, DT {s}", .{ hart_id, if (fdt != null) "ok" else "none" });

    afk.Kernel.kmain();
}
