// SPDX-License-Identifier: EUPL-1.2
pub const std = @import("std");
pub const builtin = @import("builtin");

pub const CSRAttrib = struct {
    memClobber: bool = false,
    needCpuFeatures: []const std.Target.riscv.Feature = &.{},
};

pub fn CSR(comptime N: u12, comptime attr: CSRAttrib, comptime T32: type, comptime T64: type) type {
    if (@bitSizeOf(T32) != 32) {
        @compileError(std.fmt.comptimePrint("{s} needs to be 32 bits to be used as CSR value, but is {} bits", .{
            @typeName(T32),
            @bitSizeOf(T32),
        }));
    }
    if (@bitSizeOf(T64) != 32 and @bitSizeOf(T64) != 64) {
        @compileError(std.fmt.comptimePrint("{s} needs to be 32 or 64 bits to be used as CSR value, but is {} bits", .{
            @typeName(T64),
            @bitSizeOf(T64),
        }));
    }
    const T = if (builtin.cpu.arch == .riscv64) T64 else T32;
    if (@typeInfo(T) == .@"struct" and @typeInfo(T).@"struct".layout != .@"packed") {
        @compileError(@typeName(T) ++ " is not packed");
    }
    return struct {
        pub const Value = T;

        pub inline fn get() Value {
            return asm volatile ("csrr " ++ std.fmt.comptimePrint("{}", .{N}) ++ ", %c[reg]"
                : [out] "=r" (-> Value),
            );
        }

        pub inline fn set(v: Value) void {
            if (comptime attr.memClobber) {
                _ = asm volatile ("csrw " ++ std.fmt.comptimePrint("{}", .{N}) ++ ", %[in]"
                    :
                    : [in] "r" (v),
                    : "memory"
                );
            } else {
                _ = asm volatile ("csrw " ++ std.fmt.comptimePrint("{}", .{N}) ++ ", %[in]"
                    :
                    : [in] "r" (v),
                );
            }
        }
    };
}

pub fn CSR32(comptime N: u12, comptime attr: CSRAttrib, comptime T: type) type {
    return CSR(N, attr, T, T);
}
