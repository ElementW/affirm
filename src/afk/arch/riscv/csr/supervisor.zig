// SPDX-License-Identifier: EUPL-1.2
const CSR = @import("register.zig").CSR;

pub const Status = CSR(0x100, .{}, packed struct(u32) {}, packed struct(u64) {});

pub const TrapVectorMode = @import("machine.zig").TrapVectorMode;
pub const TrapVector = CSR(0x105, .{}, packed struct(u32) {
    mode: TrapVectorMode,
    base: u30,
}, packed struct(u64) {
    mode: TrapVectorMode,
    base: u62,
});

pub const AddressTranslation = CSR(0x180, .{}, packed struct(u32) {
    pub const Mode = enum(u1) {
        bare = 0,
        sv32 = 1,
    };
    ppn: u22,
    asid: u9,
    mode: Mode,
}, packed struct(u64) {
    pub const Mode = enum(u4) {
        bare = 0,
        sv39 = 8,
        sv48 = 9,
        sv57 = 10,
        sv64 = 11,
        _,
    };
    ppn: u44,
    asid: u16,
    mode: Mode,
});

pub const Context = CSR(0x5A8, .{}, packed struct(u32) {}, packed struct(u64) {});
