// SPDX-License-Identifier: EUPL-1.2
const CSR = @import("register.zig").CSR;
const CSR32 = @import("register.zig").CSR32;
