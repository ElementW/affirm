// SPDX-License-Identifier: EUPL-1.2
const CSR = @import("register.zig").CSR;
const CSR32 = @import("register.zig").CSR32;

pub const XLEN = enum(u2) {
    unsupported = 0,
    rv32 = 1,
    rv64 = 2,
    rv128 = 3,
};

pub const Extensions = packed struct(u26) {
    a: bool,
    b: bool,
    c: bool,
    d: bool,
    e: bool,
    f: bool,
    g: bool,
    h: bool,
    i: bool,
    j: bool,
    k: bool,
    l: bool,
    m: bool,
    n: bool,
    o: bool,
    p: bool,
    q: bool,
    r: bool,
    s: bool,
    t: bool,
    u: bool,
    v: bool,
    w: bool,
    x: bool,
    y: bool,
    z: bool,
};

pub const Endianness = enum(u1) {
    little = 0,
    big = 1,
};

pub const ISA = CSR(0x100, .{}, packed struct(u32) {
    extensions: Extensions,
    _: u4 = 0,
    m_xlen: XLEN,
}, packed struct(u64) {
    extensions: Extensions,
    _: u36 = 0,
    m_xlen: XLEN,
});

pub const VendorID = CSR32(0x100, .{}, packed struct(u32) {
    offset: u7,
    bank: u25,
});

pub const ArchitectureID = CSR(0x100, .{}, u32, u64);
pub const Implementation = CSR(0x100, .{}, u32, u64);
pub const HartID = CSR(0x100, .{}, u32, u64);

pub const Status = CSR(0x100, .{}, packed struct(u32) {
    nyi: u32,
}, packed struct(u64) {
    _1: u1 = 0,
    s_interrupt_enable: bool,
    _2: u1 = 0,
    m_interrupt_enable: bool,
    _3: u1 = 0,
    s_previous_interrupt_enable: bool,
    u_endianness: Endianness,
    m_previous_interrupt_enable: bool,
    s_previous_privilege: u1,
    _4: u2 = 0,
    m_previous_privilege: u2,
    floating_state: enum(u2) {
        off = 0,
        initial = 1,
        clean = 2,
        dirty = 3,
    },
    extension_state: enum(u2) {
        all_off = 0,
        none_dirty_or_clean = 1,
        none_dirty_some_clean = 2,
        some_dirty = 3,
    },
    modify_privilege: enum(u1) {
        normal = 0,
        as_m_previous_privilege = 1,
    },
    /// When `false`, accessing pages accessible by U-mode will fault.
    s_user_memory_access: bool,
    make_executable_readable: bool,
    trap_virtual_memory: bool,
    timeout_wait: bool,
    trap_sret: bool,
    _5: u9 = 0,
    u_xlen: XLEN,
    s_xlen: XLEN,
    s_endianness: Endianness,
    m_endianness: Endianness,
    _6: u2 = 0,
    /// Signals when `floating_state` or `extension_state` report dirty state.
    some_dirty: bool,
});

pub const TrapVectorMode = enum(u2) {
    direct = 0,
    vectored = 1,
    _,
};
pub const TrapVector = CSR(0x100, .{}, packed struct(u32) {
    mode: TrapVectorMode,
    base: u30,
}, packed struct(u64) {
    mode: TrapVectorMode,
    base: u62,
});

pub const TrapDelegation = CSR(0x100, .{}, u32, u64);
pub const InterruptDelegation = CSR(0x100, .{}, u32, u64);

pub const StandardInterrupts = packed struct(u16) {
    _1: u1 = 0,
    s_software: bool,
    _2: u1 = 0,
    m_software: bool,
    _3: u1 = 0,
    s_timer: bool,
    _4: u1 = 0,
    m_timer: bool,
    _5: u1 = 0,
    s_external: bool,
    _6: u1 = 0,
    m_external: bool,
    _7: u4 = 0,
};
pub const InterruptPending = CSR(0x100, .{}, packed struct(u32) {
    standard: StandardInterrupts,
    extended: u16,
}, packed struct(u64) {
    standard: StandardInterrupts,
    extended: u48,
});
pub const InterruptEnable = CSR(0x100, .{}, packed struct(u32) {
    standard: StandardInterrupts,
    extended: u16,
}, packed struct(u64) {
    standard: StandardInterrupts,
    extended: u48,
});
