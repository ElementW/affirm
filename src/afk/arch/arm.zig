// SPDX-License-Identifier: EUPL-1.2
const afu = @import("afu");

pub const cache = @import("arm/cache.zig");
pub const cp15 = @import("arm/cp15.zig");
pub const irq = @import("arm/irq.zig");
pub const impl = struct {
    pub const sync = @import("arm/impl/sync.zig");
};
pub const mach = @import("arm/mach.zig");
pub const mm = @import("arm/mm.zig");
pub const pm = struct {
    pub const ThreadContext = afu.os.dbg.ThreadContext;
};
pub const syscall = @import("arm/syscall.zig");

pub const PSR = @import("arm/psr.zig").PSR;

pub fn waitForEvents() void {
    asm volatile ("wfi");
}

pub fn hang() noreturn {
    while (true) {
        asm volatile ("wfi");
    }
}
pub fn init() void {
    @import("arm/xrq.zig").init();
}
