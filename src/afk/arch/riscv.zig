// SPDX-License-Identifier: EUPL-1.2
pub const std = @import("std");
pub const sbi = @import("riscv/sbi.zig");

const start = @import("riscv/start.zig");

pub const default_output = struct {
    pub fn writer() std.io.AnyWriter {
        return sbi.DebugConsole.writer().any();
    }
};

pub fn waitForEvents() void {
    asm volatile ("wfi");
}

pub fn hang() noreturn {
    while (true) {
        asm volatile ("wfi");
    }
}

pub fn referenceSymbols() void {
    _ = start._header;
}
