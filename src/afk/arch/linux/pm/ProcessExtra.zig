// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");

linux_pid: std.os.linux.pid_t,
