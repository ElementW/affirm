// SPDX-License-Identifier: EUPL-1.2
const builtin = @import("builtin");
const std = @import("std");

pub fn wip() void {
    const SysemuSupport = struct { number: comptime_int, version: std.SemanticVersion };
    const sysemu_support: SysemuSupport = comptime switch (builtin.target.cpu.arch) {
        .aarch64, .aarch64_be => .{
            .number = 31,
            .version = std.SemanticVersion.parse("5.3.0") catch unreachable,
        },
        .loongarch32, .loongarch64 => .{
            .number = 0x1f,
            .version = std.SemanticVersion.parse("5.19.0") catch unreachable,
        },
        .powerpc, .powerpcle, .powerpc64, .powerpc64le => .{
            .number = 0x1d,
            .version = std.SemanticVersion.parse("4.20.0") catch unreachable,
        },
        .riscv32, .riscv64 => .{
            .number = 0x1f,
            .version = std.SemanticVersion.parse("6.4.0") catch unreachable,
        },
        .s390x => .{
            .number = 31,
            .version = std.SemanticVersion.parse("5.12.0") catch unreachable,
        },
        .x86, .x86_64 => .{
            .number = 31,
            .version = std.SemanticVersion.parse("2.6.9") catch unreachable,
        },
        else => |a| @compileError("Linux for " ++ @tagName(a) ++ " does not support PTRACE_SYSEMU"),
    };
    const uname = std.posix.uname();
    const kernel_version = v: {
        var it = std.mem.splitAny(u8, &uname.release, "-_\x00");
        break :v try std.SemanticVersion.parse(it.first());
    };
    if (std.math.compare(kernel_version, .lt, sysemu_support.version)) {
        std.log.err(
            "Host Linux version {} is too old to support PTRACE_SYSEMU (need >= {})",
            .{ kernel_version, sysemu_support.version },
        );
        return error.LinuxVersionTooLow;
    }
    const PTRACE_SYSEMU = sysemu_support.number;
    _ = PTRACE_SYSEMU;
}
