// SPDX-License-Identifier: EUPL-1.2
const builtin = @import("builtin");
const std = @import("std");
const afk = @import("../../afk.zig");

const FDT = @import("afu").dt.FDT;
const dtb align(64) = @embedFile("dtb.dtb").*;

fn getenvBuf(buffer: []u8, key: []const u8) ?[]const u8 {
    const value = std.posix.getenv(key) orelse return null;
    if (value.len > buffer.len) {
        var panic_buf: [512]u8 = undefined;
        @panic(std.fmt.bufPrint(&panic_buf, "${s} is longer than {} bytes", .{ key, buffer.len }) catch "bufPrint fail");
    }
    std.mem.copyForwards(u8, buffer, value);
    return buffer[0..value.len];
}

pub fn main() !void {
    //fdt.print(sbi.DebugConsole.writer().any()) catch @panic("print");
    //afk.Kernel.kmain();

    var afprefix_buf: [std.posix.PATH_MAX]u8 = undefined;
    const afprefix = getenvBuf(&afprefix_buf, "AFPREFIX") orelse pfx: {
        var data_home_buf: [std.posix.PATH_MAX]u8 = undefined;
        const data_home = getenvBuf(&data_home_buf, "XDG_DATA_HOME") orelse dh: {
            var home_buf: [std.posix.PATH_MAX]u8 = undefined;
            const home = getenvBuf(&home_buf, "HOME") orelse h: {
                const LOGIN_NAME_MAX = 256;
                var user_buf: [LOGIN_NAME_MAX]u8 = undefined;
                const user = getenvBuf(&user_buf, "USER") orelse @panic("$USER is unset");
                break :h try std.fmt.bufPrint(&home_buf, "/home/{s}", .{user});
            };
            break :dh try std.fmt.bufPrint(&data_home_buf, "{s}/.local/share", .{home});
        };
        break :pfx try std.fmt.bufPrint(&afprefix_buf, "{s}/affinis/default", .{data_home});
    };

    std.log.info("libc linked: {s}", .{if (builtin.link_libc) "YES" else "NO"});
    std.log.info("afprefix   : {s}", .{afprefix});
}
