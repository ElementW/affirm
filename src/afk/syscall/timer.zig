// SPDX-License-Identifier: EUPL-1.2
const afk = @import("../afk.zig");
const afu = @import("afu");
const Result = afu.Result;
const Handle = afu.Handle;

const UTimer = afu.os.sync.Timer;
const NI = afu.os.results.NotImplemented;

pub fn scCreateTimer(reset_type: afu.os.sync.ResetType) Result.With(Handle.Of(UTimer)) {
    _ = reset_type;
    return NI.without(Handle.Of(UTimer));
}

pub fn scSetTimer(timer_handle: Handle.Of(UTimer), initial: i64, interval: i64) Result {
    _ = timer_handle;
    _ = initial;
    _ = interval;
    return NI;
}

pub fn scCancelTimer(timer_handle: Handle.Of(UTimer)) Result {
    _ = timer_handle;
    return NI;
}

pub fn scClearTimer(timer_handle: Handle.Of(UTimer)) Result {
    _ = timer_handle;
    return NI;
}
