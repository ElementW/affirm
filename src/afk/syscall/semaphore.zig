// SPDX-License-Identifier: EUPL-1.2
const afk = @import("../afk.zig");
const afu = @import("afu");
const Result = afu.Result;
const Handle = afu.Handle;

const USemaphore = afu.os.sync.Semaphore;
const NI = afu.os.results.NotImplemented;

pub fn scCreateSemaphore(initial_count: u32, max_count: u32) Result.With(Handle.Of(USemaphore)) {
    _ = initial_count;
    _ = max_count;
    return NI.without(Handle.Of(USemaphore));
}

pub fn scReleaseSemaphore(semaphore_handle: Handle.Of(USemaphore), release_count: i32) Result.With(i32) {
    _ = semaphore_handle;
    _ = release_count;
    return NI.without(i32);
}
