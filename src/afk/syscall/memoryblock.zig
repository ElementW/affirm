// SPDX-License-Identifier: EUPL-1.2
const afk = @import("../afk.zig");
const afu = @import("afu");
const Result = afu.Result;
const Handle = afu.Handle;

const UMemoryBlock = afu.os.mm.MemoryBlock;
const NI = afu.os.results.NotImplemented;

pub fn scCreateMemoryBlock(
    addr: *anyopaque,
    size: usize,
    self_perm: afu.os.mm.MemoryPermission,
    other_perm: afu.os.mm.MemoryPermission,
) Result.With(Handle.Of(UMemoryBlock)) {
    _ = addr;
    _ = size;
    _ = self_perm;
    _ = other_perm;
}

pub fn scMapMemoryBlock(
    memblock_handle: Handle.Of(UMemoryBlock),
    addr: *anyopaque,
    self_perm: afu.os.mm.MemoryPermission,
    other_perm: afu.os.mm.MemoryPermission,
) Result {
    _ = memblock_handle;
    _ = addr;
    _ = self_perm;
    _ = other_perm;
}

pub fn scUnmapMemoryBlock(memblock_handle: Handle.Of(UMemoryBlock), addr: *anyopaque) Result {
    _ = memblock_handle;
    _ = addr;
}
