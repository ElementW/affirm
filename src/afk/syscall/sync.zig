// SPDX-License-Identifier: EUPL-1.2
const afk = @import("../afk.zig");
const afu = @import("afu");
const Result = afu.Result;
const Handle = afu.Handle;

const NI = afu.os.results.NotImplemented;

pub fn scWaitSynchronization(handle: Handle, nanoseconds: i64) Result {
    _ = handle;
    _ = nanoseconds;
    return NI;
}

pub fn scWaitSynchronizationN(handles: []const Handle, wait_all: bool, nanoseconds: i64) Result.With(i32) {
    _ = handles;
    _ = wait_all;
    _ = nanoseconds;
    return NI;
}

pub fn scSignalAndWait(signal_handle: Handle, wait_handles: []const Handle, wait_all: bool, nanoseconds: i64) Result.With(i32) {
    _ = signal_handle;
    _ = wait_handles;
    _ = wait_all;
    _ = nanoseconds;
    return NI;
}

pub fn scSendSyncRequest1(session_handle: Handle.Of(afu.os.ipc.Session)) Result {
    _ = session_handle;
    return NI;
}

pub fn scSendSyncRequest2(session_handle: Handle.Of(afu.os.ipc.Session)) Result {
    _ = session_handle;
    return NI;
}

pub fn scSendSyncRequest3(session_handle: Handle.Of(afu.os.ipc.Session)) Result {
    _ = session_handle;
    return NI;
}

pub fn scSendSyncRequest4(session_handle: Handle.Of(afu.os.ipc.Session)) Result {
    _ = session_handle;
    return NI;
}

pub fn scSendSyncRequest(session_handle: Handle.Of(afu.os.ipc.Session)) Result {
    _ = session_handle;
    return NI;
}
