// SPDX-License-Identifier: EUPL-1.2
const afk = @import("../afk.zig");
const afu = @import("afu");
const Result = afu.Result;
const Handle = afu.Handle;

const UThread = afu.os.pm.Thread;
const NI = afu.os.results.NotImplemented;

pub fn scCreateThread(entrypoint: UThread.Func, arg: usize, stack_top: *anyopaque, core_id: afu.os.pm.Core.Id) Result.With(Handle.Of(UThread)) {
    _ = entrypoint;
    _ = arg;
    _ = stack_top;
    _ = core_id;
    return NI;
}

pub fn scExitThread() noreturn {
    @panic("NYI");
}

pub fn scSleepThread(nanoseconds: i64) void {
    _ = nanoseconds;
    @panic("NYI");
}

pub fn scGetThreadPriority(thread_handle: Handle.Of(UThread)) Result.With(UThread.Priority) {
    _ = thread_handle;
    return NI;
}

pub fn scSetThreadPriority(thread_handle: Handle.Of(UThread), priority: UThread.Priority) Result {
    _ = thread_handle;
    _ = priority;
    return NI;
}

// TODO pub fn scGetThreadAffinityMask,

// TODO pub fn scSetThreadAffinityMask,

pub fn scGetThreadIdealCore(thread_handle: Handle.Of(UThread)) Result.With(afu.os.pm.Core.Id) {
    _ = thread_handle;
    return NI;
}

pub fn scSetThreadIdealCore(thread_handle: Handle.Of(UThread), core_id: afu.os.pm.Core.Id) Result {
    _ = thread_handle;
    _ = core_id;
    return NI;
}

pub fn scGetCurrentCoreId() afu.os.pm.Core.Id {
    @panic("NYI");
}
