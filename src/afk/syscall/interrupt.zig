// SPDX-License-Identifier: EUPL-1.2
const afk = @import("../afk.zig");
const afu = @import("afu");
const Result = afu.Result;
const Handle = afu.Handle;

const NI = afu.os.results.NotImplemented;

pub fn scBindInterrupt(
    interrupt_index: afu.os.hw.Interrupt.Index,
    wakeup_handle: Handle,
    priority: afu.os.hw.Interrupt.Priority,
    is_manual_clear: bool,
) Result {
    _ = interrupt_index;
    _ = wakeup_handle;
    _ = priority;
    _ = is_manual_clear;
    return NI;
}

pub fn scUnbindInterrupt(interrupt_index: afu.os.hw.Interrupt.Index, wakeup_handle: Handle) Result {
    _ = interrupt_index;
    _ = wakeup_handle;
    return NI;
}
