// SPDX-License-Identifier: EUPL-1.2
const afk = @import("../afk.zig");
const afu = @import("afu");
const Result = afu.Result;
const Handle = afu.Handle;

const UEvent = afu.os.sync.Event;
const NI = afu.os.results.NotImplemented;

pub fn scCreateEvent(reset_type: afu.os.sync.ResetType) Result.With(Handle.Of(UEvent)) {
    _ = reset_type;
    return NI.without(Handle.Of(UEvent));
}

pub fn scSignalEvent(event_handle: Handle.Of(UEvent)) Result {
    _ = event_handle;
    return NI;
}

pub fn scClearEvent(event_handle: Handle.Of(UEvent)) Result {
    _ = event_handle;
    return NI;
}
