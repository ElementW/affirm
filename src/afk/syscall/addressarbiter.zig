// SPDX-License-Identifier: EUPL-1.2
const afk = @import("../afk.zig");
const afu = @import("afu");
const Result = afu.Result;
const Handle = afu.Handle;

const UAddressArbiter = afu.os.sync.AddressArbiter;
const NI = afu.os.results.NotImplemented;

pub fn scCreateAddressArbiter() Result.With(Handle.Of(UAddressArbiter)) {
    return NI.without(Handle.Of(UAddressArbiter));
}

pub fn scArbitrateAddress(
    arbiter_handle: Handle.Of(UAddressArbiter),
    addr: *anyopaque,
    @"type": afu.os.pm.ArbitrationType,
    value: i32,
    nanoseconds: i64,
) Result {
    _ = arbiter_handle;
    _ = addr;
    _ = @"type";
    _ = value;
    _ = nanoseconds;
    return NI;
}
