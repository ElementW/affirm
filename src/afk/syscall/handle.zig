// SPDX-License-Identifier: EUPL-1.2
const afk = @import("../afk.zig");
const afu = @import("afu");
const Result = afu.Result;
const Handle = afu.Handle;

const NI = afu.os.results.NotImplemented;

pub fn scDuplicateHandle(handle: Handle) Result.With(Handle) {
    afk.currentProcess()
    _ = handle;
    return NI;
}

pub fn scCloseHandle(handle: Handle) Result {
    _ = handle;
    return NI;
}
