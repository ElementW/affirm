// SPDX-License-Identifier: EUPL-1.2
const afk = @import("../afk.zig");
const afu = @import("afu");
const Result = afu.Result;
const Handle = afu.Handle;

const UMutex = afu.os.sync.Mutex;
const NI = afu.os.results.NotImplemented;

pub fn scCreateMutex(initially_locked: bool) Result.With(Handle.Of(UMutex)) {
    _ = initially_locked;
    return NI.without(Handle.Of(UMutex));
}

pub fn scReleaseMutex(mutex_handle: Handle.Of(UMutex)) Result {
    _ = mutex_handle;
    return NI;
}
