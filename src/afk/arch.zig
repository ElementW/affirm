// SPDX-License-Identifier: EUPL-1.2
const builtin = @import("builtin");

pub const arm = @import("arch/arm.zig");
pub const aarch64 = @import("arch/aarch64.zig");
pub const linux = @import("arch/linux.zig");
pub const m68k = @import("arch/m68k.zig");
pub const riscv = @import("arch/riscv.zig");
pub const x86_64 = @import("arch/x86_64.zig");

pub const target = if (builtin.os.tag == .linux) linux else switch (builtin.cpu.arch) {
    .arm => arm,
    .aarch64 => aarch64,
    .m68k => m68k,
    .riscv32, .riscv64 => riscv,
    .x86_64 => x86_64,
    else => |arch| @compileError("Unsupported architecture " + @tagName(arch)),
};
