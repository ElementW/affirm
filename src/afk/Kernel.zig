// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afk = @import("afk.zig");
const afu = @import("afu");

pub var scheduler: afk.sched.Scheduler = undefined;
pub var page_allocator: afk.mm.PageAllocator = undefined;
pub var heaps: afk.heaps.Heaps = undefined;

pub fn currentCoreId() afu.os.pm.Core.Id {
    // NYI
    return @enumFromInt(0);
}

pub fn process(process_id: afu.os.pm.Process.Id) ?*afk.pm.Process {
    _ = process_id;
    return null;
}

pub fn currentProcess() ?*afk.pm.Process {
    @panic("currentProcess NYI");
}

pub fn thread(process_id: afu.os.pm.Process.Id, thread_id: afu.os.pm.Thread.Id) ?*afk.pm.Thread {
    _ = process_id;
    _ = thread_id;
    return null;
}

pub fn currentThread() ?*afk.pm.Thread {
    @panic("currentThread NYI");
}

extern const _stack_top: usize;
pub export fn kmain() noreturn {
    const log = std.log.scoped(.kmain);

    //_ = afk.arch.target.sbi.DebugConsole.write("af kernel RISC-V test\n") catch unreachable;
    // var buf: [17]u8 = undefined;
    // const sp: usize = asm volatile ("mv %[ret], sp"
    //     : [ret] "=r" (-> usize),
    // );
    // for (0..16) |i| {
    //     buf[i] = "0123456789ABCDEF"[(sp >> (@as(u6, @truncate(15 - i)) * 4)) & 0xF];
    // }
    // buf[16] = '\n';
    // _ = afk.arch.target.sbi.DebugConsole.write("sp = ") catch unreachable;
    // _ = afk.arch.target.sbi.DebugConsole.write(&buf) catch unreachable;
    // asm volatile ("addi x2, x2, 0");
    // _ = afk.arch.target.sbi.DebugConsole.write("fp = ") catch unreachable;
    // const fp = @frameAddress();
    // for (0..15) |i| {
    //     buf[i] = "0123456789ABCDEF"[(fp >> (@as(u6, @truncate(15 - i)) * 4)) & 0xF];
    // }
    // asm volatile ("addi x3, x3, 0");
    // _ = afk.arch.target.sbi.DebugConsole.write(&buf) catch unreachable;
    // _ = afk.arch.target.sbi.DebugConsole.write(std.fmt.bufPrint(&buf, "{X:0>16}", .{sp}) catch "oops") catch unreachable;"oops") catch unreachable;
    // _ = afk.arch.target.sbi.DebugConsole.write("\n") catch unreachable;
    // _ = afk.arch.target.sbi.DebugConsole.write(std.fmt.bufPrint(&buf, "{X:0>16}", .{_stack_top}) catch "oops") catch unreachable;
    // _ = afk.arch.target.sbi.DebugConsole.write("\n") catch unreachable;
    //std.log.info("hello \"{s}\" from kmain", .{"world"});
    //afk.arch.target.waitForEvents();
    log.info("{} bba", .{7});
    @panic("hello");
    // while (true) afk.arch.target.waitForEvents();
}
