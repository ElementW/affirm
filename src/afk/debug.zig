// SPDX-License-Identifier: EUPL-1.2
pub const screen = struct {
    pub const LogScreen = @import("debug/screen/LogScreen.zig");
};
