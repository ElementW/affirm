// SPDX-License-Identifier: EUPL-1.2
const builtin = @import("builtin");
const std = @import("std");
const afu = @import("afu");
const afsys = @import("afsys");
const RGBA8888 = afu.gfx.Color.RGBA8888;
const FramebufferColorFormat = afsys.plat.nintendo3ds.gfx.Framebuffer.ColorFormat;
const Self = @This();

const Margins = struct {
    left: u8,
    right: u8,
    top: u8,
    bottom: u8,
};

const Font = @import("font/Font.zig").Font;
const DefaultFont = @import("font/afTerm.zig").afTerm;
const DefaultColor = RGBA8888.White;
const DefaultMargins = Margins{
    .left = 10,
    .right = 10,
    .top = 10,
    .bottom = 10,
};

pub const Palette = struct {
    foreground: RGBA8888,
    high: RGBA8888,
    low: RGBA8888,
    background: RGBA8888,

    pub fn compute(foreground: RGBA8888, background: RGBA8888) Palette {
        const high = RGBA8888.fromRGB(
            @as(u8, @truncate((@as(u32, background.b) * 85 + @as(u32, foreground.b) * 170) >> 8)),
            @as(u8, @truncate((@as(u32, background.g) * 85 + @as(u32, foreground.g) * 170) >> 8)),
            @as(u8, @truncate((@as(u32, background.r) * 85 + @as(u32, foreground.r) * 170) >> 8)),
        );
        const low = RGBA8888.fromRGB(
            @as(u8, @truncate((@as(u32, background.b) * 170 + @as(u32, foreground.b) * 85) >> 8)),
            @as(u8, @truncate((@as(u32, background.g) * 170 + @as(u32, foreground.g) * 85) >> 8)),
            @as(u8, @truncate((@as(u32, background.r) * 170 + @as(u32, foreground.r) * 85) >> 8)),
        );
        return Palette{
            .foreground = foreground,
            .high = high,
            .low = low,
            .background = background,
        };
    }
};

fb: [*]align(4) u8,
fb_width: u16,
fb_height: u16,
fb_format: FramebufferColorFormat,
bytes_per_pixel: u8,
bytes_per_column: u16,
charsX: u16,
charsY: u16,
locked: bool = false,
font: *const Font = &DefaultFont,

pub fn lock(self: *Self) void {
    self.locked = true;
}
pub fn unlock(self: *Self) void {
    self.locked = false;
}

pub fn drawCharacter(self: *Self, character: u21, x: u16, y: u16, palette: Palette) void {
    for (0..Font.Height) |yy| {
        const xDisplacement = x * @as(u32, self.bytes_per_column);
        const yDisplacement = (self.fb_height - 1 - (y + yy)) * @as(u32, self.bytes_per_pixel);
        var screenPos = @as([*]u8, @ptrCast(self.fb)) + xDisplacement + yDisplacement;

        var char_line = self.font.data[character][yy];
        if (self.fb_format == .rgba8888) {
            for (0..Font.Width) |_| {
                @as(*u32, @ptrCast(@alignCast(screenPos))).* = switch (@as(Font.PixelType, @truncate(char_line))) {
                    0 => @byteSwap(palette.background.toU32()),
                    1 => @byteSwap(palette.low.toU32()),
                    2 => @byteSwap(palette.high.toU32()),
                    3 => @byteSwap(palette.foreground.toU32()),
                };
                screenPos += self.bytes_per_column;
                char_line >>= Font.BitDepth;
            }
        } else {
            for (0..Font.Width) |_| {
                const color = switch (@as(Font.PixelType, @truncate(char_line))) {
                    0 => palette.background,
                    1 => palette.low,
                    2 => palette.high,
                    3 => palette.foreground,
                };
                screenPos[0] = color.b;
                screenPos[1] = color.g;
                screenPos[2] = color.r;
                screenPos += self.bytes_per_column;
                char_line >>= Font.BitDepth;
            }
        }
    }
}

pub fn drawTextLine(self: *Self, str: []const u8, x: u16, y: u16, palette: Palette) void {
    if (x >= self.fb_width or y >= self.fb_height) return;
    var x_cursor = x;
    for (str) |char| {
        self.drawCharacter(char, x_cursor, y, palette);
        x_cursor += Font.Width;
        if (x_cursor >= self.fb_width) {
            break;
        }
    }
    while (x_cursor < self.fb_width) : (x_cursor += Font.Width) {
        self.drawCharacter(' ', x_cursor, y, palette);
    }
}

pub fn init(fb: [*]align(4) u8, fb_width: u16, fb_height: u16, fb_format: FramebufferColorFormat) Self {
    const fb_pixel_bytes = fb_format.byteSize();
    var self = Self{
        .fb = fb,
        .fb_width = fb_width,
        .fb_height = fb_height,
        .fb_format = fb_format,
        .bytes_per_pixel = fb_pixel_bytes,
        .bytes_per_column = @as(u16, fb_pixel_bytes) * fb_height,
        .charsX = ((fb_width - DefaultMargins.left - DefaultMargins.right) / Font.Width) + 1,
        .charsY = (fb_height - DefaultMargins.top - DefaultMargins.bottom) / Font.Height,
        .locked = false,
    };
    self.clear();
    return self;
}

var debugstr: [256 * 64]u8 = undefined; // TODO remove this static allocation

pub fn clear(self: *Self) void {
    @memset(debugstr[0 .. self.charsX * self.charsY], 0);
    const pixels = @as(u32, self.fb_width) * @as(u32, self.fb_height);
    if (self.fb_format == .rgba8888) {
        const fb32 = @as([*]u32, @ptrCast(self.fb));
        for (0..pixels) |i| {
            fb32[i] = @byteSwap(RGBA8888.Black.toU32());
        }
    } else {
        var screen = @as([*]u8, @ptrCast(@alignCast(self.fb)));
        for (0..pixels) |_| {
            screen[0] = RGBA8888.Black.b;
            screen[1] = RGBA8888.Black.g;
            screen[2] = RGBA8888.Black.r;
            screen += 3;
        }
    }
}

pub fn logLine(self: *Self, str: []const u8) void {
    std.mem.copyForwards(u8, debugstr[self.charsX..], debugstr[0 .. self.charsX * (self.charsY - 1)]);
    @memset(debugstr[(self.charsX * (self.charsY - 1))..], 0);
    @memcpy(debugstr[(self.charsX * (self.charsY - 1))..][0..str.len], str);

    if (!self.locked) {
        const palette = Palette.compute(DefaultColor, RGBA8888.Black);
        var pos_y = self.fb_height - DefaultMargins.bottom - Font.Height;
        var str_draw = @as([*:0]u8, @ptrCast(&debugstr)) + (self.charsX * (self.charsY - 1));
        while (@intFromPtr(str_draw) >= @intFromPtr(&debugstr)) : (str_draw -= self.charsX) {
            self.drawTextLine(std.mem.sliceTo(str_draw, 0), DefaultMargins.left, pos_y, palette);
            pos_y -= Font.Height;
        }
    }
}

pub fn zigLogFn(
    comptime message_level: std.log.Level,
    comptime scope: @TypeOf(.enum_literal),
    comptime format: []const u8,
    args: anytype,
) void {
    const level_char: u8 = switch (message_level) {
        .err => 'E',
        .warn => 'W',
        .info => 'I',
        .debug => 'D',
    };
    var buf: [256]u8 = undefined;
    const formatted = std.fmt.bufPrint(&buf, "{c} {s} " ++ format, .{ level_char, @tagName(scope) } ++ args) catch &[_]u8{};
    if (!std.Target.arm.featureSetHas(builtin.cpu.model.features, .has_v6)) {
        default.?.logLine(formatted);
    }
}

pub var default: ?*Self = null;
