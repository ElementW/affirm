// SPDX-License-Identifier: EUPL-1.2
const loadFont = @import("Font.zig").loadFont;

pub const afTerm = loadFont(@embedFile("afTerm.sfd")) catch unreachable;
