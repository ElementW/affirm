// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afk = @import("afk");
const afu = @import("afu");

fn FontType(comptime width: u6, comptime height: u6) type {
    return struct {
        pub const BitDepth = 2;
        pub const Width = width;
        pub const Height = height;
        pub const Characters = 256;
        pub const PixelType = @Type(.{ .Int = .{ .signedness = .unsigned, .bits = BitDepth } });
        pub const LineType = @Type(.{ .Int = .{ .signedness = .unsigned, .bits = Width * BitDepth } });

        data: [Characters][Height]LineType = undefined,
    };
}

pub const Font = FontType(6, 8);
pub fn loadFont(sfd: []const u8) !Font {
    @setEvalBranchQuota(1_000_000);
    var out_font = Font{};
    var fis = std.io.fixedBufferStream(sfd);
    const reader = fis.reader();
    var buf: [256]u8 = undefined;
    var char: ?struct {
        index: u8,
        xmin: u8,
        xmax: u8,
        ymin: u8,
        ymax: u8,
        bytes_per_line: u8,
        a85: std.BoundedArray(u8, 256),
        pub fn finalize(self: *const @This(), font: *Font) void {
            for (0..Font.Height) |y| {
                font.data[self.index][y] = 0;
            }
            if (self.a85.len == 0) {
                return;
            }
            var bytes_buf: [256]u8 = undefined;
            const bytes = afu.encoding.base85.decode(self.a85.slice(), &bytes_buf);
            for (0.., Font.Height - self.ymax - 1..Font.Height - self.ymin) |dy, y| {
                var line: Font.LineType = 0;
                for (0.., self.xmin..self.xmax + 1) |dx, x| {
                    line |= @as(Font.LineType, bytes[dy * self.bytes_per_line + dx]) << @truncate(x * Font.BitDepth);
                }
                font.data[self.index][y] = line;
            }
        }
    } = null;
    while (try reader.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        const data = if (std.mem.lastIndexOf(u8, line, ": ")) |p| line[p + 2 ..] else line[0..0];
        if (std.mem.startsWith(u8, line, "BeginChars: ")) {
            var split = std.mem.split(u8, data, " ");
            std.debug.assert(try std.fmt.parseUnsigned(u16, split.next() orelse unreachable, 0) == 256);
            std.debug.assert(try std.fmt.parseUnsigned(u16, split.next() orelse unreachable, 0) == 256);
        } else if (std.mem.startsWith(u8, line, "BitmapFont: ")) {
            var split = std.mem.split(u8, data, " ");
            const height = try std.fmt.parseUnsigned(u8, split.next() orelse unreachable, 0);
            for (0..3) |_| {
                _ = split.next() orelse unreachable;
            }
            const bit_depth = try std.fmt.parseUnsigned(u8, split.next() orelse unreachable, 0);
            std.debug.assert(height == Font.Height);
            std.debug.assert(bit_depth == Font.BitDepth);
        } else if (std.mem.startsWith(u8, line, "BDFChar: ")) {
            var split = std.mem.split(u8, data, " ");
            _ = split.next(); // Skip orig_pos
            const index = try std.fmt.parseUnsigned(u8, split.next() orelse unreachable, 0);
            std.debug.assert(try std.fmt.parseUnsigned(u8, split.next() orelse unreachable, 0) == Font.Width);
            const xmin = try std.fmt.parseUnsigned(u8, split.next() orelse unreachable, 0);
            const xmax = try std.fmt.parseUnsigned(u8, split.next() orelse unreachable, 0);
            const ymin = try std.fmt.parseUnsigned(u8, split.next() orelse unreachable, 0);
            const ymax = try std.fmt.parseUnsigned(u8, split.next() orelse unreachable, 0);
            if (char) |*c| {
                c.finalize(&out_font);
            }
            char = .{
                .index = index,
                .xmin = xmin,
                .xmax = xmax,
                .ymin = ymin,
                .ymax = ymax,
                .bytes_per_line = xmax - xmin + 1,
                .a85 = try std.BoundedArray(u8, 256).init(0),
            };
            continue;
        } else if (std.mem.startsWith(u8, line, "EndBitmapFont")) {
            if (char) |*c| {
                c.finalize(&out_font);
            }
            char = null;
        }
        if (char) |*c| {
            try c.a85.appendSlice(line);
        }
    }
    return out_font;
}
