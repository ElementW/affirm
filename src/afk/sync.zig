// SPDX-License-Identifier: EUPL-1.2
const afk = @import("afk.zig");

pub const CriticalSection = afk.arch.target.impl.sync.CriticalSection;
pub const Mutex = @import("sync/Mutex.zig");
