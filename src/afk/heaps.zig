// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afk = @import("afk.zig");

pub fn RcHeap(comptime T: type) type {
    return struct {
        const Self = @This();
        pub const Item = Rc(T);

        pool: std.heap.MemoryPool(Item),

        pub fn init(allocator: std.mem.Allocator) Self {
            return .{
                .pool = std.heap.MemoryPool(Item).init(allocator),
            };
        }

        pub fn create(self: *Self) !Ref(T) {
            const item = try self.pool.create();
            item.init();
            return item.firstRef();
        }

        fn markForFree(item: *Item) void {
            // TODO
            _ = item;
        }
    };
}

pub const HeapName = enum {
    pages,
    memory_areas,
    processes,
    threads,
};

pub const Heaps = struct {
    pages: RcHeap(afk.mm.Page),
    memory_areas: RcHeap(afk.mm.Page),

    processes: RcHeap(afk.pm.Process),
    threads: RcHeap(afk.pm.Thread),
};

comptime {
    for (std.meta.fields(HeapName)) |heap| {
        if (!@hasField(Heaps, heap.name)) {
            @compileError(@typeName(Heaps) ++ " lacks `" ++ heap.name ++ "`");
        }
    }
}

pub fn Rc(comptime T: type) type {
    const obj_info = T.ObjectInfo;
    const heap = obj_info.heap;
    return struct {
        const Self = @This();
        pub const Item = T;
        pub const Heap = heap;

        count: std.atomic.Value(i32),
        value: T,

        pub fn initFree() Self {
            return .{
                .count = std.atomic.Value(i32).init(0),
                .value = undefined,
            };
        }

        fn firstRef(self: *Self) Ref(T) {
            if (self.count.fetchAdd(1, .acquire) != 0) {
                @panic("Tried to get first ref on already referenced " ++ @typeName(T));
            }
            return .{ .rc = self };
        }

        fn increment(self: *Self) void {
            switch (self.count.fetchAdd(1, .acquire)) {
                -1 => @panic("Tried to increment reference count on unreferenced " ++ @typeName(T)),
                0 => @panic("Tried to increment reference count on free " ++ @typeName(T)),
                else => {},
            }
        }

        fn decrement(self: *Self) void {
            switch (self.count.fetchSub(1, .release)) {
                -1 => @panic("Tried to decrement reference count on unreferenced " ++ @typeName(T)),
                0 => @panic("Tried to decrement reference count on free " ++ @typeName(T)),
                1 => heap.markForFree(self),
                else => {},
            }
        }
    };
}

pub fn Ref(comptime T: type) type {
    if (!@hasField(T, "refcount")) {
        @compileError(@typeName(T) ++ " isn't refcounted");
    }
    return struct {
        // TODO pinned struct
        const Self = @This();

        rc: ?*Rc(T),

        pub fn isEmpty(self: *Self) bool {
            return !self.rc;
        }

        /// Get the value pointed to by this reference.
        /// If the reference is empty, panics.
        pub fn get(self: *Self) *T {
            return if (self.rc) |r| &r.value else @panic("Tried to get() an empty " ++ @typeName(Self));
        }

        /// Get the value pointed to by this reference.
        /// If the reference is empty, returns `null`.
        pub fn getOptional(self: *Self) ?*T {
            return if (self.rc) |r| &r.value else null;
        }

        /// Clone this reference into a new one.
        /// Increments the reference count of the target object by one.
        pub fn clone(self: *const Self) Self {
            if (self.rc) |r| r.increment();
            return self;
        }

        /// Release this reference.
        /// Decrements the reference count of the target object by one.
        pub fn release(self: *Self) void {
            if (self.rc) |r| r.decrement();
            self.rc = null;
        }
    };
}

test Ref {
    const T = struct {
        const CountedObject = struct {
            pub const ObjectInfo = .{
                .heap = &ThisTestHeaps.counted_objs,
            };
            data: u32,
        };
        const TestHeaps = struct {
            counted_objs: RcHeap(CountedObject),
        };
        var ThisTestHeaps: TestHeaps = .{
            .counted_objs = RcHeap(CountedObject).init(std.testing.allocator),
        };
    };
    const cobj1 = try T.ThisTestHeaps.counted_objs.create();
    std.testing.expect(cobj1.getOptional() != null);
}
