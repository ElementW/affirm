// SPDX-License-Identifier: EUPL-1.2
//const builtin = @import("builtin");
const build_options = @import("build_options");

pub const has_mmu: bool = true;
