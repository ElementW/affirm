// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");

pub const MaxCores = 32;

pub const CodeSet = @import("pm/CodeSet.zig");
pub const CoreMask = std.bit_set.StaticBitSet(MaxCores);
pub const Process = @import("pm/Process.zig");
pub const ResourceLimits = @import("pm/ResourceLimits.zig");
pub const Thread = @import("pm/Thread.zig");
