// SPDX-License-Identifier: EUPL-1.2
const MemoryArea = @This();

name: []const u8,
start: *anyopaque,
length: usize,

pub fn init(name: []const u8, start: usize, length: usize) MemoryArea {
    return .{
        .name = name,
        .start = @ptrFromInt(start),
        .length = length,
    };
}
