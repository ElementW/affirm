// SPDX-License-Identifier: EUPL-1.2

// Physical
// Fr: Frame
// St: Stripe
// Zb: Zebra
// ...-----------------------------------------------------------------------...
//                                    Area #1
// +---------------------------------------------------------------------------+
// |                                   Zb #1                                   |
// |          :          +---------+                                           |
// | St #2 *1 | St #1 *1 |         |                  St #0 *3                 |
// |          |          |   ...   |          :                     :          |
// | 4K Fr #4 | 4K Fr #3 |         | 4K Fr #0 |       8K Fr #1      | 4K Fr #2 |
// +----------+----------+         +----------+---------------------+----------+
