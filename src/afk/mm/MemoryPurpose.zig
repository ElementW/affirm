// SPDX-License-Identifier: EUPL-1.2
pub const MemoryPurpose = enum(u8) {
    general = 0x00,
    buffer,
    scratch,
    ipc,
    dma,
    process,
};
