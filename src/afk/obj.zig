// SPDX-License-Identifier: EUPL-1.2
pub const ObjectType = enum {
    address_arbiter,
    event,
    memory_block,
    mutex,
    semaphore,
    thread,
    timer,
};
