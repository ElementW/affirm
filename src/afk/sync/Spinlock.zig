// SPDX-License-Identifier: EUPL-1.2
const Self = @This();

state: bool,

pub fn locked(self: *Self) callconv(.Inline) bool {
    return @atomicLoad(bool, &self.state, .Unordered);
}

pub fn lock(self: *Self) callconv(.Inline) void {
    while (@cmpxchgWeak(bool, &self.state, false, true, .Unordered, .Unordered) != null) {}
}

pub fn tryLock(self: *Self) callconv(.Inline) bool {
    const wasLocked = @atomicRmw(bool, &self.state, .Xchg, true, .Unordered);
    return !wasLocked;
}
