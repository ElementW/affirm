// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afk = @import("../afk.zig");

count: std.atomic.Value(u32),
owner: extern union {
    process: *afk.pm.Process,
    thread: *afk.pm.Thread,
},
