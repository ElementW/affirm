// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afu = @import("afu");

pub const MemoryArea = @import("mm/MemoryArea.zig");
pub const PhysicalAddress = enum(usize) {
    _,

    pub fn format(self: PhysicalAddress, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = fmt;
        _ = options;
        std.fmt.format(writer, if (@bitSizeOf(usize) == 32) "0x{X:0>8}@phys" else "0x{X:0>16}@phys", .{@intFromEnum(self)});
    }
};
pub const PA = PhysicalAddress;

pub const Errors = error{
    NotEnoughMemory,
    NoContiguousMemoryAvailable,
    OutsideAllocatorRange,
    BadFreePointer,
    CorruptCanary,
};
