// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const builtin = @import("builtin");

pub const arch = @import("arch.zig");
pub const debug = @import("debug.zig");
pub const chrono = @import("chrono.zig");
pub const heaps = @import("heaps.zig");
pub const mm = @import("mm.zig");
pub const pm = @import("pm.zig");
pub const sched = @import("sched.zig");
pub const sync = @import("sync.zig");
pub const vm = @import("vm.zig");

pub const Kernel = @import("Kernel.zig");

pub const std_options = std.Options{
    .logFn = zigLogFn,
};

pub const panic = std.debug.FullPanic(@import("panic.zig").panic);

pub const os = struct {
    pub const debug = struct {};
};

fn zigLogFn(
    comptime message_level: std.log.Level,
    comptime scope: @TypeOf(.enum_literal),
    comptime format: []const u8,
    args: anytype,
) void {
    const prefix: []const u8 = comptime blk: {
        const level_char: u8 = switch (message_level) {
            .err => 'E',
            .warn => 'W',
            .info => 'I',
            .debug => 'D',
        };
        break :blk std.fmt.comptimePrint("{c} {s}: ", .{ level_char, @tagName(scope) });
    };
    var buf: [256]u8 = undefined;
    const formatted = std.fmt.bufPrint(&buf, prefix ++ format ++ "\n", args) catch "! Failed to format log message";
    _ = arch.target.default_output.writer().write(formatted) catch @panic("formatted");
}

// When Zig issue #20663 "Remove usingnamespace" is implemented, switch over to new mechanism.
// Issue text mentions TigerBeetle's `flags.zig` defines main conditionally, and unlike other
// root-based configs like `os`, cannot be currently disabled by e.g. switching it to void.
// pub const main = if (@hasDecl(arch.target, "main")) arch.target.main else {};
pub usingnamespace if (@hasDecl(arch.target, "main")) struct {
    pub const main = arch.target.main;
} else struct {};

comptime {
    arch.target.referenceSymbols();
}
