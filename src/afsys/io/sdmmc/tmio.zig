// SPDX-License-Identifier: EUPL-1.2
const std = @import("std");
const afu = @import("afu");

const assert = std.debug.assert;
const Register = afu.hw.Register;
const v = afu.hw.makeVolatile;

const Command = afu.io.sdmmc.protocol.Command;
const Commands = afu.io.sdmmc.protocol.Commands;
const SDError = afu.io.sdmmc.SDError;

const log = std.log.scoped(.tmio);

/// See Linux ``drivers/mmc/host/tmio_mmc.h`` and GBATEK "SD_IRQ_STATUS0-1"
const IRQ = packed struct(u32) {
    response_end: bool,
    _1: u1,
    data_end: bool,
    card_remove: bool,
    card_insert: bool,
    sig_state: bool,
    _2: u1,
    write_protect: bool,
    card_remove_a: bool,
    card_insert_a: bool,
    sig_state_a: bool,
    _3: u5,
    cmd_index_error: bool,
    crc_fail: bool,
    stop_bit_error: bool,
    data_timeout: bool,
    rx_overflow: bool,
    tx_underrun: bool,
    response_timeout: bool,
    _4: u1,
    rx_ready: bool,
    tx_request: bool,
    _5: u3,
    ready: bool,
    busy: bool,
    illegal_access: bool,
};

/// See GBATEK "SD_ERROR_DETAIL_STATUS0-1"
const ErrorDetail = packed struct(u32) {
    _1: u9,
    crc_status_busy_timeout: bool,
    crc_status_timeout: bool,
    data_start_bit_timeout: bool,
    _2: u2,
    resp_timeout_ai_cmd: bool,
    resp_timeout_non_ai_cmd: bool,
    _3: u4,
    crc_error_write_crc: bool,
    crc_error_read_data: bool,
    crc_error_response_ai_cmd: bool,
    crc_error_response_non_ai_cmd: bool,
    _4: u2,
    end_bit_error_write_crc: bool,
    end_bit_error_read_data: bool,
    end_bit_error_response_ai_cmd: bool,
    end_bit_error_response_non_ai_cmd: bool,
    bad_cmd_index_ai_cmd: bool,
    bad_cmd_index_non_ai_cmd: bool,
};

const DataControl = packed struct(u16) {
    _1: u1,
    enable_data32_mode: bool,
    _2: u3,
    unk_bit5: bool,
    _3: u10,
};
const DataControl32 = packed struct(u16) {
    _1: u1,
    enable_data32_mode: bool,
    _2: u6,
    rx32_ready: bool,
    tx32_request: bool,
    clear_fifo: bool,
    rx32_ready_irq_enable: bool,
    tx32_request_irq_enable: bool,
    _3: u3,
};

const DataMode = enum(u1) {
    Data16 = 0,
    Data32 = 1,
};

pub const Controller = struct {
    const Self = @This();
    const CardOption = packed struct(u16) {
        const BusWidth = enum(u1) {
            bit4 = 0,
            bit1 = 1,
        };

        _1: u4 = 0,
        busy_timeout: u4,
        unknown: u1,
        _2: u6 = 0,
        bus_width: BusWidth,
    };
    const ClkControl = packed struct(u16) {
        const Divider = enum(u8) {
            // Base clock is 67.02 MHz
            div2 = 0, // 33.51 MHz
            div4 = 1 << 0, // 16.76 MHz
            div8 = 1 << 1, //  8.38 MHz
            div16 = 1 << 2, //  4.19 MHz
            div32 = 1 << 3, //  2.09 MHz
            div64 = 1 << 4, //  1.05 MHz
            div128 = 1 << 5, //   524 kHz
            div256 = 1 << 6, //   262 kHz
            div512 = 1 << 7, //   131 kHz
        };

        divider: Divider,
        pin_enable: bool,
        freeze: bool,
        _: u6 = 0,
    };
    const Regs = extern struct {
        send_command: u16, // 0x0
        port_select: u16, // 0x2
        command_arg: u32, // 0x4
        stop_internal_action: u16, // 0x8
        blk_count: u16, // 0xA
        response: [8]u16, // 0xC
        irq_status: IRQ, // 0x1C
        irq_mask: IRQ, // 0x20
        clk_control: ClkControl, // 0x24
        blk_length: u16, // 0x26
        card_option: CardOption, // 0x28
        _1: u16, // 0x2A
        error_detail: ErrorDetail, // 0x2C
        data_fifo: u16, // 0x30
        _2: u16, // 0x32
        transaction_control: u16, // 0x34
        sdio_irq_status: u16, // 0x36
        sdio_irq_mask: u16, // 0x38
        _3: [0x9E]u8,
        data_control: DataControl, // 0xD8
        _4: [0x6]u8,
        reset: u16, // 0xE0
        version: u16, // 0xE2
        _5: [0x14]u8,
        ext_irq_status: u32, // 0xF8
        ext_irq_mask: u32, // 0xFC

        data_control32: DataControl32, // 0x100
        _6: u16,
        blk_length32: u16, // 0x104
        _7: u16,
        blk_count32: u16, // 0x108
        _8: u16,
        data_fifo32: u16, // 0x10C
    };
    comptime {
        assert(@offsetOf(Regs, "data_fifo32") == 0x10C);
    }

    regs: *Regs,

    pub fn at(addr: usize) Self {
        return Self{ .regs = @as(*Regs, @ptrFromInt(addr)) };
    }

    fn irqStatus(self: Self) Register(IRQ, .R) {
        return Register(IRQ, .R).of(&self.regs.irq_status);
    }
    fn irqMask(self: Self) Register(IRQ, .R) {
        return Register(IRQ, .R).of(&self.regs.irq_mask);
    }

    fn dataControl(self: Self) Register(DataControl, .RW) {
        return Register(DataControl, .RW).of(&self.regs.data_control);
    }
    fn dataControl32(self: Self) Register(DataControl32, .RW) {
        return Register(DataControl32, .RW).of(&self.regs.data_control32);
    }

    fn errorDetail(self: Self) Register(ErrorDetail, .RW) {
        return Register(ErrorDetail, .RW).of(&self.regs.error_detail);
    }

    fn clkControl(self: Self) Register(ClkControl, .RW) {
        return Register(ClkControl, .RW).of(&self.regs.clk_control);
    }

    fn selectPort(self: Self, index: u8) void {
        Register(u8, .RW).of(&self.regs.port_select).write(index);
    }

    pub fn setClockDivider(self: Self, div: ClkControl.Divider) void {
        self.clkControl().write(ClkControl{
            .divider = div,
            .pin_enable = true,
        });
    }

    pub fn setBusWidth(self: Self, width: CardOption.BusWidth) void {
        Register(CardOption, .RW).of(&self.regs.card_option).changeFields(.{
            .bus_width = width,
        });
    }

    pub fn setDataMode(self: Self, mode: DataMode) void {
        if (mode == .Data16) {
            self.dataControl32().changeFields(.{
                .enable_data32_mode = false,
            });
            self.dataControl().changeFields(.{
                .enable_data32_mode = false,
                .unkBit5 = false,
            });
        } else {
            self.dataControl32().changeFields(.{
                .enable_data32_mode = true,
                .clearFIFO32 = true,
            });
            self.dataControl().changeFields(.{
                // nwm does `& ~0x22 | 2`
                .enable_data32_mode = true,
                .unkBit5 = false,
            });
        }
    }

    pub fn reset(self: Self) void {
        const card_option = Register(CardOption, .RW).of(&self.regs.card_option);
        v(&self.regs.reset).* = self.regs.reset & ~@as(u16, 0b11); // Trigger SDIO controller & card reset
        v(&self.regs.reset).* = self.regs.reset | 0b11;
        // Enable auto data end, clear bit 0 (?)
        v(&self.regs.stop_internal_action).* = 0x100;
        // Log::d(Tag, "cardopt %04X", alias_cast<uint16>(opt));
        card_option.write(CardOption{
            .busy_timeout = 0xD,
            .unknown = 0,
            .bus_width = .bit1,
        });
        // Log::d(Tag, "clk %04X", alias_cast<uint16>(clk));
        self.clkControl().write(ClkControl{
            .divider = .div256,
            .pin_enable = false,
            .freeze = false,
        });
        card_option.changeFields(.{ .bus_width = .bit4 });
        card_option.changeFields(.{ .unknown = 1 });
        card_option.changeFields(.{ .unknown = 0 });
        // Log::d(Tag, "clk %04X", alias_cast<uint16>(clk));
        self.clkControl().write(ClkControl{
            .divider = .div4,
            .pin_enable = true,
            .freeze = false,
        });
    }

    fn sendCommandInternal(self: Self, cmd: Command, arg: u32, resp: ?*Command.Response, rx_data: ?[]align(4) u8, tx_data: ?[]align(4) const u8) SDError!void {
        var do_rx: bool = undefined;
        var do_tx: bool = undefined;
        if (cmd.controller_cmd.index == Commands.IORWExtended.Info.controller_cmd.index) {
            // IO_RW_EXTENDED is the only command with an argument bit determining
            // the data transfer direction.
            const rw_arg = @as(Commands.IORWExtended, @bitCast(arg));
            do_rx = (rw_arg.direction == .read);
            do_tx = !do_rx;
        } else {
            do_rx = cmd.data_transfer & .rx;
            do_tx = cmd.data_transfer & .tx;
        }

        const want_data_end = do_rx || do_tx;

        while (irqStatus().read().busy) {}
        self.irqMask().write(0); // Enable all IRQs
        self.irqStatus().write(0); // Clear all IRQ flags
        self.dataControl32().changeFields(.{
            .tx32_request = false,
            .rx32_ready = false,
            .clear_fifo32 = true,
        });
        v(&self.regs.command_arg).* = arg;
        const resp_type = switch (cmd.controller_cmd.response_type) {
            .None => "x",
            .R1 => "1/5/6/7",
            .R1B => "1B",
            .R2 => "2",
            .R3 => "3/4",
            else => "?",
        };
        log.debug("CMD{d}/{s} A:{X:0>8} R{s} D:{c}{c}", .{
            cmd.controller_cmd.index,
            cmd.name,
            arg,
            resp_type,
            if (cmd.controller_cmd.data_present) (if (cmd.controller_cmd.transfer_read) 'R' else 'W') else '-',
            if (cmd.controller_cmd.transfer_multi) '*' else '1',
        });
        v(&self.regs.send_command).* = @as(u16, @bitCast(cmd.controller_cmd));

        const blk_size = v(&self.regs.blk_length32).*;
        if (blk_size & 0b11 != 0) {
            log.err("Nonaligned transfer length {d}", .{blk_size});
            return SDError.NonalignedTransferLength;
        }
        var rx_cursor: usize = 0;
        var tx_cursor: usize = 0;
        while (true) {
            const s = self.irqStatus().read();
            const dctl32 = self.dataControl32().read();
            log.debug("s={X:0>8} dctl32={X:0>4}", .{ @as(u32, @bitCast(s)), @as(u16, @bitCast(dctl32)) });
            if (s.illegal_access or s.response_timeout or s.tx_underrun or s.rx_overflow or
                s.data_timeout or s.stop_bit_error or s.crc_fail or s.cmd_index_error)
            {
                log.err("Transfer error {s}{s}{s}{s}{s}{s}{s}{s}", .{
                    if (s.illegal_access) "IllA " else "",
                    if (s.response_timeout) "RpTO " else "",
                    if (s.tx_underrun) "TxUn " else "",
                    if (s.rx_overflow) "RxOv " else "",
                    if (s.data_timeout) "DtTO " else "",
                    if (s.stop_bit_error) "StBE " else "",
                    if (s.crc_fail) "CrcF " else "",
                    if (s.cmd_index_error) "IdxE " else "",
                });
                const errors = [32]?SDError{
                    SDError.CmdIndexError,
                    SDError.CRCFail,
                    SDError.StopBitError,
                    SDError.DataTimeout,
                    SDError.RXOverflow,
                    SDError.TXUnderrun,
                    SDError.ResponseTimeout,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    SDError.IllegalAccess,
                };
                if (errors[@ctz(@as(u32, @bitCast(s)))]) |err| {
                    return err;
                }
            }
            if (!s.busy) {
                if (!s.response_end) {
                    return SDError.EarlyResponseEnd;
                }
                if (s.data_end == want_data_end) {
                    break;
                }
            }
            if (dctl32.rx32_ready) {
                log.debug("rx_ready", .{});
                if (rx_data) |buf| {
                    self.irqStatus().changeFields(.{ .rx_ready = false });
                    const end = rx_cursor + blk_size;
                    while (rx_cursor < end) : (rx_cursor += 4) {
                        @as(*const u32, @ptrCast(&buf[rx_cursor])).* = v(&self.regs.data_fifo32).*;
                    }
                    self.dataControl32().changeFields(.{ .rx32_ready_irq_enable = false });
                }
            } else if (dctl32.tx32_request) {
                log.debug("tx_request", .{});
                if (tx_data) |buf| {
                    self.irqStatus().changeFields(.{ .tx_request = false });
                    const end = tx_cursor + blk_size;
                    while (tx_cursor < end) : (tx_cursor += 4) {
                        v(&self.regs.data_fifo32).* = @as(*u32, @ptrCast(&buf[tx_cursor])).*;
                    }
                    self.dataControl32().changeFields(.{ .tx32_request_irq_enable = false });
                }
            }
        }
        if (resp) |r| {
            const response = &self.regs.response;
            switch (cmd.controller_cmd.response_type) {
                .None => {},
                .R1, .R1B, .R3 => {
                    r.r1.uint32 = @byteSwap(response[0] | (response[1] << 16));
                },
                .R2 => {
                    //r.r2_32[0] = response[6] | (response[7] << 16);
                    //r.r2_32[1] = response[4] | (response[5] << 16);
                    //r.r2_32[2] = response[2] | (response[3] << 16);
                    //r.r2_32[3] = response[0] | (response[1] << 16);

                    //r.r2_32[0] = byteSwap32(r.r2_32[0] << 8 | r.r2_32[1] >> 24);
                    //r.r2_32[1] = byteSwap32(r.r2_32[1] << 8 | r.r2_32[2] >> 24);
                    //r.r2_32[2] = byteSwap32(r.r2_32[2] << 8 | r.r2_32[3] >> 24);
                    //r.r2_32[3] = byteSwap32(r.r2_32[3] << 8);

                    r.r2.uint32[0] = @byteSwap((response[7] << 24) | (response[6] << 8) | (response[5] >> 8));
                    r.r2.uint32[1] = @byteSwap((response[5] << 24) | (response[4] << 8) | (response[3] >> 8));
                    r.r2.uint32[2] = @byteSwap((response[3] << 24) | (response[2] << 8) | (response[1] >> 8));
                    r.r2.uint32[3] = @byteSwap((response[1] << 24) | (response[0] << 8));
                },
            }
        }
    }
};
