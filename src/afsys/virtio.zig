// SPDX-License-Identifier: EUPL-1.2

/// 5 Device Types
pub const DeviceType = enum(u32) {
    invalid = 0,
    network = 1,
    block = 2,
    console = 3,
    entropy_source = 4,
    memory_balloon_traditional = 5,
    io_memory = 6,
    rpmsg = 7,
    scsi_host = 8,
    @"9p_transport" = 9,
    mac80211 = 10,
    rproc_serial = 11,
    virtio_caif = 12,
    memory_balloon = 13,
    gpu = 16,
    timer_clock = 17,
    input = 18,
    socket = 19,
    crypto = 20,
    signal_distribution_module = 21,
    pstore = 22,
    iommu = 23,
    memory = 24,
    sound = 25,
    filesystem = 26,
    pmem = 27,
    rpmb = 28,
    mac80211_hwsim = 29,
    video_encoder = 30,
    video_decoder = 31,
    scmi = 32,
    nitro_secure_codule = 33,
    i2c = 34,
    watchdog = 35,
    can = 36,
    parameter_server = 38,
    audio_policy = 39,
    bluetooth = 40,
    gpio = 41,
    rdma = 42,
    camera = 43,
    ism = 44,
    spi_master = 45,
};

// Table 4.1: MMIO Device Register Layout
pub const MMIODeviceRegisters = extern struct {
    magic: u32,
    version: u32,
    device_id: DeviceType,
    vendor_id: u32,
    device_features: u32,
    device_features_sel: u32,
    driver_features: u32,
    driver_features_sel: u32,
    // TODO ...
};

/// 5.2 Block Device
pub const Block = struct {
    pub const FeatureBits = packed struct(u32) {
        _1: u1 = 0,
        /// Maximum size of any single segment is in `size_max`.
        size_max: bool,
        /// Maximum number of segments in a request is in `seg_max`.
        seg_max: bool,
        _2: u1 = 0,
        /// Disk-style geometry specified in `geometry`.
        geometry: bool,
        /// Device is read-only.
        ro: bool,
        /// Block size of disk is in `blk_size`.
        blk_size: bool,
        _3: u2 = 0,
        /// Cache flush command support.
        flush: bool,
        /// Device exports information on optimal I/O alignment.
        topology: bool,
        /// Device can toggle its cache between writeback and writethrough modes.
        config_wce: bool,
        /// Device supports multiqueue.
        mq: bool,
        /// Device can support discard command, maximum discard sectors size in
        /// `max_discard_sectors` and maximum discard segment number in
        /// `max_discard_seg`.
        discard: bool,
        /// Device can support write zeroes command, maximum write zeroes sectors
        /// size in `max_write_zeroes_sectors` and maximum write zeroes segment
        /// number in `max_write_zeroes_seg`.
        write_zeroes: bool,
        /// Device supports providing storage lifetime information.
        lifetime: bool,
        /// Device supports secure erase command, maximum erase sectors count in
        /// `max_secure_erase_sectors` and maximum erase segment number in
        /// `max_secure_erase_seg`.
        secure_erase: bool,
        /// Device is a Zoned Block Device
        zoned: bool,
        _4: u15 = 0,
    };
};

/// 5.3 Console Device
pub const Console = struct {
    pub const FeatureBits = packed struct(u32) {
        /// Configuration `cols` and `rows` are valid.
        size: bool,
        /// Device has support for multiple ports; `max_nr_ports` is valid and
        /// control virtqueues will be used.
        multiport: bool,
        /// Device has support for emergency write. Configuration field
        /// `emerg_wr` is valid.
        emerg_write: bool,
        _: u29 = 0,
    };

    pub const DeviceConfiguration = extern struct {
        cols: u16,
        rows: u16,
        max_nr_ports: u32,
        emerg_wr: u32,
    };
};

/// 5.7 GPU Device
pub const GPU = struct {
    pub const FeatureBits = packed struct(u32) {
        virgl: bool,
        edid: bool,
        resource_uuid: bool,
        resource_blob: bool,
        context_init: bool,
        _: u27 = 0,
    };

    pub const DeviceConfiguration = extern struct {
        events_read: u32,
        events_clear: u32,
        /// Maximum number of scanouts supported by the device. Value in [1, 16].
        num_scanouts: u32,
        /// Maximum number of capability sets supported by the device. Value in [0, inf].
        num_capsets: u32,
    };

    pub const Events = packed struct(u32) {
        /// Display configuration has changed
        display: bool,
        _: u21,
    };
};
