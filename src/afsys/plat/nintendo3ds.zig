// SPDX-License-Identifier: EUPL-1.2
pub const gfx = @import("nintendo3ds/gfx.zig");

pub const HID = @import("nintendo3ds/HID.zig");
pub const I2C = @import("nintendo3ds/I2C.zig");
pub const LCD = @import("nintendo3ds/LCD.zig");
pub const MCU = @import("nintendo3ds/MCU.zig");
pub const PXI = @import("nintendo3ds/PXI.zig");
pub const RNG = @import("nintendo3ds/RNG.zig");
