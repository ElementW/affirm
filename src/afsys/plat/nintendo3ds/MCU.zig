// SPDX-License-Identifier: EUPL-1.2
const I2C = @import("I2C.zig");

// https://www.3dbrew.org/wiki/I2C_Registers#Device_3
const Interrupts = packed struct(u32) {
    power_button_press: bool = false, // 0
    power_button_hold: bool = false, // 1
    home_button_press: bool = false, // 2
    home_button_release: bool = false, // 3
    wifi_button: bool = false, // 4
    shell_close: bool = false, // 5
    shell_open: bool = false, // 6
    watchdog: bool = false, // 7
    charger_removed: bool = false, // 8
    charger_plugged: bool = false, // 9
    rtc_alarm: bool = false, // 10
    _1: u1 = 0, // 11
    hid_update: bool = false, // 12
    _2: u1 = 0, // 13
    charge_stop: bool = false, // 14
    charge_start: bool = false, // 15
    _3: u6 = 0, // 16-21
    volume_slider_change: bool = false, // 22
    _4: u6 = 0, // 23-28
    battery_percentage_change: bool = false, // 29
    _5: u2 = 0, // 30-31
};

fn read(reg: u8) I2C.Errors!u8 {
    var val: [1]u8 = undefined;
    try I2C.readReg(.mcu, reg, &val);
    return val[0];
}
fn write(reg: u8, value: u8) I2C.Errors!void {
    return I2C.writeReg(.mcu, reg, @as(*const [1]u8, @ptrCast(&value)));
}

pub const Version = packed struct(u16) {
    high: u8,
    low: u8,
};

pub fn version() Version {
    var ver: [2]u8 = undefined;
    I2C.readReg(.mcu, 0x00, &ver) catch {
        return Version{ .high = 0, .low = 0 };
    };
    return Version{ .high = ver[0], .low = ver[1] };
}

pub fn raw3DSlider() u8 {
    return read(0x08) catch 0;
}

pub fn volumeSlider() u8 {
    return read(0x09) catch 0;
}
pub fn rawVolumeSlider() u8 {
    return read(0x27) catch 0;
}

pub fn batteryPercentage() u8 {
    return read(0x0B) catch 0;
}

pub fn voltage() u8 {
    return read(0x0D) catch 0;
}

pub fn setLCDBacklight(top: bool, bottom: bool) I2C.Errors!void {
    var val: u8 = 1 << 1; // "Push to LCD" bit
    val |= if (bottom) (1 << 3) else (1 << 2);
    val |= if (top) (1 << 5) else (1 << 4);
    try I2C.writeReg(.mcu, 0x22, &[_]u8{val});
}

pub fn poweroffLCD() I2C.Errors!void {
    try I2C.writeReg(.mcu, 0x22, &[_]u8{1});
}

pub fn powerLEDsBrightness() u8 {
    return read(0x28) catch 0;
}
pub fn setPowerLEDsBrightness(brightness: u8) I2C.Errors!void {
    try I2C.writeReg(.mcu, 0x28, &[_]u8{brightness});
}

// TODO Power LED state

pub fn wifiLEDOn() bool {
    return read(0x2A) catch false;
}
pub fn setWifiLEDOn(on: bool) I2C.Errors!void {
    try I2C.writeReg(.mcu, 0x2A, &[_]u8{@as(u8, @intCast(@intFromBool(on)))});
}

pub fn interruptMask() Interrupts {
    var bytes: [4]u8 = undefined;
    I2C.readReg(.mcu, 0x18, &bytes) catch return Interrupts{};
    return @as(Interrupts, @bitCast(~@as(u32, @bitCast(bytes))));
}
pub fn setInterruptMask(mask: Interrupts) I2C.Errors!void {
    const bitmask = @as(u32, @bitCast(mask));
    try I2C.writeReg(.mcu, 0x18, &[_]u8{ @as(u8, @truncate(bitmask >> 8)), @as(u8, @truncate(bitmask)) });
}

pub fn readInterrupts() Interrupts {
    var bytes: [4]u8 = undefined;
    I2C.readReg(.mcu, 0x10, &bytes) catch return Interrupts{};
    return @as(Interrupts, @bitCast(~@as(u32, @bitCast(bytes))));
}

pub fn poweroff() I2C.Errors!void {
    try write(0x20, 1);
}
pub fn reboot() I2C.Errors!void {
    try write(0x20, 1 << 2);
}
