// SPDX-License-Identifier: EUPL-1.2
//! RNG registers namespace.
const afu = @import("afu");
const roreg = afu.hw.roreg;

const Random = roreg(u32, 0x10011000);

pub fn int(comptime T: type) T {
    return Random.as(*const volatile T).*;
}
