// SPDX-License-Identifier: EUPL-1.2
//! https://www.3dbrew.org/wiki/LCD_Registers
//! http://problemkaputt.de/gbatek-3ds-video-lcd-registers.htm
const std = @import("std");
const afu = @import("afu");

const assert = std.debug.assert;
const reg = afu.hw.reg;
const roreg = afu.hw.roreg;
const unkreg = afu.hw.unkreg;

const Base: usize = 0x10202000;
pub const ParallaxControl = reg(packed struct {
    pub const PWM = enum(u2) {
        off = 0,
        pwm = 1,
    };
    pub const Enable = enum(u2) {
        off = 0,
        on = 1,
    };
    pwm: PWM,
    pwm_invert: bool,
    _1: u13 = 0, // SBZ
    enable: Enable,
    enable_invert: bool,
    _2: u13 = 0, // SBZ
    comptime {
        assert(@bitSizeOf(@This()) == 32);
    }
}, Base);
pub const ParallaxPWM = reg(packed struct(u32) {
    duty_off: u16,
    duty_on: u16,
}, Base + 0x4);
pub const Status = roreg(packed struct(u32) {
    _: u32 = 0,
}, Base + 0x8);
pub const ClockDisable = reg(packed struct(u32) {
    top_screen: bool,
    _1: u15 = 0,
    bottom_screen: bool,
    _2: u15 = 0,
}, Base + 0xC);
pub const Reset = reg(packed struct(u32) {
    reset: bool,
    _: u31 = 0,
}, Base + 0x14);

pub const TopScreenConfig = reg(u32, Base + 0x200);
pub const BottomScreenConfig = reg(u32, Base + 0xA00);
pub const unk0x1200 = unkreg(u32, Base + 0x1200);

pub const Config = extern struct {
    pub const FillColor = packed struct(u32) {
        r: u8,
        g: u8,
        b: u8,
        _: u7,
        enable: bool,
    };
    unk0x0: u32,
    fill_color: FillColor,
    unk0x8: u32,
    unk0xC: u32,
    unk0x10: u32,
    unk0x14: u32,
    unk0x18: u32,
    unk0x1C: u32,
    unk0x20: u32,
    unk0x24: u32,
    unk0x28: u32,
    unk0x2C: u32,
    unk0x30: u32,
    unk0x34: u32,
    unk0x38: u32,
    unk0x3C: u32,
    backlight: u32,
    unk0x44: u32,
    unk0x48: u32,
    unk0x4C: u32,
    unk0x50: u32,
    unk0x54: u32,
    unk0x58: u32,
    unk0x5C: u32,
    unk0x60: u32,
    unk0x64: u32,
    unk0x68: u32,
    unk0x6C: u32,
    unk0x70: u32,
    unk0x74: u32,
    unk0x78: u32,
    unk0x7C: u32,
    unk0x80pad: [0x80]u8,
    calibration: [0x100]u8,
};
comptime {
    assert(@offsetOf(Config, "backlight") == 0x40);
}

const DefaultBrightness: u32 = 0x26;

pub fn enableParallaxBarrier(enable: bool) void {
    ParallaxControl.changeFields(.{ .enable = if (enable) ParallaxControl.Value.Enable.on else ParallaxControl.Value.Enable.off });
}

pub fn setBacklight(top: u32, bottom: u32) void {
    TopScreenConfig.as(*volatile Config).backlight = top;
    BottomScreenConfig.as(*volatile Config).backlight = bottom;
}

pub fn init() void {
    const topCfg: *volatile Config = TopScreenConfig.as(*volatile Config);
    const bottomCfg: *volatile Config = BottomScreenConfig.as(*volatile Config);
    Reset.write(1);
    ClockDisable.changeFields(.{
        .top_screen = false,
        .bottom_screen = false,
    });
    setBacklight(DefaultBrightness, DefaultBrightness);
    topCfg.unk0x44 = 0x1023E;
    bottomCfg.unk0x44 = 0x1023E;
    //enableParallaxBarrier(false);
}
