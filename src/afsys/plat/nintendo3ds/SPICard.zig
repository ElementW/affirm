// SPDX-License-Identifier: EUPL-1.2
//! https://www.3dbrew.org/wiki/SPICARD_Registers

pub const Control = packed struct(u32) {
    pub const Baudrate = enum(u3) {
        baud512khz = 0,
        baud1mhz = 1,
        baud2mhz = 2,
        baud4mhz = 3,
        baud8mhz = 4,
        baud16mhz = 4,
    };
    pub const BusMode = enum(u1) {
        single_spi = 0,
        quad_spi = 1,
    };
    pub const TransferMode = enum(u1) {
        read = 0,
        write = 1,
    };
    baudrate: Baudrate,
    _1: u3 = 0,
    device_select: u2 = 0,
    _2: u5 = 0,
    bus_mode: BusMode,
    transfer_mode: TransferMode,
    busy: bool,
    _3: u16 = 0,
};

const roreg = @import("registers/Helpers.zig").roreg;
const reg = @import("registers/Helpers.zig").reg;

const Base: usize = 0x1000D800;
const ControlReg = reg(Control, Base);

const TransferError = error{
    TODO,
};

fn read(buffer: []u8) TransferError!void {
    _ = buffer;
}
fn write(buffer: []const u8) TransferError!void {
    _ = buffer;
}

pub const PeripheralID = struct {
    manufacturer_id: u8,
    devce_id: u16,
};
const FlashCommand = struct {
    const Read: u8 = 0x03;
    const PageProgram: u8 = 0x02;
    const ReadID: u8 = 0x9F;
    const ReadStatus: u8 = 0x05;
    const DeepPowerDown: u8 = 0xB9;
    const ExitDeepPowerDown: u8 = 0xAB;
    const Reset: u8 = 0x99;
    const EnableQuadSPI: u8 = 0x38;
    const DisableQuadSPI: u8 = 0xFF;

    fn readID() TransferError!PeripheralID {
        var id_buf: [3]u8 = undefined;
        try write([]u8{ReadID});
        try read(id_buf);
        return .{
            .manufacturer_id = id_buf[0],
            .devce_id = (@as(u16, id_buf[1]) << 8) | id_buf[2],
        };
    }
};

pub const CTRine = struct {
    // R 00 00 00: Header
    // R 00 01 xx: Descriptor for feature at index #xx
    // R 00 02 xx: Read state of feature at index #xx
    // W 00 02 xx: Write state of feature at index #xx
    // R xx yy zz: Read endpoint yy:zz of feature at index #xx
    // W xx yy zz: Write endpoint yy:zz of feature at index #xx

    pub const Header = extern struct {
        magic: u32,
        protocol_version: u32,
        feature_descriptor_size: u16,
        feature_count: u8,
        feature_state_size: u16,
        _: u16,
    };

    pub const Feature = struct {
        pub const Index = u8;
        pub const Endpoint = struct { u8, u8 };
        pub const Descriptor = extern struct {
            pub const Version = extern struct {
                major: u8,
                minor: u8,
            };
            name: [64]u8,
            version: Version,
            index: Index,
            _: u8,
        };
        pub const State = extern struct {
            enabled: u8,
        };
        index: Index,
        pub fn read(endpoint: Endpoint, buffer: []u8) TransferError!void {
            _ = buffer; // TODO
        }
        pub fn write(endpoint: Endpoint, buffer: []const u8) TransferError!void {
            _ = buffer; // TODO
        }
    };

    pub const Features = struct {
        pub const Serial = struct {
            // R 00 xx: Read channel #xx; 1 byte length followed by N data bytes
            // W 00 xx: Write bytes to channel #xx
            pub const Name = "net.elementw.ctrine.serial";
            feature: Feature,
            pub fn read(buffer: []u8) TransferError![]u8 {
                // TODO
            }
            pub fn write(buffer: []const u8) TransferError!void {
                try self.feature.write([]u8{ 0x00, 0x00 }, buffer);
            }
        };
        pub const LED = struct {
            // W 00 xx: Set led #xx RGB color
            // W 01 xx: Set led #xx mode
            pub const Name = "net.elementw.ctrine.led";
            pub const Mode = enum(u8) {
                Auto = 0,
                Off = 1,
                PWM = 2,
                PWMBlink = 3,
            };
            feature: Feature,
            pub fn auto(self: @This(), led: u8) TransferError!void {
                try self.feature.write([]u8{ 0x00, led }, []u8{ @enumToInt(Mode.Auto) });
            }
            pub fn off(self: @This(), led: u8, brightness: u8) TransferError!void {
                try self.feature.write([]u8{ 0x00, led }, []u8{ @enumToInt(Mode.Off) });
            }
            pub fn on(self: @This(), led: u8, brightness: u8) TransferError!void {
                try self.feature.write([]u8{ 0x00, led }, []u8{ @enumToInt(Mode.PWM), brightness });
            }
            pub fn blink(self: @This(), led: u8, brightness: u8, speed: u8) TransferError!void {
                try self.feature.write([]u8{ 0x00, led }, []u8{ @enumToInt(Mode.PWM), brightness, speed });
            }
        };

    };

    serial: ?Features.Serial = null,
    led: ?Features.LED = null,

    // net.elementw.ctrine.network
    // net.elementw.ctrine.gdb
    // net.elementw.ctrine.screenstream
    // net.elementw.ctrine.remoteinput
    // net.elementw.ctrine.bthci
    // net.elementw.ctrine.usb

    pub fn detect() TransferError!bool {
        const id = try rdid();
        return id.manufacturer_id == 0x74 and id.devce_id == 0xAFC7;
    }

    fn a() TransferError!void {
        
        var buf: [256]u8 = undefined;
        if () {
            try write([]u8{ FlashCommand.Read, 0x00, 0x00, 0x00 });
            try read(buf);
        }
    }
};
