// SPDX-License-Identifier: EUPL-1.2
//! LCD registers namespace.
//! https://www.3dbrew.org/wiki/LCD_Registers
//! http://problemkaputt.de/gbatek-3ds-video-lcd-registers.htm
const assert = @import("std").debug.assert;
const afu = @import("afu");
const reg = afu.hw.reg;
const roreg = afu.hw.roreg;
const unkreg = afu.hw.unkreg;

const Base: usize = 0x10202000;
pub const ParralaxControl = reg(packed struct {
    pub const PWM = enum(u2) {
        Off = 0,
        PWM = 1,
    };
    pub const Enable = enum(u2) {
        Off = 0,
        On = 1,
    };
    pwm: PWM,
    pwm_invert: bool,
    _1: u13 = 0, // SBZ
    enable: Enable,
    enable_invert: bool,
    _2: u13 = 0, // SBZ
    comptime {
        assert(@bitSizeOf(@This()) == 32);
    }
}, Base);
pub const ParralaxPWM = reg(packed struct(u32) {
    duty_off: u16,
    duty_on: u16,
}, Base + 0x4);
pub const Status = roreg(packed struct(u32) {
    _: u32 = 0,
}, Base + 0x8);
pub const ClockDisable = reg(packed struct(u32) {
    top_screen: bool,
    _1: u15 = 0,
    bottom_screen: bool,
    _2: u15 = 0,
}, Base + 0xC);
pub const Reset = reg(packed struct(u32) {
    reset: bool,
    _: u31 = 0,
}, Base + 0x14);

pub const TopScreenConfig = reg(u32, Base + 0x200);
pub const BottomScreenConfig = reg(u32, Base + 0xA00);
pub const unk0x1200 = unkreg(u32, Base + 0x1200);
