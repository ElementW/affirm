// SPDX-License-Identifier: EUPL-1.2
//! See https://www.3dbrew.org/wiki/I2C_Registers
const std = @import("std");
const afu = @import("afu");
const Register = afu.hw.register.Register;
const log = std.log.scoped(.I2C);
const assert = std.debug.assert;

pub const Errors = error{
    SelectDeviceFailed,
    SelectRegisterFailed,
    SelectFailed,
    ReadFailed,
    WriteFailed,
};

pub const Direction = enum(u1) {
    read = 1,
    write = 0,
};

// https://www.3dbrew.org/wiki/I2C_Registers#I2C_CNT
const I2CControl = packed struct(u8) {
    stop: bool,
    start: bool,
    @"error": bool,
    _: u1,
    ack: bool,
    data_direction: Direction,
    enable_irq: bool,
    enable_or_busy: bool,
};
comptime {
    assert(@sizeOf(I2CControl) == @sizeOf(u8));
}

const Bus = extern struct {
    const Self = @This();
    const SelectRetries = 8;
    data: u8,
    control: u8,
    control_ex: u16,
    clock: u16,

    fn getAck(bus: *volatile Self) bool {
        bus.waitBusy();
        return Register(I2CControl, .RW).of(&bus.control).read().ack;
    }

    fn selectDevice(bus: *volatile Self, devAddr: u8) bool {
        bus.waitBusy();
        bus.data = devAddr;
        const control_reg = Register(I2CControl, .RW).of(&bus.control);
        var control = control_reg.read();
        control.enable_or_busy = true;
        control.enable_irq = true;
        control.data_direction = .write;
        control.start = true;
        control_reg.write(control);
        return bus.getAck();
    }

    fn selectRegister(bus: *volatile Self, regAddr: u8) bool {
        bus.waitBusy();
        bus.data = regAddr;
        const control_reg = Register(I2CControl, .RW).of(&bus.control);
        var control = control_reg.read();
        control.enable_or_busy = true;
        control.enable_irq = true;
        control.data_direction = .write;
        control_reg.write(control);
        return bus.getAck();
    }

    pub fn stop(bus: *volatile Self) void {
        bus.waitBusy();
        const control_reg = Register(I2CControl, .RW).of(&bus.control);
        var control = control_reg.read();
        control.enable_or_busy = true;
        control.enable_irq = false;
        control.@"error" = true;
        control.stop = true;
        control_reg.write(control);
    }

    fn selectDeviceRegister(bus: *volatile Self, devAddr: u8, regAddr: u8, dir: Direction) Errors!void {
        retry: for (0..SelectRetries) |tries| {
            if (!bus.selectDevice(devAddr)) {
                log.debug("{d}!selectDevice {X:0>2}:{X:0>2}", .{ tries, devAddr, regAddr });
                bus.stop();
                continue :retry;
            }

            if (!bus.selectRegister(regAddr)) {
                log.debug("{d}!selectRegister {X:0>2}:{X:0>2}", .{ tries, devAddr, regAddr });
                bus.stop();
                continue :retry;
            }

            if (dir == .read and !bus.selectDevice(devAddr | 1)) {
                log.debug("{d}!selectDevice|1 {X:0>2}:{X:0>2}", .{ tries, devAddr, regAddr });
                bus.stop();
                continue :retry;
            }
            return;
        }
        log.err("Select failed {X:0>2}:{X:0>2} ({c})", .{ devAddr, regAddr, @as(u8, if (dir == .read) 'R' else 'W') });
        return Errors.SelectFailed;
    }

    pub inline fn waitBusy(bus: *const volatile Self) void {
        const control_reg = Register(I2CControl, .R).of(&bus.control);
        while (control_reg.read().enable_or_busy) {}
    }

    pub fn readReg(bus: *volatile Self, devAddr: u8, regAddr: u8, out: []u8) Errors!void {
        try selectDeviceRegister(bus, devAddr, regAddr, .read);

        const control_reg = Register(I2CControl, .RW).of(&bus.control);
        var index: usize = 0;
        while (index < out.len - 1) : (index += 1) {
            bus.waitBusy();
            var control = control_reg.read();
            control.enable_or_busy = true;
            control.enable_irq = true;
            control.data_direction = .read;
            control.ack = true;
            control_reg.write(control);
            bus.waitBusy();
            out[index] = bus.data;
        }
        bus.waitBusy();
        var control = control_reg.read();
        control.enable_or_busy = true;
        control.enable_irq = true;
        control.data_direction = .read;
        control.stop = true;
        control_reg.write(control);
        bus.waitBusy();
        out[index] = bus.data;

        switch (out.len) {
            1 => log.debug("Read 1 @ {X:0>2}:{X:0>2} {X:0>2}", .{ devAddr, regAddr, out[0] }),
            2 => log.debug("Read 2 @ {X:0>2}:{X:0>2} {X:0>4}", .{
                devAddr,
                regAddr,
                @as(u16, out[1]) | (@as(u16, out[0]) << 8),
            }),
            4 => log.debug("Read 4 @ {X:0>2}:{X:0>2} {X:0>8}", .{
                devAddr,
                regAddr,
                @as(u32, out[3]) |
                    (@as(u32, out[2]) << 8) |
                    (@as(u32, out[1]) << 16) |
                    (@as(u32, out[0]) << 24),
            }),
            else => log.debug("Read {d} @ {X:0>2}:{X:0>2}", .{ out.len, devAddr, regAddr }),
        }
    }

    pub fn writeReg(bus: *volatile Self, devAddr: u8, regAddr: u8, in: []const u8) Errors!void {
        try selectDeviceRegister(bus, devAddr, regAddr, .write);

        const control_reg = Register(I2CControl, .RW).of(&bus.control);
        var fail = false;
        for (in, 0..) |in_byte, i| {
            bus.waitBusy();
            bus.data = in_byte;
            bus.waitBusy();
            var control = control_reg.read();
            control.enable_or_busy = true;
            control.enable_irq = true;
            control.data_direction = .write;
            control.stop = (i == in.len - 1);
            control_reg.write(control);
            if (!bus.getAck()) {
                bus.stop();
                fail = true;
                break;
            }
        }

        const status = if (fail) "failed " else "";
        switch (in.len) {
            1 => log.debug("Write {s}1 @ {X:0>2}:{X:0>2} {X:0>2}", .{ status, devAddr, regAddr, in[0] }),
            2 => log.debug("Write {s}2 @ {X:0>2}:{X:0>2} {X:0>4}", .{
                status,
                devAddr,
                regAddr,
                @as(u16, in[1]) | (@as(u16, in[0]) << 8),
            }),
            4 => log.debug("Write {s}4 @ {X:0>2}:{X:0>2} {X:0>8}", .{
                status, devAddr, regAddr,
                @as(u32, in[3]) |
                    (@as(u32, in[2]) << 8) |
                    (@as(u32, in[1]) << 16) |
                    (@as(u32, in[0]) << 24),
            }),
            else => log.debug("Write {s}{d} @ {X:0>2}:{X:0>2}", .{ status, in.len, devAddr, regAddr }),
        }

        if (fail) {
            return Errors.WriteFailed;
        }
    }
};
comptime {
    assert(@sizeOf(Bus) == 6);
}

pub const BusCount: usize = 3;
pub const Buses = [_]*volatile Bus{
    @as(*volatile Bus, @ptrFromInt(0x10161000)),
    @as(*volatile Bus, @ptrFromInt(0x10144000)),
    @as(*volatile Bus, @ptrFromInt(0x10148000)),
};
const DeviceAddressTableEntry = struct {
    bus_id: u8,
    dev_addr: u8,
};
const DeviceAddressTable = [_]DeviceAddressTableEntry{
    .{ .bus_id = 0, .dev_addr = 0x4A },
    .{ .bus_id = 0, .dev_addr = 0x7A },
    .{ .bus_id = 0, .dev_addr = 0x78 },
    .{ .bus_id = 1, .dev_addr = 0x4A },
    .{ .bus_id = 1, .dev_addr = 0x78 },
    .{ .bus_id = 1, .dev_addr = 0x2C },
    .{ .bus_id = 1, .dev_addr = 0x2E },
    .{ .bus_id = 1, .dev_addr = 0x40 },
    .{ .bus_id = 1, .dev_addr = 0x44 },
    .{ .bus_id = 2, .dev_addr = 0xA6 }, // TODO: Might be 0xD6?
    .{ .bus_id = 2, .dev_addr = 0xD0 },
    .{ .bus_id = 2, .dev_addr = 0xD2 },
    .{ .bus_id = 2, .dev_addr = 0xA4 },
    .{ .bus_id = 2, .dev_addr = 0x9A },
    .{ .bus_id = 2, .dev_addr = 0xA0 },
    .{ .bus_id = 1, .dev_addr = 0xEE },
    .{ .bus_id = 0, .dev_addr = 0x40 },
    .{ .bus_id = 2, .dev_addr = 0x54 },
};

// :3dbrew:`I2C_Registers#I2C_Devices`
pub const Device = enum(u8) {
    power = 0,
    camera0 = 1,
    camera1 = 2,
    mcu = 3,
    gyroscope = 10,
    debug_pad = 12,
    ir = 13,
    eeprom = 14,
    nfc = 15,
    qtm = 16,
    n3ds_hid = 17,
};

pub fn init() void {
    for (Buses) |bus| {
        //var buf: [32]u8 = undefined;
        //const std = @import("std");
        //log(std.fmt.bufPrint(&buf, "{X:0>8}", .{@ptrToInt(bus)}) catch unreachable);
        bus.waitBusy();
        bus.control_ex = 2;
        bus.clock = 1280;
        bus.stop();
        bus.waitBusy();
        bus.control = 0;
    }
    log.debug("I2C initialized", .{});
}

pub fn readReg(dev: Device, reg_addr: u8, out: []u8) Errors!void {
    const dev_info = DeviceAddressTable[@intFromEnum(dev)];
    return Buses[dev_info.bus_id].readReg(dev_info.dev_addr, reg_addr, out);
}

pub fn writeReg(dev: Device, reg_addr: u8, in: []const u8) Errors!void {
    const dev_info = DeviceAddressTable[@intFromEnum(dev)];
    return Buses[dev_info.bus_id].writeReg(dev_info.dev_addr, reg_addr, in);
}
