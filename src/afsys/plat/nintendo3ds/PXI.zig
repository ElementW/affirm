// SPDX-License-Identifier: EUPL-1.2
//! PXI registers namespace.
//! See https://www.3dbrew.org/wiki/PXI_Registers
const std = @import("std");
const builtin = @import("builtin");
const afu = @import("afu");
const reg = afu.hw.reg;

// https://www.3dbrew.org/wiki/PXI_Registers#PXI_CNT
const PXIControl = packed struct(u16) {
    send_empty: bool, // const
    send_full: bool, // const
    send_empty_irq: bool,
    send_clear: bool,
    _1: u4,
    recv_empty: bool, // const
    recv_full: bool, // const
    recv_not_empty_irq: bool,
    _2: u3,
    @"error": bool,
    enable: bool,
};

// https://www.3dbrew.org/wiki/PXI_Registers#PXI_SYNC
const PXISync = packed struct(u32) {
    recv: u8, // const
    send: u8,
    _: u13,
    trigger_sync_11: bool,
    trigger_sync_9: bool,
    sync_irq_enable: bool,
};

const ARM9 = struct {
    pub const Sync = reg(PXISync, 0x10008000);
    pub const Control = reg(PXIControl, 0x10008004);
    pub const Send = reg(u32, 0x10008008);
    pub const Recv = reg(u32, 0x1000800C);
};

const ARM11 = struct {
    pub const Sync = reg(PXISync, 0x10163000);
    pub const Control = reg(PXIControl, 0x10163004);
    pub const Send = reg(u32, 0x10163008);
    pub const Recv = reg(u32, 0x1016300C);
};

const Regs = if (std.Target.arm.featureSetHas(builtin.cpu.model.features, .has_v6)) ARM11 else ARM9;

pub inline fn setFifoEnabled(enable: bool) void {
    Regs.Control.changeFields(.{ .enable = enable });
}

pub fn sendOne(data: u32) void {
    busyWaitForSendFifoEmpty();
    Regs.Send.write(data);
}

pub fn send(data: []const u32) void {
    for (data) |block| {
        sendOne(block);
    }
}

pub inline fn isSendFifoEmpty() bool {
    return Regs.Control.read().send_empty;
}

pub inline fn isSendFifoFull() bool {
    return Regs.Control.read().send_full;
}

pub inline fn busyWaitForSendFifoEmpty() void {
    while (!Regs.Control.read().send_empty) {}
}

pub inline fn clearSendFifo() void {
    Regs.Control.changeFields(.{ .send_clear = true });
}

pub inline fn recvOne() u32 {
    busyWaitForRecvFifoNotEmpty();
    return Regs.Recv.read();
}

//pub inline fn recv() u32 {
//    return Regs.Recv.read();
//}

pub inline fn isRecvFifoEmpty() bool {
    return Regs.Control.read().recv_empty;
}

pub inline fn isRecvFifoFull() bool {
    return Regs.Control.read().recv_full;
}

pub inline fn busyWaitForRecvFifoNotEmpty() void {
    while (Regs.Control.read().recv_empty) {}
}

pub inline fn busyDiscardUntil(wanted: u32) void {
    var received: u32 = undefined;
    while (true) {
        if (!isRecvFifoEmpty()) {
            received = recvOne();
        }
        if (received == wanted) {
            break;
        }
    }
}

pub inline fn setRemoteByte(data: u8) void {
    Regs.Sync.changeFields(.{ .send = data });
}

pub inline fn getLocalByte() u8 {
    return Regs.Sync.read().recv;
}
