// SPDX-License-Identifier: EUPL-1.2
pub const Framebuffer = @import("gfx/Framebuffer.zig");
pub const Screen = @import("gfx/Screen.zig");
pub const ScreenInit = @import("gfx/ScreenInit.zig");
