// SPDX-License-Identifier: EUPL-1.2

pub const TopWidth: u16 = 400;
pub const BottomWidth: u16 = 320;
pub const Height: u16 = 240;
