// SPDX-License-Identifier: EUPL-1.2

// https://www.3dbrew.org/wiki/GPU/External_Registers#Framebuffer_color_formats
pub const ColorFormat = enum(u3) {
    const Self = @This();

    rgba8888 = 0,
    rgb888 = 1,
    rgb565 = 2,
    rgba5551 = 3,
    rgba444 = 4,

    pub fn byteSize(self: Self) u8 {
        return switch (self) {
            .rgba8888 => 4,
            .rgb888 => 3,
            .rgb565, .rgba5551, .rgba444 => 2,
        };
    }
};

pub const Interlace = enum(u2) {
    A,
    AA,
    AB,
    BA,
};

/// https://www.3dbrew.org/wiki/GPU/External_Registers#Framebuffer_format
pub const Format = packed struct(u32) {
    color_format: ColorFormat,
    _1: u1 = 0,
    interlace: Interlace,
    top_screen: bool,
    fix_strip: bool,
    rainbow: u2,
    _2: u6 = 0,
    unk16: u1,
    _3: u2 = 0,
    unk19: u1,
    _4: u12 = 0,
};

pub const InterruptTiming = packed struct(u32) {
    assert: u12,
    _1: u4 = 0,
    deassert: u12,
    _2: u4 = 0,
};

pub const SurfaceSize = packed struct(u32) {
    horizontal: u12,
    _1: u4 = 0,
    vertical: u12,
    _2: u4 = 0,
};

pub const PixelAreaTiming = packed struct(u32) {
    start: u12,
    _1: u4 = 0,
    end: u12,
    _2: u4 = 0,
};

/// https://www.3dbrew.org/wiki/GPU/External_Registers#LCD_Source_Framebuffer_Setup
pub const Config = extern struct {
    pixel_timer: u32, // 00

    h_disp_start: u32, // 04
    h_r_border_start: u32, // 08
    h_blank_start: u32, // 0C
    h_sync_start: u32, // 10
    h_back_porch: u32, // 14
    h_l_border_start: u32, // 18
    h_interrupt_timing: InterruptTiming, // 1C
    h_front_porch: u32, // 20
    h_sync_timer: u32, // 24

    v_disp_start: u32, // 28
    v_b_border_start: u32, // 2C
    v_blank_start: u32, // 30
    v_sync_start: u32, // 34
    v_back_porch: u32, // 38
    v_t_border_start: u32, // 3C
    v_interrupt_timing: InterruptTiming, // 40
    v_front_porch: u32, // 44

    signal_polarity: packed struct(u32) { horizontal: bool, _1: u3 = 0, vertical: bool, _2: u27 = 0 }, // 48
    border_color: u32, // 4C
    h_pos_ctr: u32, // 50, const
    v_pos_ctr: u32, // 54, const
    _: u32 = 0, // 58
    surface_size: SurfaceSize, // 5C
    h_pixel_area_timing: PixelAreaTiming, // 60
    v_pixel_area_timing: PixelAreaTiming, // 64
    fb_a_addr1: ?[*]u8, // 68
    fb_a_addr2: ?[*]u8, // 6C
    format: Format, // 70
    unk0x74: u32, // 74
    fb_select: u32, // 78
    unk0x7C: u32, // 7C
    color_lut_idx: u32, // 80
    color_lut_data: u32, // 84
    unk0x88: u32, // 88
    unk0x8C: u32, // 8C
    fb_stride: u32, // 90
    fb_b_addr1: ?[*]u8, // 94
    fb_b_addr2: ?[*]u8, // 98
    unk0x9C: u32, // 9C
};
