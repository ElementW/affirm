// SPDX-License-Identifier: EUPL-1.2
const afsys = @import("../../../afsys.zig");
const Framebuffer = afsys.plat.nintendo3ds.gfx.Framebuffer;
const LCD = afsys.plat.nintendo3ds.LCD;

pub fn init(tl: [*]u8, tr: [*]u8, b: [*]u8, enableGpuFn: *const fn () void, enableBacklightFn: *const fn () void) void {
    const color_format = Framebuffer.ColorFormat.rgba8888;
    //if (*hw::registers::Config11::GPUCnt == 0x1007F) {
    //  return;
    //}
    enableGpuFn();

    LCD.init();

    const fs_top = @as(*volatile Framebuffer.Config, @ptrFromInt(0x10400400));
    // Top screen
    fs_top.pixel_timer = 450;
    fs_top.h_disp_start = 209;
    fs_top.h_r_border_start = 449;
    fs_top.h_blank_start = 449;
    fs_top.h_sync_start = 0;
    fs_top.h_back_porch = 0xcf;
    fs_top.h_l_border_start = 0xd1;
    fs_top.h_interrupt_timing = .{ .assert = 449, .deassert = 453 };
    fs_top.h_front_porch = 0x10000;
    fs_top.h_sync_timer = 0x19d;
    fs_top.v_disp_start = 0x2;
    fs_top.v_b_border_start = 0x192;
    fs_top.v_blank_start = 0x192;
    fs_top.v_sync_start = 0x192;
    fs_top.v_back_porch = 0x1;
    fs_top.v_t_border_start = 0x2;
    fs_top.v_interrupt_timing = .{ .assert = 402, .deassert = 406 };
    fs_top.v_front_porch = 0;
    fs_top.signal_polarity = .{ .horizontal = false, .vertical = false };
    fs_top.surface_size = .{ .horizontal = 400, .vertical = 240 };
    fs_top.h_pixel_area_timing = .{ .start = 0xd1, .end = 0x1c1 };
    fs_top.v_pixel_area_timing = .{ .start = 0x2, .end = 0x192 };
    fs_top.fb_a_addr1 = tl;
    fs_top.fb_a_addr2 = tl;
    fs_top.format = Framebuffer.Format{
        .color_format = color_format,
        .interlace = .A,
        .top_screen = true,
        .fix_strip = true,
        .rainbow = 2,
        .unk16 = 0,
        .unk19 = 1,
    };
    fs_top.unk0x74 = 0x10501;
    fs_top.fb_select = 0;
    fs_top.fb_stride = 240 * @as(u32, color_format.byteSize());
    fs_top.fb_b_addr1 = tr;
    fs_top.fb_b_addr2 = tr;
    fs_top.unk0x9C = 0;

    // Fill the color LUT table
    fs_top.color_lut_idx = 0;
    for (0..256) |i| {
        fs_top.color_lut_data = 0x10101 * i;
    }

    const fs_bot = @as(*volatile Framebuffer.Config, @ptrFromInt(0x10400500));
    // Bottom screen
    fs_bot.pixel_timer = 450;
    fs_bot.h_disp_start = 209;
    fs_bot.h_r_border_start = 449;
    fs_bot.h_blank_start = 449;
    fs_bot.h_sync_start = 0xcd;
    fs_bot.h_back_porch = 0xcf;
    fs_bot.h_l_border_start = 0xd1;
    fs_bot.h_interrupt_timing = .{ .assert = 449, .deassert = 453 };
    fs_bot.h_front_porch = 0x10000;
    fs_bot.h_sync_timer = 0x19d;
    fs_bot.v_disp_start = 0x52;
    fs_bot.v_b_border_start = 0x192;
    fs_bot.v_blank_start = 0x192;
    fs_bot.v_sync_start = 0x4f;
    fs_bot.v_back_porch = 0x50;
    fs_bot.v_t_border_start = 0x52;
    fs_bot.v_interrupt_timing = .{ .assert = 404, .deassert = 408 };
    fs_bot.v_front_porch = 0;
    fs_bot.signal_polarity = .{ .horizontal = true, .vertical = true };
    fs_bot.surface_size = .{ .horizontal = 320, .vertical = 240 };
    fs_bot.h_pixel_area_timing = .{ .start = 0xd1, .end = 0x1c1 };
    fs_bot.v_pixel_area_timing = .{ .start = 0x52, .end = 0x192 };
    fs_bot.fb_a_addr1 = b;
    fs_bot.fb_a_addr2 = b;
    fs_bot.format = Framebuffer.Format{
        .color_format = color_format,
        .interlace = .A,
        .top_screen = false,
        .fix_strip = true,
        .rainbow = 2,
        .unk16 = 0,
        .unk19 = 1,
    };
    fs_bot.unk0x74 = 0x10501;
    fs_bot.fb_select = 0;
    fs_bot.fb_stride = 240 * @as(u32, color_format.byteSize());
    fs_bot.fb_b_addr1 = null;
    fs_bot.fb_b_addr2 = null;
    fs_bot.unk0x9C = 0;

    // Fill the color LUT table
    fs_bot.color_lut_idx = 0;
    for (0..256) |i| {
        fs_bot.color_lut_data = 0x10101 * i;
    }

    enableBacklightFn();
}
