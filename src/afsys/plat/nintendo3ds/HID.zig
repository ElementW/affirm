// SPDX-License-Identifier: EUPL-1.2
//! https://www.3dbrew.org/wiki/HID_Registers
const afu = @import("afu");
const roreg = afu.hw.roreg;
const reg = afu.hw.reg;

pub const PadState = packed struct(u16) {
    a: bool,
    b: bool,
    select: bool,
    start: bool,
    right: bool,
    left: bool,
    up: bool,
    down: bool,
    r: bool,
    l: bool,
    x: bool,
    y: bool,
    _: u4,
};

pub const PadControl = packed struct(u16) {
    pub const IRQCondition = enum(u1) {
        Or = 0,
        And = 1,
    };
    a: bool,
    b: bool,
    select: bool,
    start: bool,
    right: bool,
    left: bool,
    up: bool,
    down: bool,
    r: bool,
    l: bool,
    x: bool,
    y: bool,
    _: u2,
    enable_irq: bool,
    irq_condition: IRQCondition,
};

const Base: usize = 0x10146000;
const PadStateReg = roreg(PadState, Base);
const PadControlReg = reg(PadControl, Base + 0x2);

pub inline fn state() PadState {
    return PadStateReg.read();
}
pub inline fn setControl(control: PadControl) void {
    return PadControlReg.write(control);
}
