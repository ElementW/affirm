// SPDX-License-Identifier: EUPL-1.2
pub const io = @import("io.zig");
pub const plat = @import("plat.zig");
pub const virtio = @import("virtio.zig");
